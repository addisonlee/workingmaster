trigger AddLee_Case on Case (before insert, before update, before delete, 
                                after insert, after update, after delete, after undelete) {
Disable_Trigger_Validation__c disabletrigger = Disable_Trigger_Validation__c.getInstance(UserInfo.getUserID());
    system.debug('userid<<<<<' +UserInfo.getUserID());
    system.debug('testtest>>>>>' +disabletrigger);
    if(disabletrigger.Bypass_Trigger_And_Validation_Rule__c){
      return;
    }
    /* Get singleton handler's instance */
    AddLee_TriggerCaseHandler handler = new AddLee_TriggerCaseHandler();

    /* Before Insert */
    if (Trigger.isInsert && Trigger.isBefore) {
        handler.onBeforeInsert(Trigger.new);
    }

    /* After Insert
    else if (Trigger.isAfter && Trigger.isInsert) {
        handler.onAfterInsert(Trigger.new, Trigger.newMap);
    }
*/
    /* Before Update */
    else if (Trigger.isUpdate && Trigger.isBefore) {
        handler.onBeforeUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
    }

    /* After Update */
    else if (Trigger.isUpdate && Trigger.isAfter) {
        handler.onAfterUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
        List<Case> listofTobeProcessedCases = new List<Case>();
        for(Case updatedNewCases : trigger.new){
            //criteria to call web service handler
            if(trigger.oldmap.containsKey(updatedNewCases.Id) 
              && updatedNewCases.Refund_Status__c <> null
              && trigger.oldmap.get(updatedNewCases.Id).Refund_Status__c  <> updatedNewCases.Refund_Status__c
              && updatedNewCases.Refund_Status__c.toUpperCase() == 'APPROVED'
              && updatedNewCases.Net_Amount_To_Refund__c <> null){
                listofTobeProcessedCases.add(updatedNewCases);
            }
        }
        if(listofTobeProcessedCases.size()>0){
            CaseWebServiceTriggerHandler.processComplaint(listofTobeProcessedCases);
        }
    
}
    /* Before Delete
    else if (Trigger.isDelete && Trigger.isBefore) {
        handler.onBeforeDelete(Trigger.old, Trigger.oldMap);
    }
*/
    /* After Delete
    else if (Trigger.isDelete && Trigger.isAfter) {
        handler.onAfterDelete(Trigger.old, Trigger.oldMap);
    }
*/    /* After Undelete
    else if (Trigger.isUnDelete) {
        handler.onAfterUndelete(Trigger.new, Trigger.newMap);
    }
*/}