/* 
  ** To populate to address on case from inbound email 
 */
trigger PopulateToAddress on EmailMessage (after insert) {
 List<case> ecases = new List<case> ();
 List<case> cas = new list<case>();
 List<String> str = new List<String>();
 List<String> ccemail = new List<String>();
 List<String> bccmail = new List<String>(); 
 set<String> toaddresses = new set<string>();
 //get inserted case from email id
  for(EmailMessage newEmail: trigger.new){
      
      if(newEmail.ParentId != null){
           cas = [Select Id,To_Address__c from case where Id =:newEmail.ParentId ];
      }
      
      // populate to email address from incoming email after creating case
       if(cas.size()>0){
          for(Case c : cas){
          if(c.To_Address__c == null && newEmail.Incoming == true ){
          //  system.debug('newEmail.ToAddress>>>'+newEmail.ToAddress);
           // add ccaddress and to email address to list
           if(newEmail.CcAddress !=null){
             string ccaddress = newEmail.CcAddress;
             ccemail = ccaddress.split(';');
           }
            // Adding Bcc address to List
           else if(newEmail.BccAddress != null){ 
            string bccaddress =newEmail.BccAddress ;
            bccmail = bccaddress.split(';');
            }           
            //adding to email and cc email addesses to set 
            if(newEmail.ToAddress != null){
            string emailstring = newEmail.ToAddress;
            str = emailstring.split(';');
            toaddresses.addall(str);
            }
            toaddresses.addall(ccemail);
            toaddresses.addall(bccmail);           
            //toaddresses.addall(toaddresses);
                        

             for(Email_to_Populate_To_Address__c ep :Email_to_Populate_To_Address__c.getall().values() ){
                if(toaddresses.contains(ep.Email_Address__c)){
                    c.To_Address__c = ep.Email_Address__c;
                }
             }
             
             ecases.add(c);
      }
   }
  }
  }
   update ecases;
}