trigger AddLee_AccountSynch on Account (before insert, before update, after insert, after update) {
//TriggersInactivityUserHierarchy__c byetrirs = TriggersInactivityUserHierarchy__c.getvalues(UserInfo.getUserId());
//if(byetrirs == null){
 Disable_Trigger_Validation__c disabletrigger = Disable_Trigger_Validation__c.getInstance(UserInfo.getUserID());
    system.debug('userid<<<<<' +UserInfo.getUserID());
    system.debug('testtest>>>>>' +disabletrigger);
    if(disabletrigger.Bypass_Trigger_And_Validation_Rule__c){
      return;
    }
    /* Instanciate the helper class */
    AddLee_AccountSynchHelper helper = new AddLee_AccountSynchHelper();
    
    
    /* Split Account based on the context (API/NotAPI) */
    Map<Id, Account> isAPIAccountMap    = new Map<Id, Account>();
    Map<Id, Account> notIsAPIAccountMap = new Map<Id, Account>();
    Map<Id, Account> postProcessed      = new Map<Id, Account>();
    //Account optouts list 
    List<Account> optoutaccounts = new List<Account>();
    List<Account> emailaccounts = new List<Account>();
    List<Account> smstaccounts = new List<Account>();
    
    
    //Industry Segments
    List<String> industrySegments = AddLee_SegmentationHelper.getAccountIndustrySegments();
    
    
    /* Split Account based on the context (API/NotAPI) For Before Insert*/
    List<Account> isApiList = new List<Account>();
    List<Account> isNotApiList = new List<Account>();
    List<Account> postProcessedList = new List<Account>();
    
    Log_Integration__c logIntegration = Log_Integration__c.getValues('shamrock_setting');
    /* All the actions to be done when an Account is created */
    if(Trigger.isInsert && Trigger.isBefore){
        isApiList = helper.getIsAPIList(Trigger.new);
        isNotApiList = helper.getNotIsAPIList(Trigger.new);
        //System.DEBUG(logginglevel.ERROR, 'firstRun : ' + AddLee_ContextVariables.firstRunAccount);
        if(AddLee_ContextVariables.firstRunAccount){
            AddLee_ContextVariables.employeeSizeBandMap = AddLee_SegmentationHelper.getEmployeeSizeBand(industrySegments);
            AddLee_ContextVariables.activeUsersMap = AddLee_SegmentationHelper.getActiveUsersSegment(industrySegments);
            AddLee_ContextVariables.expectedExpenditureSpendMap = AddLee_SegmentationHelper.getExpectedExpenditure(industrySegments);
            AddLee_ContextVariables.postCodeMap = AddLee_SegmentationHelper.getPostCodesMap();
            AddLee_ContextVariables.bandingValues = AddLee_SegmentationHelper.getBandingValues();
            AddLee_ContextVariables.recordTypeMap  = AddLee_SegmentationHelper.getRecordTypeInfos();
            AddLee_ContextVariables.firstRunAccount = false;
            helper.appendBuildingNum(Trigger.new);
        }
        //Get the current account number range
        AddLee_ContextVariables.currentAccountNumbersMap = AddLee_SegmentationHelper.populateCurrentAccountNumbersRange();
        System.DEBUG(logginglevel.Error, 'Current Account Number : ' + AddLee_ContextVariables.currentAccountNumbersMap);
        //Populate the accounts parent accounts    
        AddLee_ContextVariables.parentAccountMap = helper.populateParentAccountMap(Trigger.new,AddLee_ContextVariables.parentAccountMap);
        //populate Inner/Outer London
        AddLee_SegmentationHelper.allocatePostCodeAccount(Trigger.new,AddLee_ContextVariables.postCodeMap);
        //check if parent account exist in shamrock
        helper.checkParentAccountExistinShamrock(Trigger.new, AddLee_ContextVariables.parentAccountMap);
        //assign Account Number
        helper.assignAccountNumber(Trigger.new,AddLee_ContextVariables.currentAccountNumbersMap);
        //Check if this account can be set to current
        helper.convertAccount(Trigger.new);

        if(!logIntegration.Enable_Bulk_Processing__c){
            //Preprocess isAPI accounts
            helper.preProcess(isApiList, true, true, false);
            //Preprocess isNotAPI accounts
            helper.preProcess(isNotApiList, false, true, false);
        }
        //Allocate Banding
        AddLee_SegmentationHelper.allocateBandingValuesAccount(Trigger.new, AddLee_ContextVariables.bandingValues);
        //Segment Allocation
        AddLee_SegmentationHelper.allocateSegmentsAccount(Trigger.new,AddLee_ContextVariables.employeeSizeBandMap);
        //Active User Estimates
        AddLee_SegmentationHelper.allocateActiveUsersEstimatesForAccount(Trigger.new,AddLee_ContextVariables.activeUsersMap);
        //Spend Allocation
        AddLee_SegmentationHelper.allocateSpendExpenditureForAccount(Trigger.new,AddLee_ContextVariables.expectedExpenditureSpendMap);
    }

    /* All the actions to be done when an Account is updated and some conditions are matched */
    if(Trigger.isUpdate && Trigger.isBefore){
        isAPIAccountMap = helper.getIsAPIMap(Trigger.newMap);
        notIsAPIAccountMap = helper.getNotIsAPIMap(Trigger.newMap);
        //System.DEBUG(logginglevel.ERROR, 'firstRun : ' + AddLee_ContextVariables.firstRunAccount);
        if(AddLee_ContextVariables.firstRunAccount){
            AddLee_ContextVariables.employeeSizeBandMap = AddLee_SegmentationHelper.getEmployeeSizeBand(industrySegments);
            AddLee_ContextVariables.activeUsersMap = AddLee_SegmentationHelper.getActiveUsersSegment(industrySegments);
            AddLee_ContextVariables.expectedExpenditureSpendMap = AddLee_SegmentationHelper.getExpectedExpenditure(industrySegments);
            AddLee_ContextVariables.postCodeMap = AddLee_SegmentationHelper.getPostCodesMap();
            AddLee_ContextVariables.bandingValues = AddLee_SegmentationHelper.getBandingValues();
            AddLee_ContextVariables.recordTypeMap = AddLee_SegmentationHelper.getRecordTypeInfos();
            AddLee_ContextVariables.firstRunAccount = false;
        }
        //Get the current account number range
        AddLee_ContextVariables.currentAccountNumbersMap = AddLee_SegmentationHelper.populateCurrentAccountNumbersRange();
        System.DEBUG(logginglevel.Error, 'Current Account Number : ' + AddLee_ContextVariables.currentAccountNumbersMap);
        //Populate the accounts parent accounts    
        AddLee_ContextVariables.parentAccountMap = helper.populateParentAccountMap(Trigger.new,AddLee_ContextVariables.parentAccountMap);
        //populate Inner/Outer London
        AddLee_SegmentationHelper.allocatePostCodeAccount(Trigger.new,AddLee_ContextVariables.postCodeMap);
        //check if parent account exist in shamrock
        helper.checkParentAccountExistinShamrock(Trigger.new, AddLee_ContextVariables.parentAccountMap);
        //assign Account Number
        helper.assignAccountNumber(Trigger.new,AddLee_ContextVariables.currentAccountNumbersMap);
        
        //Check if this account can be set to current
        helper.convertAccount(Trigger.new);
        if(!logIntegration.Enable_Bulk_Processing__c){
            //Preprocess isAPI accounts
            helper.preProcess(isAPIAccountMap.values(), true, false, true);
            //Preprocess isNotAPI accounts
            helper.preProcess(notIsAPIAccountMap.values(), false, false, true);
        }
        //Allocate Banding
        AddLee_SegmentationHelper.allocateBandingValuesAccount(Trigger.new, AddLee_ContextVariables.bandingValues);
        //Segment Allocation
        AddLee_SegmentationHelper.allocateSegmentsAccount(Trigger.new,AddLee_ContextVariables.employeeSizeBandMap);
        //Active User Estimates
        AddLee_SegmentationHelper.allocateActiveUsersEstimatesForAccount(Trigger.new,AddLee_ContextVariables.activeUsersMap);
        //Spend Allocation
        AddLee_SegmentationHelper.allocateSpendExpenditureForAccount(Trigger.new, AddLee_ContextVariables.expectedExpenditureSpendMap);
        
        helper.removeCreditCardDetails(Trigger.newMap,Trigger.oldMap);
        
    }

    /* All the actions to be done when an Account is created */
    if(Trigger.isInsert && Trigger.isAfter){
        if(!logIntegration.Enable_Bulk_Processing__c){
            //Handle Insert
            postProcessedList = helper.getPostProcessList(Trigger.new);
            helper.handleInsert(postProcessedList);
            helper.handleContacts(Trigger.newMap,Trigger.oldMap,true);
        }
    }

    /* All the actions to be done when an Account is updated and some conditions are matched */
    if(Trigger.isUpdate && Trigger.isAfter){
        if(!logIntegration.Enable_Bulk_Processing__c){
            //Handle Update
            postProcessed = helper.getPostProcessMap(Trigger.newMap);
            helper.handleUpdate(postProcessed, Trigger.oldMap);
            helper.handleContacts(Trigger.newMap,Trigger.oldMap,false);
        }
    }
    //Account level opt outs
    if( Trigger.isUpdate && Trigger.isAfter){
         for(Account acc : Trigger.new){
          if(trigger.oldmap.get(acc.id).OPT_OUT_ALL_MARKETING_FOR_ACCOUNT__c <> acc.OPT_OUT_ALL_MARKETING_FOR_ACCOUNT__c) 
            {
                optoutaccounts.add(acc);
           }
           else if(trigger.oldmap.get(acc.id).Opt_Out_Of_Email_Marketing__c <> acc.Opt_Out_Of_Email_Marketing__c){
              emailaccounts.add(acc);
           }
           else if(trigger.oldmap.get(acc.id).Opt_Out_Of_SMS_Marketing__c <> acc.Opt_Out_Of_SMS_Marketing__c){
              smstaccounts.add(acc);
           }
         }
        if(!Addlee_Check_Recursive.oneRun()){
            system.debug('trigger forst run ');
            if(optoutaccounts.size()>0){
             OptoutHandler_Account handler = new OptoutHandler_Account ();
             handler.optoutHandler(optoutaccounts);
            }
            else if(emailaccounts.size()>0){
              OptoutHandler_Account handler = new OptoutHandler_Account ();
              handler.optoutHandler(emailaccounts);
            }
            else if(smstaccounts.size()>0){
             OptoutHandler_Account handler = new OptoutHandler_Account ();
             handler.optoutHandler(smstaccounts);
            }
        }
    }
    //Person account opt outs 
    if(trigger.isbefore){
   Addlee_Accountoptout.personaccountoptouts(Trigger.new,Trigger.oldMap);
   }  

}