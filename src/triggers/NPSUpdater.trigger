trigger NPSUpdater on NPS__c (after insert,after update) {

    if(NpsTriggerHandler.BYPASSTRIGGER)
    return;
   //if(NpsTriggerHandler.BYPASSTRIGGER) { 
    if(Trigger.isInsert)
    {
        
            NpsTriggerHandler.afterInsert(Trigger.new);
            NpsTriggerHandler.UpdateContact(Trigger.new);
            //NpsTriggerHandler.BYPASSTRIGGER = false;
    }
    
    
    if(Trigger.isUpdate)
    {
       
            NpsTriggerHandler.afterInsert(Trigger.new);
            NpsTriggerHandler.afterUpdate(Trigger.new,Trigger.oldMap);
            //NpsTriggerHandler.BYPASSTRIGGER = false;
        
    }
   //}
}