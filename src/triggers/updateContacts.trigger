/* Trigger to update contact with booking status field for campaigns
    
*/
trigger updateContacts on Account (after insert,after update) {
 if(!BookingRecursion.recursionBooking){
    set<Id> accId = new set<Id>();
    set<Id>Personids = new set<Id>();
    set<String> cids = new set<String>();
    List<Account> updateacc = new List<Account>();
    Set<Id> accountids = new Set<Id>();
    set<Contact> uniqcontacts = new set<Contact>();
    List<Contact> listOfCon=new List<Contact>();
    List<CampaignMember> cmember = new List<CampaignMember>();
    List<Reactivated_Campaign_Member__c> membersinsert = new List<Reactivated_Campaign_Member__c>();
    map<String ,Contact> mapcontacts = new map <String ,Contact>();
    for(Account acc:Trigger.new){
        if(acc.Booked__c== true){
        accId.add(acc.Id);
        if((acc.Marketing_Status__c =='Lapsed')||(acc.Marketing_Status__c =='No Spend')){
            accountids.add(acc.Id);             
        } 
        if(acc.PersonContactId != null){
          Personids.add(acc.PersonContactId ); 
        }
    }
   }
    // Get contacts into list and remove duplicates from list
    List<Contact> conlist = new list <Contact>([Select Id,Name,Contact_Type__c,Optout_All__c from Contact where AccountId=:accId and Contact_Type__c ='Main Contact' and IsPersonAccount =false]);
    //remove duplicate from list
    for(Contact clist :conlist){
        cids.add(clist.id);
        
    }
    //update contact booked status
    system.debug('cidssss>>>' +cids.size());
    for (Contact ucon :[Select Id,Name,Booked__c,Contact_Type__c,Optout_All__c from Contact where Id=:cids and Contact_Type__c ='Main Contact' and IsPersonAccount =false] ){
         ucon.Booked__c = true;
         listOfCon.add(ucon);
      }
      BookingRecursion.recursionBooking = true;
      update listOfCon;
      // remove the campaign members from campaigns if booked status equal to true
      for(CampaignMember cmm:[select Id,Status,ContactId,Campaign_name__c from CampaignMember where ContactId =:Personids]){
                system.debug('test.booked'+ cmm);
                if(cmm != null ){
                    Reactivated_Campaign_Member__c rcm =  new Reactivated_Campaign_Member__c ();
                    rcm.Contact__c = cmm.ContactId;
                    rcm.Status__c = 'Booked';
                    rcm.Campaign_Name__c = cmm.Campaign_name__c;
                    rcm.Reactivation_Date__c = Date.today();
                    membersinsert.add(rcm); 
                    //cmm.Status = 'Booked';
                    cmember.add(cmm);
                }
            }
             
            insert membersinsert;
            delete cmember;
     //Change to marketing status to Active when lapsed or no spend accounts has booking from marketing campaign and populate reactivated data  
     for(Account acc:[select id,Marketing_Status__c,Reactivated_Date__c,Booked__c from Account where Id =:accountids] ) {
         acc.Marketing_Status__c ='Active';
         acc.Reactivated_Date__c = system.today();
         updateacc.add(acc);
     }
     update updateacc;  
  }
}