trigger Addlee_EmailNotification on Case(before insert, before update) {
Disable_Trigger_Validation__c disabletrigger = Disable_Trigger_Validation__c.getInstance(UserInfo.getUserID());
    system.debug('userid<<<<<' +UserInfo.getUserID());
    system.debug('testtest>>>>>' +disabletrigger);
    if(disabletrigger.Bypass_Trigger_And_Validation_Rule__c){
      return;
    }
    if (trigger.isBefore) {
        if (trigger.isInsert || trigger.isUpdate) {
        Addlee_EmailNotificationHandler.populateEmail(trigger.new);
    }
}

}