trigger Adlee_PromoCode on Promo_Code__c (after Update) {
    PromoCodeHelper.sendEmail(Trigger.new,Trigger.oldMap);
}