trigger AddLee_Log_AttachedActions on Log__c (after insert, after update) {
     Disable_Trigger_Validation__c disabletrigger = Disable_Trigger_Validation__c.getInstance(UserInfo.getUserID());
    system.debug('userid<<<<<' +UserInfo.getUserID());
    system.debug('testtest>>>>>' +disabletrigger);
    if(disabletrigger.Bypass_Trigger_And_Validation_Rule__c){
      return;
    }
    AddLee_IntegrationOrchestrator orchestrator = new AddLee_IntegrationOrchestrator();
    
    /* Insert Actions */
    if(Trigger.isInsert){
        orchestrator.handleInsert(Trigger.new);
    }
    

    /* Update Actions */
    if(Trigger.isUpdate){
        orchestrator.handleUpdate(Trigger.old, Trigger.new);
    }
    

}