trigger AddLee_Contact on Contact (before insert, before update,after insert, after update) {
 //TriggersInactivityUserHierarchy__c byetrirs = TriggersInactivityUserHierarchy__c.getvalues(UserInfo.getUserId());
 //if(byetrirs == null){
 Disable_Trigger_Validation__c disabletrigger = Disable_Trigger_Validation__c.getInstance(UserInfo.getUserID());
    system.debug('userid<<<<<' +UserInfo.getUserID());
    system.debug('testtest>>>>>' +disabletrigger);
    if(disabletrigger.Bypass_Trigger_And_Validation_Rule__c){
      return;
    }
    
    AddLee_ContactHelper contactHelper = new AddLee_ContactHelper();
    ContactOptout handler = new ContactOptout ();
    AddLee_IntegrationUtility.setIsFromContact();
    
    if(AddLee_checkRecursive.isAfterInsertUpdate){
        if(trigger.isbefore && AddLee_checkRecursive.firstRunContactIsBefore){
            if(trigger.isInsert){
                contactHelper.onBeforeInsert(Trigger.new,Trigger.newMap);
                handler.optoutHandler(Trigger.new);
            }else if(trigger.isUpdate){
                contactHelper.onBeforeUpdate(Trigger.old, Trigger.oldMap, Trigger.new, Trigger.newMap);
            }
            Addlee_EmailSMSOptoutHandler.SMSEmailSetting(Trigger.new,Trigger.oldMap);
            AddLee_checkRecursive.firstRunContactIsBefore = false;
        }else if(trigger.isAfter && AddLee_checkRecursive.firstRunContactIsAfter){
            Handle_ContactName namehandler = new Handle_ContactName ();
            namehandler.selfcare_contact(Trigger.newMap);
            AddLee_checkRecursive.firstRunContactIsAfter = false;
        }
    }
}