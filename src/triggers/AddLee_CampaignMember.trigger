/**
  * Trigger to create task on account
  * Developed by Srini
**/  
trigger AddLee_CampaignMember on CampaignMember (after update, before update) {
    Disable_Trigger_Validation__c disabletrigger = Disable_Trigger_Validation__c.getInstance(UserInfo.getUserID());
    system.debug('userid<<<<<' +UserInfo.getUserID());
    system.debug('testtest>>>>>' +disabletrigger);
    if(disabletrigger.Bypass_Trigger_And_Validation_Rule__c){
      return;
    }
    AddLee_TriggerCampaignMemberHandler handler = new AddLee_TriggerCampaignMemberHandler ();
    If(trigger.isUpdate && trigger.isAfter){
        handler.onAfterUpdate (trigger.new);
    }
    
}