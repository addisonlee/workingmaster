trigger AddLee_Terms_Conditions_Trigger on AddLee_Terms_Conditions__c (after insert, before update) {
    Disable_Trigger_Validation__c disabletrigger = Disable_Trigger_Validation__c.getInstance(UserInfo.getUserID());
    system.debug('userid<<<<<' +UserInfo.getUserID());
    system.debug('testtest>>>>>' +disabletrigger);
    if(disabletrigger.Bypass_Trigger_And_Validation_Rule__c){
      return;
    }
    if(Trigger.isAfter && Trigger.isInsert) {
        ACPTNCTriggerHandler handler = new ACPTNCTriggerHandler();
        handler.tncCreated(trigger.new);
    }
    
    if(Trigger.isBefore && Trigger.isUpdate) {
        ACPTNCTriggerHandler handler = new ACPTNCTriggerHandler();
        handler.tncUpdated(trigger.oldMap, trigger.newMap);
    } 
    
    
}