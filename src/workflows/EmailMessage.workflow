<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Case_Status_to_Re_Opened</fullName>
        <field>Status</field>
        <literalValue>Re-Opened</literalValue>
        <name>Change Case Status to &quot;Re-Opened&quot;</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>Premier_Bookings_Case_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Change Owner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contact_Centre_Reassignment</fullName>
        <field>OwnerId</field>
        <lookupValue>Contact_Centre_Admin_Case_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Contact Centre Reassignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Premier_Closed_Case_Update</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Premier Closed Case Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reopened_Case</fullName>
        <field>OwnerId</field>
        <lookupValue>Premier_Bookings_Case_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Reopened Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reopened_Contact_Centre</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Reopened Contact Centre</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_In_Progress_Awa</fullName>
        <description>Update case status to In Progress Awaiting Response</description>
        <field>Status</field>
        <name>Update Case Status to &quot;In Progress - Awa</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_In_Progress_Res</fullName>
        <description>Update Case Status to In Progress Responded</description>
        <field>Status</field>
        <name>Update Case Status to &quot;In Progress - Res</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Priority_to_High</fullName>
        <description>When a case is re-opened, it updates the priority to high.</description>
        <field>Priority</field>
        <literalValue>High</literalValue>
        <name>Update Priority to High</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Re_Opened_Checkbox_to_True</fullName>
        <description>Update re-opened checkbox to true. Re-opened checkbox used in case milestone criteria</description>
        <field>Case_Re_Opened__c</field>
        <literalValue>1</literalValue>
        <name>Update &quot;Re-Opened&quot; Checkbox to True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unread_Emails_to_FALSE</fullName>
        <field>Unread_Email_s__c</field>
        <literalValue>0</literalValue>
        <name>Update &quot;Unread Emails&quot; to FALSE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Unread_Emails_to_TRUE</fullName>
        <field>Unread_Email_s__c</field>
        <literalValue>1</literalValue>
        <name>Update &quot;Unread Emails&quot; to TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>ParentId</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>Contact Centre Email Notification</fullName>
        <actions>
            <name>Contact_Centre_Reassignment</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reopened_Contact_Centre</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 )AND 4</booleanFilter>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Owner_Role__c</field>
            <operation>equals</operation>
            <value>Agent</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Owner_Role__c</field>
            <operation>equals</operation>
            <value>Team Manager</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Premier Closed Case Notifications</fullName>
        <actions>
            <name>Premier_Closed_Case_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reopened_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Owner_Role__c</field>
            <operation>contains</operation>
            <value>Premier</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Re-Open Case</fullName>
        <actions>
            <name>Change_Case_Status_to_Re_Opened</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Priority_to_High</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to change Case Status to &quot;Re-Opened&quot; when a new email is created against the case</description>
        <formula>AND(

Incoming=TRUE,
Parent.IsClosed=TRUE,
ISPICKVAL(Status,&quot;New&quot;),
NOT(CONTAINS(FromAddress,&quot;ADDISONLEE&quot;)),
OR(
Parent.Owner:User.Department=&quot;Account Executive&quot;,
Parent.Owner:User.Department=&quot;Account Management&quot;,
Parent.Owner:User.Department=&quot;Client Services&quot;,
Parent.Owner:User.Department=&quot;Contact Centre&quot;,
Parent.Owner:User.Department=&quot;Customer Relations&quot;,
Parent.Owner:User.Department=&quot;Account Executive&quot;,
Parent.Owner:User.Department=&quot;Credit Control&quot;
)

)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update %22Re-Opened%22 Checkbox</fullName>
        <actions>
            <name>Update_Re_Opened_Checkbox_to_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New,Read</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <description>Email sent in to reopen a case - When a case is reopened the Re-Opened checkbox is checked- Used in milestone entry criteria</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update %22Unread Emails%22 to FALSE</fullName>
        <actions>
            <name>Update_Unread_Emails_to_FALSE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OR( 

ISPICKVAL((Status),&quot;Read&quot;),
ISPICKVAL((Status),&quot;Replied&quot;) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update %22Unread Emails%22 to TRUE</fullName>
        <actions>
            <name>Update_Unread_Emails_to_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Incoming</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Status to %22In Progress - Awaiting Response%22</fullName>
        <actions>
            <name>Update_Case_Status_to_In_Progress_Awa</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>New,Read</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New</value>
        </criteriaItems>
        <description>Update case automatically when email message comes into the system</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Case Status to %22In Progress - Responded%22</fullName>
        <actions>
            <name>Update_Case_Status_to_In_Progress_Res</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>EmailMessage.Status</field>
            <operation>equals</operation>
            <value>Sent</value>
        </criteriaItems>
        <criteriaItems>
            <field>EmailMessage.Subject</field>
            <operation>notEqual</operation>
            <value>Thank you for contacting us</value>
        </criteriaItems>
        <description>Update case automatically when email message comes into the system</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
