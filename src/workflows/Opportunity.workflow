<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Independent_Traveller_Update_Account_Sta</fullName>
        <description>When an Independent Traveller Opportunity is Closed Won, update the Account Status to Current</description>
        <field>Account_Status__c</field>
        <literalValue>Current</literalValue>
        <name>Independent Traveller Update Account Sta</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Closed_Lost_Date</fullName>
        <description>Update opportunity closed lose date to TODAY</description>
        <field>Opportunity_Closed_Lost_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Update Closed Lost Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Opportunity_Closed_Won_Checkbox</fullName>
        <description>Update opportunity closed won checkbox to true</description>
        <field>Opportunity_Closed_Won__c</field>
        <literalValue>1</literalValue>
        <name>Update Opportunity Closed Won Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Independent Traveller Update Account Status</fullName>
        <actions>
            <name>Independent_Traveller_Update_Account_Sta</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Independent Traveller</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>When an Independent Traveller Opportunity is Closed Won, update the Account Status to Current to push to Shamrock.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Opportunity Closed Won - Lock Close Date</fullName>
        <actions>
            <name>Update_Opportunity_Closed_Won_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Won</value>
        </criteriaItems>
        <description>Update &apos;Opportunity Closed&apos; checkbox when opportunity = &apos;Closed Won&apos;. This field will then be used in a validation rule to stop users changing the closed date.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Re-Contact Date Task Opportunity</fullName>
        <actions>
            <name>Re_contact_opportunity</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Re_Contact_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When the &quot;Re-contact date&quot; field has been populated, a new task is created assigned to the opp owner with the same due date, to remind them to contact the lead at that time.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Tender Renewal Reminder</fullName>
        <actions>
            <name>Tender_Renewal_Reminder</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Tender_Opportunity__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Tender_Renewal_Reminder__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Triggered when renewal reminder date is populated for Tender Opportunities.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <offsetFromField>Opportunity.Tender_Renewal_Reminder__c</offsetFromField>
            <timeLength>0</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Closed Lost Date</fullName>
        <actions>
            <name>Update_Closed_Lost_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Lost</value>
        </criteriaItems>
        <description>Update Closed Lost Date when Opportunity is &quot;Closed Lost&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Re_contact_opportunity</fullName>
        <assignedToType>owner</assignedToType>
        <description>You have added a re-contact date to this opportunity. Please ensure you contact this opportunity again by the due date you have specified</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Opportunity.Re_Contact_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Re-contact opportunity</subject>
    </tasks>
    <tasks>
        <fullName>Tender_Renewal_Reminder</fullName>
        <assignedToType>owner</assignedToType>
        <description>Tender Renewal Reminder: Follow up on this Tender renewal.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Tender_Renewal_Reminder__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Tender Renewal Reminder</subject>
    </tasks>
</Workflow>
