<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Unique_Key_on_Spend_Per_Employee</fullName>
        <description>Update TECH unique industry</description>
        <field>TECH_Unique_Industry__c</field>
        <formula>TECH_Industry_Type__c</formula>
        <name>Update Unique Key on Spend Per Employee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Unique Industry on Spend Per Employee</fullName>
        <actions>
            <name>Update_Unique_Key_on_Spend_Per_Employee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Spend_Per_Employee__c.TECH_Industry_Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
