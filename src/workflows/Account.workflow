<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_Reminder_Insurance_Renewal_Date</fullName>
        <ccEmails>NetworkService@Addisonlee.com</ccEmails>
        <description>Email Reminder Insurance Renewal Date</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Partner_Supplier_Insurance_Renewal_Date</template>
    </alerts>
    <alerts>
        <fullName>Email_Reminder_Insurance_Renewal_Date_Expired</fullName>
        <ccEmails>NetworkService@Addisonlee.com</ccEmails>
        <description>Email Reminder Insurance Renewal Date Expired</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Partner_Supplier_Insurance_Renewal_Date_Expired</template>
    </alerts>
    <alerts>
        <fullName>Email_Reminder_Operational_License_Renewal_Date</fullName>
        <ccEmails>NetworkService@Addisonlee.com</ccEmails>
        <description>Email Reminder Operational License Renewal Date</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Partner_Supplier_Operational_License_Renewal_Date</template>
    </alerts>
    <alerts>
        <fullName>Email_Reminder_Operational_License_Renewal_Date_Expired</fullName>
        <ccEmails>NetworkService@Addisonlee.com</ccEmails>
        <description>Email Reminder Operational License Renewal Date Expired</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <field>Partner_Manager__c</field>
            <type>contactLookup</type>
        </recipients>
        <senderAddress>noreply@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Partner_Supplier_Operational_License_Renewal_Date_Expired</template>
    </alerts>
    <alerts>
        <fullName>New_account_confirmation_AL_Send_Email</fullName>
        <description>New account confirmation AL- Send Email</description>
        <protected>false</protected>
        <recipients>
            <field>SHM_main_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Personal_Web_Account_app_account_activation_Personalised</template>
    </alerts>
    <alerts>
        <fullName>New_account_confirmation_Send_Generic_Email</fullName>
        <description>New account confirmation - Send Generic Email</description>
        <protected>false</protected>
        <recipients>
            <field>SHM_main_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Web_Account_app_Personal_account_activation_Generic</template>
    </alerts>
    <alerts>
        <fullName>New_account_confirmation_W1_Send_Email</fullName>
        <description>New account confirmation W1 - Send Email</description>
        <protected>false</protected>
        <recipients>
            <field>SHM_main_email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Archived_Email_Templates/WestOne_Account_Activation_Automatic_Confirmation_Email</template>
    </alerts>
    <alerts>
        <fullName>Send_Email_notification</fullName>
        <description>Send Email notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/New_Account_Assignment</template>
    </alerts>
    <alerts>
        <fullName>Very_High_Account_Opened_Addison_Lee</fullName>
        <description>Very High Account Opened Addison Lee</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <recipients>
            <recipient>Account_Managers_Team_Manager</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Client_Services_Director</recipient>
            <type>role</type>
        </recipients>
        <recipients>
            <recipient>Client_Services_Manager_AL_LON</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_status_has_been_amended_to_Very_High_Segment</template>
    </alerts>
    <alerts>
        <fullName>Very_High_Account_Opened_WestOne</fullName>
        <description>Very High Account Opened WestOne</description>
        <protected>false</protected>
        <recipients>
            <recipient>ruth.robinson@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>tony.marson@westonecars.co.uk</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_status_has_been_amended_to_Very_High_Segment</template>
    </alerts>
    <alerts>
        <fullName>Welcome_to_Small_Business</fullName>
        <description>Welcome to Small Business</description>
        <protected>false</protected>
        <recipients>
            <field>SHM_main_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Welcome_to_Your_Small_Business_Account</template>
    </alerts>
    <alerts>
        <fullName>Welcome_to_Small_Business_Generic</fullName>
        <description>Welcome to Small Business - Generic</description>
        <protected>false</protected>
        <recipients>
            <field>SHM_main_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Welcome_to_Your_Small_Business_Account_Generic</template>
    </alerts>
    <alerts>
        <fullName>Welcome_to_your_Business_Priority_Plus_Account</fullName>
        <description>Welcome to your Business Priority Plus Account</description>
        <protected>false</protected>
        <recipients>
            <field>SHM_main_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Welcome_to_Your_Business_Account</template>
    </alerts>
    <alerts>
        <fullName>Welcome_to_your_Business_Priority_Plus_Account_Generic</fullName>
        <description>Welcome to your Business Priority Plus Account - Generic</description>
        <protected>false</protected>
        <recipients>
            <field>SHM_main_email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Welcome_to_Your_Business_Account_Generic</template>
    </alerts>
    <alerts>
        <fullName>When_New_Account_Opened_Notification_Send_Notification_To_SME</fullName>
        <ccEmails>accountdevelopment@addisonlee.com</ccEmails>
        <description>When New Account Opened Notification,Send Notification To SME</description>
        <protected>false</protected>
        <recipients>
            <recipient>Sales_Team_AL_LON</recipient>
            <type>roleSubordinates</type>
        </recipients>
        <senderAddress>sales@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>SME_Task_call_follow_up_email_folder/SME_Account_Email_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Account_Number_1</fullName>
        <field>AccountNumber</field>
        <formula>&quot;1&quot;</formula>
        <name>Account Number = 1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Status_changed_to_Current</fullName>
        <description>Update Date_Changed_to_Current__c to TODAY when Account_Status__c = &quot;Current&quot;</description>
        <field>Date_changed_to_Current__c</field>
        <formula>TODAY()</formula>
        <name>Account Status changed to Current</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Status_changed_to_Deleted</fullName>
        <description>Update Date_changed_to_Deleted__c to TODAY when Account_Status__c = &quot;Deleted&quot;</description>
        <field>Date_changed_to_Deleted__c</field>
        <formula>TODAY()</formula>
        <name>Account Status changed to Deleted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Lib_Lifestyle_Content_Email</fullName>
        <description>Update this value to be false</description>
        <field>Add_Lib_Lifestyle_Content_Email__pc</field>
        <literalValue>0</literalValue>
        <name>Add Lib Lifestyle Content Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Lib_Lifestyle_Content_Post</fullName>
        <description>Update this value to be false</description>
        <field>Add_Lib_Lifestyle_Content_Post__pc</field>
        <literalValue>0</literalValue>
        <name>Add Lib Lifestyle Content Post</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Add_Lib_Lifestyle_Content_SMS</fullName>
        <description>Update this value to be false</description>
        <field>Add_Lib_Lifestyle_Content_SMS__pc</field>
        <literalValue>0</literalValue>
        <name>Add Lib Lifestyle Content SMS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Grade2</fullName>
        <field>Grading__c</field>
        <literalValue>N3</literalValue>
        <name>Grade2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Marketing_Status_Update</fullName>
        <description>Update marketing Status to active when booking added to lapsed account or no spend account</description>
        <field>Marketing_Status__c</field>
        <literalValue>Active</literalValue>
        <name>Marketing Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_account_confirmation_Update_checkb</fullName>
        <description>Check field to send out account confirmation email</description>
        <field>Current_Account_Confirmation_Email__c</field>
        <literalValue>1</literalValue>
        <name>New account confirmation - Update checkb</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>News_Letters_And_Updates_Email</fullName>
        <description>Update this value to be false</description>
        <field>News_Letters_And_Updates_Email__pc</field>
        <literalValue>0</literalValue>
        <name>News Letters And Updates Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>News_Letters_And_Updates_Post</fullName>
        <description>Update this value to be false</description>
        <field>News_Letters_And_Updates_Post__pc</field>
        <literalValue>0</literalValue>
        <name>News Letters And Updates Post</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>News_Letters_And_Updates_SMS</fullName>
        <description>Update this value to be false</description>
        <field>News_Letters_And_Updates_SMS__pc</field>
        <literalValue>0</literalValue>
        <name>News Letters And Updates SMS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offers_Discounts_Competitions_Email</fullName>
        <description>Update this value to be false</description>
        <field>Offers_Discounts_Competitions_Email__pc</field>
        <literalValue>0</literalValue>
        <name>Offers,Discounts,Competitions Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offers_Discounts_Competitions_Post</fullName>
        <description>Update this value to be false</description>
        <field>Offers_Discounts_Competitions_Post__pc</field>
        <literalValue>0</literalValue>
        <name>Offers,Discounts,Competitions Post</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Offers_Discounts_Competitions_SMS</fullName>
        <description>Update this value to be false</description>
        <field>Offers_Discounts_Competitions_SMS__pc</field>
        <literalValue>0</literalValue>
        <name>Offers,Discounts,Competitions SMS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Date_Lapsed_Marketing</fullName>
        <field>Date_Lapsed_Marketing__c</field>
        <formula>TODAY()</formula>
        <name>Populate Date Lapsed (Marketing)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reactivated_Date</fullName>
        <field>Reactivated_Date__c</field>
        <formula>Today()</formula>
        <name>Reactivated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Read_Only_View</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Read_Only_Business_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Read Only View</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Re_activated_Re_Opend_Date</fullName>
        <field>Sales_Re_activated_Date__c</field>
        <formula>Today()</formula>
        <name>Sales Re-activated/Re-Opend Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Account_to_Current</fullName>
        <description>Set account to current if bank details captured and RAG status green</description>
        <field>Account_Status__c</field>
        <literalValue>Current</literalValue>
        <name>Set Account to Current</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_DoNotSend_Checkbox</fullName>
        <description>Set the do not send current account confirmation email to false</description>
        <field>Do_Not_Send_Current_Account_Confirmation__c</field>
        <literalValue>1</literalValue>
        <name>Set DoNotSend Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Integration_Ready_To_True</fullName>
        <description>Check the ready for integration checkbox to true</description>
        <field>integrationReady__c</field>
        <literalValue>1</literalValue>
        <name>Set Integration Ready To True</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Surveys_And_Research_Email</fullName>
        <description>Update this value to be false</description>
        <field>Surveys_And_Research_Email__pc</field>
        <literalValue>0</literalValue>
        <name>Surveys And Research Email</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Surveys_And_Research_Post</fullName>
        <description>Update this value to be false</description>
        <field>Surveys_And_Research_Post__pc</field>
        <literalValue>0</literalValue>
        <name>Surveys And Research Post</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Surveys_And_Research_SMS</fullName>
        <field>Surveys_And_Research_SMS__pc</field>
        <literalValue>0</literalValue>
        <name>Surveys And Research SMS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Managed_Checkbox</fullName>
        <description>Update account managed checkbox to true</description>
        <field>Account_Managed__c</field>
        <literalValue>1</literalValue>
        <name>Update Account Managed Checkbox</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Number</fullName>
        <description>Update account number with the auto number</description>
        <field>AccountNumber</field>
        <formula>Account_Autonumber__c</formula>
        <name>Update Account Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Number_Autonumber</fullName>
        <description>Update account number with auto-number</description>
        <field>Account_Number__c</field>
        <formula>Account_Autonumber__c</formula>
        <name>Update Account Number with Autonumber</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Administration_Fee_12_5</fullName>
        <description>Update administration fee to 12.5%</description>
        <field>Administration_Fee__c</field>
        <formula>12.5</formula>
        <name>Update Administration Fee 12.5%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Administration_Fee_17_5</fullName>
        <description>Update administration fee to 17.5%</description>
        <field>Administration_Fee__c</field>
        <formula>17.5</formula>
        <name>Update Administration Fee 17.5%</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Credit_Limit</fullName>
        <field>Credit_Limit__c</field>
        <formula>Estimated_Spend_Padded__c</formula>
        <name>Update Credit Limit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Priority_Fee</fullName>
        <field>Administration_Fee__c</field>
        <formula>SHM_serviceCharge__c</formula>
        <name>Update Priority Fee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Priority_Fee_on_Created_Account</fullName>
        <description>Updates the priority fee based on criteria from David Kingsley.</description>
        <field>Administration_Fee__c</field>
        <formula>IF( 
AND( 
ISPICKVAL(Account_Product__c, &quot;Small Business&quot;), 
OR(RecordTypeId = &quot;012b00000000rxH&quot;, RecordTypeId = &quot;012b000000016wa&quot;) ) , 


0, 

IF( 
AND( 
NOT(ISPICKVAL(Account_Product__c, &quot;Small Business&quot;)), 
ISPICKVAL(Payment_Type__c, &quot;Direct Debit&quot;), 
OR(RecordTypeId = &quot;012b00000000rxH&quot;, RecordTypeId = &quot;012b000000016wa&quot;, RecordTypeId = &quot;012b00000000rxg&quot;, RecordTypeId = &quot;012b0000000Dhyp&quot;) ), 


12.5, 

IF(RecordTypeId = &quot;012b00000000s1m&quot;, 

0, 

17.5) ) )</formula>
        <name>Update Priority Fee on Created Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Reactivated_Date_to_Today</fullName>
        <field>Sales_Re_activated_Date__c</field>
        <formula>TODAY()</formula>
        <name>Update Sales Reactivated Date to Today</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Read_Only</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Read_Only_Personal_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update to Read Only</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>grade1</fullName>
        <field>Grading__c</field>
        <literalValue>N2</literalValue>
        <name>grade1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>1st Spend Interactions</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Total_Amount_of_Bookings__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Read Only Business Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>shamrock integration,mike fleming</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Owner_Department__c</field>
            <operation>contains</operation>
            <value>inbound,new business</value>
        </criteriaItems>
        <description>A sequence of time-related tasks to engage with Accounts after their first booking. DEACTIVATED 12/10/15--duplicate of workflow called &quot;Spending - 1st Call.&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>First_Spend_Call</name>
                <type>Task</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account Contact Call%2E</fullName>
        <actions>
            <name>Account_Monthly_Call</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Last_Contacted__c</field>
            <operation>greaterThan</operation>
            <value>30</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Owner_Department__c</field>
            <operation>contains</operation>
            <value>inbound,new business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Last_Contacted__c</field>
            <operation>notContain</operation>
            <value>never,today</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.no_account_sales_workflow_tasks__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>To create a task if the Account Last Contacted days is greater than 30. DEACTIVATED 22/1/16  Account Last Contacted is a Text Formula Field and cannot use &quot;greater than&quot; and won&apos;t fire because it&apos;s a formula field.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Grade Update 2</fullName>
        <actions>
            <name>Grade2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Product__c</field>
            <operation>equals</operation>
            <value>Priority Plus</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Account</value>
        </criteriaItems>
        <description>When Account Product is Priority Plus,Update Grade value as N3.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Account Management Review WestOne</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Current</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Ledger__c</field>
            <operation>equals</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <description>Reminder 3 months after Accounts are made current to review for Account Manager to be assigned</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Account_Management_Review</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account.Date_changed_to_Current__c</offsetFromField>
            <timeLength>90</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account Not Booking Tasks</fullName>
        <actions>
            <name>X45_Days_since_Last_Booking</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>When an Account Status is Current AND The Date Changed to Current is less than or equal to 6 months ago, and the Date of Last Booking is greater than 45 days ago, then create Task to call</description>
        <formula>AND(  ISPICKVAL( Account_Status__c , &quot;Current&quot;),  
Date_of_Last_Booking__c &lt;= TODAY()-45, 
YEAR( Date_changed_to_Current__c )=YEAR(TODAY()), 
MONTH(Date_changed_to_Current__c) &gt;= MONTH(TODAY())-1,  
Owner.Alias  &lt;&gt; &quot;sinte&quot; , Owner.Alias&lt;&gt;&quot;wuser&quot;,   
Owner.Profile.Id = &quot;00eb0000000RJIS&quot;,  
RecordTypeId =&quot;012b000000016wa&quot;,
no_account_sales_workflow_tasks__c = FALSE  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Not Spending Push</fullName>
        <actions>
            <name>Not_Booking_Call</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>One Time Use. When an Account Status is Current AND the Total Booking Spend = 0 AND The Date Changed to Current is less than or equal to 14 days ago and greater than 3 months ago, create a Tasks to call.</description>
        <formula>AND(  ISPICKVAL( Account_Status__c , &quot;Current&quot;), Total_Booking_Spend__c = 0, Date_changed_to_Current__c &lt;= TODAY()-14, YEAR(Date_changed_to_Current__c)= YEAR(TODAY()), MONTH(Date_changed_to_Current__c) &gt;= MONTH(TODAY())-3,  Owner.Alias  &lt;&gt; &quot;sinte&quot; , Owner.Alias&lt;&gt;&quot;wuser&quot;,   Owner.Profile.Id = &quot;00eb0000000RJIS&quot;, RecordType.Id =&quot;012b000000016wa&quot;,  Dataloading_Update__c =False, no_account_sales_workflow_tasks__c = FALSE  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Not Spending Tasks</fullName>
        <active>true</active>
        <description>When a Business Account Status is Current AND the Total Booking Spend = 0 AND The Date Changed to Current is less than or equal to TODAY, create Tasks to call 14, 35 and 63 days after the Date Changed to Current Date. Excludes Shamrock and Web User owned.</description>
        <formula>AND(  
ISPICKVAL( Account_Status__c , &quot;Current&quot;), Total_Booking_Spend__c = 0, 
Date_changed_to_Current__c &lt;= TODAY()-14,
YEAR(Date_changed_to_Current__c)= YEAR(TODAY()), 
MONTH(Date_changed_to_Current__c) &gt;= MONTH(TODAY())-3, 
Owner.Alias &lt;&gt; &quot;sinte&quot; ,
Owner.Alias&lt;&gt;&quot;wuser&quot;,
Owner.Profile.Id = &quot;00eb0000000RJIS&quot;,
 RecordType.Id = &quot;012b000000016wa&quot;,
 no_account_sales_workflow_tasks__c = FALSE

)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>No_Spend_Call_2</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account.Date_changed_to_Current__c</offsetFromField>
            <timeLength>35</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>No_Spend_Call_1</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account.Date_changed_to_Current__c</offsetFromField>
            <timeLength>14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>No_Spend_Call_3</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Account.Date_changed_to_Current__c</offsetFromField>
            <timeLength>63</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account Status changed to Current</fullName>
        <actions>
            <name>Account_Status_changed_to_Current</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Current</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Date_changed_to_Current__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Status changed to Deleted</fullName>
        <actions>
            <name>Account_Status_changed_to_Deleted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Deleted</value>
        </criteriaItems>
        <description>Update Date_changed_to_Deleted__c with TODAY() when Account Status is changed to &quot;Deleted&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account grade Updation</fullName>
        <actions>
            <name>grade1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 OR 2) AND 3</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Product__c</field>
            <operation>equals</operation>
            <value>Small Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Product__c</field>
            <operation>equals</operation>
            <value>Standard</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Account</value>
        </criteriaItems>
        <description>When Account Product is Small Business or Standard,Update Grade value is N2.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Business Account Read Only Record Type Update</fullName>
        <actions>
            <name>Read_Only_View</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Current</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Account</value>
        </criteriaItems>
        <description>Update Business Account to Read Only when account is Current</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Marketing Status to Active</fullName>
        <actions>
            <name>Marketing_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Booked__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Change to marketing status to Active when lapsed or no spend accounts has booking from marketing campaign</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Consumers%2C Account Number %3D 1</fullName>
        <actions>
            <name>Account_Number_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>populates Account Number &quot;1&quot; on all records where consumer account number =1</description>
        <formula>AND( 

RecordType.Name=&quot;Consumer&quot;, 
ISBLANK(AccountNumber) 
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Convert Account Automatically Time Trigger</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.integrationReady__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Converted_From_Lead__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Set Account_Status__c to &quot;Current&quot; automatically (used for the auto-conversion process for D&amp;B Credit Checking)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Set_Account_to_Current</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>Account.Time_Trigger__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Do Not Send account confirmation - Update checkbox</fullName>
        <actions>
            <name>Set_DoNotSend_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Current</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.SHM_main_email__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedById</field>
            <operation>notEqual</operation>
            <value>Shamrock Integration</value>
        </criteriaItems>
        <description>Update checkbox to Do Not Send account confirmation email</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Estimated Monthly Spend to Credit Limit</fullName>
        <actions>
            <name>Update_Credit_Limit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Estimated_Spend_Padded__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.IsPersonAccount</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Insurance Renewal Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Insurance_Renewal_Date__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Supplier/Partner</value>
        </criteriaItems>
        <description>Email triggered to remind Account Owner and Partner Manager about their insurance renewal 1, 7, 14, 30 days prior to the renewal date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Reminder_Insurance_Renewal_Date</name>
                <type>Alert</type>
            </actions>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Reminder_Insurance_Renewal_Date_Expired</name>
                <type>Alert</type>
            </actions>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Reminder_Insurance_Renewal_Date</name>
                <type>Alert</type>
            </actions>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Reminder_Insurance_Renewal_Date</name>
                <type>Alert</type>
            </actions>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Insurance Renewal Original Date</fullName>
        <actions>
            <name>Insurance_Renewal</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Insurance_Renewal_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Creates a task upon a date first being entered onto the record.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Account Opened Notification</fullName>
        <actions>
            <name>When_New_Account_Opened_Notification_Send_Notification_To_SME</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Account_Opened</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <description>When Account Opened,Send email notification to SME Sales team AL LON.</description>
        <formula>AND 

( 

OR(RecordTypeId = &apos;012b00000000rxH&apos;,RecordTypeId = &apos;012b00000000rxg&apos;, 
RecordTypeId = &quot;012b000000016wa&quot;, RecordTypeId= &quot;012b0000000Dhyp&quot;), 
(ISPICKVAL(PRIORVALUE(Account_Status__c),&quot;Prospect&quot;)), 
ISPICKVAL(Account_Status__c, &quot;Current&quot;) 

)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New Personal account confirmation - Send AL Email</fullName>
        <actions>
            <name>New_account_confirmation_AL_Send_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Account_Activation_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Account.Current_Account_Confirmation_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Do_Not_Send_Current_Account_Confirmation__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.exsistInShamrock__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Personal Account,Read Only Personal Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>Salesforce Administrator,Exacttarget Integration,Web User,Shamrock Integration</value>
        </criteriaItems>
        <description>Send confirmation email to main contact email address when Current_Account_Confirmation_Email__c is TRUE. This rule sends a personalised email if the account owner is not a system user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Personal account confirmation - Send Generic Email NEW</fullName>
        <actions>
            <name>New_account_confirmation_Send_Generic_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Account_Activation_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Account.Current_Account_Confirmation_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Do_Not_Send_Current_Account_Confirmation__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.exsistInShamrock__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Personal Account,Read Only Personal Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>Salesforce Administrator,Exacttarget Integration,Web User,Shamrock Integration</value>
        </criteriaItems>
        <description>Send confirmation email to main contact email address when Current_Account_Confirmation_Email__c is TRUE. This rule sends a generic email when the account is owned by a system user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New account confirmation - Send W1 Email</fullName>
        <actions>
            <name>New_account_confirmation_W1_Send_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Account_Activation_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Current_Account_Confirmation_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Ledger__c</field>
            <operation>equals</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Do_Not_Send_Current_Account_Confirmation__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.exsistInShamrock__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send W1 confirmation email to main contact email address when Current_Account_Confirmation_Email__c is TRUE</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New account confirmation - Update checkbox</fullName>
        <actions>
            <name>New_account_confirmation_Update_checkb</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Current</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.SHM_main_email__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedById</field>
            <operation>notEqual</operation>
            <value>Shamrock Integration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>Shamrock Integration</value>
        </criteriaItems>
        <description>Update hidden checkbox on account Current_Account_Confirmation_Email__c</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Operation License Renewal Date</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Operational_License_Renewal__c</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Supplier/Partner</value>
        </criteriaItems>
        <description>Email triggered to remind Account Owner and Partner Manager about their operational license 1, 7, 14, 30 days prior to the renewal date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Reminder_Operational_License_Renewal_Date</name>
                <type>Alert</type>
            </actions>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Reminder_Operational_License_Renewal_Date</name>
                <type>Alert</type>
            </actions>
            <timeLength>-14</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Reminder_Operational_License_Renewal_Date_Expired</name>
                <type>Alert</type>
            </actions>
            <timeLength>-1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Email_Reminder_Operational_License_Renewal_Date</name>
                <type>Alert</type>
            </actions>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Person Account Opt-Out All Rule</fullName>
        <actions>
            <name>Add_Lib_Lifestyle_Content_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Lib_Lifestyle_Content_Post</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Add_Lib_Lifestyle_Content_SMS</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>News_Letters_And_Updates_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>News_Letters_And_Updates_Post</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>News_Letters_And_Updates_SMS</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Offers_Discounts_Competitions_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Offers_Discounts_Competitions_Post</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Offers_Discounts_Competitions_SMS</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Surveys_And_Research_Email</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Optout_All__pc</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>When opt-out all is false,update all preference center fields also false.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Person Account Opt-Out All Rule 2</fullName>
        <actions>
            <name>Surveys_And_Research_Post</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Surveys_And_Research_SMS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Optout_All__pc</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>When person account optout all is false,update all preference to false</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Personal Account Read Only Record Type Update</fullName>
        <actions>
            <name>Update_to_Read_Only</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Current</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Personal Account</value>
        </criteriaItems>
        <description>Update Personal Account to Read Only when account is Current</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Date Lapsed %28Marketing%29</fullName>
        <actions>
            <name>Populate_Date_Lapsed_Marketing</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISPICKVAL(Marketing_Status__c,&quot;Lapsed&quot;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Marketing Reactivation Date</fullName>
        <actions>
            <name>Reactivated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISCHANGED( Marketing_Status__c),OR(ISPICKVAL(PRIORVALUE(Marketing_Status__c),&quot;Lapsed&quot;),ISPICKVAL(PRIORVALUE(Marketing_Status__c),&quot;No Spend&quot;)),ISPICKVAL(Marketing_Status__c,&quot;Active&quot;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Sales Re-activated%2FRe-Open Date</fullName>
        <actions>
            <name>Sales_Re_activated_Re_Opend_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>This field indicates the sales definition of a lapsed booking, where the date of last booking was &gt;=90 days from this date. DEACTIVATED because this will be a more manual process.</description>
        <formula>AND( ISCHANGED( Sales_Status__c),OR(ISPICKVAL(PRIORVALUE(Sales_Status__c),&quot;Lapsed&quot;),ISPICKVAL(Sales_Status__c,&quot;Re-Activated&quot;),ISPICKVAL(Sales_Status__c,&quot;Re-Opend&quot;) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Send Email to New Account Owner</fullName>
        <actions>
            <name>Send_Email_notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>When the account owner is changed, email the new owner to let them know. Created when the &quot;notify user&quot; box was no longer working due to a process builder known issue.</description>
        <formula>ISCHANGED( OwnerId   )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Integration Ready Business Account</fullName>
        <actions>
            <name>Set_Integration_Ready_To_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Integration Ready to TRUE for Accounts ready for Shamrock Integration</description>
        <formula>AND(RecordType.Name == &apos;Business Account&apos;,  OR( ISPICKVAL(Account_Status__c, &apos;Current&apos;), ISPICKVAL(Account_Status__c, &apos;On Hold&apos;), ISPICKVAL(Account_Status__c, &apos;Deleted&apos;), ISPICKVAL(Account_Status__c, &apos;Ask Accounts&apos;))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Integration Ready Personal Account</fullName>
        <actions>
            <name>Set_Integration_Ready_To_True</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set Integration Ready to TRUE for Accounts ready for Shamrock Integration</description>
        <formula>AND(RecordType.Name == &apos;Personal Account&apos;, OR( ISPICKVAL(Account_Status__c, &apos;Current&apos;), ISPICKVAL(Account_Status__c, &apos;On Hold&apos;), ISPICKVAL(Account_Status__c, &apos;Deleted&apos;), ISPICKVAL(Account_Status__c, &apos;Ask Accounts&apos;))  )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Spending - 1st Call</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Account.Total_Amount_of_Bookings__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Read Only Business Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Owner_Department__c</field>
            <operation>contains</operation>
            <value>inbound,new business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.no_account_sales_workflow_tasks__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>A sequence of time-related tasks to engage with Accounts after their first booking.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Spending_First_Call</name>
                <type>Task</type>
            </actions>
            <timeLength>3</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Update Account Managed Checkbox</fullName>
        <actions>
            <name>Update_Account_Managed_Checkbox</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Manager__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Update Account Managed checkbox if Account Manager has been assigned and fed through to Salesforce</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Administration Fee on Accounts - 17%2E5</fullName>
        <actions>
            <name>Update_Administration_Fee_17_5</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Payment_Type__c</field>
            <operation>notEqual</operation>
            <value>Direct Debit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>Shamrock Integration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedById</field>
            <operation>notEqual</operation>
            <value>Shamrock Integration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Consumer</value>
        </criteriaItems>
        <description>Update Administration_Fee__c on Accounts to 17.5%</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Administration Fee on Accounts 12%2E5</fullName>
        <actions>
            <name>Update_Administration_Fee_12_5</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Account.Payment_Type__c</field>
            <operation>equals</operation>
            <value>Direct Debit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>Shamrock Integration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.CreatedById</field>
            <operation>notEqual</operation>
            <value>Shamrock Integration</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>notEqual</operation>
            <value>Consumer</value>
        </criteriaItems>
        <description>Update Administration_Fee__c on Accounts to 12.5%</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Priority Fee</fullName>
        <actions>
            <name>Update_Priority_Fee</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(SHM_serviceCharge__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Priority Fee on Created Account</fullName>
        <actions>
            <name>Update_Priority_Fee_on_Created_Account</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.CreatedDate</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </criteriaItems>
        <description>Automatically updates the priority fee business account/business account read-only with account product &quot;small business&quot; = 0, business account/business account read-only/personal account with payment type &quot;direct debit&quot; = 12.5, consumer = 0, all else 17.5</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Sales Reactivated Date</fullName>
        <actions>
            <name>Update_Sales_Reactivated_Date_to_Today</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When the Sales Status has been changed to &quot;Re-Opened,&quot; update the Sales Reactivated Date to Today.</description>
        <formula>ISPICKVAL(Sales_Status__c, &quot;Re-Opened&quot;)    &amp;&amp;   ISCHANGED( Sales_Status__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Very High Account Opening Addison Lee</fullName>
        <actions>
            <name>Very_High_Account_Opened_Addison_Lee</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Current</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Segment__c</field>
            <operation>equals</operation>
            <value>Very High</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <description>Notification to Director of Services when a &quot;Very High&quot; Account is opened</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Welcome to your Business Priority Plus Account</fullName>
        <actions>
            <name>Welcome_to_your_Business_Priority_Plus_Account</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Business_Priority_Plus_Account_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Account.Current_Account_Confirmation_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Do_Not_Send_Current_Account_Confirmation__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.exsistInShamrock__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Product__c</field>
            <operation>equals</operation>
            <value>Priority Plus</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Account,Read Only Business Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>Salesforce Administrator,Exacttarget Integration,Web User,Shamrock Integration</value>
        </criteriaItems>
        <description>All web-to-lead accounts will be Business Priority Plus accounts by default and should receive the BPP email. This email will be a personalised email since the account owner is not a system user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Welcome to your Business Priority Plus Account - Send Generic Email NEW</fullName>
        <actions>
            <name>Welcome_to_your_Business_Priority_Plus_Account_Generic</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Account_Activation_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Account.Current_Account_Confirmation_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Do_Not_Send_Current_Account_Confirmation__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.exsistInShamrock__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Product__c</field>
            <operation>equals</operation>
            <value>Priority Plus</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Account,Read Only Business Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>Salesforce Administrator,Exacttarget Integration,Web User,Shamrock Integration</value>
        </criteriaItems>
        <description>All web-to-lead accounts will be Business Priority Plus accounts by default and should receive the BPP email. This email will be a generic email since the a system user owns the account.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Welcome to your Small Business</fullName>
        <actions>
            <name>Welcome_to_Small_Business</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Welcome_email_for_small_business_has_been_sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Account.Current_Account_Confirmation_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Do_Not_Send_Current_Account_Confirmation__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.exsistInShamrock__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Product__c</field>
            <operation>equals</operation>
            <value>Small Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Account,Read Only Business Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>notEqual</operation>
            <value>Salesforce Administrator,Exacttarget Integration,Web User,Shamrock Integration</value>
        </criteriaItems>
        <description>All accounts where &apos;Small Business’ is selected in the ‘Account Product’ field should receive the Small Business email. This personalised email is sent when the account owner is not a system user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Welcome to your Small Business - Send Generic Email NEW</fullName>
        <actions>
            <name>Welcome_to_Small_Business_Generic</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Account_Activation_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 AND 7</booleanFilter>
        <criteriaItems>
            <field>Account.Current_Account_Confirmation_Email__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Do_Not_Send_Current_Account_Confirmation__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.exsistInShamrock__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Product__c</field>
            <operation>equals</operation>
            <value>Small Business</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.RecordTypeId</field>
            <operation>equals</operation>
            <value>Business Account,Read Only Business Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.OwnerId</field>
            <operation>equals</operation>
            <value>Salesforce Administrator,Exacttarget Integration,Web User,Shamrock Integration</value>
        </criteriaItems>
        <description>All accounts where &apos;Small Business’ is selected in the ‘Account Product’ field should receive the Small Business email. Sends a generic email when the account owner is a system user.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Account_Activation_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Account Activation Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Account_Management_Review</fullName>
        <assignedTo>donna.bloomfield@addisonlee.co.uk</assignedTo>
        <assignedToType>user</assignedToType>
        <description>This Account has been Current for 90 days. Please review for Account Management assignment.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Account Management Review</subject>
    </tasks>
    <tasks>
        <fullName>Account_Monthly_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>It is over 30 days since your account was last contacted.  Please make a call to touch base, and see how things are.  Can you gather any new useful company information that is not completed in the account record?</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Account - Monthly Call</subject>
    </tasks>
    <tasks>
        <fullName>Account_Opened</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Account Opened</subject>
    </tasks>
    <tasks>
        <fullName>Business_Priority_Plus_Account_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Business Priority Plus Account Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>First_Spend_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>This account has made its first booking.  Call your account contact and check that the booking went well.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>First Spend Call</subject>
    </tasks>
    <tasks>
        <fullName>Insurance_Renewal</fullName>
        <assignedTo>david.cederholm@addisonlee.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>The insurance for this supplier/partner is due for renewal. Please follow up to check that the insurance has been renewed.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Insurance_Renewal_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Insurance Renewal</subject>
    </tasks>
    <tasks>
        <fullName>No_Spend_Call_1</fullName>
        <assignedToType>owner</assignedToType>
        <description>Make a service call, check all account details received. Build rapport.</description>
        <dueDateOffset>14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Date_changed_to_Current__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>No Spend Call 1</subject>
    </tasks>
    <tasks>
        <fullName>No_Spend_Call_2</fullName>
        <assignedToType>owner</assignedToType>
        <description>This account has no spend, please call and ascertain any reasons for the no spend.  Promote the benefits of the account. Expand contacts within the account.</description>
        <dueDateOffset>35</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Date_changed_to_Current__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>No Spend Call 2</subject>
    </tasks>
    <tasks>
        <fullName>No_Spend_Call_3</fullName>
        <assignedToType>owner</assignedToType>
        <description>It is now 9 weeks since this account went active.  Please call and ascertain any reasons for no spend.  Re-qualify needs.  Find out dates for future bookings.</description>
        <dueDateOffset>63</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Date_changed_to_Current__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>No Spend Call 3</subject>
    </tasks>
    <tasks>
        <fullName>Not_Booking_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>This account has not made a booking in the last 45 days.  Please make contact and check if there are any reasons for the lack of bookings.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Not Booking? Call</subject>
    </tasks>
    <tasks>
        <fullName>Operational_License_Renewal</fullName>
        <assignedTo>david.cederholm@addisonlee.com</assignedTo>
        <assignedToType>user</assignedToType>
        <description>The operational license for this supplier/partner is due for renewal. Please follow up to check that the operational license has been renewed.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Account.Operational_License_Renewal__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Operational License Renewal</subject>
    </tasks>
    <tasks>
        <fullName>Spending_First_Call</fullName>
        <assignedToType>owner</assignedToType>
        <description>This account has made its first booking.  Call your account contact and check that the booking went well.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Spending - 1st Call</subject>
    </tasks>
    <tasks>
        <fullName>Welcome_email_for_small_business_has_been_sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Welcome email for small business has been sent</subject>
    </tasks>
    <tasks>
        <fullName>X45_Days_since_Last_Booking</fullName>
        <assignedToType>owner</assignedToType>
        <description>This account has not made a booking in 45 days, please make contact to encourage bookings.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>45 Days since Last Booking</subject>
    </tasks>
</Workflow>
