<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Entitlement_Renewal_Reminder</fullName>
        <description>Entitlement Renewal Reminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account Manager</recipient>
            <type>accountTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Entitlement_Renewal_Reminder</template>
    </alerts>
</Workflow>
