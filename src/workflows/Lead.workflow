<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Almost_There_Business_Email_1</fullName>
        <description>&quot;Almost There&quot; Business Email 1</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Archived_Email_Templates/Almost_there_1_Business</template>
    </alerts>
    <alerts>
        <fullName>Almost_There_Business_Email_2</fullName>
        <description>Almost There&quot; Business Email 2</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Archived_Email_Templates/Almost_there_2_Busines</template>
    </alerts>
    <alerts>
        <fullName>Almost_There_Customer_Reminder</fullName>
        <description>&quot;Almost There&quot; Customer Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Archived_Email_Templates/Almost_There_Business_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Almost_There_Email_Sent</fullName>
        <description>Almost There  Email Sent</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priority@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_Almost_there_business_reminder_02_15</template>
    </alerts>
    <alerts>
        <fullName>Almost_There_Internal_Reminder</fullName>
        <ccEmails>priorityaccountsteam@addisonlee.com</ccEmails>
        <description>&quot;Almost There&quot; Internal Reminder</description>
        <protected>false</protected>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Almost_There_Internal_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Almost_There_Internal_Six_Hours_Reminder</fullName>
        <ccEmails>priorityaccountsteam@addisonlee.com</ccEmails>
        <description>&quot;Almost There&quot; Internal Six Hours Reminder</description>
        <protected>false</protected>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Almost_There_Internal_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Almost_There_Personal_Reminder</fullName>
        <description>Almost There Personal Reminder</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Almost_There_Personal_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Almost_There_Personal_Reminder_Email_2</fullName>
        <description>Almost There Personal Reminder - Email 2</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Archived_Email_Templates/Almost_there2_personal</template>
    </alerts>
    <alerts>
        <fullName>Almost_There_Reminder_Email_Sent1</fullName>
        <description>&quot;Almost There&quot; Reminder Email Sent</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priority@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_Almost_there_personal_reminder_02_15</template>
    </alerts>
    <alerts>
        <fullName>Almost_There_Reminder_Email_Sent_Business</fullName>
        <description>&quot;Almost There&quot; Reminder Email Sent</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priority@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_Almost_there_business_reminder_02_15</template>
    </alerts>
    <alerts>
        <fullName>Almost_There_Reminder_Email_Sent_Personal</fullName>
        <description>&quot;Almost There&quot; Reminder Email Sent</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priority@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_Almost_there_personal_reminder_02_15</template>
    </alerts>
    <alerts>
        <fullName>Almost_There_personal_1</fullName>
        <description>Almost There  personal-1</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_Almost_there_personal_reminder_02_15</template>
    </alerts>
    <alerts>
        <fullName>Almost_There_personal_account_Internal_Reminder</fullName>
        <ccEmails>priorityaccountsteam@addisonlee.com</ccEmails>
        <description>&quot;Almost There&quot; personal account Internal Reminder</description>
        <protected>false</protected>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Almost_There_Internal_Reminder</template>
    </alerts>
    <alerts>
        <fullName>Auto_Response_AL_Business_Accounts</fullName>
        <description>Auto-Response AL Business Accounts Red/Amber</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_thank_you_for_your_application_02_15</template>
    </alerts>
    <alerts>
        <fullName>Auto_Response_AL_Business_Accounts_Green_Bacs_and_Credit_Card</fullName>
        <description>Auto-Response AL Business Accounts Green - Bacs and Credit Card</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priority@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_thank_you_for_your_application_02_15</template>
    </alerts>
    <alerts>
        <fullName>Auto_Response_AL_Personal_Accounts</fullName>
        <description>Auto-Response AL Personal Accounts Red/Amber</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_thank_you_for_your_application_02_15</template>
    </alerts>
    <alerts>
        <fullName>Business_Account_Application_Almost_There</fullName>
        <description>Business Account Application Almost There</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_Almost_there_business_reminder_02_15</template>
    </alerts>
    <alerts>
        <fullName>Credit_Control_Alert_Red_Amber_Lead</fullName>
        <ccEmails>creditcontrol@addisonlee.com</ccEmails>
        <description>Credit Control Alert - Red/Amber Lead</description>
        <protected>false</protected>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Credit_Control_Red_Amber_Lead_Alert</template>
    </alerts>
    <alerts>
        <fullName>Email_Sales_Admins_for_New_WebLead</fullName>
        <description>Email Sales Admins for New WebLead</description>
        <protected>false</protected>
        <recipients>
            <recipient>andrew.maddock@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>david.kingsley@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>mariap@addisonlee.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Auto_Response_Templates/New_Web_Lead_Created</template>
    </alerts>
    <alerts>
        <fullName>Email_Sent</fullName>
        <description>Email Sent</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priority@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_thank_you_for_your_application_02_15</template>
    </alerts>
    <alerts>
        <fullName>Send_Ts_And_Cs_Business</fullName>
        <description>Send Ts And Cs Business</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Templates/Business_Account_Terms_Conditions</template>
    </alerts>
    <alerts>
        <fullName>Send_Ts_And_Cs_Personal</fullName>
        <description>Send Ts And Cs Personal</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Addison_Lee_Templates/Personal_Account_Terms_Conditions</template>
    </alerts>
    <alerts>
        <fullName>Web_Account_App_Thank_You_Personal</fullName>
        <description>Web Account App-Thank You-Personal</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priority@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_thank_you_for_your_application_02_15</template>
    </alerts>
    <alerts>
        <fullName>Web_Account_App_Thank_you</fullName>
        <description>Web Account App-Thank you</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priority@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_thank_you_for_your_application_02_15</template>
    </alerts>
    <alerts>
        <fullName>lmost_There_One_Email_Sent_Business</fullName>
        <description>lmost There One Email Sent - Business</description>
        <protected>false</protected>
        <recipients>
            <field>Email</field>
            <type>email</type>
        </recipients>
        <senderAddress>priorityaccountsteam@addisonlee.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Addison_Lee_Auto_Response_Templates/Account_app_Almost_there_business_reminder_02_15</template>
    </alerts>
    <fieldUpdates>
        <fullName>Assignment_for_Credit_Control</fullName>
        <description>Change lead owner to credit control</description>
        <field>OwnerId</field>
        <lookupValue>Credit_Control_Lead_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assignment for Credit Control</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_Convert</fullName>
        <description>Check auto-convert lead field when the credit check and bank details have been captured for a lead</description>
        <field>Convert_To_Account__c</field>
        <literalValue>1</literalValue>
        <name>Auto Convert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BA_Credit_Limit</fullName>
        <field>Credit_Limit__c</field>
        <formula>$Setup.Credit_Limit__c.Credit_Limit_BA__c</formula>
        <name>BA Credit Limit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bank_Details_Captured_Updation</fullName>
        <field>Bank_Details_Captured__c</field>
        <literalValue>1</literalValue>
        <name>Bank Details Captured Updation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Credit_Limit</fullName>
        <description>Credit limit for business account is 500.</description>
        <field>Credit_Limit__c</field>
        <formula>$Setup.Credit_Limit_PA__c.CreditLimit__c</formula>
        <name>Lead Credit Limit</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Ready_to_Convert</fullName>
        <description>Check the auto-convert checkbox on a lead</description>
        <field>Convert_To_Account__c</field>
        <literalValue>1</literalValue>
        <name>Lead Ready to Convert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Source_to_Business_Travel_Show</fullName>
        <description>Updates lead source to = &quot;Business Travel Show&quot;</description>
        <field>LeadSource</field>
        <literalValue>Business Travel Show</literalValue>
        <name>Lead Source to Business Travel Show</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Map_Credit_Debit_to_Single</fullName>
        <description>Map the Payment_Type_Web__c into the Payment_Type__c</description>
        <field>Payment_Type__c</field>
        <literalValue>Single Credit Card</literalValue>
        <name>Map Credit/Debit to Single</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Map_DD_to_DD_Lead</fullName>
        <description>Map the Payment_Type_Web__c into the Payment_Type__c</description>
        <field>Payment_Type__c</field>
        <literalValue>Direct Debit</literalValue>
        <name>Map DD to DD Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Map_Invoice_to_BACS</fullName>
        <description>Map the Payment_Type_Web__c into the Payment_Type__c</description>
        <field>Payment_Type__c</field>
        <literalValue>BACS</literalValue>
        <name>Map Invoice to BACS</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_Terms_and_Conditions</fullName>
        <description>Check the send T&amp;Cs checkbox</description>
        <field>Tech_Send_Ts_And_Cs__c</field>
        <literalValue>1</literalValue>
        <name>Send Terms and Conditions</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Record_Type_to_Business</fullName>
        <description>Set the lead record type to business</description>
        <field>RecordTypeId</field>
        <lookupValue>Business_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Lead Record Type to Business</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_Record_Type_to_Personal</fullName>
        <description>Set the lead record type of personal</description>
        <field>RecordTypeId</field>
        <lookupValue>Personal_Account</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Lead Record Type to Personal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Lead_owner_to_Web_User</fullName>
        <description>Sets Lead Owner to Web User</description>
        <field>OwnerId</field>
        <lookupValue>webuser@addisonlee.com</lookupValue>
        <lookupValueType>User</lookupValueType>
        <name>Set Lead owner to Web User</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_RAG_status_Amber</fullName>
        <description>Update RAG status to amber</description>
        <field>RAG_Status__c</field>
        <literalValue>Amber</literalValue>
        <name>Set RAG status Amber</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Rag_Status_Green</fullName>
        <description>Update RAG status to green</description>
        <field>RAG_Status__c</field>
        <literalValue>Green</literalValue>
        <name>Set Rag Status Green</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>T_C_Accepted_TRUE</fullName>
        <field>T_C_Accepted__c</field>
        <literalValue>1</literalValue>
        <name>T&amp;C Accepted = TRUE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Lead_Source</fullName>
        <description>Update account lead source with lead source</description>
        <field>Account_Lead_Source__c</field>
        <formula>TEXT(LeadSource)</formula>
        <name>Update Account Lead Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Closed_Not_Converted_Date</fullName>
        <description>Update lead closed date to TODAY</description>
        <field>Lead_Closed_Date__c</field>
        <formula>TODAY ()</formula>
        <name>Update Closed Not Converted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Closed_Further_Detail</fullName>
        <description>Update lead closed lost detail with credit rating information</description>
        <field>Reason_Closed_Further_Detail__c</field>
        <formula>&quot;Credit rating for this lead is not sufficient&quot;</formula>
        <name>Update Lead Closed Further Detail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Closed_Reason_if_RAG_Red</fullName>
        <description>Update lead closed lost reason to credit rating</description>
        <field>Reason_Not_Converted__c</field>
        <literalValue>Credit Rating</literalValue>
        <name>Update Lead Closed Reason if RAG=Red</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Owner_to_Credit_Control</fullName>
        <description>Update lead owner to credit control queue</description>
        <field>OwnerId</field>
        <lookupValue>Credit_Control_Lead_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Lead Owner to Credit Control</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Lead_Status_to_Closed_if_RAG_Red</fullName>
        <description>Update lead status to closed not converted</description>
        <field>Status</field>
        <literalValue>Closed - Not Converted</literalValue>
        <name>Update Lead Status to Closed if RAG=Red</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>%22Almost There%22 Lead Alert - Business</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Bank_Details_Captured__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Payment_Type_Web__c</field>
            <operation>equals</operation>
            <value>Direct Debit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Business Account Application</value>
        </criteriaItems>
        <description>Used for Leads where there is a Green RAG, but no bank details</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_There_Internal_Reminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Almost_There_Internal_Reminder_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_There_Customer_Reminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Almost_There_Reminder_Email_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>%22Almost There%22 Lead Alert - Personal</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Bank_Details_Captured__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Payment_Type_Web__c</field>
            <operation>equals</operation>
            <value>Direct Debit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Individual/Family Account Application</value>
        </criteriaItems>
        <description>Used for Leads where there is a Green RAG, but no bank details</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_There_Personal_Reminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Almost_There_Reminder_Email_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_There_Internal_Reminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Almost_There_Internal_Reminder_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>%22Almost There%22 Lead Business-1</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Bank_Details_Captured__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Payment_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Business Account Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Ledger__c</field>
            <operation>equals</operation>
            <value>Sales Ledger</value>
        </criteriaItems>
        <description>&apos;Almost there&apos; email prompt with link to credit check form (web form 2)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_There_Business_Email_1</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Almost_There_First_Email_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>%22Almost There%22 Lead Business-2</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5)</booleanFilter>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Bank_Details_Captured__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Payment_Type__c</field>
            <operation>equals</operation>
            <value>Direct Debit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Business Account Application</value>
        </criteriaItems>
        <description>Used for Leads where there is a Green RAG, but no bank details</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_There_Business_Email_2</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Almost_There_Reminder_Second_Email_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_There_Internal_Six_Hours_Reminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Almost_There_Six_Hours_Internal_Reminder_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>%27Almost there%27 Lead Personal-2</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Bank_Details_Captured__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Payment_Type_Web__c</field>
            <operation>equals</operation>
            <value>Direct Debit</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Individual/Family Account Application</value>
        </criteriaItems>
        <description>Used for Leads where there is a Green RAG, but no bank details</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_There_personal_account_Internal_Reminder</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Almost_There_Personal_Account_Internal_Reminder_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>6</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_There_Personal_Reminder_Email_2</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Almost_There_Reminder_Email_Sent_Personal</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account app - Almost there Personal reminder</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 and 6</booleanFilter>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Bank_Details_Captured__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Ledger__c</field>
            <operation>equals</operation>
            <value>Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Payment_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Individual/Family Account Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <description>&apos;Almost there&apos; email prompt with link to credit check form (web form 2) for Personal</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_There_personal_1</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Almost_There_One_Email_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account app - Almost there business reminder</fullName>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6 and 7</booleanFilter>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Bank_Details_Captured__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Payment_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Business Account Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Ledger__c</field>
            <operation>equals</operation>
            <value>Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <description>Account app - Almost there business reminder - 02/15</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>lmost_There_One_Email_Sent_Business</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>lmost_There_One_Email_Sent</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Assignment for Credit Control</fullName>
        <actions>
            <name>Assignment_for_Credit_Control</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Lead.Lead_Alias__c</field>
            <operation>equals</operation>
            <value>wuser</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>2,3</value>
        </criteriaItems>
        <description>Assignment rule to assign Leads to credit control queue</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Auto-Response Rule AL Business</fullName>
        <actions>
            <name>Auto_Response_AL_Business_Accounts</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Thank_you_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Business Account Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>2,3</value>
        </criteriaItems>
        <description>Auto-response template to send out to customer when RAG does not equal 1.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto-Response Rule AL Personal</fullName>
        <actions>
            <name>Auto_Response_AL_Personal_Accounts</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Thank_you_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND 3 AND 4</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Individual/Family Account Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>2,3</value>
        </criteriaItems>
        <description>Auto-response template to send out to customer when RAG does not equal 1.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Bank Details Captured Rule</fullName>
        <actions>
            <name>Bank_Details_Captured_Updation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Payment_Type_Web__c</field>
            <operation>equals</operation>
            <value>Invoice (BACS/Cheque)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RAG_Status__c</field>
            <operation>equals</operation>
            <value>Green</value>
        </criteriaItems>
        <description>When Payment Type(Web) is BACS n RAG Status Is Green, Then Update Bank details Captured is equal to True.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Close Lead when RAG %3D Red</fullName>
        <actions>
            <name>Update_Lead_Closed_Further_Detail</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_Closed_Reason_if_RAG_Red</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Lead_Status_to_Closed_if_RAG_Red</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RAG_Status__c</field>
            <operation>equals</operation>
            <value>Red</value>
        </criteriaItems>
        <description>Set the Lead Status to Closed when the RAG = Red</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit Alert for RAG - Red or Amber</fullName>
        <actions>
            <name>Credit_Control_Alert_Red_Amber_Lead</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>notEqual</operation>
            <value>1</value>
        </criteriaItems>
        <description>Alert sent to Credit Control team when RAG Status is Amber or Red</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Credit Limit BA</fullName>
        <actions>
            <name>BA_Credit_Limit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Business Account Credit Limit</description>
        <formula>AND(RecordTypeId =&apos;012b0000000kHwd&apos;,ISPICKVAL(LeadSource ,&apos;Web&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Credit Limit PA</fullName>
        <actions>
            <name>Lead_Credit_Limit</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Web Leads credit limit</description>
        <formula>AND(RecordTypeId = &apos;012b0000000kHwf&apos;,ISPICKVAL( LeadSource ,&apos;Web&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Default Owner to Web User</fullName>
        <actions>
            <name>Set_Lead_owner_to_Web_User</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Owner_Alias__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Default leads captured through website to be owned by Web user</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Sales Admins for New WebLead</fullName>
        <actions>
            <name>Email_Sales_Admins_for_New_WebLead</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Web User</value>
        </criteriaItems>
        <description>email alert to inform sales admins of new webleads</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Assignment</fullName>
        <actions>
            <name>Update_Lead_Owner_to_Credit_Control</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 AND 2 AND (3 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>2,3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RAG_Status__c</field>
            <operation>equals</operation>
            <value>Red,Amber</value>
        </criteriaItems>
        <description>Update Lead Owner to Credit Control Queue if D_B_Recommendation__c = 2,3</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Created at Trade Show</fullName>
        <actions>
            <name>Lead_Source_to_Business_Travel_Show</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>User.ProfileId</field>
            <operation>equals</operation>
            <value>Trade Show Profile</value>
        </criteriaItems>
        <description>Automatically makes lead source &quot;Business Travel Show&quot; when user profile is Trade Show.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Ready to Auto Convert</fullName>
        <actions>
            <name>Auto_Convert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Bank_Details_Captured__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.D_B_Credit_Limit__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RAG_Status__c</field>
            <operation>equals</operation>
            <value>Green</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <description>Set Convert_To_Account__c = TRUE if Lead meets criteria for auto-conversion.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Ready to Convert</fullName>
        <actions>
            <name>Send_Terms_and_Conditions</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Bank_Details_Captured__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>notEqual</operation>
            <value>Web</value>
        </criteriaItems>
        <description>Update Tech_Send_Ts_And_Cs__c = TRUE if the Account has bank details captured but needs to agree to T&amp;Cs</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Map Credit%2FDebit to Single</fullName>
        <actions>
            <name>Map_Credit_Debit_to_Single</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Payment_Type_Web__c</field>
            <operation>equals</operation>
            <value>Debit/Credit Card</value>
        </criteriaItems>
        <description>Map Payment Type(Web) to Shamrock/Account Payment Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Map DD to DD Lead</fullName>
        <actions>
            <name>Map_DD_to_DD_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Payment_Type_Web__c</field>
            <operation>equals</operation>
            <value>Direct Debit</value>
        </criteriaItems>
        <description>Map Payment Type(Web) to Shamrock/Account Payment Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Map Invoice to BACS</fullName>
        <actions>
            <name>Map_Invoice_to_BACS</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Payment_Type_Web__c</field>
            <operation>equals</operation>
            <value>Invoice (BACS/Cheque)</value>
        </criteriaItems>
        <description>Map Payment Type(Web) to Shamrock/Account Payment Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Re-Contact Date Task Lead</fullName>
        <actions>
            <name>Re_contact_lead</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Re_Contact_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>When the &quot;Re-contact date&quot; field has been populated, a new task is created assigned to the lead owner with the same due date, to remind them to contact the lead at that time.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Terms and Conditions - Business</fullName>
        <actions>
            <name>Send_Ts_And_Cs_Business</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Tech_Send_Ts_And_Cs__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Send Personal T&amp;Cs if Tech_Send_Ts_And_Cs__c = TRUE and Company not equal to BLANK</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Terms and Conditions - Personal</fullName>
        <actions>
            <name>Send_Ts_And_Cs_Personal</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Tech_Send_Ts_And_Cs__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Individual/Family Account Application</value>
        </criteriaItems>
        <description>Send Personal T&amp;Cs if Tech_Send_Ts_And_Cs__c = TRUE and Company = BLANK</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Send Terms and Conditions - Personal %2F Production Company</fullName>
        <actions>
            <name>Send_Ts_And_Cs_Personal</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <booleanFilter>(1 AND 2) OR (3 AND 4 AND 5)</booleanFilter>
        <criteriaItems>
            <field>Lead.Tech_Send_Ts_And_Cs__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Is_Production_Company__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Tech_Send_Ts_And_Cs__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Rule used to send the T&amp;Cs to production companies, who have 15 days T&amp;Cs unlike other business accounts</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Lead Record Type to Business</fullName>
        <actions>
            <name>Set_Lead_Record_Type_to_Business</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Business Account Application</value>
        </criteriaItems>
        <description>Set the lead record type of business when the nature of enquiry is set to personal</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Lead Record Type to Personal</fullName>
        <actions>
            <name>Set_Lead_Record_Type_to_Personal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Individual/Family Account Application</value>
        </criteriaItems>
        <description>Set the lead record type of personal when the nature of enquiry is set to personal</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set RAG Status Green</fullName>
        <actions>
            <name>Set_Rag_Status_Green</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set RAG_Status__c to &quot;Green&quot; if D_B_Recommendation__c = 1</description>
        <formula>IF( ISPICKVAL( D_B_Recommendation__c , &apos;1&apos;) , true, false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set RAG Status Red</fullName>
        <actions>
            <name>Set_RAG_status_Amber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>2,3</value>
        </criteriaItems>
        <description>Set RAG_Status__c to &quot;Amber&quot; if D_B_Recommendation__c = 2,3</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>T%26C Accepted %3D TRUE</fullName>
        <actions>
            <name>T_C_Accepted_TRUE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <description>updates the T&amp;C Accepted field value to True when a lead with Lead Source of Web is created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Account Lead Source</fullName>
        <actions>
            <name>Update_Account_Lead_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Lead Source needs to be mapped to the Account. This workflow is used to update the hidden field on the Lead to allow the field to be mapped onto the Account record.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Closed Not Converted Date</fullName>
        <actions>
            <name>Update_Closed_Not_Converted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Closed - Not Converted,Closed - Company in Administration</value>
        </criteriaItems>
        <description>Update Closed Not Converted date when lead is set to &quot;Closed Not Converted&quot; or &quot;Closed Company in Adminstration&quot;</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Web Account App-Thank You-Business-Fail</fullName>
        <actions>
            <name>Web_Account_App_Thank_you</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Thank_you_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Business Account Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>2,3</value>
        </criteriaItems>
        <description>Auto-response template to send out to customer when RAG does not equal 1.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Web Account App-Thank You-Business-Pass</fullName>
        <actions>
            <name>Auto_Response_AL_Business_Accounts_Green_Bacs_and_Credit_Card</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Thank_you_Email_Sent_2</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5</booleanFilter>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Business Account Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Payment_Type_Web__c</field>
            <operation>equals</operation>
            <value>Debit/Credit Card,Invoice (BACS/Cheque)</value>
        </criteriaItems>
        <description>For Bacs,invoice and card Payments</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Web Account App-Thank You-Personal-Fail</fullName>
        <actions>
            <name>Web_Account_App_Thank_You_Personal</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Thank_you_Email_Sent</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Individual/Family Account Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>2,3</value>
        </criteriaItems>
        <description>Auto-response template to send out to customer when RAG does not equal 1.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Web Account App-Thank You-Personal-Pass</fullName>
        <actions>
            <name>Email_Sent</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Thank_You_Email_Sent_1</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Individual/Family Account Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Sales_Ledger__c</field>
            <operation>notEqual</operation>
            <value>WestOne Sales Ledger</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Payment_Type_Web__c</field>
            <operation>equals</operation>
            <value>Debit/Credit Card</value>
        </criteriaItems>
        <description>Auto-response template to send out to customer when RAG does not equal 1.</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Web Account Follow Up Task For NON-DD</fullName>
        <actions>
            <name>Web_Account_Lead_Assignment_Reminder</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>2,3</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RAG_Status__c</field>
            <operation>equals</operation>
            <value>Red,Amber</value>
        </criteriaItems>
        <description>Web account follow up task for NON-DD payment types</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Web Account Follow Up Task For NON-DD D%26B1</fullName>
        <actions>
            <name>Web_Account_Lead_Assignment_Reminder1</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.LeadSource</field>
            <operation>equals</operation>
            <value>Web</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RAG_Status__c</field>
            <operation>equals</operation>
            <value>Green</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Payment_Type_Web__c</field>
            <operation>equals</operation>
            <value>Debit/Credit Card,Invoice (BACS/Cheque)</value>
        </criteriaItems>
        <description>Web account follow up task for NON-DD payment types with D &amp; B 1</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Web Account app-Almost There - Business</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Bank_Details_Captured__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Business Account Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Web User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Payment_Type_Web__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Used for Leads where there is a Green RAG, but no bank details</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_There_Reminder_Email_Sent_Business</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Time_Trigger__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_there_Email_Sent_Business</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.Time_Trigger_Task__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Web Account app-Almost There - Personal</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND (4 OR 5) AND 6</booleanFilter>
        <criteriaItems>
            <field>Lead.D_B_Recommendation__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Bank_Details_Captured__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Web User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Company</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Nature_of_Enquiry__c</field>
            <operation>equals</operation>
            <value>Individual/Family Account Application</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Payment_Type_Web__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Used for Leads where there is a Green RAG, but no bank details</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_there_Email_Sent_Personal</name>
                <type>Task</type>
            </actions>
            <offsetFromField>Lead.Time_Trigger_Task__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Almost_There_Reminder_Email_Sent_Personal</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Lead.Time_Trigger__c</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Almost_There_First_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Almost There First Email Sent Business</subject>
    </tasks>
    <tasks>
        <fullName>Almost_There_Internal_Reminder_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>&quot;Almost There&quot; Internal Reminder Sent</subject>
    </tasks>
    <tasks>
        <fullName>Almost_There_Internal_Reminder_Sent1</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Almost There Internal Reminder</subject>
    </tasks>
    <tasks>
        <fullName>Almost_There_One_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Almost There One Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Almost_There_Personal_Account_Internal_Reminder_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Almost There&quot; Personal Account Internal Reminder Sent</subject>
    </tasks>
    <tasks>
        <fullName>Almost_There_Reminder_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>&quot;Almost There&quot; Reminder Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Almost_There_Reminder_Email_Sent_Personal</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>&quot;Almost There&quot; Reminder Email Sent Personal</subject>
    </tasks>
    <tasks>
        <fullName>Almost_There_Reminder_Second_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>&quot;Almost There&quot; Reminder Second Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Almost_There_Six_Hours_Internal_Reminder_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>&quot;Almost There&quot; Six Hours Internal Reminder Sent</subject>
    </tasks>
    <tasks>
        <fullName>Almost_there_Email_Sent_2</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Almost there&apos; Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Almost_there_Email_Sent_Business</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Almost there&apos; Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Almost_there_Email_Sent_Personal</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Almost there&apos; Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Re_contact_lead</fullName>
        <assignedToType>owner</assignedToType>
        <description>You have added a re-contact date to this lead. Please ensure you contact this lead again by the due date you have specified.</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Lead.Re_Contact_Date__c</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Re-contact lead</subject>
    </tasks>
    <tasks>
        <fullName>Thank_You_Email_Sent_1</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Thank You Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Thank_you_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Web Account App-Thank You</subject>
    </tasks>
    <tasks>
        <fullName>Thank_you_Email_Sent_2</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Thank you Email Sent</subject>
    </tasks>
    <tasks>
        <fullName>Web_Account_Lead_Assignment_Reminder</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Web Account Lead Assignment Reminder</subject>
    </tasks>
    <tasks>
        <fullName>Web_Account_Lead_Assignment_Reminder1</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Web Account Lead Assignment Reminder</subject>
    </tasks>
    <tasks>
        <fullName>lmost_There_One_Email_Sent</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>lmost There One Email Sent</subject>
    </tasks>
</Workflow>
