<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Unique_Key_on_Active_Users_Object</fullName>
        <description>Update TECH Unique Industry</description>
        <field>TECH_Unique_Industry__c</field>
        <formula>TECH_Industry_Type__c</formula>
        <name>Update Unique Key on Active Users Object</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Unique Industry on Active Users</fullName>
        <actions>
            <name>Update_Unique_Key_on_Active_Users_Object</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Active_Users__c.TECH_Industry_Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Workflow that updates a field to unique to prevent duplicates in the industry type field on the Active Users Object</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
