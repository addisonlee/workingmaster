<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <allowInChatterGroups>true</allowInChatterGroups>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>For customer feedback/NPS.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSearch>true</enableSearch>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <description>Account of contact who filled out NPS survey.</description>
        <externalId>false</externalId>
        <inlineHelpText>Account of contact who filled out NPS survey.</inlineHelpText>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>NPS</relationshipLabel>
        <relationshipName>NPS</relationshipName>
        <relationshipOrder>1</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Case__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <description>Case created for a response to the NPS survey.</description>
        <externalId>false</externalId>
        <inlineHelpText>Case created for a response to the NPS survey.</inlineHelpText>
        <label>Case</label>
        <referenceTo>Case</referenceTo>
        <relationshipLabel>NPS</relationshipLabel>
        <relationshipName>NPS</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Category__c</fullName>
        <description>Category customer has selected when filling out NPS survey.</description>
        <externalId>false</externalId>
        <inlineHelpText>Category customer has selected when filling out NPS survey.</inlineHelpText>
        <label>Category</label>
        <picklist>
            <picklistValues>
                <fullName>Arrival Time</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Booking Experience</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Driver Quality</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Vehicle Quality</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Journey Price</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Comments__c</fullName>
        <description>Comments customer has entered on NPS survey.</description>
        <externalId>false</externalId>
        <inlineHelpText>Comments customer has entered on NPS survey.</inlineHelpText>
        <label>Comments</label>
        <length>32768</length>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Contact__c</fullName>
        <description>Contact who submitted the NPS survey.</description>
        <externalId>false</externalId>
        <inlineHelpText>Contact who submitted the NPS survey.</inlineHelpText>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>NPS</relationshipLabel>
        <relationshipName>NPS</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Date_Responded__c</fullName>
        <description>Date/time the customer clicked the link in their email to fill out the NPS survey.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date/time the customer clicked the link in their email to fill out the NPS survey.</inlineHelpText>
        <label>Date Responded</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Date_Sent__c</fullName>
        <description>Date the NPS survey email was sent to the customer.</description>
        <externalId>false</externalId>
        <inlineHelpText>Date the NPS survey email was sent to the customer.</inlineHelpText>
        <label>Date Sent</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>Email_Not_Sent__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Email Not Sent</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>External_Id__c</fullName>
        <caseSensitive>false</caseSensitive>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>18</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>NPS_Response__c</fullName>
        <externalId>false</externalId>
        <formula>IF(Score_Numeric__c  = 0, 0, 1)</formula>
        <label>NPS Response</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Score_Numeric__c</fullName>
        <externalId>false</externalId>
        <formula>IF( ISPICKVAL( Score__c , &quot;Green&quot;) , 4, 


IF(ISPICKVAL( Score__c , &quot;Light Green&quot;), 3, 


IF(ISPICKVAL( Score__c , &quot;Amber&quot;), 2, 


IF(ISPICKVAL( Score__c , &quot;Red&quot;), 1, 0) ) ) )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Score Numeric</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Score__c</fullName>
        <description>The NPS score that the customer has submitted.</description>
        <externalId>false</externalId>
        <inlineHelpText>The NPS score that the customer has submitted.</inlineHelpText>
        <label>Score</label>
        <picklist>
            <picklistValues>
                <fullName>Red</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Amber</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Light Green</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Green</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Submit__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Submit</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Time_Trigger_15__c</fullName>
        <externalId>false</externalId>
        <formula>NOW()  - 0.038</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Time Trigger 15</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>DateTime</type>
    </fields>
    <fields>
        <fullName>nps_score__c</fullName>
        <externalId>false</externalId>
        <formula>IF(ISPICKVAL(  Score__c ,&apos;green&apos;),1,
IF(ISPICKVAL(  Score__c ,&apos;light green&apos;), 0, 
IF(ISPICKVAL(  Score__c ,&apos;amber&apos;), -1,
IF(ISPICKVAL(  Score__c ,&apos;red&apos;), -1, 0
))))</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>NPS Score</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>NPS</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>External_Id__c</columns>
        <columns>OBJECT_ID</columns>
        <columns>Case__c</columns>
        <columns>Score__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>Red_Amber</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>Contact__c</columns>
        <columns>Case__c</columns>
        <columns>Score__c</columns>
        <columns>Score_Numeric__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Score__c</field>
            <operation>equals</operation>
            <value>Red,Amber</value>
        </filters>
        <label>Red / Amber</label>
    </listViews>
    <listViews>
        <fullName>Today_NPS</fullName>
        <columns>NAME</columns>
        <columns>Account__c</columns>
        <columns>Contact__c</columns>
        <columns>Case__c</columns>
        <columns>nps_score__c</columns>
        <columns>Score__c</columns>
        <columns>Score_Numeric__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>CREATED_DATE</field>
            <operation>equals</operation>
            <value>TODAY</value>
        </filters>
        <label>Today NPS</label>
    </listViews>
    <nameField>
        <displayFormat>NPS-{0000}</displayFormat>
        <label>NPS Name</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>NPS</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Account__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Case__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Contact__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Category__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Comments__c</customTabListAdditionalFields>
        <excludedStandardButtons>New</excludedStandardButtons>
        <excludedStandardButtons>ChangeOwner</excludedStandardButtons>
        <excludedStandardButtons>Accept</excludedStandardButtons>
        <lookupDialogsAdditionalFields>Contact__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Score__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Category__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Comments__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Contact__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Score__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Category__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Comments__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Contact__c</searchFilterFields>
        <searchFilterFields>Score__c</searchFilterFields>
        <searchFilterFields>Category__c</searchFilterFields>
        <searchFilterFields>Case__c</searchFilterFields>
        <searchResultsAdditionalFields>Account__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Case__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Contact__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Category__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Comments__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
