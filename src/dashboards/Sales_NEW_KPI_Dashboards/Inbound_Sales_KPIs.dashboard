<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardFilters>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Robert Morgan</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Chuks Nwachuku</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Zane Faleye</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Kivanc Sezen</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>David Boggis</values>
        </dashboardFilterOptions>
        <dashboardFilterOptions>
            <operator>equals</operator>
            <values>Sam Murphy</values>
        </dashboardFilterOptions>
        <name>Account Owner</name>
    </dashboardFilters>
    <dashboardType>SpecifiedUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>Current Month Revenue for Accounts Opened/Reactivated in Last 6 Mths</footer>
            <gaugeMax>70000.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>Monthly Revenue Target</header>
            <indicatorBreakpoint1>10000.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>55000.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Sales_NEW_KPI_Dashboard/Inbound_Monthly_Revenue_Target</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Target: 55K - 70K</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartSummary>
                <aggregate>Sum</aggregate>
                <axisBinding>y</axisBinding>
                <column>Account.First_Time_Use_This_Month__c</column>
            </chartSummary>
            <componentType>Gauge</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <footer>Accounts Opened/Reactivated in Last 6 Months with First Time Use This Month</footer>
            <gaugeMax>100.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>First Time Use This Month</header>
            <indicatorBreakpoint1>25.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>75.0</indicatorBreakpoint2>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Sales_NEW_KPI_Dashboard/Inbound_First_Time_Use</report>
            <showPercentage>false</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
            <title>Target: 100</title>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>ColumnStacked100</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <groupingColumn>Account.Date_changed_to_Current__c</groupingColumn>
            <groupingColumn>Account.Date_of_Last_Booking__c</groupingColumn>
            <header>Account Retention From Creation</header>
            <legendPosition>Right</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Inbound_Sales_Account_Retention</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>BarStacked100</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>true</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Accounts Opened/Reactivated This Month by Payment Type</footer>
            <groupingColumn>USERS.NAME</groupingColumn>
            <groupingColumn>Account.SHM_invoiceClearanceType__c</groupingColumn>
            <header>Direct Debit Uptake</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Inbound_Direct_Debit_Uptake2</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowValueAscending</sortBy>
            <title>Target: 70%</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Accounts Opened/Reactivated in the Last 6 months – not used</footer>
            <groupingColumn>USERS.NAME</groupingColumn>
            <header>Accounts with No Use</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Inbound_No_Use_Last_6_Months</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <dashboardFilterColumns>
                <column>ACCOWNER</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>Open Activities - Current &amp; Previous Month</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>Sales_NEW_KPI_Dashboard/Inbound_Open_Activities</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Manual</chartAxisRange>
            <chartAxisRangeMax>50.0</chartAxisRangeMax>
            <chartAxisRangeMin>0.0</chartAxisRangeMin>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>FORMULA1</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Integer</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>% of Accounts Opened/Reactivated 6 mths ago that have used This Mth</footer>
            <groupingColumn>USERS.NAME</groupingColumn>
            <header>Retention Rate</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Inbound_Retention_Rate</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <title>Target: 50%</title>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>false</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <chartSummary>
                <axisBinding>y</axisBinding>
                <column>RowCount</column>
            </chartSummary>
            <componentType>Column</componentType>
            <dashboardFilterColumns>
                <column>USERS.NAME</column>
            </dashboardFilterColumns>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <footer>Accounts Opened/Reactivated this month – not used this month</footer>
            <groupingColumn>USERS.NAME</groupingColumn>
            <header>Accounts with No Use This Month</header>
            <legendPosition>Bottom</legendPosition>
            <report>Sales_NEW_KPI_Dashboard/Inbound_No_Use_This_Month</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>julia.doctoroff@addisonlee.co.uk</runningUser>
    <textColor>#000000</textColor>
    <title>Inbound Sales KPIs</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
