<?xml version="1.0" encoding="UTF-8"?>
<Dashboard xmlns="http://soap.sforce.com/2006/04/metadata">
    <backgroundEndColor>#FFFFFF</backgroundEndColor>
    <backgroundFadeDirection>Diagonal</backgroundFadeDirection>
    <backgroundStartColor>#FFFFFF</backgroundStartColor>
    <dashboardType>MyTeamUser</dashboardType>
    <isGridLayout>false</isGridLayout>
    <leftSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>WestOne Case Origins - This Week</header>
            <legendPosition>Bottom</legendPosition>
            <report>WestOne_Client_Services/WestOne_Case_Origins</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>WestOne Case Resolutions - This Week</header>
            <legendPosition>Bottom</legendPosition>
            <report>WestOne_Client_Services/WestOne_Case_Resolutions</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Table</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <header>My Average Case Age - Last 30 Days</header>
            <indicatorHighColor>#54C254</indicatorHighColor>
            <indicatorLowColor>#C25454</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>WestOne_Client_Services/WestOne_Cases_by_Owner_and_Age</report>
            <showPicturesOnTables>true</showPicturesOnTables>
            <sortBy>RowValueDescending</sortBy>
        </components>
    </leftSection>
    <middleSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <componentType>Gauge</componentType>
            <displayUnits>Auto</displayUnits>
            <gaugeMax>75.0</gaugeMax>
            <gaugeMin>0.0</gaugeMin>
            <header>My Case Violations - This Week</header>
            <indicatorBreakpoint1>25.0</indicatorBreakpoint1>
            <indicatorBreakpoint2>50.0</indicatorBreakpoint2>
            <indicatorHighColor>#C25454</indicatorHighColor>
            <indicatorLowColor>#54C254</indicatorLowColor>
            <indicatorMiddleColor>#C2C254</indicatorMiddleColor>
            <report>WestOne_Client_Services/WestOne_Case_Milestone_Violations</report>
            <showPercentage>true</showPercentage>
            <showTotal>true</showTotal>
            <showValues>false</showValues>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Bar</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>My Activities Logged - This Week</header>
            <legendPosition>Bottom</legendPosition>
            <report>WestOne_Client_Services/WestOne_Number_of_Activities_for_Cases</report>
            <showPercentage>false</showPercentage>
            <showPicturesOnCharts>true</showPicturesOnCharts>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Pie</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>true</expandOthers>
            <header>My Case Types - This Week</header>
            <legendPosition>Bottom</legendPosition>
            <report>WestOne_Client_Services/WestOne_Case_Types</report>
            <showPercentage>true</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowValueDescending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
    </middleSection>
    <rightSection>
        <columnSize>Wide</columnSize>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Funnel</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>My Case Pipeline - This Week</header>
            <legendPosition>Bottom</legendPosition>
            <report>WestOne_Client_Services/WestOne_Case_Pipeline</report>
            <showPercentage>false</showPercentage>
            <showValues>true</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
        <components>
            <autoselectColumnsFromReport>true</autoselectColumnsFromReport>
            <chartAxisRange>Auto</chartAxisRange>
            <componentType>Line</componentType>
            <displayUnits>Auto</displayUnits>
            <drillEnabled>false</drillEnabled>
            <drillToDetailEnabled>false</drillToDetailEnabled>
            <enableHover>false</enableHover>
            <expandOthers>false</expandOthers>
            <header>WestOne Cases Raised - This Week</header>
            <legendPosition>Bottom</legendPosition>
            <report>WestOne_Client_Services/WestOne_Cases_by_Date_Time_Opened</report>
            <showPercentage>false</showPercentage>
            <showValues>false</showValues>
            <sortBy>RowLabelAscending</sortBy>
            <useReportChart>false</useReportChart>
        </components>
    </rightSection>
    <runningUser>erin.elliott@addisonlee.com</runningUser>
    <textColor>#000000</textColor>
    <title>WestOne Client Services Dashboard</title>
    <titleColor>#000000</titleColor>
    <titleSize>12</titleSize>
</Dashboard>
