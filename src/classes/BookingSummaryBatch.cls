/* 
  Booking Summary Batch to Update Booker Name and
  Update contact booked is to true. 
  GVK
*/
global class BookingSummaryBatch implements Database.Batchable<sObject>{
 global datetime startdatetime;
   global datetime enddatetime;
   Date todayDate = Date.today() ;
    public integer year=system.today().year();
     public integer month=system.today().month();
      public integer day=system.today().day();
        public BookingSummaryBatch(){
        startdatetime=datetime.newInstance(year, month, day, 0, 0, 0);
        enddatetime=system.now();           
         }
        global Database.QueryLocator start(Database.BatchableContext BC) {
        //String query = 'SELECT Id,Account__c,Individual_Id__c FROM Booking_Summary__c where Individual_Id__c != null and createddate>=:startdatetime and createddate<=:enddatetime';
        String query = 'SELECT Id,Account__c,Individual_Id__c FROM Booking_Summary__c where Individual_Id__c != null and Booker__c=null and createddate>=:todayDate';
        return Database.getQueryLocator(query);
        } 
        global void execute(Database.BatchableContext BC, List<sobject> scope){
        list<Booking_Summary__c> listtoprocess = new list<Booking_Summary__c>();
        for(sobject b:scope){
        Booking_Summary__c c=(Booking_Summary__c)b;
        listtoprocess.add(c);
        }
        system.debug('@@@listtoprocess '+listtoprocess );
        Addlee_BookerHandler b = new Addlee_BookerHandler();
        b.bookerhandler(listtoprocess);               
        }   
      global void finish(Database.BatchableContext BC)
     {
    }
   }