/*
Calculate MonthlySpend On Person Accounts
*/

global class MonthlySpendForPersonAccounts_Schedular implements Schedulable {
  global void execute (SchedulableContext sc) {
    List<String> recordtypeNames=new List<String>{'Personal Account','Read Only Personal Account','Consumer'};
    CalculateMonthlyBookingAverageOnAccount cms = new CalculateMonthlyBookingAverageOnAccount();
     cms.Query = 'select id,Date_of_Last_Booking__c,Consumer_Account_Number__c,Name,Grading__c,Account_Status__c,Date_changed_to_Current__c,IsPersonAccount,Marketing_Status__c,Sales_Ledger__c,Account_Managed__c,Total_Amount_of_Bookings__c from Account where (RecordType.Name = ' + '\'' + 'Consumer' + '\'' + ' OR RecordType.Name =' + '\'' + 'Read Only Personal Account' + '\'' + 'OR RecordType.Name =' + '\'' + 'Personal Account' + '\'' + ') and Date_of_Last_Booking__c = LAST_N_MONTHS:1 and Total_Amount_of_Bookings__c <= 49999';
     Database.executeBatch(cms,20);
    
  }
  

}