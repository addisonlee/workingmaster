@isTest
global class AddleeRefundsGetJobsMock implements WebserviceMock{
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.jobsResponse respElement =  new Addlee_refunds.jobsResponse();
             respElement.jobs = New list<Addlee_refunds.Job>();
             Addlee_refunds.Job testJob = new Addlee_refunds.Job();
               testJob.accountNumber = 789;
              // testJob.description ='test';
               testJob.docket = true;
               testJob.errorCode = '0';
               testJob.id = 65656757;
               testJob.dropStop = 'test';
               testJob.invoiceNumber = 54654;
               testJob.jobDate = system.today();
               testJob.pickupStop = 'test';
               testJob.serviceCode = '7687676';
               testJob.uuid='7676767';
               
               
             respElement.jobs.add(testJob);  
             respElement.errorCode = 0;
            // respElement.description = 'Nothing';  
         
       response.put('response_x', respElement); 
   }
}