@istest
public class Addlee_BookerHandlerTest {
    public static testmethod void TestBookerHandler() {
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account Acc = new account(name = 'Test acc', accountnumber ='123');
        insert acc;
        contact con = new contact(lastname = 'Tst con', accountid = acc.id,Booker_Id__c = 'TSTCON123099');
        insert con;
        contact con1 = new contact(lastname = 'Tst con1', accountid = acc.id,Booker_Id__c = 'TSTCON123100');
        insert con1;
        list<booking_Summary__c> BookingList = new list<booking_Summary__c>();
        booking_Summary__c b = new booking_Summary__c(name ='tst booking', Account__c = acc.id,Individual_Id__c = 'TSTCON123099');
        insert b;
        booking_Summary__c b1 = new booking_Summary__c(name ='tst booking1', Account__c = acc.id,Individual_Id__c = 'TSTCON123100');
        insert b1;
        system.debug(acc.id + 'accid' +  con.accountid + 'con accid==' + con.Booker_Id__c + 'book' + b.account__c);
        bookingList.add(b);
        bookingList.add(b1);
        Addlee_BookerHandler booker = new Addlee_BookerHandler();
        booker.bookerhandler(bookingList);
    }
}