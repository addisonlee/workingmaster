public with sharing class Addlee_Crypto {
    

    public static blob getCryptoKey(){
        Blob cryptoKey;
        Map<String, PrivateKey__c> privateKeyMap = PrivateKey__c.getAll();
        String privateKey = privateKeyMap.get('Key').CurrentKey__c;
        System.DEBUG('Private Key : ' + privateKey);
        cryptoKey = blob.valueOf(privateKey);
        return cryptoKey;
    }

    /*public Addlee_Crypto() {
        // Generate an AES key for the purpose of this sample. 
        // Normally this key should be stored in a protected custom setting 
        // or an encrypted field on a custom object

        Map<String, PrivateKey__c> privateKeyMap = PrivateKey__c.getAll();
        String privateKey = privateKeyMap.get('Key').CurrentKey__c;
        System.DEBUG('Private Key : ' + privateKey);

        Blob cryptoKey = blob.valueOf(privateKey);

        encrypt('Test data to encrypted',cryptoKey);
        
    }*/
    
    public static String encrypt(String stringToEncrypt,Blob cryptoKey){
        String encodedCipherText;
        // Generate the data to be encrypted.
        if(stringToEncrypt != null && stringToEncrypt.length() > 0){
            Blob data = Blob.valueOf(stringToEncrypt);
            System.debug('cryptoKey : ' + cryptoKey);
            System.debug('data : ' + data);
            // Encrypt the data and have Salesforce.com generate the initialization vector
            Blob encryptedData = Crypto.encryptWithManagedIV('AES128', cryptoKey, data);
            encodedCipherText = EncodingUtil.base64Encode(encryptedData); 
            
        }
        return encodedCipherText;
        /*System.debug(encryptedData);
        System.DEBUG('String encrypted : ' + encodedCipherText);
        Blob encodedEncryptedBlob = EncodingUtil.base64Decode(encodedCipherText);

        // Decrypt thencryptedDatae; data - the first 16 bytes contain the initialization vector
        Blob decryptedData = Crypto.decryptWithManagedIV('AES128', cryptoKey, encodedEncryptedBlob);

        // Decode the decrypted data for subsequent use
        String decryptedDataString = decryptedData.toString();
        System.DEBUG(decryptedDataString);*/
    }

    public static String decrypt(String stringToDecrypt,Blob cryptoKey){
        String decryptedDataString;
        if(stringToDecrypt != null && stringToDecrypt.length() > 0){
            System.debug(stringToDecrypt);
            System.DEBUG('String encrypted : ' + stringToDecrypt);
            Blob encodedEncryptedBlob = EncodingUtil.base64Decode(stringToDecrypt);

            // Decrypt thencryptedDatae; data - the first 16 bytes contain the initialization vector
            Blob decryptedData = Crypto.decryptWithManagedIV('AES128', cryptoKey, encodedEncryptedBlob);

            // Decode the decrypted data for subsequent use
            decryptedDataString = decryptedData.toString();
            System.DEBUG(decryptedDataString);
        }
        return decryptedDataString;
    }

    public static void encryptBankFields(List<Lead> newLeads, Blob cryptoKey){

        for(Lead eachLead : newLeads){
            if( eachLead.Payment_Type_Web__c == 'Direct Debit' || eachLead.Payment_Type__c == 'Direct Debit' ){
                if((eachLead.Bank_Account_Number__c != null || eachLead.Bank_Sort_Code__c != null) 
                    && eachLead.TECH_Bank_Encryption_Set__c != true ){
                    eachLead.Bank_Account_Number__c = Addlee_Crypto.encrypt(eachLead.Bank_Account_Number__c, cryptoKey);
                    eachLead.Bank_Sort_Code__c = Addlee_Crypto.encrypt(eachLead.Bank_Sort_Code__c,cryptoKey); 
                    eachLead.TECH_Bank_Encryption_Set__c = true;
                }
            } else if ( eachLead.Payment_Type__c == 'Single Credit Card' 
                        && eachLead.Card_Long_Number__c != null
                        && eachLead.TECH_Bank_Encryption_Set__c != true) {
                    eachLead.Card_Long_Number__c = Addlee_Crypto.encrypt(eachLead.Card_Long_Number__c, cryptoKey);
                    eachLead.TECH_Bank_Encryption_Set__c = true;
            } else {
                //do nothing
            }
        }
    }
}