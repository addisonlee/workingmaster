/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AddLee_BookingSummaryHelper_Test {

    @isTest
    static void BookingSummaryTestFlow() {
        Integer expectedValue = 20;
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account acc = AddLee_Trigger_Test_Utils.createAccounts(1).get(0);
        Test.startTest();
            insert acc;
            List<Booking_Summary__c> testBookingSummary = AddLee_Trigger_Test_Utils.createBookingSummaryTest(acc.Id, expectedValue);
            insert testBookingSummary;
        Test.stopTest();
        
        Integer actualValue = [SELECT count() FROM Booking_Summary__c WHERE account__c = :acc.Id];
        
        //System.AssertEquals(expectedValue,actualValue);
        
    }

    @isTest
    static void BookingSummaryAccountOneTestFlow() {
        Integer expectedValue = 20;
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account acc = AddLee_Trigger_Test_Utils.createAccountOne(1).get(0);
        Test.startTest();
            insert acc;
            List<Booking_Summary__c> testBookingSummary = AddLee_Trigger_Test_Utils.createBookingSummaryTest(acc.Id, expectedValue);
            //insert testBookingSummary;
        Test.stopTest();
        
        Integer actualValue = [SELECT count() FROM Booking_Summary__c WHERE account__c = :acc.Id];
        
        //System.AssertEquals(expectedValue,actualValue);
        
    }

}