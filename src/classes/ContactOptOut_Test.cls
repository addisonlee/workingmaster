/* test class for Contactoptout class
   Date : 12/08/2015
*/
@istest
public with sharing class ContactOptOut_Test {
	static testMethod void contactoptOutFirstTest() {
    	Addlee_Trigger_Test_Utils.insertCustomSettings();
      	Contact optoutEmail =Addlee_Trigger_Test_Utils.optOutContacts(1) [0];
    	insert  optoutEmail;
    	system.assert(optoutEmail.Surveys_And_Research_Email__c == false);
    	Contact optoutSMS =Addlee_Trigger_Test_Utils.optOutContacts2(1) [0];
    	insert  optoutSMS;
    	system.assert(optoutSMS.News_Letters_And_Updates_SMS__c == false);
    	Contact con = Addlee_Trigger_Test_Utils.createContacts(1)[0];
    	insert con;
      	
    }

}