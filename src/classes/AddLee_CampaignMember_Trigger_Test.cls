@isTest
private class AddLee_CampaignMember_Trigger_Test{
	@isTest 
	static void CampaignMember_IsCampaignMemberUpdated() {
		
		Addlee_Trigger_Test_Utils.insertCustomSettings();
		Account testAccount = Addlee_Trigger_Test_Utils.createAccounts(1)[0];
		insert testAccount;
		
		Contact testContact = new Contact(LastName = '2 Weeks Test', accountId = testAccount.Id, Contact_Type__c = 'Main Contact', Email = 'test@test.com');
		insert testContact;

		Campaign testCampaign = new Campaign(Name = '1A_2 Weeks Send App Promotional Email', isActive = true);
		insert testCampaign;

		CampaignMemberStatus testCampaignMemberStatus = new CampaignMemberStatus(campaignId = testCampaign.Id, label = 'Email sent', sortOrder = 3);
		insert testCampaignMemberStatus;

		CampaignMember testCampaignMember = new CampaignMember(contactId = testContact.ID, campaignId = testCampaign.Id);
		insert testCampaignMember;
		testCampaignMember.Status = 'Email sent';
		
		Test.StartTest();
			upsert testCampaignMember;
		Test.StopTest();

		//System.assertEquals('Email sent', [Select Status FROM CampaignMember WHERE Id =: testCampaignMember.Id].status);//ensure that campaignMember status is updated
		//System.assertEquals('1A_2 Weeks Send App Promotional Email', [SELECT Subject FROM Task WHERE whatId =: testAccount.Id].Subject);//ensure task is created
	}
}