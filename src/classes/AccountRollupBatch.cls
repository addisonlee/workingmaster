//*********************************************************************************** 
//
//  AccountRollupBatch
//
//  Desciption:
//  This class was created by InSitu Software for AddisonLee. The purpose of the class is to provide a schedulable
//  Apex Job routine to provide 'Rollup' calculations of values on the Account object, including roll-ups from related Cases
//  and Opportunities. The routine will summarize the values for the Ultimate Parent Account and all the child accounts 
//  within the account family and put the total value in the designated field for each parent account in the hierarchy.
//	FOR EXCLUSIVE USE OF ADDISON LEE ONLY
//
//  History:
//  InSitu  02/16/20165  Original version.
// ***********************************************************************************

global with sharing class AccountRollupBatch implements Database.Batchable<sObject>
{
    public class RollupException extends Exception {}

    ///////////////////////////////////////////////////
    //  Constants
    ///////////////////////////////////////////////////

	public static final Integer RU_BATCHSIZE = 20;
    
	public static final String RUTYPE_RELATEDOBJS = 'RelatedRollup';
    public static final String RUTYPE_UP = 'UPRollup';
    public static final String RUTYPE_NOTINHIER = 'NotInHier';
    
    public static final ID RECTYPEID_COMPLAINTS = '012b0000000DhDc';
    
    ///////////////////////////////////////////////////
    //  Member variables
    ///////////////////////////////////////////////////
   
    global integer m_iLimit;        // Need this to limit the amount of processing for unit test purposes.
    global String  m_strType;       // NotInHier or UPRollup
    
    private Map<ID, AccountTotals> 	m_mapIDtoAcctTotals = new Map<ID, AccountTotals>();
   	private Map<ID, List<Account>> 	m_mapParentIDToAccounts = new Map<ID, List<Account>>();

    // Constructor
    global AccountRollupBatch (string strType, integer iLimit)
    {
        m_strType = strType;
        m_iLimit = iLimit;      
    }

    //----------------------------------------------------------------------- 
    //  runAccountRollupBatch
    //
    //  This method is provided in order to initiate the batch job from 
    //  any Apex code.
    //
    //  iLimit - determines the number of records to be processed.
    //  iBatchSize - determines the number of records in each batch size. Minimum is 1, Maximum is 200, Default is 10.
    //----------------------------------------------------------------------- 

    global static ID runAccountRollupBatch(integer iLimit, integer iBatchSize, String strType)
    {
        Id idJob = null;
        
        // Make sure the batch size is greater than 0, but less than 201.
        if (iBatchSize == null || iBatchSize < 1)
            iBatchSize = 10;
        else if (iBatchSize > 200)
            iBatchSize = 200;
            
        // Check governor limits first - Determine if there are already more than 5 jobs in the queue before adding another one.
        Set<String> setStatus = new Set<String>{'Queued', 'Processing', 'Preparing'};
        Set<String> setJobType = new Set<String>{'BatchApex'};
        integer iCnt = [SELECT count() FROM AsyncApexJob WHERE Status in :setStatus AND JobType in :setJobType];
        if (iCnt < 5)
        {
            // The calculation is going to be done in batch. Create an instance of the Apex Batch class and
            // initiate the batch processing.
            AccountRollupBatch batchARCalc = new AccountRollupBatch(strType, iLimit);
            idJob = database.executeBatch(batchARCalc, iBatchSize);
            system.debug(Logginglevel.WARN, 'InSitu Debug: Batch job successfully queued.');
        }
        else
        {
            system.debug(Logginglevel.WARN, 'InSitu Debug: Cannot queue batch job since there are already 5 in the queue.');
        }
            
        return idJob;
    }

    global Database.QueryLocator start(Database.BatchableContext BC)
    {
       // Query String that drives the batch process - Find all Top Level accounts.
        String sQuery;
        
		if (m_strType == RUTYPE_RELATEDOBJS)
		{
			// Query all related Cases and Bookings for any account that is in a hierarchy
            sQuery = 'SELECT id, Name, InSituCah__Ultimate_Parent__r.ID,' +
                 	 ' Total_Open_Cases__c, Total_Complaints__c, Total_Open_Complaints__c,' +
                 	 ' Total_Bookings_Amount_CM__c, Total_Bookings_Amount_CY__c' +
                     ' FROM Account WHERE InSituCah__Ultimate_Parent__c != null';
		}
		else if (m_strType == RUTYPE_UP)
        {
            // Query all ultimate parent records
            sQuery = 'SELECT id, Name, InSituCah__Ultimate_Parent__r.ID, ' +
							'Total_Spend_YTD__c, Parent_Total_Spend_YTD__c, ' +
							'Total_Spend_MTD__c, Parent_Total_Spend_MTD__c, ' +
							'Total_Booking_Spend__c, Parent_Total_Booking_Spend__c, ' +
							'Total_Amount_of_Bookings__c, Parent_Total_Num_Bookings_for_Account__c, ' +
							'Total_Bookings_Amount_CY__c, Parent_Total_Bookings_Amount_CY__c, ' +
							'Total_Bookings_Amount_CM__c, Parent_Total_Bookings_Amount_CM__c, ' +
							'Date_of_Last_Booking__c, Parent_Date_of_Last_Booking__c, ' +
							
							'Total_Expected_Revenue__c, Parent_Total_Expected_Revenue__c, ' +
							'Total_Expected_Revenue_Open_Opps__c, Parent_Total_Expected_Revenue_Open_Opps__c, ' +
							'Total_Expected_Revenue_Closed_Opps__c, Parent_Total_Expected_Rev_Closed_Opp__c, ' +
							'Max_Opp_Close_Date__c, Parent_Max_Opp_Close_Date__c, ' +
							'Min_Opp_Close_Date__c, Parent_Min_Opp_Close_Date__c, ' +
							
							'Total_Complaints__c, Parent_Total_Complaints__c, ' +
							'Total_Open_Complaints__c, Parent_Total_Open_Complaints__c, ' +
							'Total_Open_Cases__c, Parent_Total_Open_Cases__c' +

                     ' FROM Account WHERE InSituCah__Ultimate_Parent__c != null AND ParentID = null';
        }
        else if (m_strType == RUTYPE_NOTINHIER)
        {
            // Query for any account that is not in a hierarchy and has a value in any of the Parent Total fields.          
            sQuery = 'SELECT id, Name, InSituCah__Ultimate_Parent__r.ID, ' +
							'Parent_Total_Spend_YTD__c, ' +
							'Parent_Total_Spend_MTD__c, ' +
							'Parent_Total_Booking_Spend__c, ' +
							'Parent_Total_Num_Bookings_for_Account__c, ' +
							'Parent_Total_Bookings_Amount_CY__c, ' +
							'Parent_Total_Bookings_Amount_CM__c, ' +
							'Parent_Date_of_Last_Booking__c, ' +
							
							'Parent_Total_Expected_Revenue__c, ' +
							'Parent_Total_Expected_Revenue_Open_Opps__c, ' +
							'Parent_Total_Expected_Rev_Closed_Opp__c, ' +
							'Parent_Max_Opp_Close_Date__c, ' +
							'Parent_Min_Opp_Close_Date__c, ' +
							
							'Parent_Total_Complaints__c, ' +
							'Parent_Total_Open_Complaints__c, ' +
							'Parent_Total_Open_Cases__c' +
                     ' FROM Account WHERE InSituCah__Ultimate_Parent__c = null AND IsPersonAccount = false AND' +  
	         			 ' (Parent_Total_Spend_YTD__c <> null OR Parent_Total_Spend_MTD__c <> null OR Parent_Total_Booking_Spend__c <> null OR' +
	         			 ' Parent_Total_Num_Bookings_for_Account__c <> null OR Parent_Total_Bookings_Amount_CY__c <> null OR Parent_Total_Bookings_Amount_CM__c <> null OR' +
	         			 ' Parent_Date_of_Last_Booking__c <> null OR Parent_Total_Expected_Revenue__c <> null OR Parent_Total_Expected_Revenue_Open_Opps__c <> null OR' +
	         			 ' Parent_Total_Expected_Rev_Closed_Opp__c <> null OR Parent_Max_Opp_Close_Date__c <> null OR Parent_Min_Opp_Close_Date__c <> null OR' + 
	         			 ' Parent_Total_Complaints__c <> null OR Parent_Total_Open_Complaints__c <> null OR Parent_Total_Open_Cases__c <> null)';
        }
            
        if (m_iLimit > 0)
            sQuery += ' LIMIT ' + m_iLimit;
        
        return Database.getQueryLocator(sQuery );
        
    }

    global void execute(Database.BatchableContext BC, sObject[] listObjects)
    {
		if (m_strType == RUTYPE_RELATEDOBJS)
			doRelatedRollup(listObjects);
        else if (m_strType == RUTYPE_UP)
            doUPParentRollup(listObjects);
        else if (m_strType == RUTYPE_NOTINHIER)
            doNotInHier(listObjects);
    }
    
    private void doRelatedRollup(sObject[] listObjects)
    {
        system.debug(Logginglevel.WARN, 'InSitu Debug: In doRelatedRollup() - listObjects.size() = ' + listObjects.size());
        
        // Build set of Account Ids for records that will be aggregated.
        List<Account> listAccts = (List<Account>)listObjects;
        Set<ID> setAcctIds = new Set<ID>();
        for (Account acct : listAccts)
        {
            setAcctIds.add(acct.ID);
        }
        
		// Use separate aggregate queries to do all the totaling.
		
		// ------------------------------------------------------------------
		// Total_Amount_of_Bookings__c (SUM) – this CY (dynamic criteria)
		// ------------------------------------------------------------------

        // Let the database aggregate the records and provide the total.
        AggregateResult[] groupedResults = 
            [SELECT Account__c idAcct,
                    SUM(Total_Price__c) Total_Amount
             FROM Booking_Summary__c
             WHERE Account__c in :setAcctIds AND
                   Date_Time__c = THIS_YEAR
             GROUP BY Account__c ];
             
        // Put the results in a map for lookup purposes.
        Map<ID, AggregateResult> mapId2ARBookingCY = new Map<ID,AggregateResult>();
        for (AggregateResult ar : groupedResults)  
        {
            mapId2ARBookingCY.put((ID)ar.get('idAcct'), ar);
        }

		// ------------------------------------------------------------------
		// Total_Amount_of_Bookings__c (SUM) – this CY (dynamic criteria)
		// ------------------------------------------------------------------

        groupedResults = 
            [SELECT Account__c idAcct,
                    SUM(Total_Price__c) Total_Amount
             FROM Booking_Summary__c
             WHERE Account__c in :setAcctIds AND
                   Date_Time__c = THIS_MONTH
             GROUP BY Account__c ];
             
        // Put the results in a map for lookup purposes.
        Map<ID, AggregateResult> mapId2ARBookingCM = new Map<ID,AggregateResult>();
        for (AggregateResult ar : groupedResults)  
        {
            mapId2ARBookingCM.put((ID)ar.get('idAcct'), ar);
        }
        
		// ------------------------------------------------------------------
		// Complaint__c (COUNT)
		// ------------------------------------------------------------------

        groupedResults = 
            [SELECT AccountID idAcct,
                    COUNT(ID) Total_Count
             FROM Case
             WHERE AccountID in :setAcctIds AND
                   RecordTypeID =: RECTYPEID_COMPLAINTS
             GROUP BY AccountID ];
             
        // Put the results in a map for lookup purposes.
        Map<ID, AggregateResult> mapId2ARBookingComplaintCnt = new Map<ID,AggregateResult>();
        for (AggregateResult ar : groupedResults)  
        {
            mapId2ARBookingComplaintCnt.put((ID)ar.get('idAcct'), ar);
        }
        
 		// ------------------------------------------------------------------
		// Complaint__c (COUNT) – Closed=FALSE
		// ------------------------------------------------------------------

        groupedResults = 
            [SELECT AccountID idAcct,
                    COUNT(ID) Total_Count
             FROM Case
             WHERE AccountID in :setAcctIds AND
                   RecordTypeID =: RECTYPEID_COMPLAINTS AND
                   IsClosed = false
             GROUP BY AccountID ];
             
        // Put the results in a map for lookup purposes.
        Map<ID, AggregateResult> mapId2ARBookingOpenComplaintCnt = new Map<ID,AggregateResult>();
        for (AggregateResult ar : groupedResults)  
        {
            mapId2ARBookingOpenComplaintCnt.put((ID)ar.get('idAcct'), ar);
        }
        
  		// ------------------------------------------------------------------
		// ALL Cases (COUNT) – closed = FALSE
		// ------------------------------------------------------------------

        groupedResults = 
            [SELECT AccountID idAcct,
                    COUNT(ID) Total_Count
             FROM Case
             WHERE AccountID in :setAcctIds AND
             	   IsClosed = false
             GROUP BY AccountID ];
             
        // Put the results in a map for lookup purposes.
        Map<ID, AggregateResult> mapId2ARBookingAllCasesCnt = new Map<ID,AggregateResult>();
        for (AggregateResult ar : groupedResults)  
        {
            mapId2ARBookingAllCasesCnt.put((ID)ar.get('idAcct'), ar);
        }
        
        // Now iterate the list of Accounts and update the associated Rollup field values.
        List<Account> listAcctsToUpdate = new List<Account>();
        for (Account acct : listAccts)
        {
            Boolean bUpdate = false;
            
            AggregateResult ar = mapId2ARBookingCY.get(acct.ID);
            Decimal dTotalAmount  = (ar != null) ? (decimal)ar.get('Total_Amount') : 0;
            if (acct.Total_Bookings_Amount_CY__c != dTotalAmount)
            {
                acct.Total_Bookings_Amount_CY__c = dTotalAmount;
                bUpdate = true;
            }
            
            ar = mapId2ARBookingCM.get(acct.ID);
            dTotalAmount  = (ar != null) ? (decimal)ar.get('Total_Amount') : 0;
            if (acct.Total_Bookings_Amount_CM__c != dTotalAmount)
            {
                acct.Total_Bookings_Amount_CM__c = dTotalAmount;
                bUpdate = true;
            }
            
            ar = mapId2ARBookingComplaintCnt.get(acct.ID);
            Integer iCnt  = (ar != null) ? (integer)ar.get('Total_Count') : 0;
            if (acct.Total_Complaints__c != iCnt)
            {
                acct.Total_Complaints__c = iCnt;
                bUpdate = true;
            }

            ar = mapId2ARBookingOpenComplaintCnt.get(acct.ID);
            iCnt  = (ar != null) ? (integer)ar.get('Total_Count') : 0;
            if (acct.Total_Open_Complaints__c != iCnt)
            {
                acct.Total_Open_Complaints__c = iCnt;
                bUpdate = true;
            }

            ar = mapId2ARBookingAllCasesCnt.get(acct.ID);
            iCnt  = (ar != null) ? (integer)ar.get('Total_Count') : 0;
            if (acct.Total_Open_Cases__c != iCnt)
            {
                acct.Total_Open_Cases__c = iCnt;
                bUpdate = true;
            }
            
            if (bUpdate)
            	listAcctsToUpdate.add(acct);
        }

        system.debug(Logginglevel.WARN, 'InSitu Debug: In doRelatedRollup() - listAcctsToUpdate.size() = ' + listAcctsToUpdate.size());
        
        // Only update accounts whose value has changed.
        if (listAcctsToUpdate.size() > 0)
            update listAcctsToUpdate;
    }


    // -----------------------------------------------------
    //  doUPParentRollup
    // -----------------------------------------------------
    private void doUPParentRollup(Account[] listUPAccounts)
    {
        system.debug(Logginglevel.WARN, 'In doUPParentRollup: listUPAccounts.size() = ' + listUPAccounts.size());
        
        // Put all the UP Account ID info into a map indexed by UPID.
        Map<ID, Account> mapUPIDToUPAccount = new Map<ID, Account>();
        for (Account acct : listUPAccounts)  
        {
        	mapUPIDToUPAccount.put(acct.InSituCah__Ultimate_Parent__c, acct);
        } 
        
		// Get the set of UP IDs.
        Set<ID> setUPIds = mapUPIDToUPAccount.KeySet();
		List<Account> listAccounts;
		
        try
        {
			listAccounts = [SELECT ID, ParentID, Name, InSituCah__Ultimate_Parent__c,
								Total_Spend_YTD__c, Parent_Total_Spend_YTD__c, 
								Total_Spend_MTD__c, Parent_Total_Spend_MTD__c, 
								Total_Booking_Spend__c, Parent_Total_Booking_Spend__c, 
								Total_Amount_of_Bookings__c, Parent_Total_Num_Bookings_for_Account__c, 
								Total_Bookings_Amount_CY__c, Parent_Total_Bookings_Amount_CY__c, 
								Total_Bookings_Amount_CM__c, Parent_Total_Bookings_Amount_CM__c, 
								Date_of_Last_Booking__c, Parent_Date_of_Last_Booking__c, 
								
								Total_Expected_Revenue__c, Parent_Total_Expected_Revenue__c, 
								Total_Expected_Revenue_Open_Opps__c, Parent_Total_Expected_Revenue_Open_Opps__c, 
								Total_Expected_Revenue_Closed_Opps__c, Parent_Total_Expected_Rev_Closed_Opp__c, 
								Max_Opp_Close_Date__c, Parent_Max_Opp_Close_Date__c, 
								Min_Opp_Close_Date__c, Parent_Min_Opp_Close_Date__c, 
								
								Total_Complaints__c, Parent_Total_Complaints__c, 
								Total_Open_Complaints__c, Parent_Total_Open_Complaints__c, 
								Total_Open_Cases__c, Parent_Total_Open_Cases__c
							 FROM Account
							 WHERE InSituCah__Ultimate_Parent__c in :setUPIds
							 ORDER BY InSituCah__Ultimate_Parent__c, ParentID]; 
							 
			system.debug(Logginglevel.WARN, 'In doUPParentRollup: listAccounts.size() = ' + listAccounts.size());
			
			ID idCurrentUP;
			for (Account acct : listAccounts)
			{
                if (idCurrentUP == Null)
                {
                	idCurrentUP = acct.InSituCah__Ultimate_Parent__c;
                }
                else if (idCurrentUP != acct.InSituCah__Ultimate_Parent__c)
                {
					// --------------------------------------------------------
					// Complete the processing for the hierarchy.
					// --------------------------------------------------------

					system.debug(Logginglevel.WARN, 'Complete the processing for the hierarchy. UPID = ' + idCurrentUP);

					// Get the Account for the current UP.
					Account acctUP = mapUPIDToUPAccount.get(idCurrentUP);
					system.debug(Logginglevel.WARN, 'Complete the processing for the hierarchy. UPAccount = ' + acctUP.Name);

					SumAccountTotals(acctUP);
					system.debug(Logginglevel.WARN, 'Processing Completed for hierarchy. UPAccount = ' + acctUP.Name);
					
					// Clear out the map.
					m_mapParentIDToAccounts.clear();
					
					// Start processing the next hierarchy.
					idCurrentUP = acct.InSituCah__Ultimate_Parent__c;                	
                }
                
                // If the Parent id is not in the map, then add it.
                if (!m_mapParentIDToAccounts.containsKey(acct.ParentID))
                {
                    // Inner map contains grandchild node. Add to grandchildren map, indexed by parent's id.
                    List<Account> listChildAccts = new List<Account>();
                    listChildAccts.add(acct);
                    m_mapParentIDToAccounts.put(acct.ParentID, listChildAccts);
                }
                else
                {
                    // Get the ID list. Add the account id to the list.
                    List<Account> listChildAccts = m_mapParentIDToAccounts.get(acct.ParentID);
                    listChildAccts.add(acct);
                }
            }
            
			// --------------------------------------------------------
            // Process the last hierarchy set.
			// --------------------------------------------------------

			system.debug(Logginglevel.WARN, 'Complete the processing for the hierarchy. UPID = ' + idCurrentUP);
			
			// Get the Account for the current UP.
			Account acctUP = mapUPIDToUPAccount.get(idCurrentUP);
			try
			{
				SumAccountTotals(acctUP);
	    	}        
	        catch(Exception e)
	        {   
	            // Display error message. 
	            throw new RollupException('The following error occurred while Summing the Account Totals for the last account in the list: ' + e.getMessage());
	        }
    	}        
        catch(Exception e)
        {   
            // Display error message. 
            throw new RollupException('The following error occurred while Summing the Account Totals in the Account Hierarchy: ' + e.getMessage());
        }
        
        List<Account> listAcctsToUpdate = new List<Account>();
        try
        {
        	// Iterate the list of accounts again and update amount fields as appropriate.
        	for (Account acct : listAccounts)
        	{
        		// Get the totals amount from the map for the account.
				AccountTotals acctTotals = m_mapIDtoAcctTotals.get(acct.ID);
				if (acctTotals == null)
				{
					acctTotals = new AccountTotals();
				}
				
				// See if the values in the total amount fields have changed.
				if (acct.Parent_Total_Spend_YTD__c != acctTotals.Sum_Total_Spend_YT || 
					acct.Parent_Total_Spend_MTD__c != acctTotals.Sum_Total_Spend_MTD || 
					acct.Parent_Total_Booking_Spend__c != acctTotals.Sum_Total_Booking_Spend || 
					acct.Parent_Total_Num_Bookings_for_Account__c != acctTotals.Sum_Total_Num_Bookings_for_Account || 
					acct.Parent_Total_Bookings_Amount_CY__c != acctTotals.Sum_Total_Bookings_Amount_CY || 
					acct.Parent_Total_Bookings_Amount_CM__c != acctTotals.Sum_Total_Bookings_Amount_CM || 
					acct.Parent_Date_of_Last_Booking__c != acctTotals.Date_of_Last_Booking || 
					
					acct.Parent_Total_Expected_Revenue__c != acctTotals.Sum_Total_Expected_Revenue || 
					acct.Parent_Total_Expected_Revenue_Open_Opps__c != acctTotals.Sum_Total_Expected_Revenue_Open_Opps || 
					acct.Parent_Total_Expected_Rev_Closed_Opp__c != acctTotals.Sum_Total_Expected_Rev_Closed_Opp || 
					acct.Parent_Max_Opp_Close_Date__c != acctTotals.Max_Opp_Close_Date || 
					acct.Parent_Min_Opp_Close_Date__c != acctTotals.Min_Opp_Close_Date || 
					
					acct.Parent_Total_Complaints__c != acctTotals.Sum_Total_Complaints || 
					acct.Parent_Total_Open_Complaints__c != acctTotals.Sum_Total_Open_Complaints || 
					acct.Parent_Total_Open_Cases__c != acctTotals.Sum_Total_Open_Cases)
				{
					acct.Parent_Total_Spend_YTD__c = acctTotals.Sum_Total_Spend_YT; 
					acct.Parent_Total_Spend_MTD__c = acctTotals.Sum_Total_Spend_MTD; 
					acct.Parent_Total_Booking_Spend__c = acctTotals.Sum_Total_Booking_Spend; 
					acct.Parent_Total_Num_Bookings_for_Account__c = acctTotals.Sum_Total_Num_Bookings_for_Account; 
					acct.Parent_Total_Bookings_Amount_CY__c = acctTotals.Sum_Total_Bookings_Amount_CY; 
					acct.Parent_Total_Bookings_Amount_CM__c = acctTotals.Sum_Total_Bookings_Amount_CM; 
					acct.Parent_Date_of_Last_Booking__c = acctTotals.Date_of_Last_Booking; 
					
					acct.Parent_Total_Expected_Revenue__c = acctTotals.Sum_Total_Expected_Revenue; 
					acct.Parent_Total_Expected_Revenue_Open_Opps__c = acctTotals.Sum_Total_Expected_Revenue_Open_Opps; 
					acct.Parent_Total_Expected_Rev_Closed_Opp__c = acctTotals.Sum_Total_Expected_Rev_Closed_Opp; 
					acct.Parent_Max_Opp_Close_Date__c = acctTotals.Max_Opp_Close_Date; 
					acct.Parent_Min_Opp_Close_Date__c = acctTotals.Min_Opp_Close_Date; 
					
					acct.Parent_Total_Complaints__c = acctTotals.Sum_Total_Complaints; 
					acct.Parent_Total_Open_Complaints__c = acctTotals.Sum_Total_Open_Complaints; 
					acct.Parent_Total_Open_Cases__c = acctTotals.Sum_Total_Open_Cases;

					listAcctsToUpdate.add(acct); 	
				}	
        	}
 
			system.debug(Logginglevel.WARN, 'listAcctsToUpdate.size() = ' + listAcctsToUpdate.size());
        	if (listAcctsToUpdate.size() > 0)
        		update listAcctsToUpdate;
    	}        
        catch(Exception e)
        {   
            // Display error message. 
            throw new RollupException('The following error occurred while updating the records in the Account Hierarchy: ' + e.getMessage());
        }
    }
    
    private void SumAccountTotals(Account acctUP)
    {
        //---------------------------------------------------------------------------------------------------------------
        // From the map, sum the values at each level in the hierarchy.
        //---------------------------------------------------------------------------------------------------------------
        
		// Build the Anchor Node - we better have the ultimate parent account.
		if (acctUP != null)
		{
			system.debug(Logginglevel.WARN, 'In SumAccountTotals for acctUP.Name = ' + acctUP.Name);

			// Store the totals for the current account.
			AccountTotals acctTotal = new AccountTotals();
			
			// Traverse down the Parent/Child chain, adding totals as we go.
			AccountTotals acctTotalChild = TotalChildAccounts(acctUP, 0);
			
			// Make sure there were some child accounts.
			if (acctTotalChild != null)
			{
				// Add in the values from the children. 
		    	acctTotal.Sum_Total_Spend_YT += acctTotalChild.Sum_Total_Spend_YT;						
		    	acctTotal.Sum_Total_Spend_MTD += acctTotalChild.Sum_Total_Spend_MTD;						
		    	acctTotal.Sum_Total_Booking_Spend += acctTotalChild.Sum_Total_Booking_Spend;					
		    	acctTotal.Sum_Total_Num_Bookings_for_Account += acctTotalChild.Sum_Total_Num_Bookings_for_Account;		
		    	acctTotal.Sum_Total_Bookings_Amount_CY += acctTotalChild.Sum_Total_Bookings_Amount_CY;
		    	acctTotal.Sum_Total_Bookings_Amount_CM += acctTotalChild.Sum_Total_Bookings_Amount_CM;	
		    	if (acctTotalChild.Date_of_Last_Booking != null && 
		    	   (acctTotal.Date_of_Last_Booking == null || acctTotalChild.Date_of_Last_Booking > acctTotal.Date_of_Last_Booking))
		    			acctTotal.Date_of_Last_Booking = acctTotalChild.Date_of_Last_Booking; 	
		    
		    	acctTotal.Sum_Total_Expected_Revenue += acctTotalChild.Sum_Total_Expected_Revenue;				
		    	acctTotal.Sum_Total_Expected_Revenue_Open_Opps += acctTotalChild.Sum_Total_Expected_Revenue_Open_Opps;	
		    	acctTotal.Sum_Total_Expected_Rev_Closed_Opp += acctTotalChild.Sum_Total_Expected_Rev_Closed_Opp;	

		    	if (acctTotalChild.Max_Opp_Close_Date != null && 
		    	   (acctTotal.Max_Opp_Close_Date == null || acctTotalChild.Max_Opp_Close_Date > acctTotal.Max_Opp_Close_Date))
		    			acctTotal.Max_Opp_Close_Date = acctTotalChild.Max_Opp_Close_Date; 	

		    	if (acctTotalChild.Min_Opp_Close_Date != null && 
		    	   (acctTotal.Min_Opp_Close_Date == null || acctTotalChild.Min_Opp_Close_Date < acctTotal.Min_Opp_Close_Date))
		    			acctTotal.Min_Opp_Close_Date = acctTotalChild.Min_Opp_Close_Date; 	

		    	acctTotal.Sum_Total_Complaints += acctTotalChild.Sum_Total_Complaints;					
		    	acctTotal.Sum_Total_Open_Complaints += acctTotalChild.Sum_Total_Open_Complaints;				
		    	acctTotal.Sum_Total_Open_Cases += acctTotalChild.Sum_Total_Open_Cases;					
			}
			
			system.debug(Logginglevel.WARN, 'SumAccountTotals for UPAccount = ' + acctTotal);
			
			// Add to map for updating purposes.
			m_mapIDtoAcctTotals.put(acctUP.ID, acctTotal);
		}
		else
		{
            // Display error message. Current user may not have proper access rights 
            return;         	
		}
    }
    
    // -----------------------------------------------------
    //  TotalChildAccounts
    //	-  Recursive method used to sum up the field values based on parent id.
    // -----------------------------------------------------
	private AccountTotals TotalChildAccounts(Account acctParent, Integer iParentLevel)
	{
		system.debug(Logginglevel.WARN, 'In TotalChildAccounts for acctParent.Name = ' + acctParent.Name);

		// Store the totals for the current account. 
		AccountTotals acctTotal = new AccountTotals();
    	acctTotal.Sum_Total_Spend_YT = acctParent.Total_Spend_YTD__c;						
    	acctTotal.Sum_Total_Spend_MTD = acctParent.Total_Spend_MTD__c;						
    	acctTotal.Sum_Total_Booking_Spend = acctParent.Total_Booking_Spend__c;					
    	acctTotal.Sum_Total_Num_Bookings_for_Account = (integer)acctParent.Total_Amount_of_Bookings__c;		
    	acctTotal.Sum_Total_Bookings_Amount_CY = acctParent.Total_Bookings_Amount_CY__c;
    	acctTotal.Sum_Total_Bookings_Amount_CM = acctParent.Total_Bookings_Amount_CM__c;	
    	acctTotal.Date_of_Last_Booking = (date)acctParent.Date_of_Last_Booking__c;	
	    
    	acctTotal.Sum_Total_Expected_Revenue = acctParent.Total_Expected_Revenue__c;				
    	acctTotal.Sum_Total_Expected_Revenue_Open_Opps = acctParent.Total_Expected_Revenue_Open_Opps__c;	
    	acctTotal.Sum_Total_Expected_Rev_Closed_Opp = acctParent.Total_Expected_Revenue_Closed_Opps__c;	
    	acctTotal.Max_Opp_Close_Date = (date)acctParent.Max_Opp_Close_Date__c;
    	acctTotal.Min_Opp_Close_Date = (date)acctParent.Min_Opp_Close_Date__c;	

    	acctTotal.Sum_Total_Complaints = (integer)acctParent.Total_Complaints__c;					
    	acctTotal.Sum_Total_Open_Complaints = (integer)acctParent.Total_Open_Complaints__c;				
    	acctTotal.Sum_Total_Open_Cases = (integer)acctParent.Total_Open_Cases__c;		

		// Get list of children for Parent Account.
		List<Account> listChildAccounts = m_mapParentIDToAccounts.get(acctParent.ID);
		
		// If no children, then return.
		if (listChildAccounts == null)
		{
			// Since this is a leaf node, don't add to map. If we can't find an account id in the map, then we'll know to null out values.
			system.debug(Logginglevel.WARN, 'Leaf Node Account = ' + acctParent.Name);
			
			// Return Account amounts for current account.
			return acctTotal;
		}

		// Iterate and traverse the list of child accounts - adding to the list of nodes as we go.
		for (Account acctChild : listChildAccounts)
		{
			// Traverse down the Parent/Child chain, adding totals as we go.
			AccountTotals acctTotalChild = TotalChildAccounts(acctChild, iParentLevel + 1);
			
			// Make sure there were some child accounts.
			if (acctTotalChild != null)
			{
				// Add in the values from the children based on record type. 
		    	acctTotal.Sum_Total_Spend_YT += acctTotalChild.Sum_Total_Spend_YT;						
		    	acctTotal.Sum_Total_Spend_MTD += acctTotalChild.Sum_Total_Spend_MTD;						
		    	acctTotal.Sum_Total_Booking_Spend += acctTotalChild.Sum_Total_Booking_Spend;					
		    	acctTotal.Sum_Total_Num_Bookings_for_Account += acctTotalChild.Sum_Total_Num_Bookings_for_Account;		
		    	acctTotal.Sum_Total_Bookings_Amount_CY += acctTotalChild.Sum_Total_Bookings_Amount_CY;
		    	acctTotal.Sum_Total_Bookings_Amount_CM += acctTotalChild.Sum_Total_Bookings_Amount_CM;	
		    	if (acctTotalChild.Date_of_Last_Booking != null && 
		    	   (acctTotal.Date_of_Last_Booking == null || acctTotalChild.Date_of_Last_Booking > acctTotal.Date_of_Last_Booking))
		    			acctTotal.Date_of_Last_Booking = acctTotalChild.Date_of_Last_Booking; 	
		    
		    	acctTotal.Sum_Total_Expected_Revenue += acctTotalChild.Sum_Total_Expected_Revenue;				
		    	acctTotal.Sum_Total_Expected_Revenue_Open_Opps += acctTotalChild.Sum_Total_Expected_Revenue_Open_Opps;	
		    	acctTotal.Sum_Total_Expected_Rev_Closed_Opp += acctTotalChild.Sum_Total_Expected_Rev_Closed_Opp;	

		    	if (acctTotalChild.Max_Opp_Close_Date != null && 
		    	   (acctTotal.Max_Opp_Close_Date == null || acctTotalChild.Max_Opp_Close_Date > acctTotal.Max_Opp_Close_Date))
		    			acctTotal.Max_Opp_Close_Date = acctTotalChild.Max_Opp_Close_Date; 	

		    	if (acctTotalChild.Min_Opp_Close_Date != null && 
		    	   (acctTotal.Min_Opp_Close_Date == null || acctTotalChild.Min_Opp_Close_Date < acctTotal.Min_Opp_Close_Date))
		    			acctTotal.Min_Opp_Close_Date = acctTotalChild.Min_Opp_Close_Date; 	

		    	acctTotal.Sum_Total_Complaints += acctTotalChild.Sum_Total_Complaints;					
		    	acctTotal.Sum_Total_Open_Complaints += acctTotalChild.Sum_Total_Open_Complaints;				
		    	acctTotal.Sum_Total_Open_Cases += acctTotalChild.Sum_Total_Open_Cases;					
			}
		}
					
		// Add to map for updating purposes.
		m_mapIDtoAcctTotals.put(acctParent.ID, new AccountTotals(acctTotal));
		system.debug(Logginglevel.WARN, 'SumAccountTotals for Account = ' + acctParent.Name + '; AcctTotals = ' + acctTotal);
		
		return acctTotal;
	}

    // -----------------------------------------------------
    //  doNotInHier
    // -----------------------------------------------------
    private void doNotInHier (Account[] listAccts)
    {
        // Iterate list of Accounts not in a hierarchy and set the values to null
        for (Account acct : listAccts)
        {
			acct.Parent_Total_Spend_YTD__c = null;
			acct.Parent_Total_Spend_MTD__c = null;
			acct.Parent_Total_Booking_Spend__c = null;
			acct.Parent_Total_Num_Bookings_for_Account__c = null;
			acct.Parent_Total_Bookings_Amount_CY__c = null;
			acct.Parent_Total_Bookings_Amount_CM__c = null;
			acct.Parent_Date_of_Last_Booking__c = null;
			
			acct.Parent_Total_Expected_Revenue__c = null;
			acct.Parent_Total_Expected_Revenue_Open_Opps__c = null;
			acct.Parent_Total_Expected_Rev_Closed_Opp__c = null;
			acct.Parent_Max_Opp_Close_Date__c = null;
			acct.Parent_Min_Opp_Close_Date__c = null;
			
			acct.Parent_Total_Complaints__c = null;
			acct.Parent_Total_Open_Complaints__c = null;
			acct.Parent_Total_Open_Cases__c = null;
		}

       	update listAccts;
    }

    global void finish(Database.BatchableContext BC)
    {
        // Get the ID of the AsyncApexJob representing this batch job from Database.BatchableContext.
        // Query the AsyncApexJob object to retrieve the current job's information.
        ID idJob = BC.getJobId();
        AsyncApexJob apexjobUP =  [SELECT Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email
                                    FROM AsyncApexJob WHERE Id = :idJob];

        // Because of test coverage issues in this class, need to create the 'failure' email message and only
        // send if there is truly a failure.
        // I can't seem to find a way to fail the job from a test.
        Messaging.SingleEmailMessage emailNotify = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {apexjobUP.CreatedBy.Email};
        emailNotify.setToAddresses(toAddresses);
        emailNotify.setSubject('Account Rollup Fields Calculation - Completed with Errors');
        emailNotify.setPlainTextBody(
            'Your Account Rollup Fields Calculation request completed, however the job reported ' + apexjobUP.NumberOfErrors + 
            ' errors. The accounts processed as part of the batch(es) in which errors occurred were not updated. If your Account ' +
            'Rollup Fields Calculation requests continue to fail, contact your system administrator.');
            
        // if the job did not complete successfully, then send an email to the job's submitter letting them know that there was a problem.
        if (apexjobUP.Status == 'Failed')
        {
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { emailNotify });
        }
        else if (m_strType == RUTYPE_RELATEDOBJS && !Test.isRunningTest())
        {
            // Queue up the calc to process accounts that are not in a hierarchy, but have a location value != 1.
            AccountRollupBatch.runAccountRollupBatch(m_iLimit, RU_BATCHSIZE, RUTYPE_UP);
        }
        else if (m_strType == RUTYPE_UP && !Test.isRunningTest())
        {
            // Queue up the calc to process accounts that are not in a hierarchy, but have a location value != 1.
            AccountRollupBatch.runAccountRollupBatch(m_iLimit, RU_BATCHSIZE, RUTYPE_NOTINHIER);
        }
    }


	// ***********************************************************
	//  AccountTotals
	//
	//  Description: 
	//  Inner class
	// ***********************************************************
	public with sharing class AccountTotals 
	{
	    ///////////////////////////////////////////////////
	    //  Member variables
	    ///////////////////////////////////////////////////
	
	    // Class attributes
	    public Decimal		Sum_Total_Spend_YT						{get; set {Sum_Total_Spend_YT = (value == null) ? 0 : value;}}
	    public Decimal		Sum_Total_Spend_MTD						{get; set {Sum_Total_Spend_MTD = (value == null) ? 0 : value;}}
	    public Decimal		Sum_Total_Booking_Spend					{get; set {Sum_Total_Booking_Spend = (value == null) ? 0 : value;}}
	    public Integer		Sum_Total_Num_Bookings_for_Account		{get; set {Sum_Total_Num_Bookings_for_Account = (value == null) ? 0 : value;}}
	    public Decimal		Sum_Total_Bookings_Amount_CY			{get; set {Sum_Total_Bookings_Amount_CY = (value == null) ? 0 : value;}}
	    public Decimal		Sum_Total_Bookings_Amount_CM			{get; set {Sum_Total_Bookings_Amount_CM = (value == null) ? 0 : value;}}
	    public Date			Date_of_Last_Booking					{get; set;}
	    
	    public Decimal		Sum_Total_Expected_Revenue				{get; set {Sum_Total_Expected_Revenue = (value == null) ? 0 : value;}}
	    public Decimal		Sum_Total_Expected_Revenue_Open_Opps	{get; set {Sum_Total_Expected_Revenue_Open_Opps = (value == null) ? 0 : value;}}
	    public Decimal		Sum_Total_Expected_Rev_Closed_Opp			{get; set {Sum_Total_Expected_Rev_Closed_Opp = (value == null) ? 0 : value;}}
	    public Date			Max_Opp_Close_Date						{get; set;}
	    public Date			Min_Opp_Close_Date						{get; set;}

	    public Integer		Sum_Total_Complaints					{get; set {Sum_Total_Complaints = (value == null) ? 0 : value;}}
	    public Integer		Sum_Total_Open_Complaints				{get; set {Sum_Total_Open_Complaints = (value == null) ? 0 : value;}}
	    public Integer		Sum_Total_Open_Cases					{get; set {Sum_Total_Open_Cases = (value == null) ? 0 : value;}}

	    ///////////////////////////////////////////////////
	    // Constructor 
	    ///////////////////////////////////////////////////
	    public AccountTotals() 
	    {
	    	// all values are zero;
	    	Sum_Total_Spend_YT = 0;						
	    	Sum_Total_Spend_MTD = 0;						
	    	Sum_Total_Booking_Spend = 0;					
	    	Sum_Total_Num_Bookings_for_Account = 0;		
	    	Sum_Total_Bookings_Amount_CY = 0;
	    	Sum_Total_Bookings_Amount_CM = 0;		
	    
	    	Sum_Total_Expected_Revenue = 0;				
	    	Sum_Total_Expected_Revenue_Open_Opps = 0;	
	    	Sum_Total_Expected_Rev_Closed_Opp = 0;			

	    	Sum_Total_Complaints = 0;					
	    	Sum_Total_Open_Complaints = 0;				
	    	Sum_Total_Open_Cases = 0;					
	    }
	    
	    public AccountTotals(AccountTotals copyFrom) 
	    {  
			// Set member variables - make sure we don't have any null values.
			Sum_Total_Spend_YT = copyFrom.Sum_Total_Spend_YT;
			Sum_Total_Spend_MTD = copyFrom.Sum_Total_Spend_MTD;
			Sum_Total_Booking_Spend = copyFrom.Sum_Total_Booking_Spend;
			Sum_Total_Num_Bookings_for_Account = copyFrom.Sum_Total_Num_Bookings_for_Account;
			Sum_Total_Bookings_Amount_CY = copyFrom.Sum_Total_Bookings_Amount_CY;
			Sum_Total_Bookings_Amount_CM = copyFrom.Sum_Total_Bookings_Amount_CM;
			Date_of_Last_Booking = copyFrom.Date_of_Last_Booking;
			
			Sum_Total_Expected_Revenue = copyFrom.Sum_Total_Expected_Revenue;
			Sum_Total_Expected_Revenue_Open_Opps = copyFrom.Sum_Total_Expected_Revenue_Open_Opps;
			Sum_Total_Expected_Rev_Closed_Opp = copyFrom.Sum_Total_Expected_Rev_Closed_Opp;
			Max_Opp_Close_Date = copyFrom.Max_Opp_Close_Date;
			Min_Opp_Close_Date = copyFrom.Min_Opp_Close_Date;
			
			Sum_Total_Complaints = copyFrom.Sum_Total_Complaints;
			Sum_Total_Open_Complaints = copyFrom.Sum_Total_Open_Complaints;
			Sum_Total_Open_Cases = copyFrom.Sum_Total_Open_Cases;
	    } 
	    
	}
}