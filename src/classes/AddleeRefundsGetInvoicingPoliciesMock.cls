@isTest
global class AddleeRefundsGetInvoicingPoliciesMock implements webservicemock{
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.invoicingPolicyResponse respElement =  new Addlee_refunds.invoicingPolicyResponse();
        Addlee_refunds.invoicingPolicy testPolicy = new Addlee_refunds.invoicingPolicy();
               testPolicy.code = '54654';
               testPolicy.id = 768767;
               testPolicy.name = 'test';
         respElement.invoicingPolicies = new List<Addlee_refunds.invoicingPolicy>();
         respElement.invoicingPolicies.add(testPolicy);
       response.put('response_x', respElement); 
   }
}