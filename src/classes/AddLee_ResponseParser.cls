public with sharing class AddLee_ResponseParser {
	
	private boolean isCreateAccount;
	private AddLee_SoapIntegration_Test_v2_Fix.customerResponse response;
	private Log__c log;
	private Account account; 
	private AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort soapClient; 
	private AddLee_SoapIntegration_Test_v2_Fix.customer customer; 
	private String sessionId;
	private String accountId;
	private String logId;
	
	
	
	public AddLee_ResponseParser(){}
	
	private void initialize(String logId, String accountId, String sessionId, boolean isCreateAccount){
		this.logId = logId;
		this.accountId = accountId;
		this.sessionId = sessionId;
		this.isCreateAccount = isCreateAccount;
	}
	
	public AddLee_SoapIntegration_Test_v2_Fix.customerResponse createCustomer(Log__c log, Account account, String sessionId){
		this.log = log;
		this.account = account;
		initialize(log.Id,account.Id,sessionId,true);
		//Business Logic to call the CreateAccount Service
		AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort soapClient = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
		Map<String,AddLee_SoapIntegration_Test_v2_Fix.customer> customers = AddLee_IntegrationUtility.createSoapCustomer(account.Id,account.parentId);
		System.DEBUG(logginglevel.ERROR,'customers : ' + customers);
		List<AddLee_SoapIntegration_Test_v2_Fix.customer> childCustomers = new List<AddLee_SoapIntegration_Test_v2_Fix.customer>();
		AddLee_SoapIntegration_Test_v2_Fix.customer parentCustomer;
		AddLee_SoapIntegration_Test_v2_Fix.customer customer;
		if(customers.get('parentAccount') != null){
			parentCustomer = customers.get('parentAccount');
			customer = customers.get('childAccount');
			childCustomers.add(customer);
		}else{
			parentCustomer = null;
			customer = customers.get('childAccount');
		}
		
		AddLee_SoapIntegration_Test_v2_Fix.customerResponse wsResponse;
		if(parentCustomer !=null ){
			wsResponse = soapClient.createCustomer(sessionId, parentCustomer, childCustomers);
			System.debug(logginglevel.ERROR,'parent update 1: ' + parentCustomer);
			System.DEBUG(logginglevel.ERROR,'sessionId 1: ' + sessionId);
			System.DEBUG(logginglevel.ERROR,'childCustomers 1: ' + childCustomers);
		}else{
			wsResponse = soapClient.createCustomer(sessionId, customer, null);
			System.DEBUG(logginglevel.ERROR,'First Callout response : ' + wsResponse);
			System.debug(logginglevel.ERROR,'single update');
		}
		//Perform Creation
		//String sessionId = AddLee_IntegrationUtility.getSessionId();
		//System.debug('Creating a callout - Create Customer ...  ');
		//AddLee_SoapIntegration_Test_v2_Fix.customerResponse response = soapClient.CreateCustomer(sessionId, cust, null);
		
		//return response; uncomment this when recovery procedure is fixed
		return wsResponse;
		//this.parseResponse(response);
	}
	
	public AddLee_SoapIntegration_Test_v2_Fix.customerResponse amendCustomer(Log__c log, Account account, String sessionId){
		this.log = log;
		this.account = account;
		initialize(log.Id,account.Id,sessionId,false);
		//Business Logic to call the CreateAccount Service
		AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort soapClient = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
		Map<String,AddLee_SoapIntegration_Test_v2_Fix.customer> customers = AddLee_IntegrationUtility.createSoapCustomer(account.Id,account.parentId);
		System.DEBUG('customers : ' + customers);
		List<AddLee_SoapIntegration_Test_v2_Fix.customer> childCustomers = new List<AddLee_SoapIntegration_Test_v2_Fix.customer>();
		AddLee_SoapIntegration_Test_v2_Fix.customer parentCustomer;
		AddLee_SoapIntegration_Test_v2_Fix.customer customer;
		if(customers.get('parentAccount') != null){
			parentCustomer = customers.get('parentAccount');
			customer = customers.get('childAccount');
			childCustomers.add(customer);
		}else{
			parentCustomer = null;
			customer = customers.get('childAccount');
		}
		
		AddLee_SoapIntegration_Test_v2_Fix.customerResponse wsResponse;
			
		wsResponse = soapClient.amendCustomer(sessionId, customer, parentCustomer);
		System.DEBUG('Response : ' + wsResponse);
		
		
		//AddLee_SoapIntegration_Test_v2_Fix.customer cust = new AddLee_SoapIntegration_Test_v2_Fix.customer(account.Id);
		
		//Perform Creation
		//String sessionId = AddLee_IntegrationUtility.getSessionId();
		//System.debug('Creating a callout - Amend Customer ...  ');
		//AddLee_SoapIntegration_Test_v2_Fix.customerResponse response = soapClient.amendCustomer(sessionId, cust);
		
		return wsResponse;
		//return null;
		//this.parseResponse(response);
	}
	
	public void parseResponse(AddLee_SoapIntegration_Test_v2_Fix.customerResponse response){
		System.DEBUG('Response : ' + response);
		if(response.errorCode == 0 && response.errorList == null){
			//Log the result - Completed
			this.log.Description__c = 'SFDC(Action Performed with success.)';
			log.Long_Description__c = 'SFDC(Action Performed with success.)';
			log.Status__c = 'Completed'; 
			
			//Flag exsistence of the account in Salesforce
			if(isCreateAccount){	
				account.exsistInShamrock__c = true;	
				//update account;	
			}		
		}else if(response.errorCode == -4 && response.description == 'Session expired'){
			
			//Try to generate a new sessionId
			sessionId = AddLee_IntegrationWrapper.loginCalloutSynch();
			//System.debug('<<<Response Code : SessionId Refresh>>> : ' + response.errorCode);
			//Retry the creation
			if(isCreateAccount){
				System.debug('Creating a callout - Create Customer ... Parse Response ');
				response = createCustomer(this.log, this.account, this.sessionId);
				parseResponse(response);
				//createResponseDebug(response);
			}else{
				System.debug('Creating a callout - Amend Customer ... Parse Response error code -4 ');
				response = amendCustomer(this.log, this.account, this.sessionId);
				parseResponse(response);
				//amendResponseDebug(response);
			}		
		}else if(response.errorCode == -13300){
			System.debug('Creating a callout - Amend Customer ...  error code -13300 inside session expire');
			response = amendCustomer(this.log, this.account, this.sessionId);
			parseResponse(response);
		}else if(response.errorCode == 0 && response.errorList != null){
			//Log the result - Failed
			if(isCreateAccount){
				if(String.valueOf(response.errorList).length() > 250)
					log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(250)+')';
				log.Long_Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(250)+')';
			}
			else{
				if(String.valueOf(response.errorList).length() > 250)	
					log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
				log.Long_Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(250)+')';
			}
			log.Status__c = 'Failed'; 			
		}else{
			//Log the result - Failed
			//if(String.valueOf(response.errorList).length() > 250)
			log.Description__c = 'SHM (CODE:'+response.errorCode+' INFO:'+response.errorInfo+' DESCRIPTION:'+response.description+')';
			log.Long_Description__c = 'SHM (CODE:'+response.errorCode+' INFO:'+response.errorInfo+' DESCRIPTION:'+response.description+')';
			log.Status__c = 'Failed'; 
		}
	}
}