public without sharing class AddLee_PersonAccountSearch_Controller {
    
    public Lead thisLead { get; set; }
    public List<AddLee_DAndBSearch_Controller.AccountWrapper> accountWrappers { get; set; }
    public List<AddLee_DAndBSearch_Controller.LeadWrapper> leadWrappers { get; set; }
    public List<Account> accounts { get; set; }
    public List<Lead> leads { get; set; } 
    public boolean renderExisting { get; set; }
    public boolean renderExistingLead { get; set; }
    public boolean captureContactDetails { get; set; }
    public String buildingNumber { get; set; }
    public Call_Credit__c callCreditCredentials { get; set; }

    public AddLee_PersonAccountSearch_Controller(ApexPages.StandardController controller) {
        callCreditCredentials = Call_Credit__c.getValues('credentials');
        thisLead = (Lead)controller.getRecord();
        PageReference pageRef = ApexPages.currentPage();
        System.DEBUG(pageRef);
        System.DEBUG(thisLead);
        if( pageRef.getParameters().get('Id') != null ){
            thisLead = [SELECT Id, FirstName, LastName, Company, Phone, Street, State, Industry, Industry_Sub_Sector__c, City, Country, Birthdate__c,
													Website, Segment__c, D_B_Credit_Limit__c, Estimated_Monthly_Spend__c, Days_since_last_Credit_Check__c,
													No_of_London_Employees__c, Total_London_Employees__c, Sales_Ledger__c, Is_Production_Company__c,
													salutation, Title, email, MobilePhone, How_did_you_hear_about_us__c, Further_Details__c, Authorised_By__c,
													Postalcode, CompanyDunsNumber, Owner.Name, LastModifiedDate, Administration_Fee__c, Credit_Limit__c,
													Customer_Type__c, Credit_Checked__c, Invoicing_Frequency__c, Payment_Terms__c, Salesman__c, Payment_Type__c  
												 FROM Lead WHERE id =: pageRef.getParameters().get('Id')];
            search();
        }
        if( pageRef.getParameters().get('firstName') != null ){
            thisLead.FirstName = pageRef.getParameters().get('firstName');
            thisLead.LastName  = pageRef.getParameters().get('lastName');
            if(ApexPages.currentPage().getUrl().contains('CaptureContactDetails'))
                thisLead.Birthdate__c = Date.valueOf(pageRef.getParameters().get('birthdate'));
            else{
                String dateString = pageRef.getParameters().get('birthdate');
                thisLead.Birthdate__c = Date.valueOf(dateString);
            }
            thisLead.Email = pageRef.getParameters().get('email');
            thisLead.Salutation = pageRef.getParameters().get('salutation');
            if(thisLead.LastName != null)
                search();
        }
    }
    
    public String explode(String dateString){
        String[] explodedString = dateString.split('/');
        System.DEBUG(explodedString);
        String newString = explodedString[2] + '-' + explodedString[1] + '-' + explodedString[0];
        return newString;
    }

    public PageReference search(){
        /*accountWrappers = new List<AddLee_DAndBSearch_Controller.AccountWrapper>();
        leadWrappers = new List<AddLee_DAndBSearch_Controller.LeadWrapper>();
        accounts = [SELECT Id, FirstName, LastName, IsPersonAccount, PersonEmail, Birthdate__pc, 
                                Owner.Name, LastModifiedDate, Account_Status__c FROM Account 
                                WHERE (FirstName =: thisLead.FirstName 
                                    AND LastName =: thisLead.LastName) 
                                    OR (PersonEmail =: thisLead.Email AND isPersonAccount = true)];
        leads = [SELECT Id, FirstName, LastName, phone,Email,Birthdate__c,PostalCode,
                        Salutation,D_B_Recommendation__c,D_B_Credit_Limit__c,Street, 
                        Owner.Name, LastModifiedDate FROM Lead 
                            WHERE (isConverted = false AND Company = Null) 
                            AND (Email =: thisLead.Email 
                                OR (FirstName =: thisLead.FirstName AND LastName =: thisLead.LastName))];
        if(accounts.size() > 0){
            renderExisting = true;
            for(Account eachAccount : accounts){
                accountWrappers.add(new AddLee_DAndBSearch_Controller.AccountWrapper(eachAccount));
            }
        }
        if(leads.size() > 0){
            renderExistingLead = true;
            for(Lead eachLead : leads){
                leadWrappers.add(new AddLee_DAndBSearch_Controller.LeadWrapper(eachLead));
            }
        }
        if(accounts.size() == 0 && leads.size() == 0){
            captureContactDetails = true;
        }*/
        captureContactDetails = true;
        return null;
    }
    
    public PageReference createLead(){
        renderExisting = false;
        renderExistingLead = false;
        captureContactDetails = true;
        PageReference redirectPage = new PageReference('/apex/CaptureContactDetails');
        redirectPage.getParameters().put('firstName',thisLead.firstName);
        redirectPage.getParameters().put('lastName',thisLead.lastName);
        redirectPage.getParameters().put('email',thisLead.email);
        redirectPage.getParameters().put('birthdate',String.valueOf(thisLead.Birthdate__c));
        redirectPage.getParameters().put('salutation',thisLead.salutation);
        System.DEBUG(redirectPage);
        redirectPage.setRedirect(true);
        return redirectPage;
    }

    public PageReference mergeLead(){
        for( AddLee_DAndBSearch_Controller.LeadWrapper eachLeadWrapper : leadWrappers){
            if(eachLeadWrapper.selected){
                PageReference redirectPage = new PageReference('/apex/CaptureContactDetails');
                redirectPage.getParameters().put('Id',eachLeadWrapper.eachLead.Id);
                redirectPage.setRedirect(true);
                return redirectPage;
            }
        }
        return null;
    }

    public PageReference insertLead(){
        AddLee_Call_Credit.callcreditheaders_element credentials = new AddLee_Call_Credit.callcreditheaders_element();
        credentials.company = this.callCreditCredentials.CompanyName__c;
        credentials.username = this.callCreditCredentials.UserName__c;
        credentials.password = this.callCreditCredentials.Password__c;
        System.DEBUG('Password : ' + credentials);
        AddLee_Call_Credit.CT_SearchDefinition SearchDefinition =  new AddLee_Call_Credit.CT_SearchDefinition();
        searchDefinition.yourreference = 'Test';
        searchDefinition.creditrequest = new AddLee_Call_Credit.CT_searchrequest();
        searchDefinition.creditrequest.score = '1';
        searchDefinition.creditrequest.purpose = 'CA';
        //searchDefinition.creditrequest.autosearch = '1';
        //searchDefinition.creditrequest.autosearchmaximum = '3';
        searchDefinition.creditrequest.schemaversion = '7.1';
        searchDefinition.creditrequest.datasets = 255;
        searchDefinition.creditrequest.applicant = new List<AddLee_Call_Credit.CT_searchapplicant>();
        AddLee_Call_Credit.CT_searchapplicant searchApplicant = new AddLee_Call_Credit.CT_searchapplicant();
        searchApplicant.dob = thisLead.Birthdate__c;
        searchApplicant.address = new List<AddLee_Call_Credit.CT_inputaddress>();
        AddLee_Call_Credit.CT_inputaddress inputAddress = new AddLee_Call_Credit.CT_inputaddress();
        inputAddress.buildingno = buildingNumber;
        inputAddress.postcode = thisLead.postalCode;
        searchApplicant.address.add(inputAddress);
        searchApplicant.name = new List<AddLee_Call_Credit.CT_inputname>();
        AddLee_Call_Credit.CT_inputname inputName = new AddLee_Call_Credit.CT_inputname();
        inputName.forename = thisLead.firstName;
        inputName.surname = thisLead.lastName;
        searchApplicant.name.add(inputName);
        searchDefinition.creditrequest.applicant.add(searchApplicant);
        System.DEBUG(searchDefinition);
        AddLee_Call_Credit.Soap11 request = new AddLee_Call_Credit.Soap11();
        request.callcreditheaders = credentials;
        System.debug(request);
        AddLee_Call_Credit.CT_SearchResult value ;
        try{
        	//if(thisLead.Days_since_last_Credit_Check__c < 180){
        		value = request.Search07a(SearchDefinition);
        	//} else {
        		//upsert thisLead;
	            //redirectToCapturPayment();
        	//}
        }catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,e.getMessage());
            ApexPages.addMessage(myMsg);
            return null;
        }
        
        System.DEBUG('Search Def : ' + SearchDefinition);
        System.DEBUG('callout : ' + value);
        if(value.creditreport.applicant[0].creditscores.creditscore[0].score!=null){
            String creditScore = value.creditreport.applicant[0].creditscores.creditscore[0].score;
            thisLead.D_B_Credit_Limit__c = 200;
            thisLead.Country = 'United Kingdom';
            thisLead.Convert_To_Account__c = true;
            thisLead.Credit_Checked__c = true;
            thisLead.Nature_of_Enquiry__c = 'Individual/Family Account Application';
        	thisLead.Last_Credit_Checked_Date__c = system.today();
        	thisLead.T_C_External_Id__c = getExternalId();
        	thisLead.Tech_Send_Ts_And_Cs__c = true;
            if(Integer.valueOf(creditscore) > callCreditCredentials.Threshold__c && Integer.valueOf(creditscore) != 9999){
                thisLead.D_B_Recommendation__c = '1';              
            }else{
                thisLead.D_B_Recommendation__c = '2';
            }
            upsert thisLead;
            allocateTNC(thisLead);
            return redirectToCapturPayment();
        }
        return null;
    }
    
    private static String getExternalId(){
    	Integer len = 14;
	    final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
	    String randStr = '';
	    while (randStr.length() < len) {
	       Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
	       randStr += chars.substring(idx, idx+1);
	    }
		return randStr; 
    }
    
    public static void allocateTNC(Lead thisLead){
		List<AddLee_Terms_Conditions__c> existingTCList = [Select id, Internal_Id__c, External_Id__c 
															From AddLee_Terms_Conditions__c
															Where Internal_Id__c =: thisLead.id
															Limit 1]; 
		if(existingTCList.isEmpty()){
			AddLee_Terms_Conditions__c newTNC = new AddLee_Terms_Conditions__c();
	        newTNC.External_Id__c = thisLead.T_C_External_Id__c;
			newTNC.Internal_Id__c = thisLead.id;
	        insert newTNC;
		}
    	
    }
    
    public PageReference redirectToCapturPayment(){
    	if(thisLead.Payment_Type__c == 'Direct Debit'){
            PageReference redirectPage = new PageReference('/apex/PaymentCapture?id=' + thisLead.Id + '&type=bank');
            redirectPage.setRedirect(true);
            return redirectPage;
        }else if(thisLead.Payment_Type__c == 'Single Credit Card'){
        	PageReference redirectPage = new PageReference('/apex/PaymentCapture?id=' + thisLead.Id + '&type=card');
            redirectPage.setRedirect(true);
            return redirectPage;
        }else{
            PageReference redirectPage = new PageReference('/apex/CreditCheckPass?id=' + thisLead.Id);
            redirectPage.setRedirect(true);
            return redirectPage;
        }
    }

    public PageReference newSearch(){
        PageReference redirectPage = new PageReference('/apex/AccountCreation_CreatePA');
        return redirectPage;
    }
    
    public Class AccountWrapper {
        public Account eachAccount { get; set; }
        
        public AccountWrapper(Account account){
            eachAccount = account;
        }
    }

    public Class LeadWrapper {
        public Lead eachLead { get; set; }
        public boolean selected { get; set; }
        public LeadWrapper(Lead lead){
            eachLead = lead;
            selected = false;
        }
    }

}