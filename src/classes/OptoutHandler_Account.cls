public class OptoutHandler_Account {
     
    List<Contact> con = new list<Contact>();
    List<Contact> updatecon = new list<Contact>();
    List<Contact> smsupdate = new list<Contact>();
    Map<Id, Contact> mapContact = new Map<ID, Contact>();
    List<Contact> finalcon = new list<Contact>();
    List<Contact> econ = new list<Contact>();
    List<Contact> scon = new list<Contact>();
       
    
    List<Account> acc = new List<Account> ();
    List<ID> accids  = new list<ID>();
    List<ID> accemail = new List<ID>();
    List<ID> accsms = new List<ID>();
    
    //contact opt out when account has been opt out 
    public void optoutHandler(List<Account> acc){
        for(Account optacc : acc){
            if(optacc.OPT_OUT_ALL_MARKETING_FOR_ACCOUNT__c)
            accids.add(optacc.id);
            if(optacc.Opt_Out_Of_Email_Marketing__c)
            accemail.add(optacc.id);
            if(optacc.Opt_Out_Of_SMS_Marketing__c)
            accsms.add(optacc.id);
            
        }
        if(accids.size()>0){
        con = [select id, Name,HasOptedOutOfEmail,Optout_All__c,SMS_Opt_Out__c from Contact where AccountId in:accids ];
        for (Contact optcon : con  ){
            optcon.HasOptedOutOfEmail = true;
            optcon.Optout_All__c = true;
            system.debug('optcon.Optout_All__c' + optcon.Optout_All__c);
            optcon.SMS_Opt_Out__c = true;
            system.debug('optcon.SMS_Opt_Out__c' + optcon.SMS_Opt_Out__c);
            updatecon.add(optcon);
        }
        }
        if(accids.size()<1 && accemail.size()>0){
            econ = [select id, Name,Add_Lib_Lifestyle_Content_Email__c,
                          News_Letters_And_Updates_Email__c,
                          Offers_Discounts_Competitions_Email__c,
                          Surveys_And_Research_Email__c
                          from Contact where AccountId in:accemail ];
            for(Contact conemail : econ ){
                conemail.Add_Lib_Lifestyle_Content_Email__c = false;
                conemail.News_Letters_And_Updates_Email__c = false;
                conemail.Offers_Discounts_Competitions_Email__c = false;
                conemail.Surveys_And_Research_Email__c = false;
                conemail.HasOptedOutOfEmail = true;
                updatecon.add(conemail);                
            }
        }
        if(accids.size()<1  && accsms.size()>0){
            scon = [select Id,Name,Add_Lib_Lifestyle_Content_SMS__c,
                    News_Letters_And_Updates_SMS__c,
                    Offers_Discounts_Competitions_SMS__c,
                    Surveys_And_Research_SMS__c
                    from Contact where AccountId in:accsms ];
            for(Contact consms : scon ) {
                consms.Add_Lib_Lifestyle_Content_SMS__c = false;
                consms.News_Letters_And_Updates_SMS__c = false;
                consms.Offers_Discounts_Competitions_SMS__c = false;
                consms.Surveys_And_Research_SMS__c = false;
                consms.SMS_Opt_Out__c = true;
                smsupdate.add(consms);
            }   
        }
        if(!AddLee_Oprout_Recursive.optoutRecursive()){
        Update smsupdate;
        system.debug('updatecon>>>>'+updatecon);
        Stop_Trigger  sr = new Stop_Trigger ();
        sr.prevent_Contact_Trigger_Run();
        update updatecon;
        }
    }

   
    
}