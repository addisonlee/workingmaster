@isTest
global class AdleeRefundServiceGetAgentsMock implements WebServiceMock{
global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.agentResponse respElement =  new Addlee_refunds.agentResponse();
             respElement.agents = New list<Addlee_refunds.agent>();
             Addlee_refunds.agent testAgent =  new Addlee_refunds.agent ();
           testAgent.email = 'Dummy@demo.com';
            testAgent.id = 213123123;
            testAgent.initials = 'Mr.';
             testAgent.name = 'Name';
             respElement.agents.add(testAgent);
          
       response.put('response_x', respElement); 
   }

}