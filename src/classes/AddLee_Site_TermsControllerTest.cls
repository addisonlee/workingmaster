@isTest
private class AddLee_Site_TermsControllerTest {

    static testMethod void handleTandCTest() {
        
        AddLee_Trigger_Test_Utils.insertCustomSettings();
    	insert TestDataFactory.createTestLead('Test Lead');
    	Lead testLead = [Select id from Lead limit 1];
        insert TestDataFactory.createTnC(testLead);
        AddLee_Terms_Conditions__c testTNC = [Select Id,Internal_Id__c,External_Id__c 
        										from AddLee_Terms_Conditions__c limit 1];
        PageReference testSitePage = Page.ACP_Site_Terms;
        Test.setCurrentPage(testSitePage);
        ApexPages.currentPage().getParameters().put('accept', 'true');
        ApexPages.currentPage().getParameters().put('Id', testTNC.Id);
        AddLee_Site_TermsController.handleTandC();
        
    }
}