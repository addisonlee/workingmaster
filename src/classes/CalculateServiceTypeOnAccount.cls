global class CalculateServiceTypeOnAccount implements Database.Batchable<sObject>{
   global String Query;
   global String AccountId;

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(Query);
   }

   global void execute(Database.BatchableContext BC, 
                       List<Account> bscope){
                       for(Account accc:bscope){
                       AccountId = accc.id;
                       DateTime dt = (System.now()-20);
                       Datetime startDate = Datetime.newInstance(dt.year(), dt.month(), dt.day(), 0, 0, 0);
                       List<Booking_Summary__c> lstBooking  =  [SELECT Id,Account__c,Date_Time__c,Number_of_Bookings__c,Date_From__c,Total_Price__c,Total_Journey_Price_Currency__c,Service_Type__c,Booking_Channel__c from Booking_Summary__c 
                       											where Account__c=:AccountId  and Service_Type__c != null limit 49999 ]; //Lastmodifieddate>:startDate and 
 
                       AddLee_BookingSummaryHelper addLee_Booking = new AddLee_BookingSummaryHelper();
                       addLee_Booking.handleServiceTypeAction(lstBooking);
                        }
                       }
                       
                       
                      
   global void finish(Database.BatchableContext BC){

   
   }

}