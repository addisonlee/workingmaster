global class ScheduleBookingSummaryAllBatch implements schedulable {
    global void execute (SchedulableContext SC){
        BookingSummaryAllBatch bSB = new BookingSummaryAllBatch();
        Database.executebatch(bSB);
    }
}