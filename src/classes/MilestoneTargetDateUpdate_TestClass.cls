@isTest
private class MilestoneTargetDateUpdate_TestClass{
/*
Test class to test Addlee_MilestoneTargetDateUpdate & Addlee_ScheduleMilestoneTargetDateUpdate 

*/

    public static testMethod void Validate_MilestoneTargetDateUpdateBatch(){
    
        /*List<Case_Milestone_Scheduler_Settings__c > settingList = new list<Case_Milestone_Scheduler_Settings__c >();
        settingList.add(new Case_Milestone_Scheduler_Settings__c (Name ='Setting' ,Time_Stamp__c=system.now().addDays(-1)));
        insert settingList;
*/
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        SLAProcess slaObj=[SELECT Id,IsActive,Name,NameNorm FROM SlaProcess WHERE NameNorm like '%24 hour support%' AND IsVersionDefault= true AND IsActive= true limit 1];
        Account testAcc= new Account(name='Dummy Entitlement Account');
        insert testAcc;
        Entitlement e = new Entitlement();
        e.AccountId = testAcc.Id;
        e.Name = 'Gold';
        e.SlaProcessId=slaObj.id;
        e.StartDate = Date.today();
        insert e;    
        List <Case> testCases = new List<Case>();
        for(integer i = 0; i<10; i++){
            Case c = new Case(Origin='Intranet - Internal',status='New',EntitlementId=e.Id); 
            testCases.add(c);
        }
        insert testCases;

        Test.StartTest();
        Datetime sysTime = System.now().addSeconds(2);
        String chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
        Addlee_ScheduleMilestoneTargetDateUpdate CaseMilestoneBatch = new Addlee_ScheduleMilestoneTargetDateUpdate();
        id cronid=System.schedule('MilestoneTargetDateUpdate' + sysTime.getTime(),chron_exp,CaseMilestoneBatch); 
        Addlee_MilestoneTargetDateUpdate obj= new Addlee_MilestoneTargetDateUpdate();
        Id batchprocessid = Database.executeBatch(obj);
        Test.StopTest();
        
        list<caseMileStone> caseMilestoneList=[SELECT id,TargetDate,IsCompleted,CreatedDate,LastModifiedDate,CaseId,case.isClosed,case.id,case.Entitlement_Expiry_Time__c FROM CaseMilestone where case.isClosed=false and IsCompleted=false ORDER BY TargetDate ASC];
        for(caseMilestone cMObj:caseMilestoneList)
            system.assertEquals(cMObj.TargetDate,cMobj.case.Entitlement_Expiry_Time__c); 
        
   }
}