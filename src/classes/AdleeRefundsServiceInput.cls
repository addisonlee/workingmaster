/*
* Input Class for CreateComplain Callout
*/
global with sharing class AdleeRefundsServiceInput {
    
  
   global AdleeServiceRequest adleeRequest;
    
    public AdleeRefundsServiceInput(){
    }
   
   //Property Wrapper for Adleeservicerequest
   global Class AdleeServiceRequest{
      public String parentCaseId;
      public ComplainRequest createComplain;
   }
   //propertywrapper foe ComplainRequest
   global Class ComplainRequest{
           public decimal adminfee;
           public String  Type;
           public String  TypeId;
           public String  subType;
           public String  account;
           public String  Driver;
           public String  Name;
           public String  docketId;
           public String docketValue;           
           public Decimal  net;
           public Decimal discount;
           public decimal gross;
           public String  InvoiceNumber;
           public String Note;
           public String Description;
           public decimal vat;
   }
  
   

}