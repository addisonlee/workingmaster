/**
 *The batch process to insert Campaign member in campaigns
 *Developed by Nimil
**/


global class Addlee_CampaignMember_Account_Batch implements Database.Batchable<sObject> {
    

    String query = 'SELECT ID, Date_changed_to_Current__c, Total_Amount_of_Bookings__c, (SELECT FirstName, LastName, email, Account.Date_changed_to_Current__c, Account.Total_Amount_of_Bookings__c FROM Contacts WHERE Contact_Type__c = \'Main Contact\') FROM Account WHERE (Date_changed_to_Current__c = ';
    String filterCriteriaDate;
    List<Campaign> campaigns;
    Map<String, Campaign> campaignsMap;
    Map<String,String> campaignsToSelect;
    List<Campaign_Automation__c> campaignAutomationSettings;
    

    global Addlee_CampaignMember_Account_Batch() {
        campaignsToSelect = new Map<String, String>();
        campaignAutomationSettings = Campaign_Automation__c.getAll().values();

        for(Campaign_Automation__c eachCampaignSetting : campaignAutomationSettings){
            campaignsToSelect.put(eachCampaignSetting.Tech_Name__c,eachCampaignSetting.Campaign_Name__c);
        }

        System.DEBUG(campaignsToSelect);
        campaigns = new List<Campaign>([SELECT Id,Name FROM Campaign WHERE Name in: campaignsToSelect.values()]);
        System.DEBUG('Campaigns Selected : ' + campaigns);
        campaignsMap = new Map<String, Campaign>();
        for(Campaign eachCampaign :  campaigns){
            if(!campaignsMap.containsKey(eachCampaign.Name)){
                campaignsMap.put(eachCampaign.Name,eachCampaign);
            }
        }
        System.DEBUG('Campaigns Map : ' + campaignsMap);
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC) {
        Integer counter = 0;
        for(Campaign_Automation__c eachCampaignSetting : campaignAutomationSettings){
            if(counter == 0){
                filterCriteriaDate = String.valueOf(System.today().addDays(-Integer.valueOf(eachCampaignSetting.WeeksActive__c) * 7));
                query += filterCriteriaDate;
                counter++;
            }else {
                filterCriteriaDate = String.valueOf(System.today().addDays(-Integer.valueOf(eachCampaignSetting.WeeksActive__c) * 7));
                query += ' OR Date_changed_to_Current__c = ' + filterCriteriaDate;
            }
        }
        counter = 0;//Set it back to 0 if Database.stateful will be used later
        query += ') AND Grading__c not in (\'P1\',\'P2\',\'N1\') AND Account_Status__c = \'Current\' AND Sales_Ledger__c = \'Sales Ledger\'';// Get only accounts that are not P1, P2 or N1
        System.DEBUG('Created Query : ' + query);
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope) {
        List<CampaignMember> campaignMembers = new List<CampaignMember>();
        List<Contact> contactsToAddActiveAccounts = new List<Contact>();
        List<Contact> contactsToAddNoSpendAccounts = new List<Contact>();
        for(SObject eachSObject : scope){
            Account eachAccount = (Account)eachSObject;
            if(eachAccount.Total_Amount_of_Bookings__c >= 1){
                contactsToAddActiveAccounts.addAll(eachAccount.Contacts);
            }else if(eachAccount.Total_Amount_of_Bookings__c == 0){
                contactsToAddNoSpendAccounts.addAll(eachAccount.Contacts);
            }
        }

        for(Campaign_Automation__c eachCampaignSetting : campaignAutomationSettings){
            if(contactsToAddActiveAccounts.size() > 0){
                for(Contact eachContact : contactsToAddActiveAccounts){
                    if(eachCampaignSetting.IsActive__c){
                        if(campaignsMap.get(campaignsToSelect.get(eachCampaignSetting.Tech_Name__c)) != null && String.valueOf(eachContact.Account.Date_changed_to_Current__c) == String.valueOf(System.today().addDays(-Integer.valueOf(eachCampaignSetting.WeeksActive__c) * 7))){
                            CampaignMember campaignMember = new CampaignMember(contactId = eachContact.Id, campaignId = campaignsMap.get(campaignsToSelect.get(eachCampaignSetting.Tech_Name__c)).Id);
                            campaignMembers.add(campaignMember);
                        }
                        System.DEBUG('Each Contact : ' + eachContact);
                        System.DEBUG('Campaigns Map : ' + campaignsMap);
                        System.DEBUG('campaignsToSelect Map : ' + campaignsToSelect);
                    }
                }
            }
        
            if(contactsToAddNoSpendAccounts.size() > 0){
                for(Contact eachContact : contactsToAddNoSpendAccounts){
                    if(!eachCampaignSetting.IsActive__c){
                        if(campaignsMap.get(campaignsToSelect.get(eachCampaignSetting.Tech_Name__c)) != null && String.valueOf(eachContact.Account.Date_changed_to_Current__c) == String.valueOf(System.today().addDays(-Integer.valueOf(eachCampaignSetting.WeeksActive__c) * 7))){
                            CampaignMember campaignMember = new CampaignMember(contactId = eachContact.Id, campaignId = campaignsMap.get(campaignsToSelect.get(eachCampaignSetting.Tech_Name__c)).Id);
                            campaignMembers.add(campaignMember);
                        }
                        System.DEBUG('Each Contact : ' + eachContact);
                        System.DEBUG('Campaigns Map : ' + campaignsMap);
                        System.DEBUG('campaignsToSelect Map : ' + campaignsToSelect);
                    }
                }
            }
        }
        if(campaignMembers.size() > 0){
            DataBase.upsert(campaignMembers,false);
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
    
}