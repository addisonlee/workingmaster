@isTest(seealldata=true)
Public Class LeadCompetitorTest{
public static testMethod void LeadCompetitorTest(){
    Lead led = new Lead();
    led.LastName = 'New Lead';
    led.Sales_Ledger__c = 'Sales Ledger';
    led.Rating = 'Cold';
    led.Status = 'Open';
    led.Phone = '9898989898';
    led.Payment_Type__c = 'BACS';
    insert led;
    
    Competitor__c comp = new Competitor__c();
    comp.Name = 'Test';
    insert comp;
    
    JNC_CompetitorLead__c JNCCompLead = new JNC_CompetitorLead__c();
    JNCCompLead.Competitor__c = comp.id; 
    JNCCompLead.Supplier_or_Competitor__c= 'Competitor'; 
    JNCCompLead.Lead__c = led.id; 
    insert JNCCompLead;
    PageReference ref = new PageReference('/apex/LeadCompetitor?id=' + led.id);
    Test.setCurrentPage(ref);
    LeadCompetitor lc = new LeadCompetitor();
    lc.SaveRecord();
     }
     }