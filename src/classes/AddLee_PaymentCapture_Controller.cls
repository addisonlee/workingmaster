public with sharing class AddLee_PaymentCapture_Controller {
    
    public Boolean isApexTestRunning { get; set; }
    public static Lead thisLeadRecord { get; set; }
    public static Boolean isApexTestRunning { get; set; }
    public static Account_Creation_Settings__c accountCreationSettings { get; set; }
    public String result {get;set;}
    public String successMessage {get;set;} 
    public String icon {get;set;}
    public Boolean showCreditCheckResult {get;set;}

    public AddLee_PaymentCapture_Controller (){
        String thisLeadId = ApexPages.currentPage().getParameters().get('id');
        Lead selectedLead = retreiveLead(thisLeadId);

        if(ApexPages.currentPage().getParameters().get('showresult') == '1')
        {
            showCreditCheckResult = true;
        }
        
        if(selectedLead.D_B_Recommendation__c == '1') {
            icon = 'check-circle';
            result = 'success';
            successMessage = 'Credit Check Successful';

        } else if(selectedLead.D_B_Recommendation__c != '1') {
            icon = 'exclamation-circle';
            result = 'warning';
            successMessage = 'Credit Check Unsuccessful';
        }
    }
    
    @RemoteAction
    public static Map<Object, Object> updateCardDetails(Map<String, String> cardDetails){
        Map<Object, Object> dto = new Map<Object, Object>();
        retreiveLead(cardDetails.get('recordId'));
        String cardHolderName = cardDetails.get('crdHldrName') == null ? '' : cardDetails.get('crdHldrName');
        String cardBillingAddr = cardDetails.get('crdBillAddr') == null ? '' : cardDetails.get('crdBillAddr');
        String cardLongNumber = cardDetails.get('crdNumber') == null ? '' : cardDetails.get('crdNumber');
        String cardExpiryDate = cardDetails.get('crdExpDate') == null ? '' : cardDetails.get('crdExpDate');
        
        if(validateCard(cardLongNumber).IsValid){
            thisLeadRecord.Card_Holder_Name__c = cardHolderName;
            thisLeadRecord.Billing_Address__c = cardBillingAddr;
            thisLeadRecord.Card_Long_Number__c = cardLongNumber;
            thisLeadRecord.Card_Expiry_Date__c = getCardExpirayDate(cardExpiryDate);
            thisLeadRecord.Bank_Details_Captured__c = true;
            thisLeadRecord.TECH_Bank_Encryption_Set__c = false;
            system.debug(thisLeadRecord);
            try{
                update thisLeadRecord;
            } catch (Exception e) {
                dto.put('sfSuccess', e);
                return dto;
            }
            dto.put('sfSuccess', true);
            return dto;
        }else{
            dto.put('sfSuccess', false);
            return dto;
        }
    }
    
    @RemoteAction
    public static Map<Object, Object> updateBankDetails(Map<String, String> cardDetails){
        system.debug('Card Details: '+cardDetails);
        Map<Object, Object> dto = new Map<Object, Object>();
        retreiveLead(cardDetails.get('recordId'));
        String bankAccName = cardDetails.get('bankAccName') == null ? '' : cardDetails.get('bankAccName');
        String bankAccSrtCode = cardDetails.get('bankSrtCode') == null ? '' : cardDetails.get('bankSrtCode');
        String bankAccNumber = cardDetails.get('bankAccNumber') == null ? '' : cardDetails.get('bankAccNumber');
        Addlee_PostcodeAnywhereBankValidity.Results result = validateBank(bankAccSrtCode, bankAccNumber);
        if(result.IsCorrect && result.IsDirectDebitCapable){
            thisLeadRecord.Bank_Account_Name__c = bankAccName;
            thisLeadRecord.Bank_Sort_Code__c = bankAccSrtCode;
            thisLeadRecord.Bank_Account_Number__c = bankAccNumber;
            thisLeadRecord.Bank__c = result.Bank;
            thisLeadRecord.Bank_Date_of_DD__c = System.today();
            thisLeadRecord.Bank_Address__c = result.ContactAddressLine1 + '\n' + result.ContactPostcode + '\n' + result.ContactPostTown;
            thisLeadRecord.Name_of_Approver__c = thisLeadRecord.FirstName + ' ' + thisLeadRecord.LastName;
            thisLeadRecord.Bank_Details_Captured__c = true;
            thisLeadRecord.TECH_Bank_Encryption_Set__c = false;
            update thisLeadRecord;     
            system.debug(thisLeadRecord);
            try{
                update thisLeadRecord;
            } catch (Exception e) {
                dto.put('sfSuccess', e);
                return dto;
            }
            dto.put('sfSuccess', true);
            return dto;
        }else{
            dto.put('sfSuccess', false);
            return dto;
        }
    }
    
    public static Addlee_PostcodeAnywhereBankValidity.Results validateBank(String srtCode, String accNumber){
        accountCreationSettings = Account_Creation_Settings__c.getInstance('credentials');
        Addlee_PostcodeAnywhereBankValidity.WebServiceSoap request = new Addlee_PostcodeAnywhereBankValidity.WebServiceSoap();
        Addlee_PostcodeAnywhereBankValidity.ArrayOfResults results = new Addlee_PostcodeAnywhereBankValidity.ArrayOfResults();
        if(isApexTestRunning != null && isApexTestRunning){
            Addlee_PostcodeAnywhereBankValidity.Results result = new Addlee_PostcodeAnywhereBankValidity.Results();
            result.IsCorrect = true;
            result.ContactAddressLine1 = 'TestaddressLine1';
            result.ContactPostcode = 'testpostcode';
            result.ContactPostTown = 'testPostTown';
            result.Bank = 'TestBank';
            result.IsDirectDebitCapable = true;
            results.Results = new List<Addlee_PostcodeAnywhereBankValidity.Results>();
            results.results.add(result);
        }else{
            results = request.validate( accountCreationSettings.pcaKey__c, accNumber, srtCode );  
        }
        return results.results[0];

    }
    
    public static Addlee_PostcodeAnywhereCardValidity.Results validateCard(String cardNumber){
        accountCreationSettings = Account_Creation_Settings__c.getInstance('credentials');
        Addlee_PostcodeAnywhereCardValidity.WebServiceSoap request = new Addlee_PostcodeAnywhereCardValidity.WebServiceSoap();
        Addlee_PostcodeAnywhereCardValidity.ArrayOfResults results = new Addlee_PostcodeAnywhereCardValidity.ArrayOfResults();
        if(isApexTestRunning != null && isApexTestRunning){
            Addlee_PostcodeAnywhereCardValidity.Results result = new Addlee_PostcodeAnywhereCardValidity.Results();
            result.IsValid = true;
            result.CardNumber = '4000 1234 1234 1234';
            result.CardType = 'VISA';
            results.Results = new List<Addlee_PostcodeAnywhereCardValidity.Results>();
            results.results.add(result);
        }else{
            results = request.validate( accountCreationSettings.pcaKey__c, cardNumber );  
        }
        return results.results[0];
    }
    
    public String getIsPersonalLead(){
        if( thisLeadRecord.Company == '' || thisLeadRecord.Company == null ){
            return 'display:none;';
        } else {
            return '';
        }
    }
    
    public static Lead retreiveLead(Id thisLeadId) {
        if( thisLeadRecord == null ){
            thisLeadRecord = [Select Name, FirstName, LastName, Street, Owner.Name, Company,
                               CompanyDunsNumber, PostalCode, State, Last_Credit_Checked_Date__c, D_B_Recommendation__c
                            From Lead
                            Where id=:thisLeadId limit 1];
        }
        return thisLeadRecord;
    }
    
    public Lead getLeadDetail() {
        return thisLeadRecord;
    }
    
    public string getThisMonth(){
        Date today = System.today();
        return String.valueOf(today.year())+'-'+String.valueOf(today.month());
    }
    
    public static Date getCardExpirayDate(String dateString) {
        Date firstDayOfMonth = date.valueOf(dateString + '-01');
        Integer numberOfDays = Date.daysInMonth(firstDayOfMonth.year(), firstDayOfMonth.month());
        Date lastDayOfMonth = Date.newInstance(firstDayOfMonth.year(), firstDayOfMonth.month(), numberOfDays);
        return lastDayOfMonth;
    }
    
    public String getInitVariables() {
        accountCreationSettings = Account_Creation_Settings__c.getInstance('credentials');
        Map<Object,Object> thisVar = new Map<Object,Object>();
        thisVar.put( 'pcaKey', accountCreationSettings.pcaKey__c );
        return JSON.serialize(thisVar);
    }
}