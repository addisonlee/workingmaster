public with sharing class AddLee_CaseAccountAssignment {

	public void assignAccount(List<Case> newObjects){
		Set<String> foriegnKeys = new Set<String>(); 
		Map<String,Id> foriegnKeysAccountsMap = new Map<String,Id>();
		for(Case eachCase : newObjects){
			if(eachCase.Web_Account_Number__c != null && eachCase.Web_Account_Number__c.length() > 0){
				foriegnKeys.add(eachCase.Web_Account_Number__c);
			}
		}
		System.DEBUG('foriegnKeys :' + foriegnKeys);
		System.DEBUG('foreign key size : ' + foriegnKeys.size());
		if(foriegnKeys != null){
			for(Account eachAccount : [SELECT Id, Account_Number__c FROM Account WHERE Account_Number__c in : foriegnKeys]){
				System.DEBUG(eachAccount);
				foriegnKeysAccountsMap.put(eachAccount.Account_Number__c,eachAccount.Id);
			}

			System.DEBUG(foriegnKeysAccountsMap);
			for(Case eachCase : newObjects){
				System.DEBUG(eachCase);
				if(eachCase.Web_Account_Number__c != null && eachCase.AccountId == null){
					eachCase.AccountId = foriegnKeysAccountsMap.get(eachCase.Web_Account_Number__c);
					System.DEBUG(eachCase);
				}//this section checks if we need to update a case with the account that is mentioned in the web account number
				//this can happen when a case is raised by a contact in an account, but needs to be related to another account(the account that the email was sent too)
				else if(eachCase.Web_Account_Number__c != null && AddLee_EmailMessageHelper.isUpdatedInEmailMessageTrigger != null && AddLee_EmailMessageHelper.isUpdatedInEmailMessageTrigger && eachCase.AccountId != null){
					eachCase.AccountId = foriegnKeysAccountsMap.get(eachCase.Web_Account_Number__c);
					eachCase.ContactId = null;
					System.DEBUG('Test account assignment : ' + eachCase);
				}
			}
		}
	}
}