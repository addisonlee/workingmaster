public with sharing class AddLee_ContextVariables {
	public static boolean firstRunLead = true;
	public static boolean firstRunAccount = true; 	
	public static Map<String,String> postCodeMap;
	public static Map<Id,Account> parentAccountMap;
	public static Map<String, Map<String, String>> employeeSizeBandMap;
	public static Map<String, Map<String, Decimal>> activeUsersMap;
	public static Map<String, Map<String, Decimal>> expectedExpenditureSpendMap;
	public static Map<String, TECH_Auto_Number__c> currentAccountNumbersMap;
	public static Blob cryptoKey;
	public static Map<String,Integer> bandingValues;
	public static Map<ID, Schema.RecordTypeInfo> recordTypeMap;
	// Added by Manish
	public static boolean firstRunLeadAfterUpdate = true;
}