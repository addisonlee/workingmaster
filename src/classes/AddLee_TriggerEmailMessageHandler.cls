public with sharing class AddLee_TriggerEmailMessageHandler {

    /**
    * @author Prateek Gandhi
    * @description Checks for Account: if no account is associated with the case associated with the emailMessage and
    *  this email is the only one through which the case is raised then the records are processed to associate the Account
    */    
    
    public String feedBack = 'feedback received, job';
    
    public void onAfterInsert(Final Map<id,EmailMessage> newEmails) {
        Map<Id,Id> relatedCaseEmailMessageMap= new Map<Id,Id>();
		Map<Id,Case> EmailMessageIdCaseMap = new Map<Id,Case> ();
		Map<Id, EmailMessage> mapCIdEMesgFeedback =  new Map<Id, EmailMessage>();
		System.DEBUG('New Emails : ' + newEmails);
        for(EmailMessage email : newEmails.values()){
            if(String.valueOf(email.parentId).startsWith('500') && email.Incoming){
                relatedCaseEmailMessageMap.put(email.parentId,email.Id);
            }
        }
        List<Case> casesEmail = [select id,AccountId,Account_Number__c,Web_Account_Number__c, 
                                        Driver_Rating__c, Overall_Rating__c, External_Id__c,
                                        Subject, Job_Number__c, Send_Feedback_Comment_Emial__c,
                                        (select id,parentId 
                                          from EmailMessages 
                                          where Incoming=true AND Id NOT IN :newEmails.keyset()) 
                                 from Case 
                                 Where id IN :relatedCaseEmailMessageMap.keyset()];
        System.DEBUG(' casesEmail : ' + casesEmail);
        for(Case caseRec: casesEmail){
            if(caseRec.EmailMessages.Size()==0){ //&& caseRec.AccountId == null){// checks whether this is the email through which the case is raised
                EmailMessageIdCaseMap.put(relatedCaseEmailMessageMap.get(caseRec.Id),caseRec);
                for(EmailMessage email : newEmails.values()){
                    system.debug('MTDebug email.Subject : ' + email.Subject);
                    if(email.Subject != null && email.Subject.containsIgnoreCase(feedBack)){
                        if(String.valueOf(email.parentId).startsWith('500') && email.parentId == caseRec.Id){
                            mapCIdEMesgFeedback.put(email.parentId, email);
                        }
                    }
                }
            }
        }
        System.DEBUG(' EmailMessageIdCaseMap : ' + EmailMessageIdCaseMap);
        if(EmailMessageIdCaseMap.size()>0){
            AddLee_EmailMessageHelper.AssignAccountAndFeedbackRollups(EmailMessageIdCaseMap, newEmails, mapCIdEMesgFeedback);           
        }
    }
}