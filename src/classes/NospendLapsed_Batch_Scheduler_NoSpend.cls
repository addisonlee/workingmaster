/*
 **  **Description : schedular class for 'CalculateLapsedNoSpendOnAccount' class to create campaign members for no spend campaigns
     
*/	
global class NospendLapsed_Batch_Scheduler_NoSpend implements Schedulable  {

 global void execute(SchedulableContext SC){
     CalculateLapsedNoSpendOnAccount csa = new CalculateLapsedNoSpendOnAccount();
     csa.Query = 'select id,Date_of_Last_Booking__c,Consumer_Account_Number__c,Name,Grading__c,Account_Status__c,Date_changed_to_Current__c,IsPersonAccount,Marketing_Status__c,Sales_Ledger__c,Account_Managed__c,Total_Amount_of_Bookings__c from Account where Consumer_Account_Number__c != \'1\' and Reason_for_opening_account__c = \'Business\' and Date_of_Last_Booking__c = null';
     Database.executeBatch(csa);
 }	 	
	 	
}