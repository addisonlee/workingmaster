@isTest
private class AddLee_TriggerCaseCommentHandlerTest {
    
    @testSetup 
    static void setupMethod() {
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Disable_Trigger_Validation__c disabletrigger = new Disable_Trigger_Validation__c(SetupOwnerId = UserInfo.getUserID(), Bypass_Trigger_And_Validation_Rule__c = false);
     	insert disabletrigger;
        NoValidations__c noValidation = new NoValidations__c(SetupOwnerId = UserInfo.getUserID(), Active_Users__c = true);
     	insert noValidation;
	}
    
    static testMethod void NegativeCaseWithNoComment() {
        Map<String, Schema.SObjectType> sobjectSchemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType sObjType = sobjectSchemaMap.get('case') ;
        Schema.DescribeSObjectResult cfrSchema = sObjType.getDescribe() ;
        Map<String,Schema.RecordTypeInfo> RecordTypeInfo = cfrSchema.getRecordTypeInfosByName();
    	Id caseRecordTypeId = RecordTypeInfo.get('Reactive Cases').getRecordTypeId();
        
        List<Contact> con = AddLee_Trigger_Test_Utils.createContacts(1);
        
        List<Case> casesToInsert = new List<Case>();
        casesToInsert.add(new Case(Status='New', Origin='Email', Subject='Test Subject',RecordTypeId=caseRecordTypeId,SuppliedEmail='test@test.com', ContactId=con[0].Id ));
        insert casesToInsert;
        
        string htmlBody = 'Job: #368180 2016-04-13 16:30';

        htmlBody += 'Name: Seona Bell';
        htmlBody += '<a href="mailto: dusan.zivkovic1@me.com">dusan.zivkovic@me.com</a>';
        htmlBody += 'Phone: 07856025631';
        htmlBody += 'Pick Up: Andrew Duffus Ltd, Unit 4/A, 2-4 Orsman Road, London, N1 5QJ';
        htmlBody += 'Drop Off: Bell Staff Clothing, 11-15 Emerald Street, London, WC1N 3QL';
        htmlBody += 'Driver Rating: &#9733;&#9733; ';
        htmlBody += 'Overall Rating: &#9733;&#9733; ';
        htmlBody += 'Comments:  </strong> <br/>\n';
        htmlBody += '                     <br/>';

        List<EmailMessage> emailMessages= New List<EmailMessage>();
        emailMessages.add(new EmailMessage(ParentId=casesToInsert[0].Id, 
                                           ToAddress='example0@testclass.com', 
                                           FromAddress='example0@testclass.com', 
                                           Incoming = true, 
                                           Subject = 'Addison Lee Feedback. Job No 1114512',
                                           HtmlBody = htmlBody
                                          ));
        insert emailMessages;
        
        System.debug('MTDebug emailMessages : ' + emailMessages[0].HtmlBody);
        test.startTest();
        CaseComment com = new CaseComment();
        com.ParentId = casesToInsert[0].Id;
        com.CommentBody= 'comment';
        insert com;
        test.stopTest();
        
        Case caseToVerify = [Select Id, Status, External_Id__c, FeedBackEmail__c From Case Limit 1];
        system.assertEquals('Open', caseToVerify.Status);
        system.assertEquals(null, caseToVerify.External_Id__c);
     }
}