global class BatchRunReports implements Database.Batchable<sObject>, Database.Stateful{
    
     //Map<String, Nps_Report__c> nsp;
       //Map<String, Nps_Report__c> nsp;
    public Map<String,String> mapIdpert=new Map<String,String>();
    global List<Nps_Report__c> nsp;
    public List<NPS__c> lstnps=new List<NPS__c>();
    public Map<Id,List<Id>> mapContactbookingSummIds=new Map<Id,List<Id>>();
    public List<String> reportNames =new List<String>();
    public boolean haspercent =false;
    public integer npsreportpert;
    global BatchRunReports()
    {
        nsp= Nps_Report__c.getAll().values(); // Custom setting 
        //List<Nps_Report__c> lstnps=Nps_Report__c.getAll().values();
        for(Nps_Report__c nreport:nsp)
            {
                mapIdpert.put(nreport.Report_Name__c,nreport.No_of_records_Percentage__c);
                reportNames.add(nreport.Report_Name__c);
            }
        System.debug(nsp);
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator('Select DeveloperName,FolderName,Format,Id,Name from Report Where Name IN : reportNames');
    }
    
    global void execute(Database.BatchableContext BC, List<sobject> scope){
        
        // Run a report synchronously
        
        List<Batch_Logs__c> lstBatchLogs=new List<Batch_Logs__c>();
        
        for(Sobject repot:scope)
        {
            Report rpt=(Report)repot;
            Batch_Logs__c log=new Batch_Logs__c();
            log.Report_Name__c = rpt.Name;
            
            
            Reports.reportResults results = Reports.ReportManager.runReport(rpt.Id, true);
            if(rpt.Format == 'Summary') // Summary report
            {
                List<String> lstdetailcolumns=results.getReportMetadata().getDetailColumns();
                integer columnvalue=0;
                integer bookingsummcolumnvalue=0;
                for(Integer j=0;j<lstdetailcolumns.size();j++)
                {
                    if(lstdetailcolumns[j] =='Account.Name')
                    {
                        columnvalue =j;
                    }
                    if(lstdetailcolumns[j] =='booking_Summary__c.Name' || lstdetailcolumns[j] == 'CUST_NAME')
                    {
                        bookingsummcolumnvalue =j;
                    }
                }
                Reports.Dimension dim = results.getGroupingsDown();
                Reports.ReportFactWithDetails detailFact = (Reports.ReportFactWithDetails)results.getFactMap().get('T!T');
                
                Map<Id,List<Id>> mapAccountContactIds=new Map<Id,List<Id>>();
                Map<Id,List<Id>> mapbookingSummContactIds=new Map<Id,List<Id>>();
                Integer recordSize;
                if(mapIdpert.get(rpt.Name).contains('%')) // Segmenation based on % calcualtion based on report or No of records given as number
                {
                    recordSize=(Integer.valueOf(detailFact.getAggregates()[0].getvalue())*Integer.valueOf(mapIdpert.get(rpt.Name).removeEnd('%')))/100;
                }else{
                    recordSize =Integer.valueOf(mapIdpert.get(rpt.Name));
                }
                System.debug(recordSize);
                
               log.Criteria__c=mapIdpert.get(rpt.Name);
                 //log.Criteria__c='20%';
                log.Total_in_the_report__c = Integer.valueOf(detailFact.getAggregates()[0].getvalue());
                log.Segmentation_no_of_reports__c =recordSize;
                lstBatchLogs.add(log);
                for(Integer i=0;i<dim.getGroupings().size();i++)
                {
                    Reports.GroupingValue groupingVal = dim.getGroupings()[i];
                    String factMapKey = groupingVal.getKey() + '!T';
                    Reports.ReportFactWithDetails factDetails =(Reports.ReportFactWithDetails)results.getFactMap().get(factMapKey);
                    for(Reports.ReportDetailRow rows:factDetails.getRows())
                    {
                        if(recordSize > mapAccountContactIds.size() ){
                            Id contactId=(Id)groupingVal.getValue();
                            Id accountId=(Id)rows.getDataCells()[columnvalue].getValue();
                            if(mapAccountContactIds.containsKey(contactId))
                            {
                                List<Id> lstaccount=mapAccountContactIds.get(contactId);
                                lstaccount.add(accountId);
                                mapAccountContactIds.put(contactId,lstaccount);
                            }else{
                                mapAccountContactIds.put(contactId,new List<Id>{accountId});
                            }
                        }
                        if(recordSize > mapbookingSummContactIds.size() ){
                            Id contactId=(Id)groupingVal.getValue();
                            Id bookingSummId=(Id)rows.getDataCells()[bookingsummcolumnvalue].getValue();
                            if(mapbookingSummContactIds.containsKey(contactId))
                            {
                                List<Id> lstbooksum=mapbookingSummContactIds.get(contactId);
                                lstbooksum.add(bookingSummId);
                                mapbookingSummContactIds.put(contactId,lstbooksum);
                            }else{
                                mapbookingSummContactIds.put(contactId,new List<Id>{bookingSummId});
                            }
                        }    
                    }
                }
                
                if(!mapAccountContactIds.isEmpty()) // Creating NPS Records and assigning values
                {
                    
                    for(Id contactId:mapAccountContactIds.keySet())
                    {
                        NPS__c nps=new NPS__c();
                        nps.Contact__c =contactId;
                        nps.External_Id__c=getExternalId();
                        nps.Date_Sent__c=date.Today();
                        nps.Account__c =mapAccountContactIds.get(contactId)[0];
                        lstnps.add(nps);
                    }
                }
                
                if(!mapbookingSummContactIds.isEmpty())
                {
                    mapContactbookingSummIds.putAll(mapbookingSummContactIds);
                }
            }
            
            if(rpt.Format == 'Tabluar') // Tabular report segmentation
            {
                for(String data:results.getFactMap().keySet())
                {
                    Reports.ReportFactWithDetails factDetails =(Reports.ReportFactWithDetails)results.getFactMap().get(data);
                    //for(Reports.ReportDetailRow detailRow:factDetails.getRows())
                    Integer recordSize;
                    if(mapIdpert.get(rpt.Name).contains('%'))
                    {
                        recordSize=(Integer.valueOf(factDetails.getAggregates()[0].getLabel())*Integer.valueOf(mapIdpert.get(rpt.Name).removeEnd('%')))/100;
                    }else{
                        recordSize =Integer.valueOf(mapIdpert.get(rpt.Name));
                    }
                    
                    for(integer i=0;i<recordSize;i++)
                    {   
                        if(lstnps.size() >= recordSize)
                        {
                            break;
                        }
                        
                        NPS__c nps=new NPS__c(); 
                        for(Reports.ReportDataCell detailCell:factDetails.getRows()[i].getDataCells())
                        {
                            if(detailCell.getlabel().startswith('003')) // Getting contactid
                            {
                                nps.Contact__c =(Id)(detailCell.getvalue());
                            }
                            if(detailCell.getlabel().startswith('001'))// Getitng accountid
                            {
                                nps.Account__c =(Id)detailCell.getvalue();
                            }
                            if(detailCell.getlabel().startswith(booking_Summary__c.sObjectType.getDescribe().getKeyPrefix())){
                                mapContactbookingSummIds.put(nps.Contact__c,new List<Id>{(Id)detailCell.getvalue()});
                            }
                        }
                        nps.External_Id__c=getExternalId();
                        lstnps.add(nps);
                    }
                }
            }
        }
        
        if(!lstBatchLogs.isEmpty())
        {
            insert lstBatchLogs;
        }
    }
    
    global void finish(Database.BatchableContext BC) //External id random creation on NPS record
    {
        System.debug(lstnps.size());
        System.debug(mapContactbookingSummIds);
        System.debug(mapContactbookingSummIds.size());
        
        if(!lstnps.isEmpty() && !Test.isRunningTest())
        {
            Database.executeBatch(new BatchCreateNPSRecords(lstnps,mapContactbookingSummIds),200);
        }
    }
    
    private static String getExternalId(){
        Integer len = 14;
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String randStr = '';
        while (randStr.length() < len) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), 62);
            randStr += chars.substring(idx, idx+1);
        }
        return randStr; 
    }
    
}