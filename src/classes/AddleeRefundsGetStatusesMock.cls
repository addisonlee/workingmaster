@isTest
global class AddleeRefundsGetStatusesMock implements webservicemock {
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.statusResponse respElement =  new Addlee_refunds.statusResponse();
       respElement.statuses = new List<Addlee_refunds.status>();
               Addlee_refunds.status testStatus = new Addlee_refunds.status();
               testStatus.code = '546465';
               testStatus.id = 655675;
               testStatus.name = 'test';
         respElement.statuses.add(testStatus);
       response.put('response_x', respElement); 
   }
    
}