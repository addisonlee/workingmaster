@isTest
private class ACPTNCTriggerHandlerTest {
	
	static testMethod void tncCreated_Test() {
		
		AddLee_Trigger_Test_Utils.insertCustomSettings();
    	insert TestDataFactory.createTestLead('Test Lead');
    	Lead testLead = [Select id, T_C_Sent__c from Lead limit 1];
    	insert TestDataFactory.createTnC(testLead);
    	AddLee_Terms_Conditions__c newTNC = [Select id, Internal_Id__c, External_Id__c, Response__c from AddLee_Terms_Conditions__c limit 1];
		List<AddLee_Terms_Conditions__c> newTNCs = new List<AddLee_Terms_Conditions__c>{newTNC};
		Test.startTest();
			ACPTNCTriggerHandler handler = new ACPTNCTriggerHandler();
			handler.tncCreated(newTNCs);
			//system.assertEquals(testLead.T_C_Sent__c, true);
		Test.stopTest();
		
	}
	
	static testMethod void tncUpdated_Test() {
		
		AddLee_Trigger_Test_Utils.insertCustomSettings();
    	insert TestDataFactory.createTestLead('Test Lead');
    	Lead testLead = [Select id, T_C_Sent__c from Lead limit 1];
    	insert TestDataFactory.createTnC(testLead);
    	AddLee_Terms_Conditions__c newTNC = [Select id, Internal_Id__c, External_Id__c, Response__c
    											from AddLee_Terms_Conditions__c limit 1];
    	newTNC.Response__c = 'Accepted';										
    	AddLee_Terms_Conditions__c oldTNC = [Select id, Internal_Id__c, External_Id__c, Response__c
    											from AddLee_Terms_Conditions__c limit 1];
    	oldTNC.Response__c = 'Rejected';
		List<AddLee_Terms_Conditions__c> newTNCs = new List<AddLee_Terms_Conditions__c>{newTNC};
		List<AddLee_Terms_Conditions__c> oldTNCs = new List<AddLee_Terms_Conditions__c>{oldTNC};
		Map<Id, AddLee_Terms_Conditions__c> newTNCMap = new Map<Id, AddLee_Terms_Conditions__c>(newTNCs);
		Map<Id, AddLee_Terms_Conditions__c> oldTNCMap = new Map<Id, AddLee_Terms_Conditions__c>(oldTNCs);
		system.debug('$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$');

		Test.startTest();
			ACPTNCTriggerHandler handler = new ACPTNCTriggerHandler();
			handler.tncUpdated(oldTNCMap, newTNCMap);
			system.assertEquals('', '');
		Test.stopTest();
		
	}
	
}