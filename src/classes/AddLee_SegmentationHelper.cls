public with sharing class AddLee_SegmentationHelper {

    public static String[] nonOverwritableSegments = new String[]{'Very High', 'Very Low', 'Deprioritise'};
    public static Set<String> nonOverwritableSegmentsSet = new Set<String>(nonOverwritableSegments);
    
    public static List<String> getAccountIndustrySegments(){
        Schema.DescribeFieldResult fieldResult = Account.Industry_Sub_Sector__c.getDescribe();
        List<String> industrySegments  = new List<String>();
        List<Schema.PicklistEntry> picklistEntry = fieldResult.getPicklistValues();
           for( Schema.PicklistEntry eachPicklistEntry : picklistEntry){
               
               industrySegments.add(eachPicklistEntry.getValue());
               
           }
        return industrySegments;
    }
    
    public static List<String> getLeadIndustrySegments(){
        Schema.DescribeFieldResult fieldResult = Lead.Industry_Sub_Sector__c.getDescribe();
        List<String> industrySegments  = new List<String>();
        List<Schema.PicklistEntry> picklistEntry = fieldResult.getPicklistValues();
           for( Schema.PicklistEntry eachPicklistEntry : picklistEntry){
               
               industrySegments.add(eachPicklistEntry.getValue());
               
           }
        return industrySegments;
    }


    public static Map<String, TECH_Auto_Number__c> populateCurrentAccountNumbersRange(){
        Map<String, TECH_Auto_Number__c> currentAccountNumbersMap = new Map<String, TECH_Auto_Number__c>();
        for(TECH_Auto_Number__c eachAccountNumber : [SELECT Name, Current_Number__c, End_Number__c, Start_Number__c FROM TECH_Auto_Number__c WHERE NAME =: 'WestOneRange' OR NAME =: 'AddLeeRange']){
            currentAccountNumbersMap.put(eachAccountNumber.NAME, eachAccountNumber);
            System.DEBUG(logginglevel.Error, 'Current Account Number : ' + eachAccountNumber);
        }
        System.DEBUG(logginglevel.Error, 'Current Account Number Map: ' + currentAccountNumbersMap);

        return currentAccountNumbersMap;
    }

    public static Map<ID, Schema.RecordTypeInfo> getRecordTypeInfos(){
        return Schema.SObjectType.Account.getRecordTypeInfosById();
    }

    public static Map<String, String> getPostCodesMap(){
        Map<String,String> postCodeMap = new Map<String, String>();
        //List<Post_Code_Lookup__c> postcodes = ;//WHERE Inner_Outer__c != 'Outside'
        for(Post_Code_Lookup__c eachLookup : [Select Inner_Outer__c, PostCode__c FROM Post_Code_Lookup__c LIMIT 2000]){
            postCodeMap.put(eachLookup.PostCode__c, eachLookup.Inner_Outer__c);
        }
        return postCodeMap;
    }
    
    public static Map<String, Map<String,String>> getEmployeeSizeBand(List<String> industrySegments){
        
        Map<String, Map<String, String>> employeeBandsMap = new Map<String, Map<String, String>>();
        //List<Swim_Lane__c> employeeSizeBands = 
        for(Swim_Lane__c eachBand : [SELECT e.X6_to_10__c, e.X50_to_99__c, e.X500_to_999__c, e.X20_to_49__c, 
                                                            e.X200_to_499__c, e.X1_to_5__c, e.X11_to_19__c, e.X100_to_199__c, e.X1000__c, 
                                                            e.Sole_Trader__c, e.Industry_Type__c, e.Id FROM Swim_Lane__c e WHERE e.Industry_Type__c in : industrySegments]){
            if(employeeBandsMap.get(eachBand.Industry_Type__c) == null){
                employeeBandsMap.put(eachBand.Industry_Type__c, new Map<String, String>());
                employeeBandsMap.get(eachBand.Industry_Type__c).put('1-5',eachBand.X1_to_5__c);
                employeeBandsMap.get(eachBand.Industry_Type__c).put('6-10',eachBand.X6_to_10__c);
                employeeBandsMap.get(eachBand.Industry_Type__c).put('11-19',eachBand.X11_to_19__c);
                employeeBandsMap.get(eachBand.Industry_Type__c).put('20-49',eachBand.X20_to_49__c);
                employeeBandsMap.get(eachBand.Industry_Type__c).put('50-99',eachBand.X50_to_99__c);
                employeeBandsMap.get(eachBand.Industry_Type__c).put('100-199',eachBand.X100_to_199__c);
                employeeBandsMap.get(eachBand.Industry_Type__c).put('200-499',eachBand.X200_to_499__c);
                employeeBandsMap.get(eachBand.Industry_Type__c).put('500-999',eachBand.X500_to_999__c);
                employeeBandsMap.get(eachBand.Industry_Type__c).put('>1000',eachBand.X1000__c);
            }
        }
        return employeeBandsMap;
    }   
    
    public static Map<String,Map<String,Decimal>> getActiveUsersSegment(List<String> industrySegments){
        Map<String, Map<String, Decimal>> activeUsersEstimatedSpendMap = new Map<String, Map<String, Decimal>>();
        //List<Active_Users__c> activeUsers = ;
        for(Active_Users__c eachBand : [SELECT a.X6_to_10__c, a.X50_to_99__c, a.X500_to_999__c, a.X20_to_49__c, 
                                                        a.X200_to_499__c, a.X1_to_5__c, a.X11_to_19__c, a.X100_to_199__c, a.X1000__c, a.Type__c, 
                                                        a.TECH_Unique_Industry__c, a.TECH_Industry_Type__c, a.Sole_Trader__c, 
                                                        a.Industry_Type__c FROM Active_Users__c a WHERE a.Industry_Type__c in : industrySegments]){
            if(activeUsersEstimatedSpendMap.get(eachBand.TECH_Unique_Industry__c) == null){
                activeUsersEstimatedSpendMap.put(eachBand.TECH_Unique_Industry__c, new Map<String, Decimal>());
                activeUsersEstimatedSpendMap.get(eachBand.TECH_Unique_Industry__c).put('1-5',eachBand.X1_to_5__c);
                activeUsersEstimatedSpendMap.get(eachBand.TECH_Unique_Industry__c).put('6-10',eachBand.X6_to_10__c);
                activeUsersEstimatedSpendMap.get(eachBand.TECH_Unique_Industry__c).put('11-19',eachBand.X11_to_19__c);
                activeUsersEstimatedSpendMap.get(eachBand.TECH_Unique_Industry__c).put('20-49',eachBand.X20_to_49__c);
                activeUsersEstimatedSpendMap.get(eachBand.TECH_Unique_Industry__c).put('50-99',eachBand.X50_to_99__c);
                activeUsersEstimatedSpendMap.get(eachBand.TECH_Unique_Industry__c).put('100-199',eachBand.X100_to_199__c);
                activeUsersEstimatedSpendMap.get(eachBand.TECH_Unique_Industry__c).put('200-499',eachBand.X200_to_499__c);
                activeUsersEstimatedSpendMap.get(eachBand.TECH_Unique_Industry__c).put('500-999',eachBand.X500_to_999__c);
                activeUsersEstimatedSpendMap.get(eachBand.TECH_Unique_Industry__c).put('>1000',eachBand.X1000__c);
            }
        }
        return activeUsersEstimatedSpendMap;
    }
    
    public static Map<String,Map<String,Decimal>> getExpectedExpenditure(List<String> industrySegments){
        Map<String, Map<String, Decimal>> expectedExpenditureSpendMap = new Map<String, Map<String, Decimal>>();
        //List<Spend_Per_Employee__c> expectedExpenditure = ;
        for(Spend_Per_Employee__c eachBand : [SELECT a.X6_to_10__c, a.X50_to_99__c, a.X500_to_999__c, a.X20_to_49__c, 
                                                        a.X200_to_499__c, a.X1_to_5__c, a.X11_to_19__c, a.X100_to_199__c, a.X1000__c, a.Type__c, 
                                                        a.TECH_Unique_Industry__c, a.TECH_Industry_Type__c, a.Sole_Trader__c, 
                                                        a.Industry_Type__c FROM Spend_Per_Employee__c a WHERE a.Industry_Type__c in : industrySegments]){
            if(expectedExpenditureSpendMap.get(eachBand.TECH_Unique_Industry__c) == null){
                expectedExpenditureSpendMap.put(eachBand.TECH_Unique_Industry__c, new Map<String, Decimal>());
                expectedExpenditureSpendMap.get(eachBand.TECH_Unique_Industry__c).put('1-5',eachBand.X1_to_5__c);
                expectedExpenditureSpendMap.get(eachBand.TECH_Unique_Industry__c).put('6-10',eachBand.X6_to_10__c);
                expectedExpenditureSpendMap.get(eachBand.TECH_Unique_Industry__c).put('11-19',eachBand.X11_to_19__c);
                expectedExpenditureSpendMap.get(eachBand.TECH_Unique_Industry__c).put('20-49',eachBand.X20_to_49__c);
                expectedExpenditureSpendMap.get(eachBand.TECH_Unique_Industry__c).put('50-99',eachBand.X50_to_99__c);
                expectedExpenditureSpendMap.get(eachBand.TECH_Unique_Industry__c).put('100-199',eachBand.X100_to_199__c);
                expectedExpenditureSpendMap.get(eachBand.TECH_Unique_Industry__c).put('200-499',eachBand.X200_to_499__c);
                expectedExpenditureSpendMap.get(eachBand.TECH_Unique_Industry__c).put('500-999',eachBand.X500_to_999__c);
                expectedExpenditureSpendMap.get(eachBand.TECH_Unique_Industry__c).put('>1000',eachBand.X1000__c);
            }
        }
        return expectedExpenditureSpendMap;
    }

    public static Map<String,Integer> getBandingValues(){
        List<Segmentation_London_Employees_Banding__c> segmentationBandingSettings = Segmentation_London_Employees_Banding__c.getAll().Values();
        Map<String,Integer> bandingValuesMap = new Map<String,Integer>();
        for(Segmentation_London_Employees_Banding__c eachSegmentationBanding : segmentationBandingSettings){
            bandingValuesMap.put(eachSegmentationBanding.Number_of_London_Employees__c,Integer.Valueof(eachSegmentationBanding.London_Employee_Value__c));
        }
        return bandingValuesMap;
    }
    
    public static void allocateBandingValues(List<Lead> leadTriggerList, Map<String,Integer> bandingValues){
        for(Lead eachLead : leadTriggerList){
            if(eachLead.No_of_London_Employees__c != null && eachLead.Total_London_Employees__c == null){
                eachLead.Total_London_Employees__c = bandingValues.get(eachLead.No_of_London_Employees__c);
            }else if(eachLead.Total_London_Employees__c != null){
                if(eachLead.Total_London_Employees__c > 0 && eachLead.Total_London_Employees__c <=5 ){
                    eachLead.No_of_London_Employees__c = '1-5';
                }else if(eachLead.Total_London_Employees__c > 5 && eachLead.Total_London_Employees__c <=10 ){
                    eachLead.No_of_London_Employees__c = '6-10';
                }else if(eachLead.Total_London_Employees__c > 10 && eachLead.Total_London_Employees__c <=19 ){
                    eachLead.No_of_London_Employees__c = '11-19';
                }else if(eachLead.Total_London_Employees__c > 19 && eachLead.Total_London_Employees__c <=49 ){
                    eachLead.No_of_London_Employees__c = '20-49';
                }else if(eachLead.Total_London_Employees__c > 49 && eachLead.Total_London_Employees__c <=99 ){
                    eachLead.No_of_London_Employees__c = '50-99';
                }else if(eachLead.Total_London_Employees__c > 99 && eachLead.Total_London_Employees__c <=199 ){
                    eachLead.No_of_London_Employees__c = '100-199';
                }else if(eachLead.Total_London_Employees__c > 199 && eachLead.Total_London_Employees__c <=499 ){
                    eachLead.No_of_London_Employees__c = '200-499';
                }else if(eachLead.Total_London_Employees__c > 499 && eachLead.Total_London_Employees__c <=999 ){
                    eachLead.No_of_London_Employees__c = '500-999';
                }else if(eachLead.Total_London_Employees__c > 0 && eachLead.Total_London_Employees__c <=1000 ){
                    eachLead.No_of_London_Employees__c = '>1000';
                }
            }
        }
    }

    public static void allocateBandingValuesAccount(List<Account> accountTriggerList, Map<String,Integer> bandingValues){
        for(Account eachAccount : accountTriggerList){
            if(eachAccount.No_of_London_Employees__c != null && eachAccount.Total_London_Employees__c == null){
                eachAccount.Total_London_Employees__c = bandingValues.get(eachAccount.No_of_London_Employees__c);
            }else if(eachAccount.Total_London_Employees__c != null){
                if(eachAccount.Total_London_Employees__c > 0 && eachAccount.Total_London_Employees__c <=5 ){
                    eachAccount.No_of_London_Employees__c = '1-5';
                }else if(eachAccount.Total_London_Employees__c > 5 && eachAccount.Total_London_Employees__c <=10 ){
                    eachAccount.No_of_London_Employees__c = '6-10';
                }else if(eachAccount.Total_London_Employees__c > 10 && eachAccount.Total_London_Employees__c <=19 ){
                    eachAccount.No_of_London_Employees__c = '11-19';
                }else if(eachAccount.Total_London_Employees__c > 19 && eachAccount.Total_London_Employees__c <=49 ){
                    eachAccount.No_of_London_Employees__c = '20-49';
                }else if(eachAccount.Total_London_Employees__c > 49 && eachAccount.Total_London_Employees__c <=99 ){
                    eachAccount.No_of_London_Employees__c = '50-99';
                }else if(eachAccount.Total_London_Employees__c > 99 && eachAccount.Total_London_Employees__c <=199 ){
                    eachAccount.No_of_London_Employees__c = '100-199';
                }else if(eachAccount.Total_London_Employees__c > 199 && eachAccount.Total_London_Employees__c <=499 ){
                    eachAccount.No_of_London_Employees__c = '200-499';
                }else if(eachAccount.Total_London_Employees__c > 499 && eachAccount.Total_London_Employees__c <=999 ){
                    eachAccount.No_of_London_Employees__c = '500-999';
                }else if(eachAccount.Total_London_Employees__c > 0 && eachAccount.Total_London_Employees__c <=1000 ){
                    eachAccount.No_of_London_Employees__c = '>1000';
                }
            }
        }
    }

    public static void allocateSegmentsAccount(List<Account> accountsTriggerList, Map<String, Map<String, String>> employeeSizeBandMap){
        for(Account eachAccount : accountsTriggerList){
            if((eachAccount.Industry_Sub_Sector__c != null && employeeSizeBandMap.get(eachAccount.Industry_Sub_Sector__c) != null) 
                && (eachAccount.Inner_Outer__c.equalsIgnoreCase('Inner London') || eachAccount.Inner_Outer__c.equalsIgnoreCase('Outer London'))){
                if(!nonOverwritableSegmentsSet.contains(eachAccount.Segment__c))
                    eachAccount.Segment__c = employeeSizeBandMap.get(eachAccount.Industry_Sub_Sector__c).get(eachAccount.No_of_London_Employees__c);
            }
            else{
                eachAccount.Segment__c = '';
            }
        }
    }
    
    public static void allocateSegmentsLead(List<Lead> leadsTriggerList, Map<String, Map<String, String>> employeeSizeBandMap){
        for(Lead eachLead : leadsTriggerList){
            if((eachLead.Industry_Sub_Sector__c != null && employeeSizeBandMap.get(eachLead.Industry_Sub_Sector__c) != null) 
                && (eachLead.Inner_Outer__c.equalsIgnoreCase('Inner London') || eachLead.Inner_Outer__c.equalsIgnoreCase('Outer London'))){
                if(!nonOverwritableSegmentsSet.contains(eachLead.Segment__c))
                    eachLead.Segment__c = employeeSizeBandMap.get(eachLead.Industry_Sub_Sector__c).get(eachLead.No_of_London_Employees__c);
            }
            else{
                eachLead.Segment__c = '';
            }
        }
    }
        
    public static void allocateActiveUsersEstimatesForAccount(List<Account> accountsTriggerList, Map<String,Map<String,Decimal>> activeUsersEstimatedSpendMap){
        String targetSpendKey = 'Target Users';
        String expectedSpendKey = 'Expected Users';
        System.Debug('accountsTriggerList : ' + accountsTriggerList);
        System.debug('activeUsersEstimatedSpendMap : ' + activeUsersEstimatedSpendMap);
        for(Account eachAccount : accountsTriggerList){
            if((eachAccount.Industry_Sub_Sector__c != null && activeUsersEstimatedSpendMap.get(eachAccount.Industry_Sub_Sector__c+'#'+expectedSpendKey) != null && activeUsersEstimatedSpendMap.get(eachAccount.Industry_Sub_Sector__c+'#'+targetSpendKey) != null) 
                && (eachAccount.Inner_Outer__c.equalsIgnoreCase('Inner London') || eachAccount.Inner_Outer__c.equalsIgnoreCase('Outer London'))){
                eachAccount.Expected_Active_Users__c = activeUsersEstimatedSpendMap.get(eachAccount.Industry_Sub_Sector__c+'#'+expectedSpendKey).get(eachAccount.No_of_London_Employees__c);
                eachAccount.Target_Active_Users__c = activeUsersEstimatedSpendMap.get(eachAccount.Industry_Sub_Sector__c+'#'+targetSpendKey).get(eachAccount.No_of_London_Employees__c);
            }
        }
    }
    
    public static void allocateActiveUsersEstimatesForLead(List<Lead> leadsTriggerList, Map<String,Map<String,Decimal>> activeUsersEstimatedSpendMap){
        String targetSpendKey = 'Target Users';
        String expectedSpendKey = 'Expected Users';
        for(Lead eachLead : leadsTriggerList){
            if((eachLead.Industry_Sub_Sector__c != null && activeUsersEstimatedSpendMap.get(eachLead.Industry_Sub_Sector__c+'#'+expectedSpendKey) != null && activeUsersEstimatedSpendMap.get(eachLead.Industry_Sub_Sector__c+'#'+targetSpendKey) != null) 
                && (eachLead.Inner_Outer__c.equalsIgnoreCase('Inner London') || eachLead.Inner_Outer__c.equalsIgnoreCase('Outer London'))){
                eachLead.Expected_Active_Users__c = activeUsersEstimatedSpendMap.get(eachLead.Industry_Sub_Sector__c+'#'+expectedSpendKey).get(eachLead.No_of_London_Employees__c);
                eachLead.Target_Active_Users__c = activeUsersEstimatedSpendMap.get(eachLead.Industry_Sub_Sector__c+'#'+targetSpendKey).get(eachLead.No_of_London_Employees__c);
            }
        }
    }
    
    public static void allocateSpendExpenditureForAccount(List<Account> accountsTriggerList, Map<String,Map<String,Decimal>> expectedExpenditureSpendMap){
        String targetSpendKey = 'Target Spend';
        String expectedSpendKey = 'Expected Spend';
        for(Account eachAccount : accountsTriggerList){
            if((eachAccount.Industry_Sub_Sector__c != null && expectedExpenditureSpendMap.get(eachAccount.Industry_Sub_Sector__c+'#'+expectedSpendKey) != null && expectedExpenditureSpendMap.get(eachAccount.Industry_Sub_Sector__c+'#'+targetSpendKey) != null) 
                && (eachAccount.Inner_Outer__c.equalsIgnoreCase('Inner London') || eachAccount.Inner_Outer__c.equalsIgnoreCase('Outer London'))){
                eachAccount.Expected_Monthly_Spend__c = expectedExpenditureSpendMap.get(eachAccount.Industry_Sub_Sector__c+'#'+expectedSpendKey).get(eachAccount.No_of_London_Employees__c);
                eachAccount.Target_Monthly_Spend__c = expectedExpenditureSpendMap.get(eachAccount.Industry_Sub_Sector__c+'#'+targetSpendKey).get(eachAccount.No_of_London_Employees__c);
            }
        }
    }
    
    public static void allocateSpendExpenditureForLead(List<Lead> leadsTriggerList, Map<String,Map<String,Decimal>> expectedExpenditureSpendMap){
        String targetSpendKey = 'Target Spend';
        String expectedSpendKey = 'Expected Spend';
        for(Lead eachLead : leadsTriggerList){
            if((eachLead.Industry_Sub_Sector__c != null && expectedExpenditureSpendMap.get(eachLead.Industry_Sub_Sector__c+'#'+expectedSpendKey) != null && expectedExpenditureSpendMap.get(eachLead.Industry_Sub_Sector__c+'#'+targetSpendKey) != null) 
                && (eachLead.Inner_Outer__c.equalsIgnoreCase('Inner London') || eachLead.Inner_Outer__c.equalsIgnoreCase('Outer London'))){
                eachLead.Expected_Monthly_Spend_Annual__c = expectedExpenditureSpendMap.get(eachLead.Industry_Sub_Sector__c+'#'+expectedSpendKey).get(eachLead.No_of_London_Employees__c);
                eachLead.Target_Monthly_Spend__c = expectedExpenditureSpendMap.get(eachLead.Industry_Sub_Sector__c+'#'+targetSpendKey).get(eachLead.No_of_London_Employees__c);
            }
        }
    }
    
    public static void allocatePostCodeAccount(List<account> accounts, Map<String, String> postcodeLookupMap){
        String postCodeFound = '';
        for(Account eachAccount : accounts){
           String postCodeNoWhiteSpace = eachAccount.TECH_Postcode_Area__c;  
           postCodeFound = postcodeLookupMap.get(postCodeNoWhiteSpace);    
           if (postCodeFound != null){
               if(postCodeFound.equalsIgnoreCase('Inner')){
                 eachAccount.Inner_Outer__c = 'Inner London';
               }else if(postCodeFound.equalsIgnoreCase('Outer')){
                 eachAccount.Inner_Outer__c = 'Outer London';
               }else if(postCodeFound.equalsIgnoreCase('Outside')){
                 eachAccount.Inner_Outer__c = 'Outside London';
               }
           }else if(postCodeFound == null){
             eachAccount.Inner_Outer__c = 'NA';
           }else if(postCodeNoWhiteSpace.equalsIgnoreCase('NA')){
             eachAccount.Inner_Outer__c = 'NA';
           }
        }
    }
    
    public static void allocatePostCodeLead(List<Lead> leads, Map<String, String> postcodeLookupMap){
        String postCodeFound = '';
        for(Lead eachLead : leads){
          String postCodeNoWhiteSpace = eachLead.TECH_Postcode_Area__c;
          postCodeFound = postcodeLookupMap.get(postCodeNoWhiteSpace);      
            if (postCodeFound != null){
                if(postCodeFound.equalsIgnoreCase('Inner')){
                  eachLead.Inner_Outer__c = 'Inner London';
                }else if(postCodeFound.equalsIgnoreCase('Outer')){
                  eachLead.Inner_Outer__c = 'Outer London';
                }else if(postCodeFound.equalsIgnoreCase('Outside')){
                  eachLead.Inner_Outer__c = 'Outside London';
                }
            }else if(postCodeFound == null){
              eachLead.Inner_Outer__c = 'NA';
            }else if(postCodeNoWhiteSpace.equalsIgnoreCase('NA')){
              eachLead.Inner_Outer__c = 'NA';
            }
        }
    }
    
    /*public static void handleTNC( Map<Id, Lead> oldLeadMap, Map<Id, Lead> newLeadMap ){
        Set<Id> toDeleteTNCIds = new Set<Id>();
        for(Lead oldLead : oldLeadMap.values()){
			if(oldLead.T_C_Accepted__c == false && newLeadMap.get(oldLead.Id).T_C_Accepted__c == true){
				toDeleteTNCIds.add(oldLead.Id);
			}
		}
		List<AddLee_Terms_Conditions__c> toDeleteTNCList = new List<AddLee_Terms_Conditions__c>();
		try {
			toDeleteTNCList = [Select id, Internal_Id__c from AddLee_Terms_Conditions__c 
																		Where Internal_Id__c IN :toDeleteTNCIds];
		} catch (Exception e) {
			system.debug(e);
		}
		if(!toDeleteTNCList.isEmpty()){
			delete toDeleteTNCList;
		}
    }*/
    
    public static void allocateTNC(Map<id,Lead> oldLeadMap, Map<id,Lead> newLeadMap){
    	Map<id, AddLee_Terms_Conditions__c> leadTandCMap = getLeadTandCMap(newLeadMap.values());
        List<AddLee_Terms_Conditions__c> newTNCList = new List<AddLee_Terms_Conditions__c>();
        for(Lead newLead : newLeadMap.values()){
        	if( (oldLeadMap.get(newLead.id).Tech_Send_Ts_And_Cs__c == false &&
        		 newLead.Tech_Send_Ts_And_Cs__c == true ) &&
        		leadTandCMap.get(newLead.id) == null){
	        	AddLee_Terms_Conditions__c newTNC = new AddLee_Terms_Conditions__c();
		        newTNC.External_Id__c = newLead.T_C_External_Id__c;
				newTNC.Internal_Id__c = newLead.id;
				newTNCList.add(newTNC);
        	}
        }
        insert newTNCList;
    }
    
    public static Map<id, AddLee_Terms_Conditions__c> getLeadTandCMap(List<Lead> leads){
    	Map<id, AddLee_Terms_Conditions__c> thisMap = new Map<id, AddLee_Terms_Conditions__c>();
    	List<AddLee_Terms_Conditions__c> relatedTandCList = new List<AddLee_Terms_Conditions__c>();
    	relatedTandCList = retrieveTandC(leads);
    	if(!relatedTandCList.isEmpty()){
    		for(AddLee_Terms_Conditions__c thisTandC : relatedTandCList){
	    		thisMap.put(thisTandC.Internal_Id__c, thisTandC);
	    	}
    	}
    	return thisMap;
    }
    
    public static List<AddLee_Terms_Conditions__c> retrieveTandC(List<Lead> leads){
    	List<AddLee_Terms_Conditions__c> relatedTandCList = new List<AddLee_Terms_Conditions__c>();
    	set<id> leadIdSet = new set<id>();
    	for(Lead thisLead : leads){
    		leadIdSet.add(thisLead.id);
    	}
    	if(!leadIdSet.isEmpty()){
    		relatedTandCList = [Select id, Internal_Id__c 
    							from AddLee_Terms_Conditions__c
    							where Internal_Id__c IN :leadIdSet];
    	}
    	return relatedTandCList;
    }
    
}