public with sharing class AddLee_EmailMessageHelper {

    public static boolean isUpdatedInEmailMessageTrigger;
    public String posFeedBack = 'Positive feedback received';
    static Map<string,string> emailAccountMap =  new Map<string,string>();
    // Mapping between Email Address and Account Number
    static Map<string,string> emailAddressAccountNumberMap{
        get{
            if(emailAccountMap.size()==0){
                for(EmailAddressAccountNumberMappings__c eam : EmailAddressAccountNumberMappings__c.getall().values()){
                    emailAccountMap.put(eam.To_Email__c,eam.Account_Number__c);
                }
            }
            return emailAccountMap;
        }set;
    }
    /**
    * @author Prateek Gandhi
    * @description Assigns account on the bases of ToAddress and the mapping between ToAddress
    * and Account is fetched from EmailAddressAccountNumberMappings__c custom setting
    */    
    public static void AssignAccountAndFeedbackRollups(Map<Id,Case> EmailMessageIdCaseMap, Map<id,EmailMessage> newEmails, Map<Id, EmailMessage> mapCIdEMesgFeedback){
        System.DEBUG(' EmailMessageIdCaseMap : ' + EmailMessageIdCaseMap);
        System.DEBUG(' newEmails : ' + newEmails);
        List<Case> casesToUpdate = new List<Case>();
        Map<Id, Case> mapIdCase = new Map<Id, Case>();
        Map<string,List<Case>> accountNumberCasesMappings = new Map<string,List<Case>>();// cases that have accounts mentioned in custom settings
        //Map<Id,Account> AccountIdsWithAccountNumbers = new Map<Id,Account>([SELECT Id,Account_Number__c FROM Account WHERE Account_Number__c in: emailAddressAccountNumberMap.values()]);
        for(Id emailMessageId: EmailMessageIdCaseMap.KeySet()){ // iterating over emailMessages which have created a new case
            if(EmailMessageIdCaseMap.get(emailMessageId).AccountId == null){ // if the emailMessage's related case has no account ie. we'll have to look for an account 
                if(emailAddressAccountNumberMap.containsKey(newEmails.get(emailMessageId).ToAddress)){// checks if the emaiMessage toAddress is linked to any of the Accounts 
                    Case c = new Case();
                    c = EmailMessageIdCaseMap.get(emailMessageId);
                    c.Web_Account_Number__c=emailAddressAccountNumberMap.get(newEmails.get(emailMessageId).ToAddress);
                    System.DEBUG(' case : ' + c);
                    casesToUpdate.add(c);
                    if(c.Id != null) mapIdCase.put(c.Id, c);
                }
            }else if(EmailMessageIdCaseMap.get(emailMessageId).Account_Number__c != emailAddressAccountNumberMap.get(newEmails.get(emailMessageId).ToAddress)){//check if the account number of a case is different from that which is mapped to the to-address 
                // eg : Account number of the case is 5555, but the to-address mapped in the EmailAddressAccountNumberMappings__c is 4444
                // this can happen if one of the customer relations user is a contact    on the 5555 account
                Case caseToUpdate = EmailMessageIdCaseMap.get(emailMessageId);
                //Assign the case to the web_Account_number field, this field can drive to which account we want the case to be associated too
                System.DEBUG(' case : ' + caseToUpdate);   
                caseToUpdate.Web_Account_Number__c = emailAddressAccountNumberMap.get(newEmails.get(emailMessageId).ToAddress);
                casesToUpdate.add(caseToUpdate);
                if(caseToUpdate.Id != null) mapIdCase.put(caseToUpdate.Id, caseToUpdate);
            }  
        }
        
        if(!mapCIdEMesgFeedback.isEmpty()){
            RecordType rt = [select Id from RecordType where Name = 'Journey Feedback' and SobjectType = 'Case' limit 1];
            for(Id cId : mapCIdEMesgFeedback.keySet()){
                if(mapIdCase.containsKey(cId)){
                    parseEmailContent.response res = new parseEmailContent.response();
                    res = parseEmailContent.parseEmail((Case) mapIdCase.get(cId), (EmailMessage) mapCIdEMesgFeedback.get(cId)) ;
                    System.DEBUG('MTDebug  res 1 : ' + res);
                    mapIdCase.get(cId).Driver_Rating__c = Integer.valueOf(res.driverRating);
                    mapIdCase.get(cId).Overall_Rating__c = Integer.valueOf(res.overallRating);
                    mapIdCase.get(cId).External_Id__c = res.externalId;
                    mapIdCase.get(cId).Job_Number__c = res.jobNumber;
                    mapIdCase.get(cId).Send_Feedback_Comment_Emial__c = res.sendCommentEmail;
                    mapIdCase.get(cId).Type = 'Driver';
                    mapIdCase.get(cId).Case_Type__c = 'Driver Feedback';
                    mapIdCase.get(cId).RecordType = rt;
                    if(res.custEmail != null) mapIdCase.get(cId).FeedBackEmail__c = res.custEmail;
                    if(res.avgRating > 3) {
                        mapIdCase.get(cId).Send_Feedback_Comment_Emial__c = false;
                        mapIdCase.get(cId).Status = 'Closed';
                    }
                    if(res.avgRating <= 3 && res.sendCommentEmail){
                        mapIdCase.get(cId).Status = 'Closed';
                    } 
                } else{
                    parseEmailContent.response res = new parseEmailContent.response();
                    Case cse = (Case) EmailMessageIdCaseMap.get(mapCIdEMesgFeedback.get(cId).Id);
                    res = parseEmailContent.parseEmail(cse, (EmailMessage) mapCIdEMesgFeedback.get(cId)) ;
                    System.DEBUG('MTDebug  res 2 : ' + res);
                    cse.Driver_Rating__c = Integer.valueOf(res.driverRating);
                    cse.Overall_Rating__c = Integer.valueOf(res.overallRating);
                    cse.External_Id__c = res.externalId;
                    cse.Job_Number__c = res.jobNumber;
                    cse.Send_Feedback_Comment_Emial__c = res.sendCommentEmail;
                    cse.Type = 'Driver';
                    cse.Case_Type__c = 'Driver Feedback';
                    cse.RecordType = rt;
                    if(res.custEmail != null) cse.FeedBackEmail__c = res.custEmail;
                    if(res.avgRating > 3) {
                        cse.Send_Feedback_Comment_Emial__c = false;
                        cse.Status = 'Closed';
                    }
                    if(res.avgRating <= 3 && res.sendCommentEmail) {
                        cse.Status = 'Closed';
                    }
                    casesToUpdate.add(cse);
                }
                
            }
        }
        System.DEBUG(' casesToUpdate : ' + casesToUpdate);
        if(casesToUpdate.size()>0){
            isUpdatedInEmailMessageTrigger= true;
            Database.UpsertResult[] results = Database.upsert(casesToUpdate, Schema.case.id, false);
            System.DEBUG(' MTDebug results : ' + results);
        }
    }
}