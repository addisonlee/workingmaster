/**
 * Test class for Handler contact name 
 * 30/10/2015
 
 */
@istest
public class Handle_ContactName_test{
@Testvisible
public static testmethod void testmeth(){
AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account acc = AddLee_Trigger_Test_Utils.createAccounts(1)[0];
        insert acc;
        Contact con = new contact(FirstName ='tests',Lastname='test',AccountId =acc.Id,email='test@gmail.com',SHM_contactName__c = 'test');       
        string [] nameslist;
        string str = string.valueof(con.SHM_contactName__c);
        nameslist = str.split(' ');
        test.starttest();
        Contact con1 = new contact(FirstName ='tests',Lastname='test',AccountId =acc.Id,email='test@gmail.com',SHM_contactName__c = 'test test1');       
        insert con1;
        update con1;
        Contact con2 = new contact(FirstName ='tests',Lastname='test',AccountId =acc.Id,email='test@gmail.com',SHM_contactName__c = 'test test1 test2');       
        insert con2;
        Contact con3 = new contact(FirstName ='tests',Lastname='test',AccountId =acc.Id,email='test@gmail.com',SHM_contactName__c = 'test');       
        insert con3;
        Contact con4 = new contact(FirstName ='tests',Lastname='test',AccountId =acc.Id,email='test@gmail.com',SHM_contactName__c = 'test test1 test3 test4');       
        insert con4;      
        test.stoptest();
        system.assertequals(con1.lastname,nameslist[0]);
        }
        }