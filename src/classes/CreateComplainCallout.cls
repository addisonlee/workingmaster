/*
Web service callout class for CreateComplain Method
*/
global class CreateComplainCallout {
  
       public with sharing class AdleeRefundsServiceWrapper extends WebServiceBaseWrapper { 
         
        //request parameter      
        Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
        Addlee_refunds.complaintRequest createComplaintRequest  = new  Addlee_refunds.complaintRequest();     
        Refund_Integration__c setting;  
        public String sessionId = ''; 
        
        String parentCaseId;
        
                   
        //Response parameters
        Addlee_refunds.complaintResponse createComplaintResponse = new Addlee_refunds.complaintResponse();
        
        
        String interfaceError = '';
        public boolean isTest = false;
        
        //constructor
        public AdleeRefundsServiceWrapper(String crName,String cName, String mName) {
            super(crName,mName, cName);
        }
        
        //implement the prepare data method
        public override void prepareData() {  
            setting = Refund_Integration__c.getInstance('Shamrock Refund');         
            stub.inputHttpHeaders_x=this.getAuthenticationKeyMap();
            if(setting <> null && setting.End_Point__c <> null){
              stub.endpoint_x = setting.End_Point__c; //custom setting
            }
            stub.timeout_x = 60000;  
        }
        
        //Implement the execute method
        public override void execute() {
            try{
                
                  if(setting <> null && setting.Username__c <> null && setting.password__c <> null) {
                    if(!Test.isRunningTest()){
                      sessionId = stub.login(setting.Username__c,setting.password__c );//custom setting
                    }    
                   if(sessionId <> null){    
                    system.debug('*createComplaintRequest**'+createComplaintRequest);
                    createComplaintResponse = stub.CreateComplaint(sessionId,createComplaintRequest);
                        
                   }
                  
                
                 system.debug('**createComplaintResponse*'+Json.serialize(createComplaintResponse));
                } 
            }
            catch (Exception e) {
                system.debug('***** Exception received = '+e.getMessage());
                throw e;
            } 
        }
        
        //implementation of handleException
        public override void handleException(Exception e) { 
            //system.debug('$$$$$$$-'+e.getMessage());
           // interfaceError = e.getMessage();
             Utility.processErrorLog(null,e.getmessage(),parentcaseId,null,null);
            
        }       
   
   } 
   
    webservice static  AdleeServiceOutput  processAdleeServiceCallout(AdleeRefundsServiceInput input) {
               
               try{
                AdleeRefundsServiceWrapper adleeRefundsServiceWrapper = new AdleeRefundsServiceWrapper('Addlee_refunds.SalesForceManagementWebServicePort.CreateComplaint','Addlee_refunds.SalesForceManagementWebServicePort','CreateComplaint');                 
               
               //calling processCreateComplainInput to map the captured input parameters to the variables defined in adleeRefundsServiceWrapper
                adleeRefundsServiceWrapper = processCreateComplainInput(adleeRefundsServiceWrapper, input);
                
               //calling the performWebserviceCall which internally calls preparedate() and execute() methods
                adleeRefundsServiceWrapper.performWebserviceCall();
                
                if(adleeRefundsServiceWrapper.interfaceError == ''){
                                    
                     AdleeServiceOutput output = new AdleeServiceOutput();

                         if(adleeRefundsServiceWrapper.createComplaintResponse <> null){
                            output = processCreateComplaintResponse(adleeRefundsServiceWrapper.createComplaintResponse);
                         }                        
                     return output;                       
                }
                
               } catch (Exception e){
                system.debug('Callout Exit Exception = '+e.getMessage());
                throw e;
            }  
            
            return null;     
              
    }
    
        
    public static  AdleeRefundsServiceWrapper processCreateComplainInput(AdleeRefundsServiceWrapper adleewrapper,AdleeRefundsServiceInput input) {
        
        if(input.adleeRequest <> null){
        
           adleewrapper.parentCaseId = input.adleeRequest.parentCaseId;
           
           if(input.adleeRequest.createComplain <> null){
             //ComplainType            
               addlee_refunds.complaint complaint = new addlee_refunds.complaint();
               addlee_refunds.complaintType complaintType = new addlee_refunds.complaintType();
                complaintType.Name = input.adleeRequest.createComplain.Type ; 
                complaintType.id = Long.valueof(input.adleeRequest.createComplain.TypeId) ; 
               complaint.complaintType = complaintType;
              
              //ComplainSubtype 
               Addlee_refunds.complaintSubType complaintSubType = new Addlee_refunds.complaintSubType();
                complaintSubType.name = input.adleeRequest.createComplain.subtype;//subtype
               complaint.complaintSubType = complaintSubType;
               
               //AdminFee
                complaint.adminFee = input.adleeRequest.createComplain.adminfee;//admin fee            
               
               //Docket Info
                List<Addlee_refunds.complaintJobItem> listofJobItems = new List<Addlee_refunds.complaintJobItem>();
                 Addlee_refunds.complaintJobItem jobItem = new Addlee_refunds.complaintJobItem();
                 jobItem.docket = false;
                 if(input.adleeRequest.createComplain.docketvalue <> null && input.adleeRequest.createComplain.docketvalue.touppercase() == 'TRUE'){
                   jobItem.docket = true;
                 }
                 if(input.adleeRequest.createComplain.docketid <> null){
                    jobItem.id= Long.valueof(input.adleeRequest.createComplain.docketid); //jobnumber
                 }
                 listofJobItems.add(jobItem); 
                complaint.jobItems = listofJobItems;
                
                //net
                complaint.net = input.adleeRequest.createComplain.net; 
                //discount
                complaint.discount= input.adleeRequest.createComplain.discount; 
                // gross
                complaint.gross = input.adleeRequest.createComplain.gross;
                //Note          
                complaint.note = input.adleeRequest.createComplain.Note;
                //Description       
                complaint.details = input.adleeRequest.createComplain.description;
                //vat
                complaint.vat = input.adleeRequest.createComplain.vat;
               
               adleewrapper.createComplaintRequest.complaint = complaint;
           }
        }
           
            return adleewrapper;
    }
    

    
     public static  AdleeServiceOutput processCreateComplaintResponse(Addlee_refunds.complaintResponse response) {
       
         AdleeServiceOutput output = new AdleeServiceOutput();
         output.createComplainOutput = new AdleeServiceOutput.CreateComplainOutput();
         output.createComplainOutput.errorCode = response.errorCode;
         output.createComplainOutput.id = response.id;
         output.createComplainOutput.numberValue = response.number_x;
         output.createComplainOutput.errorDescription = response.description;
                             
        return output;        
    }
    
    
    



}