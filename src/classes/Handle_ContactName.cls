/*****
  ** populate firstname and last name after inserting contact record from shamrock
   * 29/10/2015

**/
public with sharing class Handle_ContactName  {
    
    List<Contact> listcon = new list<Contact>();
    public void selfcare_contact (Map<Id, Contact>  conmap) {
        List<Contact> newcon = [select id, Firstname,LastName,SHM_contactName__c from Contact where ID IN:conmap.KeySet()];
    
        for(Contact con :newcon  ){
            if(con.SHM_contactName__c<> null){
                String [] firstlastlist ;
                // get shm_contact name to populate first name and last name 
                string  str = string.valueof(con.SHM_contactName__c); 
                firstlastlist = str.split(' ');
                if(firstlastlist.size() == (2)){
                 con.firstname = firstlastlist[0];
                 con.lastname =  firstlastlist[1];
                }
                else if(firstlastlist.size() == 1){
                  con.lastname = firstlastlist[0];  
                    
                }
                else if(firstlastlist.size() == 3){
                  con.firstname = firstlastlist[0];
                  con.lastname =  firstlastlist[1]+' ' +firstlastlist[2];
                }
                else if(firstlastlist.size() == 4){
                  con.firstname = firstlastlist[0];
                  con.lastname =  firstlastlist[1]+' ' +firstlastlist[2] +' ' +firstlastlist[3];
                }
                listcon.add(con);
            }
            AddLee_checkRecursive.isAfterInsertUpdate = false;
            update listcon;
        }
    }
}