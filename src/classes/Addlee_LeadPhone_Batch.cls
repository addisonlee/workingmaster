global class Addlee_LeadPhone_Batch implements Database.Batchable<Sobject>{
    global string query;
    global database.querylocator start(Database.BatchableContext BC){
        return Database.getquerylocator(query);
    }
    global void execute (Database.BatchableContext BC,list<Lead>leads){
        
        list<Lead>getnos = new list<Lead>();
        for(Lead cc:leads){
            if(cc.Phone!=null){
                string s1 = cc.Phone;
                string ss = s1.deletewhitespace();
                string regExp = '[a-zA-Z;,+.?+/:-_(){}~#|@&*£%$=!]';
                string replacement = '';
                string s2 = ss.replaceAll(regExp,replacement);
                cc.Phone = s2;
                getnos.add(cc); 
                    }
                     }
                     update getnos;
                     }
    global void finish(Database.BatchableContext BC){
    }
}