@isTest
global class AddLeeGetSpecialInstructionTypesMock implements webservicemock {
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.specialInstructionTypesResponse respElement =  new Addlee_refunds.specialInstructionTypesResponse();
          respElement.specialInstructionTypes = new List<Addlee_refunds.specialInstructionType>();
               Addlee_refunds.specialInstructionType testType = new Addlee_refunds.specialInstructionType();
               testType.code = '576576';
               testType.id = 78768;
               testType.name = 'type';
           respElement.specialInstructionTypes.add(testType);     
       response.put('response_x', respElement); 
   }
}