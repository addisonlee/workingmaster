@isTest
private class AddLee_DAndBSearch_Controller_Test {
	
	@isTest(seeallData = false)
	static void AddLee_DAndBSearch_Controller_searchExisting() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();

		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.PostalCode = 'SE1 0HS';
		insert testLead;

		Account testAccount = new Account(Name = 'test');
		insert testAccount;

		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());

		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('businessName','test');
		pageRef.getParameters().put('leadId',testLead.Id);
		pageRef.getParameters().put('accType','BusinessAccount');

		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		//theController.isApexTestRunning = true;
		theController.actionMethod();
		theController.responseWrapperList[0].selected = true;
		theController.checkExistingNew();

		testLead.CompanyDunsNumber = 'test';
		testAccount.DunsNumber = 'test';

		update testLead;
		update testAccount;

		AddLee_DAndBSearch_Controller.isApexTestRunning = true;

		theController.checkExistingNew();

		//theController.newSearch();

		//thPeController.createLead();

		theController.insertLead();

		theController.statusResponse = new AddLee_DAndBSearch_Controller.DAndBStatusResponseWrapper();

		//theController.leadWrappers[0].selected = true;
		

	}

	@isTest(seeallData = false)
	static void AddLee_DAndBSearch_Controller_startWithExistingDUNSNumber() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();

		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.PostalCode = 'SE1 0HS';
		testLead.CompanyDunsNumber = '229515499';
		testLead.Company = 'D&B Sample';

		insert testLead;

		Account testAccount = new Account(Name = 'test');
		insert testAccount;

		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());

		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('businessName','test');
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('dunsReference', testLead.CompanyDunsNumber);
		pageRef.getParameters().put('accType','BusinessAccount');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		//theController.isApexTestRunning = true;
		theController.actionMethod();
		theController.responseWrapperList[0].selected = true;
		theController.checkExistingNew();

		testLead.CompanyDunsNumber = 'test';
		testAccount.DunsNumber = 'test';

		update testLead;
		update testAccount;

		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		theController.saveSelectedLead();
		theController.checkExistingNew();

		//theController.newSearch();

		//thPeController.createLead();

		theController.insertLead();

		//theController.statusResponse = new AddLee_DAndBSearch_Controller.DAndBStatusResponseWrapper();

		//theController.leadWrappers[0].selected = true;
		

	}
	
    @isTest(seeallData = false)
	static void AddLee_DAndBSearch_Controller_startWithExistingDUNSNumberWithDuplicateLead() {

		// Scenario
		// 1. Lead has a duns number
		// 2. Another lead with the same duns number already exists

		AddLee_Trigger_Test_Utils.insertCustomSettings();

		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.PostalCode = 'SE1 0HS';
		testLead.CompanyDunsNumber = '229515499';
		testLead.Company = 'D&B Sample';

		insert testLead;


		Lead duplicateLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		duplicateLead.Bank_Date_of_DD__c = System.today();
		duplicateLead.PostalCode = 'SE1 0HS';
		duplicateLead.CompanyDunsNumber = '229515499';
		duplicateLead.Company = 'D&B Sample Duplicate';

		insert duplicateLead;



		Account testAccount = new Account(Name = 'test');
		insert testAccount;

		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());

		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('businessName','test');
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('dunsReference', testLead.CompanyDunsNumber);
		pageRef.getParameters().put('accType','BusinessAccount');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		//theController.isApexTestRunning = true;
		theController.actionMethod();
		theController.responseWrapperList[0].selected = true;
		theController.checkExistingNew();

		testLead.CompanyDunsNumber = 'test';
		testAccount.DunsNumber = 'test';

		update testLead;
		update testAccount;

		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		theController.saveSelectedLead();
		theController.checkExistingNew();

		//theController.newSearch();

		//thPeController.createLead();

		theController.insertLead();

		//theController.statusResponse = new AddLee_DAndBSearch_Controller.DAndBStatusResponseWrapper();
		theController.cancelAccount();
		//theController.leadWrappers[0].selected = true;
		

	}
	
	@isTest(seeallData = false)
	static void AddLee_DAndBSearch_Controller_PersonAccountLead() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();
		Date myDateLead = Date.newInstance(1960, 2, 17);
        Date myDateAccount = Date.newInstance(1960, 2, 17);

		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.FirstName = 'Fname';
        testLead.LastName = 'Lname';
		testLead.Email = 'sample@test.com';
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.PostalCode = 'SE1 0HS';
		testLead.Company = null;
		testLead.Payment_Type__c = 'BACS';
		testLead.Phone = '447837231718';
		testLead.Street = 'test street';
		testLead.City = 'city';
		testLead.PostalCode = 'pc';
        testLead.isConverted = false;
        testLead.Birthdate__c = myDateLead;
		
		insert testLead;

        Account testAccount = new Account();
        testAccount.RecordTypeID = AddLee_Trigger_Test_Utils.accRecordType('Consumer');
        testAccount.PersonEmail = 'sample@test.com';
        testAccount.FirstName = 'Fname';
        testAccount.LastName = 'Lname';
        testAccount.Phone = '447837231718';
        testAccount.Birthdate__pc = myDateAccount;
        
        insert testAccount;
        
        
		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        //Test.StartTest();
		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('firstName', testLead.FirstName);
		pageRef.getParameters().put('lastName', testLead.LastName);
		pageRef.getParameters().put('email', testLead.Email);
		pageRef.getParameters().put('postCode',testLead.PostalCode);
		pageRef.getParameters().put('accType','PersonAccount');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		//theController.isApexTestRunning = true;
		pagereference pageRef1 = theController.actionMethod();
		System.DEBUG('MTDebug pagereference : ' + pageRef1);
		
		list<AddLee_Terms_Conditions__c> addLeeTNC = [Select Id From AddLee_Terms_Conditions__c];
		lead leadToCheck = [Select Id, D_B_Credit_Limit__c, Country, Convert_To_Account__c, Credit_Checked__c, Nature_of_Enquiry__c, Tech_Send_Ts_And_Cs__c From Lead Where Id =: testLead.Id];
		system.assert(String.ValueOf(pageRef1).contains('/apex/CreditCheckPass?id'));
		system.assertEquals(200, leadToCheck.D_B_Credit_Limit__c);
		system.assertEquals('United Kingdom', leadToCheck.Country);
        system.assertEquals(true, leadToCheck.Convert_To_Account__c);
        system.assertEquals(true, leadToCheck.Credit_Checked__c);
        system.assertEquals('Individual/Family Account Application', leadToCheck.Nature_of_Enquiry__c);
        system.assertEquals(true, leadToCheck.Tech_Send_Ts_And_Cs__c);
	}
	
	@isTest(seeallData = false)
	static void AddLee_DAndBSearch_Controller_PersonAccountLeadWithDirectDebit() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();
        Date myDateLead = Date.newInstance(1960, 2, 17);
        Date myDateAccount = Date.newInstance(1960, 2, 17);

		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.FirstName = 'Fname';
        testLead.LastName = 'Lname';
		testLead.Email = 'sample@test.com';
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.PostalCode = 'SE1 0HS';
		testLead.Company = null;
		testLead.Payment_Type__c = 'Direct Debit';
		testLead.Phone = '447837231718';
		testLead.Street = 'test street';
		testLead.City = 'city';
		testLead.PostalCode = 'pc';
        testLead.isConverted = false;
        testLead.Birthdate__c = myDateLead;
		insert testLead;

        Account testAccount = new Account();
        testAccount.RecordTypeID = AddLee_Trigger_Test_Utils.accRecordType('Consumer');
        testAccount.PersonEmail = 'sample@test.com';
        testAccount.FirstName = 'Difffname';
        testAccount.LastName = 'Difflname';
        testAccount.Phone = '447837231718';
        testAccount.Birthdate__pc = myDateAccount;
        
        insert testAccount;
        
        
		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        //Test.StartTest();
		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('firstName', testLead.FirstName);
		pageRef.getParameters().put('lastName', testLead.LastName);
		pageRef.getParameters().put('email', testLead.Email);
		pageRef.getParameters().put('postCode',testLead.PostalCode);
		pageRef.getParameters().put('accType','PersonAccount');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		//theController.isApexTestRunning = true;
		pagereference pageRef1 = theController.actionMethod();
		System.DEBUG('MTDebug pagereference : ' + pageRef1);
		
		list<AddLee_Terms_Conditions__c> addLeeTNC = [Select Id From AddLee_Terms_Conditions__c];
		lead leadToCheck = [Select Id, D_B_Credit_Limit__c, Country, Convert_To_Account__c, Credit_Checked__c, Nature_of_Enquiry__c, Tech_Send_Ts_And_Cs__c From Lead Where Id =: testLead.Id];
		system.assert(String.ValueOf(pageRef1).contains('/apex/PaymentCapture?id'));
		system.assertEquals(1, addLeeTNC.Size());
		system.assertEquals(200, leadToCheck.D_B_Credit_Limit__c);
		system.assertEquals('United Kingdom', leadToCheck.Country);
        system.assertEquals(true, leadToCheck.Convert_To_Account__c);
        system.assertEquals(true, leadToCheck.Credit_Checked__c);
        system.assertEquals('Individual/Family Account Application', leadToCheck.Nature_of_Enquiry__c);
        system.assertEquals(true, leadToCheck.Tech_Send_Ts_And_Cs__c);
	}
	
	@isTest(seeallData = false)
	static void AddLee_DAndBSearch_Controller_PersonAccountLeadWithSingleCreditCard() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();
		Date myDateLead = Date.newInstance(1960, 2, 17);
        Date myDateAccount = Date.newInstance(1960, 2, 17);
        
		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.FirstName = 'Fname';
        testLead.LastName = 'Lname';
		testLead.Email = 'sample@test.com';
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.PostalCode = 'SE1 0HS';
		testLead.Company = null;
		testLead.Payment_Type__c = 'Single Credit Card';
		testLead.Phone = '447837231718';
		testLead.Street = 'test street';
		testLead.City = 'city';
		testLead.PostalCode = 'pc';
        testLead.isConverted = false;
        testLead.Birthdate__c = myDateLead;

		insert testLead;

        Account testAccount = new Account();
        testAccount.RecordTypeID = AddLee_Trigger_Test_Utils.accRecordType('Consumer');
        testAccount.PersonEmail = 'sample@test.com';
        testAccount.FirstName = 'Fname';
        testAccount.LastName = 'Lname';
        testAccount.Phone = '447837231718';
        testAccount.Birthdate__pc = myDateAccount;
        
        insert testAccount;
        
        
		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        //Test.StartTest();
		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('firstName', testLead.FirstName);
		pageRef.getParameters().put('lastName', testLead.LastName);
		pageRef.getParameters().put('email', testLead.Email);
		pageRef.getParameters().put('postCode',testLead.PostalCode);
		pageRef.getParameters().put('accType','PersonAccount');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		//theController.isApexTestRunning = true;
		pagereference pageRef1 = theController.actionMethod();
		System.DEBUG('MTDebug pagereference : ' + pageRef1);
		
		list<AddLee_Terms_Conditions__c> addLeeTNC = [Select Id From AddLee_Terms_Conditions__c];
		lead leadToCheck = [Select Id, D_B_Credit_Limit__c, Country, Convert_To_Account__c, Credit_Checked__c, Nature_of_Enquiry__c, Tech_Send_Ts_And_Cs__c From Lead Where Id =: testLead.Id];
		system.assert(String.ValueOf(pageRef1).contains('/apex/PaymentCapture?id'));
		system.assertEquals(1, addLeeTNC.Size());
		system.assertEquals(200, leadToCheck.D_B_Credit_Limit__c);
		system.assertEquals('United Kingdom', leadToCheck.Country);
        system.assertEquals(true, leadToCheck.Convert_To_Account__c);
        system.assertEquals(true, leadToCheck.Credit_Checked__c);
        system.assertEquals('Individual/Family Account Application', leadToCheck.Nature_of_Enquiry__c);
        system.assertEquals(true, leadToCheck.Tech_Send_Ts_And_Cs__c);
	}
	
	@isTest(seeallData = false)
	static void AddLee_DAndBSearch_Controller_PADupLeadSameEmail() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();
		Date myDateLead = Date.newInstance(1960, 2, 17);
        Date myDateAccount = Date.newInstance(1960, 2, 18);

		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.FirstName = 'Fname';
        testLead.LastName = 'Lname';
		testLead.Email = 'sample@test.com';
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.PostalCode = 'SE1 0HS';
		testLead.Company = null;
		testLead.Payment_Type__c = 'Single Credit Card';
		testLead.Phone = '447837231718';
		testLead.Street = 'test street';
		testLead.City = 'city';
		testLead.PostalCode = 'pc';
        testLead.isConverted = false;
        testLead.Birthdate__c = myDateLead;

		insert testLead;

        Lead duplicateLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		duplicateLead.FirstName = 'DiffFname';
        duplicateLead.LastName = 'DiffLname';
		duplicateLead.Email = 'sample@test.com';
		duplicateLead.Bank_Date_of_DD__c = System.today();
		duplicateLead.PostalCode = 'SE1 0HS';
		duplicateLead.Company = null;
		duplicateLead.Payment_Type__c = 'Single Credit Card';
		duplicateLead.Phone = '447837231719';
		duplicateLead.Street = 'Diff test street';
		duplicateLead.City = 'Diff city';
		duplicateLead.PostalCode = 'diff pc';
        duplicateLead.isConverted = false;
        duplicateLead.Birthdate__c = myDateLead.addDays(2);

		insert duplicateLead;

        Account testAccount = new Account();
        testAccount.RecordTypeID = AddLee_Trigger_Test_Utils.accRecordType('Consumer');
        testAccount.PersonEmail = 'sample@test.com';
        testAccount.FirstName = 'Fname';
        testAccount.LastName = 'Lname';
        testAccount.Phone = '447837231718';
        testAccount.Birthdate__pc = myDateAccount;
        
        insert testAccount;

		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        //Test.StartTest();
		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('firstName', testLead.FirstName);
		pageRef.getParameters().put('lastName', testLead.LastName);
		pageRef.getParameters().put('email', testLead.Email);
		pageRef.getParameters().put('postCode',testLead.PostalCode);
		pageRef.getParameters().put('accType','PersonAccount');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		//theController.isApexTestRunning = true;
		pagereference pageRef1 = theController.actionMethod();
		System.DEBUG('MTDebug pagereference : ' + pageRef1);
		
		//list<AddLee_Terms_Conditions__c> addLeeTNC = [Select Id From AddLee_Terms_Conditions__c];
		system.assert(String.ValueOf(pageRef1).contains('/apex/leadmerge?accType=PersonAccount'));
		//system.assertEquals(1, addLeeTNC.Size());
	}
    
    @isTest(seeallData = false)
	static void AddLee_DAndBSearch_Controller_PADupLeadSamePhone() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();
        Date myDateLead = Date.newInstance(1960, 2, 17);
        Date myDateAccount = Date.newInstance(1960, 2, 18);

		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.FirstName = 'Fname';
        testLead.LastName = 'Lname';
		testLead.Email = 'sample@test.com';
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.PostalCode = 'SE1 0HS';
		testLead.Company = null;
		testLead.Payment_Type__c = 'Single Credit Card';
		testLead.Phone = '447837231718';
		testLead.Street = 'test street';
		testLead.City = 'city';
		testLead.PostalCode = 'pc';
        testLead.isConverted = false;
        testLead.Birthdate__c = myDateLead;

		insert testLead;

        Lead duplicateLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		duplicateLead.FirstName = 'DiffFname';
        duplicateLead.LastName = 'DiffLname';
		duplicateLead.Email = 'diffsample@test.com';
		duplicateLead.Bank_Date_of_DD__c = System.today();
		duplicateLead.PostalCode = 'SE1 0HS';
		duplicateLead.Company = null;
		duplicateLead.Payment_Type__c = 'Single Credit Card';
		duplicateLead.Phone = '447837231718';
		duplicateLead.Street = 'Diff test street';
		duplicateLead.City = 'Diff city';
		duplicateLead.PostalCode = 'diff pc';
        duplicateLead.isConverted = false;
        duplicateLead.Birthdate__c = myDateLead.addDays(2);

		insert duplicateLead;

        Account testAccount = new Account(Name = 'test');
		insert testAccount;

		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        //Test.StartTest();
		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('firstName', testLead.FirstName);
		pageRef.getParameters().put('lastName', testLead.LastName);
		pageRef.getParameters().put('email', testLead.Email);
		pageRef.getParameters().put('postCode',testLead.PostalCode);
		pageRef.getParameters().put('accType','PersonAccount');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		//theController.isApexTestRunning = true;
		pagereference pageRef1 = theController.actionMethod();
		System.DEBUG('MTDebug pagereference : ' + pageRef1);
		
		//list<AddLee_Terms_Conditions__c> addLeeTNC = [Select Id From AddLee_Terms_Conditions__c];
		system.assert(String.ValueOf(pageRef1).contains('/apex/leadmerge?accType=PersonAccount'));
		//system.assertEquals(1, addLeeTNC.Size());
	}
	
    @isTest(seeallData = false)
	static void AddLee_DAndBSearch_PADupLeadSameFnameLnamePhonePostcode() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();
        Date myDateLead = Date.newInstance(1960, 2, 17);
        Date myDateAccount = Date.newInstance(1960, 2, 18);

		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.FirstName = 'Fname';
		testLead.LastName = 'Lname';
		testLead.Email = 'sample@test.com';
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.PostalCode = 'SE1 0HS';
		testLead.Company = null;
		testLead.Payment_Type__c = 'Single Credit Card';
		testLead.Phone = '447837231718';
		testLead.Street = 'test street';
		testLead.City = 'city';
		testLead.PostalCode = 'SE1 0HS';

		insert testLead;

        Lead duplicateLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		duplicateLead.FirstName = 'Fname';
		duplicateLead.LastName = 'Lname';
		duplicateLead.Email = 'sample1@test.com';
		duplicateLead.Bank_Date_of_DD__c = System.today();
		duplicateLead.PostalCode = 'SE1 0HS';
		duplicateLead.Company = null;
		duplicateLead.Payment_Type__c = 'Single Credit Card';
		duplicateLead.Phone = '447837231718';
		duplicateLead.Street = 'Diff test street';
		duplicateLead.City = 'Diff city';
		duplicateLead.PostalCode = 'SE1 0HS';

		insert duplicateLead;

        Account testAccount = new Account(Name = 'test');
		insert testAccount;

		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        //Test.StartTest();
		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('firstName', testLead.FirstName);
		pageRef.getParameters().put('lastName', testLead.LastName);
		pageRef.getParameters().put('email', testLead.Email);
		pageRef.getParameters().put('phone',testLead.Phone);
		pageRef.getParameters().put('postCode',testLead.PostalCode);
		pageRef.getParameters().put('accType','PersonAccount');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		//theController.isApexTestRunning = true;
		pagereference pageRef1 = theController.actionMethod();
		System.DEBUG('MTDebug pagereference : ' + pageRef1);
		
		//list<AddLee_Terms_Conditions__c> addLeeTNC = [Select Id From AddLee_Terms_Conditions__c];
		system.assert(String.ValueOf(pageRef1).contains('/apex/leadmerge?accType=PersonAccount'));
		//system.assertEquals(1, addLeeTNC.Size());
	}
	
    @isTest(seeallData = false)
	static void AddLee_DAndBSearch_Controller_PALeadWithDupAccSameEmail() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();
        Date myDateLead = Date.newInstance(1960, 2, 17);
        Date myDateAccount = Date.newInstance(1960, 2, 18);
		
		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.FirstName = 'fname';
		testLead.LastName = 'lname';
		testLead.Email = 'sample@test.com';
		testLead.Bank_Date_of_DD__c = System.today();
        testLead.PostalCode = 'SE1 0HS';
		testLead.Birthdate__c = myDateLead;
		testLead.CompanyDunsNumber = '229515499';
		testLead.Company = 'D&B Sample';
		testLead.Payment_Type__c = 'Single Credit Card';
        testLead.Phone = '+447800000001';
        testLead.isConverted = false;

		insert testLead;

        Account testAccount = new Account();
        testAccount.RecordTypeID = AddLee_Trigger_Test_Utils.accRecordType('Personal Account');
        testAccount.PersonEmail = 'sample@test.com';
        testAccount.FirstName = 'Difffname';
        testAccount.LastName = 'Difflname';
        testAccount.Phone = '+447800000000';
        testAccount.Birthdate__pc = myDateAccount;
        
        insert testAccount;
        
        
		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        //Test.StartTest();
		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('firstName', testLead.FirstName);
		pageRef.getParameters().put('lastName', testLead.LastName);
		pageRef.getParameters().put('email', testLead.Email);
		pageRef.getParameters().put('postCode',testLead.PostalCode);
		pageRef.getParameters().put('accType','PersonAccount');
		
        Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		//theController.isApexTestRunning = true;
		pagereference pageRef1 = theController.actionMethod();
		System.DEBUG('MTDebug pagereference : ' + pageRef1);
		
		system.assertEquals(null, pageRef1);
	}
		
	@isTest(seeallData = false)
	static void AddLee_DAndBSearch_Controller_PALeadWithDupAccSamePhone() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();
		Date myDateLead = Date.newInstance(1960, 2, 17);
        Date myDateAccount = Date.newInstance(1960, 2, 18);
		
		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.FirstName = 'fname';
		testLead.LastName = 'lname';
		testLead.Email = 'sample@test.com';
		testLead.Bank_Date_of_DD__c = System.today();
        testLead.PostalCode = 'SE1 0HS';
		testLead.Birthdate__c = myDateLead;
		testLead.CompanyDunsNumber = '229515499';
		testLead.Company = null;
		testLead.Payment_Type__c = 'Single Credit Card';
        testLead.Phone = '+447837231718';
        testLead.isConverted = false;
		insert testLead;

        Account testAccount = new Account();
        testAccount.RecordTypeID = AddLee_Trigger_Test_Utils.accRecordType('Personal Account');
        testAccount.PersonEmail = 'diffsample@test.com';
        testAccount.FirstName = 'Difffname';
        testAccount.LastName = 'Difflname';
        testAccount.Phone = '+447837231718';
        testAccount.Birthdate__pc = myDateAccount;
        
        insert testAccount;
        
        Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        //Test.StartTest();
		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('firstName', testLead.FirstName);
		pageRef.getParameters().put('lastName', testLead.LastName);
		pageRef.getParameters().put('email', testLead.Email);
		pageRef.getParameters().put('phone',testLead.Phone);
		pageRef.getParameters().put('postCode',testLead.PostalCode);
		pageRef.getParameters().put('accType','PersonAccount');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		//theController.isApexTestRunning = true;
		pagereference pageRef1 = theController.actionMethod();
		System.DEBUG('MTDebug pagereference : ' + pageRef1);
		
		system.assertEquals(null, pageRef1);
	}
	
	@isTest(seeallData = false)
	static void AddLee_DAndBSearch_PALeadWithDupAccSameFNameLNameDOBPostCode() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();
		Date myDateLead = Date.newInstance(1960, 2, 17);
        Date myDateAccount = Date.newInstance(1960, 2, 17);
		
		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.FirstName = 'fname';
		testLead.LastName = 'lname';
		testLead.Email = 'sample@test.com';
		testLead.Bank_Date_of_DD__c = System.today();
        testLead.PostalCode = 'SE1 0HS';
		testLead.Birthdate__c = myDateLead;
		testLead.CompanyDunsNumber = '229515499';
		testLead.Company = null;
		testLead.Payment_Type__c = 'Single Credit Card';
        testLead.Phone = '+447837231718';
        testLead.isConverted = false;
		insert testLead;

        Account testAccount = new Account();
        testAccount.RecordTypeID = AddLee_Trigger_Test_Utils.accRecordType('Personal Account');
        testAccount.PersonEmail = 'diffsample@test.com';
        testAccount.FirstName = 'fname';
        testAccount.LastName = 'lname';
        testAccount.Phone = '+447837231718';
        testAccount.Birthdate__pc = myDateAccount;
        testAccount.BillingPostalCode = 'SE1 0HS';
        
        insert testAccount;
        
        Account testAccount1 = new Account();
        testAccount1.RecordTypeID=AddLee_Trigger_Test_Utils.accRecordType('Business Account');
        testAccount1.Name = 'Lname';
        testAccount1.BillingStreet = 'test street';
        testAccount1.BillingCity = 'city';
        testAccount1.BillingPostalCode = 'pc';
        
        insert testAccount1;
        
        
		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        //Test.StartTest();
		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('firstName', testLead.FirstName);
		pageRef.getParameters().put('lastName', testLead.LastName);
		pageRef.getParameters().put('email', testLead.Email);
		pageRef.getParameters().put('phone',testLead.Phone);
		pageRef.getParameters().put('postCode',testLead.PostalCode);
		pageRef.getParameters().put('accType','PersonAccount');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		//theController.isApexTestRunning = true;
		pagereference pageRef1 = theController.actionMethod();
		System.DEBUG('MTDebug pagereference : ' + pageRef1);
		
		system.assertEquals(null, pageRef1);
	}
	
	@isTest(seeallData = false)
	static void AddLee_DAndBSearch_PersonAccountLeadWithDuplicateBussAccount() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();
		
		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.FirstName = 'Fname';
		testLead.LastName = 'Lname';
		testLead.Email = 'sample@test.com';
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.PostalCode = 'SE1 0HS';
		testLead.Company = null;
		testLead.Payment_Type__c = 'Single Credit Card';
		testLead.Phone = '447837231718';
		testLead.Street = 'test street';
		testLead.City = 'city';
		testLead.PostalCode = 'pc';

		insert testLead;

        Account testAccount = new Account();
        testAccount.RecordTypeID=AddLee_Trigger_Test_Utils.accRecordType('Business Account');
        testAccount.Name = 'Lname';
        testAccount.BillingStreet = 'test street';
        testAccount.BillingCity = 'city';
        testAccount.BillingPostalCode = 'pc';
        
        insert testAccount;
        
        
		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        //Test.StartTest();
		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('firstName', testLead.FirstName);
		pageRef.getParameters().put('lastName', testLead.LastName);
		pageRef.getParameters().put('email', testLead.Email);
		pageRef.getParameters().put('phone',testLead.Phone);
		pageRef.getParameters().put('postCode',testLead.PostalCode);
		pageRef.getParameters().put('accType','PersonAccount');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		pagereference pageRef1 = theController.actionMethod();
		
		list<AddLee_Terms_Conditions__c> addLeeTNC = [Select Id From AddLee_Terms_Conditions__c];
		lead leadToCheck = [Select Id, D_B_Credit_Limit__c, Country, Convert_To_Account__c, Credit_Checked__c, Nature_of_Enquiry__c, Tech_Send_Ts_And_Cs__c, D_B_Recommendation__c From Lead Where Id =: testLead.Id];
		system.assert(String.ValueOf(pageRef1).contains('/apex/PaymentCapture?id'));
		system.assertEquals(1, addLeeTNC.Size());
		system.assertEquals(200, leadToCheck.D_B_Credit_Limit__c);
		system.assertEquals('United Kingdom', leadToCheck.Country);
        system.assertEquals(true, leadToCheck.Convert_To_Account__c);
        system.assertEquals(true, leadToCheck.Credit_Checked__c);
        system.assertEquals('Individual/Family Account Application', leadToCheck.Nature_of_Enquiry__c);
        system.assertEquals(true, leadToCheck.Tech_Send_Ts_And_Cs__c);
        system.assertEquals('1', leadToCheck.D_B_Recommendation__c);
    }
    
    @isTest(seeallData = false)
	static void AddLee_DAndBSearch_PerAccLeadWithDupPAccConsumerRecType() {

		AddLee_Trigger_Test_Utils.insertCustomSettings();
        Date myDateLead = Date.newInstance(1960, 2, 17);
        Date myDateAccount = Date.newInstance(1960, 2, 18);
		
		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.FirstName = 'Fname';
		testLead.LastName = 'Lname';
		testLead.Email = 'sample@test.com';
		testLead.Bank_Date_of_DD__c = System.today();
		testLead.PostalCode = 'SE1 0HS';
		testLead.Company = null;
		testLead.Payment_Type__c = 'Single Credit Card';
		testLead.Phone = '447837231718';
		testLead.Street = 'test street';
		testLead.City = 'city';
		testLead.PostalCode = 'pc';

		insert testLead;

        Account testAccount = new Account();
        testAccount.RecordTypeID = AddLee_Trigger_Test_Utils.accRecordType('Consumer');
        testAccount.PersonEmail = 'sample@test.com';
        testAccount.FirstName = 'Difffname';
        testAccount.LastName = 'Difflname';
        testAccount.Phone = '447837231718';
        testAccount.Birthdate__pc = myDateAccount;
        
        insert testAccount;
        
        
		Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
        //Test.StartTest();
		// Make a callout to the page. 
		PageReference pageRef = Page.DAndBSearch;
		pageRef.getParameters().put('leadId', testLead.Id);
		pageRef.getParameters().put('firstName', testLead.FirstName);
		pageRef.getParameters().put('lastName', testLead.LastName);
		pageRef.getParameters().put('email', testLead.Email);
		pageRef.getParameters().put('phone',testLead.Phone);
		pageRef.getParameters().put('postCode',testLead.PostalCode);
		pageRef.getParameters().put('accType','PersonAccount');
		Test.setCurrentPage(pageRef);
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		//controller.
		AddLee_DAndBSearch_Controller.isApexTestRunning = true;
		AddLee_DAndBSearch_Controller theController = new AddLee_DAndBSearch_Controller(controller);
		pagereference pageRef1 = theController.actionMethod();
		
		list<AddLee_Terms_Conditions__c> addLeeTNC = [Select Id From AddLee_Terms_Conditions__c];
		lead leadToCheck = [Select Id, D_B_Credit_Limit__c, Country, Convert_To_Account__c, Credit_Checked__c, Nature_of_Enquiry__c, Tech_Send_Ts_And_Cs__c, D_B_Recommendation__c From Lead Where Id =: testLead.Id];
		system.assert(String.ValueOf(pageRef1).contains('/apex/PaymentCapture?id'));
		system.assertEquals(1, addLeeTNC.Size());
		system.assertEquals(200, leadToCheck.D_B_Credit_Limit__c);
		system.assertEquals('United Kingdom', leadToCheck.Country);
        system.assertEquals(true, leadToCheck.Convert_To_Account__c);
        system.assertEquals(true, leadToCheck.Credit_Checked__c);
        system.assertEquals('Individual/Family Account Application', leadToCheck.Nature_of_Enquiry__c);
        system.assertEquals(true, leadToCheck.Tech_Send_Ts_And_Cs__c);
        system.assertEquals('1', leadToCheck.D_B_Recommendation__c);
    }
}