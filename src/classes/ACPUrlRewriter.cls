global with sharing class ACPUrlRewriter implements Site.UrlRewriter {

    String USRFRDLY_PAGE = '/AddisonLee';
    String TERMS_VISUALFORCE_PAGE = '/ACP_Site_Terms?';

    global PageReference mapRequestUrl(PageReference myFriendlyUrl){
        String url = myFriendlyUrl.getUrl();
        system.debug('Input URL is '+url);
        Map<String, String> urlParams = myFriendlyUrl.getParameters();

        if(url.startsWith(USRFRDLY_PAGE)){
            
            String extId = urlParams.get('sid');
            String accept = urlParams.get('accept');
			AddLee_Terms_Conditions__c tnc = new AddLee_Terms_Conditions__c();
            if(extId.length() == 14){
				try{
        			tnc = [SELECT Id FROM AddLee_Terms_Conditions__c WHERE External_Id__c =:extId LIMIT 1];
				} catch (System.QueryException qe) {
					return new PageReference('/FileNotFound');
				}
				PageReference result = new PageReference(TERMS_VISUALFORCE_PAGE+ 'id=' + tnc.id + '&accept=' +accept);
        		system.debug('Result is '+result);
        		return result;
            } else {
            	return new PageReference('/FileNotFound');
            }
        }

        return null;

    }
    
    global List<PageReference> generateUrlFor(List<PageReference> mySalesforceUrls){

        List<PageReference> myFriendlyUrls = new List<PageReference>();
        List<id> tncIds = new List<id>();
        
        for(PageReference mySalesforceUrl : mySalesforceUrls){	
        	String url = mySalesforceUrl.getUrl();
        	system.debug('The given internal Page is '+url);
 			Map<String, String> urlParams = mySalesforceUrl.getParameters();
            if(url.startsWith(TERMS_VISUALFORCE_PAGE)){
                String id = urlParams.get('Id');
                system.debug('The URL is '+id);
                tncIds.add(id);
            } else {
				myFriendlyUrls.add(mySalesforceUrl);
			}
        }
		system.debug('The URL List is '+tncIds);
		List <AddLee_Terms_Conditions__c> tncList = [SELECT Id, External_Id__c FROM AddLee_Terms_Conditions__c WHERE Id IN :tncIds];
	    system.debug('The tncList is '+tncList);
	    for(AddLee_Terms_Conditions__c tnc : tncList) {
			myFriendlyUrls.add(new PageReference(USRFRDLY_PAGE+'?sid='+ tnc.External_Id__c));
	    }
		return myFriendlyUrls;
	}

}