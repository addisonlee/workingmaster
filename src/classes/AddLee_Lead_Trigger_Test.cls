@isTest
private class AddLee_Lead_Trigger_Test {
	@isTest
    static void LeadTrigger_InsertLead_isLeadInserted() {
		Integer expectedValue = 1000;
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
		List<Lead> leads = AddLee_Trigger_Test_Utils.createLeads(expectedValue);
		
		Test.StartTest();
			insert leads;
		Test.stopTest();
		 
		Integer actualValue = [SELECT count() FROM Lead WHERE Id in :leads];
		System.AssertEquals(expectedValue,actualValue);
    }
    
    @isTest
    static void LeadTrigger_UpdateLead_isLeadUpdated() {
		Integer expectedValue = 1000;
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
		List<Lead> leads = AddLee_Trigger_Test_Utils.createLeads(expectedValue);
		insert leads;
		
		for(Lead eachLead : leads){
			eachLead.Company = 'Modified';
		}
		Test.StartTest();
			update leads;	
		Test.stopTest();
		 
		Integer actualValue = [SELECT count() FROM Lead WHERE Id in :leads];
		System.AssertEquals(expectedValue,actualValue);
    }
    
    @isTest
    static void LeadTrigger_DeleteLead_isLeadDeleted() {
		Integer expectedValue = 1000;
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
		List<Lead> leads = AddLee_Trigger_Test_Utils.createLeads(expectedValue);
		insert leads;
		
		Test.StartTest();
			delete leads;
		Test.stopTest();
		 
		Integer actualValue = [SELECT count() FROM Lead WHERE Id in :leads];
		
		System.AssertEquals(0,actualValue);
    }
    
    @isTest
    static void LeadTrigger_UnDeleteLead_isLeadUndeleted() {
		Integer expectedValue = 1000;
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
		List<Lead> leads = AddLee_Trigger_Test_Utils.createLeads(expectedValue);
		insert leads;
		delete leads;
		
		Test.StartTest();
			undelete leads;
		Test.stopTest();
		 
		Integer actualValue = [SELECT count() FROM Lead WHERE Id in :leads];
		System.AssertEquals(expectedValue,actualValue);
    }
}