public class AddLee_TriggerCaseCommentHandler  {
	
    public void onAfterInsert(final List<CaseComment> newObjects, final Map<Id, CaseComment> newObjectsMap) {
        Set<Id> cseIds = new Set<Id>();
        List<Case> cseList = new List<Case>();
        List<Case> cseToUpdate = new List<Case>();
        for(CaseComment cc : newObjects){
            if(cc.ParentId != null ){
                cseIds.add(cc.ParentId);
            }
        }	                               
        if(!cseIds.isEmpty()){
            cseList = [Select Id, External_Id__c From Case Where Id IN : cseIds];
        }
        if(!cseList.isEmpty()){
            for(Case c : cseList){
                c.External_Id__c = null;
                c.Status = 'Open';
                cseToUpdate.add(c);
            }
        }
        system.debug('MT Debug cseToUpdate : ' + cseToUpdate);
        if(!cseToUpdate.isEmpty()) update cseToUpdate;
    }
}