@isTest
global class AddleeRefundsCreateCustomerMock implements WebServiceMock{
global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.customerResponse respElement =  new Addlee_refunds.customerResponse();
       respElement.errorList = new List<Addlee_refunds.customerError>();
               Addlee_refunds.customerError error = new Addlee_refunds.customerError();
               error.description = 'test';
               error.errorCode = 0;
               error.name = 'test';
               error.number_x = 43543;
               error.stackTrace = 'test';
        respElement.errorList.add(error);       
       response.put('response_x', respElement); 
   }
    
}