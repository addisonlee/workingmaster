public with sharing class AddLee_BookingSummaryHelper {

    public static String wrapperConsumersNumber = '1';
    
    //Handle Insert/Update
    public void handleAction(List<Booking_Summary__c> bookings){
    
        //General 
        List<Booking_Summary__c> summaries              = bookings;
        List<Booking_Summary__c> bookerSummaries        = new List<Booking_Summary__c>();
        List<Booking_Summary__c> consumerSummaries      = new List<Booking_Summary__c>();
        Map<Id, Account> bookingsAccount                = new Map<Id, Account>();
        List<String> accountList                        = new List<String>();
        List<Contact> bookers                           = new List<Contact>();
        List<Account> consumers                         = new List<Account>();
        Map<String, Id> bookersMap                      = new Map<String, Id>();
        Map<String, Id> consumerMap                     = new Map<String, Id>();
        String RecTypeId                                = [SELECT Id FROM RecordType WHERE (Name='Consumer') and (SobjectType='Account')].Id;
        
        System.debug('<<<CHECK_POINT_HANDLE_ACTION>>>:'+summaries);
        
        //Create Account Map
        for(Booking_Summary__c bs:summaries){
            accountList.add(bs.Account__c);
        }
        
        bookingsAccount = new Map<ID, Account>([SELECT Id, Account_Number__c FROM Account WHERE Id IN :accountList]);
        
                
        //Create Booker List
        for(Booking_Summary__c bs:summaries){
            Account acc = bookingsAccount.get(bs.Account__c);
            
            System.debug('<<<ACCOUNT_BOOKING>>>:'+acc);
            //System.debug('<<<ACCOUNT_BOOKING_NUMBER>>>:'+acc.Account_Number__c);
            
            //Create Booker if the Account related is not the NumberOne otherwise the a consumer account can be related 
            if(acc!=null && acc.Account_Number__c != wrapperConsumersNumber){
                Contact booker          = new Contact();
                booker.Phone            = bs.Individual_Telephone__c;
                booker.Booker_Id__c     = bs.Individual_Id__c;
                booker.LastName         = bs.Individual_Name__c;
                booker.PIN__c           = bs.Individual_Pin__c;
                booker.Email            = bs.Individual_Email__c;
                booker.AccountId        = bs.Account__c;
                booker.Contact_Type__c  = 'Booker Contact';
                bookers.add(booker);
                
                //Add the booking to the list of the BookerSummaries    
                bookerSummaries.add(bs);            
            
            }else{
                Account consumer                    = new Account();
                //consumer.Phone                      = bs.Individual_Telephone__c;
                consumer.ExternalId_Consumer__c     = bs.Individual_Id__c;
                //consumer.LastName                   = bs.Individual_Name__c;
                //consumer.PersonEmail                = bs.Individual_Email__c;
                //consumer.RecordTypeId               = RecTypeId;
                consumers.add(consumer);
                
                //Add the booking to the list of the ConsumerSummaries
                consumerSummaries.add(bs);
            }
        }
        
        //Upsert Bookers
        Database.upsert(bookers, Contact.Booker_Id__c, false);
        
        //Upsert Consumers
        Database.upsert(consumers, Account.ExternalId_Consumer__c, false);
        
        
        //Create Map Bookers
        for(Contact b : bookers){
            bookersMap.put(b.Booker_Id__c, b.Id);
        }

        //Create Map Consumers
        for(Account a : consumers){
            consumerMap.put(a.ExternalId_Consumer__c, a.Id);
        }
        
        //Realate the Booking Summary to the Booker
        for(Booking_Summary__c bs : bookerSummaries){
            //if()
            bs.Booker__c = bookersMap.get(bs.Individual_Id__c); 
        }

        //Realate the Booking Summary to the Consumer
        for(Booking_Summary__c bs : consumerSummaries){
            //if()
            bs.Account__c = consumerMap.get(bs.Individual_Id__c);   
        }
    }
    
 // Handle service type and booking channel for Marketing     
public void handleServiceTypeAction(List<Booking_Summary__c> bookings){
    DateTime dt = (System.now()-20);
    Datetime startDate = Datetime.newInstance(dt.year(), dt.month(), dt.day(), 0, 0, 0);
    //for(Booking_Summary__c bookings:[SELECT Id,Account__c,Date_Time__c,Number_of_Bookings__c,Date_From__c,Total_Price__c,Total_Journey_Price_Currency__c,Service_Type__c FROM Booking_Summary__c where Account__c=:AccountId]){
    Set<Id> AccountIds = new Set<Id>();
    Integer totalcount =0;
    List<Booking_Summary__c> bbook = new list<Booking_Summary__c>();
    Map<String,Integer> mapBookServiceTypeCount = new Map<String,Integer>();
    Map<String,Integer> mapBookingChannelCount = new Map<String,Integer>();
    for(Booking_Summary__c book:bookings){
    AccountIds.Add(book.Account__c);
    }
     //bbook =[select id,name,Service_Type__c, Number_of_Bookings__c, Booking_Channel__c from Booking_Summary__c where Account__c=:AccountIds LIMIT 49998];
    //for(Booking_Summary__c book:[select id,name,Service_Type__c,Number_of_Bookings__c, Booking_Channel__c from Booking_Summary__c where Account__c=:AccountIds LIMIT 49900]){
     for(Booking_Summary__c book:bookings){
    // for(Booking_Summary__c book:bbook){
     if(book.Number_of_Bookings__c!=null){
     totalcount = totalcount+Integer.valueof(book.Number_of_Bookings__c);
     }
      system.debug('totalcount++++'+ totalcount ); 
        if(book.Number_of_Bookings__c!=null){    
        if(mapBookServiceTypeCount.containskey(book.Service_Type__c.toupperCase())){
        mapBookServiceTypeCount.put(book.Service_Type__c.toupperCase(),mapBookServiceTypeCount.get(book.Service_Type__c.toupperCase())+Integer.valueof(book.Number_of_Bookings__c));
    }
     else {
        mapBookServiceTypeCount.put(book.Service_Type__c.toupperCase(),Integer.valueof(book.Number_of_Bookings__c));
    }
    }
    if(book.Booking_Channel__c==null || book.Booking_Channel__c==''){book.Booking_Channel__c='AA';}
    if(book.Number_of_Bookings__c!=null){ 
     if(mapBookingChannelCount.containskey(book.Booking_Channel__c.toupperCase())){
        System.debug('test '+book.Booking_Channel__c+' ##### '+mapBookingChannelCount.get(book.Booking_Channel__c.toupperCase()));
        mapBookingChannelCount.put(book.Booking_Channel__c.toupperCase(),mapBookingChannelCount.get(book.Booking_Channel__c.toupperCase())+Integer.valueof(book.Number_of_Bookings__c));
    } 
    else {
        mapBookingChannelCount.put(book.Booking_Channel__c.toupperCase(),Integer.valueof(book.Number_of_Bookings__c));
    }
       // totalcount++;
        }
    }
    system.debug('totalcount++++'+ totalcount ); 
    
    List<Account> lstAccount = [Select id,Affiliates__c,Affiliates_Usage__c,Old_Android_App__c,Old_Android_App_Percent__c,Full_Websites__c,Full_Websites_Usage__c,Windows_Phone_App__c,Windows_Phone_App_Percentage__c,Other_Channels__c,Other_Channels_Percentage__c,GG_andriod_app__c,GG_Andriod_App_Percentage__c,Quick_Booker__c,Quick_Booker_Usage__c,Platforms__c,Platforms_Usage__c,Classic_Andriod_App__c,Classic_Andriod_App_Percentage__c,Classic_Blackberry_App__c,Classic_Blackberry_App_Percentage__c, New_Android_App__c, GG_App__c,GG_App_Percent__c, Somo_App__c,Somo_App_Percent__c, Classic_App__c, Classic_App_Percent__c, Web_booking__c,Web_booking_Percent__c,Contact_Centre__c,Contact_Centre_Percentage__c,Delivery_Service__c,Delivery_Percentage__c,Hybrid_Cars__c,Hybrid_Percentage__c,Executive_Cars__c,Executive_Percentage__c,Vans__c ,vans_Percent__c ,VIP_Cars__c,VIP_Cars_Percent__c,TaxyBike__c,TaxyBikePercent__c,Sub_Contractedm__c,Sub_Contracted__c,Standard_Cars__c,Standard_Cars_Percent__c,S_Class_Mercedes__c,S_Class_MercedesPercent__c,Mobility_Cars__c,Mobility_Cars_Percent__c,Luton_Van__c,Luton_Van_Percent__c,Green__c,Green_Percent__c,Bikes__c,Bikes_Percentage__c,Eco__c,Eco_Percentage__c from Account where Id=:AccountIds];
    for(Account accc:lstAccount){
        system.debug('enter 138' + mapBookingChannelCount);
        accc.Full_Websites__c = false;
        accc.Full_Websites_Usage__c = 0;
        accc.Affiliates__c = false;
        accc.Affiliates_Usage__c = 0;
        accc.Old_Android_App__c = false;
        accc.Old_Android_App_Percent__c = 0;
        accc.Windows_Phone_App__c = false;
        accc.Windows_Phone_App_Percentage__c = 0;
        accc.Other_Channels__c = false;
        accc.Other_Channels_Percentage__c = 0;
        accc.GG_andriod_app__c = false;
        accc.GG_Andriod_App_Percentage__c = 0;
        accc.Quick_Booker__c = false;
        accc.Quick_Booker_Usage__c = 0;
        accc.Platforms__c = false;
        accc.Platforms_Usage__c = 0;
        accc.Classic_Blackberry_App__c = false;
        accc.Classic_Blackberry_App_Percentage__c = 0;
        accc.Contact_Centre__c = false;
        accc.Contact_Centre_Percentage__c = 0;
        accc.Classic_Andriod_App__c = false;
        accc.Classic_Andriod_App_Percentage__c = 0;
        accc.Web_booking__c = false;
        accc.Web_booking_Percent__c = 0;
        accc.Classic_App__c = false;
        accc.Classic_App_Percent__c = 0;
        accc.Somo_App__c = false;
        accc.Somo_App_Percent__c = 0;
        accc.GG_App__c = false;
        accc.GG_App_Percent__c = 0;
        accc.New_Android_App__c = false;
        accc.New_Android_App_Percent__c = 0;
        accc.Executive_Cars__c = false;
        accc.Executive_Percentage__c = 0;
        accc.Hybrid_Cars__c = false;
        accc.Hybrid_Percentage__c = 0;
        accc.Delivery_Service__c = false;
        accc.Delivery_Percentage__c = 0;
        accc.Bikes__c = false;
        accc.Bikes_Percentage__c = 0;
        accc.TaxyBike__c = false;
        accc.TaxyBikePercent__c = 0;
        accc.Eco__c = false;
        accc.Eco_Percentage__c = 0;
        accc.Green__c = false;
        accc.Green_Percent__c = 0;
        accc.Luton_Van__c = false;
        accc.Luton_Van_Percent__c = 0;
        accc.Mobility_Cars__c = false;
        accc.Mobility_Cars_Percent__c = 0;
        accc.Parcels__c = false;
        accc.Parcels_Percent__c = 0;
        accc.S_Class_Mercedes__c = false;
        accc.S_Class_MercedesPercent__c = 0;
        accc.Standard_Cars__c = false;
        accc.Standard_Cars_Percent__c = 0;
        accc.Sub_Contractedm__c = false;
        accc.Sub_Contracted__c = 0;
        accc.Vans__c = false;
        accc.vans_Percent__c = 0;
        accc.VIP_Cars__c = false;
        accc.VIP_Cars_Percent__c = 0;
      system.debug('enter 181');  
    if(mapBookingChannelCount.containskey('Contact Centre'.toupperCase())){
        accc.Contact_Centre__c = true;
        accc.Contact_Centre_Percentage__c = (Decimal.valueof(mapBookingChannelCount.get('Contact Centre'.toupperCase())*100)/totalcount);
    } 
    if(mapBookingChannelCount.containskey('Classic Android App'.toupperCase())){
        accc.Classic_Andriod_App__c = true;
        accc.Classic_Andriod_App_Percentage__c = (Decimal.valueof(mapBookingChannelCount.get('Classic Android App'.toupperCase())*100)/totalcount);
    } 
    if(mapBookingChannelCount.containskey('GG Android App'.toupperCase())){
        accc.GG_andriod_app__c = true;
        accc.GG_Andriod_App_Percentage__c = (Decimal.valueof(mapBookingChannelCount.get('GG Android App'.toupperCase())*100)/totalcount);
    } 
    if(mapBookingChannelCount.containskey('Classic Blackberry App'.toupperCase())){
        accc.Classic_Blackberry_App__c = true;
        accc.Classic_Blackberry_App_Percentage__c = (Decimal.valueof(mapBookingChannelCount.get('Classic Blackberry App'.toupperCase())*100)/totalcount);
    } 
    if(mapBookingChannelCount.containskey('Platforms'.toupperCase())){
        accc.Platforms__c = true;
        accc.Platforms_Usage__c = (Decimal.valueof(mapBookingChannelCount.get('Platforms'.toupperCase())*100)/totalcount);
    } 
     if(mapBookingChannelCount.containskey('Full Website'.toupperCase())){
        accc.Full_Websites__c = true;
        accc.Full_Websites_Usage__c = (Decimal.valueof(mapBookingChannelCount.get('Full Website'.toupperCase())*100)/totalcount);
    } 
    if(mapBookingChannelCount.containskey('Quick Booker'.toupperCase())){
        accc.Quick_Booker__c = true;
        accc.Quick_Booker_Usage__c = (Decimal.valueof(mapBookingChannelCount.get('Quick Booker'.toupperCase())*100)/totalcount);
    } 
     if(mapBookingChannelCount.containskey('Other Channels'.toupperCase())){
        accc.Other_Channels__c = true;
        accc.Other_Channels_Percentage__c = (Decimal.valueof(mapBookingChannelCount.get('Other Channels'.toupperCase())*100)/totalcount);
    } 
    if(mapBookingChannelCount.containskey('Windows Phone App'.toupperCase())){
        accc.Windows_Phone_App__c = true;
        accc.Windows_Phone_App_Percentage__c = (Decimal.valueof(mapBookingChannelCount.get('Windows Phone App'.toupperCase())*100)/totalcount);
    } 
    if(mapBookingChannelCount.containskey('Web booking'.toupperCase())){
        accc.Web_booking__c = true;
        accc.Web_booking_Percent__c = (Decimal.valueof(mapBookingChannelCount.get('Web booking'.toupperCase())*100)/totalcount);
    } 
    if(mapBookingChannelCount.containskey('Classic Iphone App'.toupperCase())){
        accc.Classic_App__c = true;
        accc.Classic_App_Percent__c = (Decimal.valueof(mapBookingChannelCount.get('Classic Iphone App'.toupperCase())*100)/totalcount);
    } 
    if(mapBookingChannelCount.containskey('Somo Iphone App'.toupperCase())){
        accc.Somo_App__c = true;
        accc.Somo_App_Percent__c = (Decimal.valueof(mapBookingChannelCount.get('Somo Iphone App'.toupperCase())*100)/totalcount);
    } 
    if(mapBookingChannelCount.containskey('GG Iphone App'.toupperCase())){
        accc.GG_App__c = true;
        accc.GG_App_Percent__c = (Decimal.valueof(mapBookingChannelCount.get('GG Iphone App'.toupperCase())*100)/totalcount);
    } 
    if(mapBookingChannelCount.containskey('Old Android App'.toupperCase())){
        accc.Old_Android_App__c = true;
        accc.Old_Android_App_Percent__c = (Decimal.valueof(mapBookingChannelCount.get('Old Android App'.toupperCase())*100)/totalcount);
    } 
    if(mapBookingChannelCount.containskey('New Android App'.toupperCase())){
        accc.New_Android_App__c = true;
        accc.New_Android_App_Percent__c = (Decimal.valueof(mapBookingChannelCount.get('New Android App'.toupperCase())*100)/totalcount);
    } 

 system.debug('enter 211' );
    if(mapBookServiceTypeCount.containskey('bikes'.toupperCase())){
        system.debug('enter into bikes');
        accc.Bikes__c = true;
        accc.Bikes_Percentage__c = (Decimal.valueof(mapBookServiceTypeCount.get('bikes'.toupperCase())*100)/totalcount);
    } 
     if(mapBookServiceTypeCount.containskey('Affiliates'.toupperCase())){
        system.debug('enter into Affiliates');
        accc.Affiliates__c = true;
        accc.Affiliates_Usage__c = (Decimal.valueof(mapBookServiceTypeCount.get('Affiliates'.toupperCase())*100)/totalcount);
    } 
    if(mapBookServiceTypeCount.containskey('ECO'.toupperCase()) || mapBookServiceTypeCount.containskey('GREEN'.toupperCase())){
        accc.Hybrid_Cars__c = true;
        Integer hc = 0;
    if(mapBookServiceTypeCount.containskey('ECO'.toupperCase())){
        hc = mapBookServiceTypeCount.get('ECO'.toupperCase());
    }
    if(mapBookServiceTypeCount.containskey('GREEN'.toupperCase())){
    hc = hc + mapBookServiceTypeCount.get('GREEN'.toupperCase());
    }
    accc.Hybrid_Percentage__c = (Decimal.valueof(hc*100)/totalcount);
    } 
    if(mapBookServiceTypeCount.containskey('LUTON VAN'.toupperCase()) || mapBookServiceTypeCount.containskey('PARCELS'.toupperCase()) || mapBookServiceTypeCount.containskey('VANS'.toupperCase())){
        accc.Delivery_Service__c = true;
        Integer Ds= 0;
    if(mapBookServiceTypeCount.containskey('LUTON VAN'.toupperCase())){Ds = Ds +mapBookServiceTypeCount.get('LUTON VAN'.toupperCase());
    }
    if(mapBookServiceTypeCount.containskey('PARCELS'.toupperCase())){
        Ds = Ds + mapBookServiceTypeCount.get('PARCELS'.toupperCase());
    }
    if(mapBookServiceTypeCount.containskey('VANS'.toupperCase())){
        Ds = Ds + mapBookServiceTypeCount.get('VANS'.toupperCase());
    }
        accc.Delivery_Percentage__c = (Decimal.valueof(Ds *100)/totalcount);
    } 
    if(mapBookServiceTypeCount.containskey('MOBILITY CARS'.toupperCase())){
        accc.Mobility_Cars__c = true;
        accc.Mobility_Cars_Percent__c = (Decimal.valueof(mapBookServiceTypeCount.get('MOBILITY CARS'.toupperCase())*100)/totalcount);
    }  
    if(mapBookServiceTypeCount.containskey('S CLASS MERCEDES'.toupperCase()) || mapBookServiceTypeCount.containskey('VIP CARS'.toupperCase())){
        accc.Executive_Cars__c = true;
        Integer ec= 0;
    if(mapBookServiceTypeCount.containskey('S CLASS MERCEDES'.toupperCase())){
        ec = ec + mapBookServiceTypeCount.get('S CLASS MERCEDES'.toupperCase());
        }
    if(mapBookServiceTypeCount.containskey('VIP CARS'.toupperCase())){
        ec = ec + mapBookServiceTypeCount.get('VIP CARS'.toupperCase());
        }
        accc.Executive_Percentage__c = (Decimal.valueof(ec*100)/totalcount);
        } 
    if(mapBookServiceTypeCount.containskey('STANDARD CARS'.toupperCase())){
        accc.Standard_Cars__c = true;
        accc.Standard_Cars_Percent__c = (Decimal.valueof(mapBookServiceTypeCount.get('STANDARD CARS'.toupperCase())*100)/totalcount);
//accc.Standard_Cars_Percent__c = (Decimal.valueof(mapBookServiceTypeCount.get(stttt)*100)/mapBookServiceTypeCount.get(stttt));
system.debug('accc.Standard_Cars_Percent__c+++++'+accc.Standard_Cars_Percent__c);
    }  
    if(mapBookServiceTypeCount.containskey('TAXYBIKE'.toupperCase()) ||mapBookServiceTypeCount.containskey('BIKES'.toupperCase())){
        accc.TaxyBike__c = true;
        Integer tb= 0;
    if(mapBookServiceTypeCount.containskey('TAXYBIKE')){
        tb = tb + mapBookServiceTypeCount.get('TAXYBIKE'.toupperCase());
    }
    if(mapBookServiceTypeCount.containskey('BIKES'.toupperCase())){
        tb = tb + mapBookServiceTypeCount.get('BIKES'.toupperCase());
    }
        accc.TaxyBikePercent__c = (Decimal.valueof(tb*100)/totalcount);
    } 
     else {}

  }

    update lstAccount;
     }
  }  
//}