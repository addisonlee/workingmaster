@istest(seealldata = true)
public class BatchCreateRecordsNPSTest {
    public static testmethod void BatchTest() {
        //Nps_Report__c Nps = new Nps_Report__c(Name = 'Report Data', No_of_records_Percentage__c = '10' ,Report_Name__c= 'NPS B2C Contact Centre FINAL');
       // insert Nps;
        Nps_Report__c nps= Nps_Report__c.getValues('Report Data');
        List <Report> reportList = [SELECT DeveloperName,FolderName,Format,Id,Name FROM Report where name =: nps.Report_Name__c];
      
        Database.BatchableContext BC;
        List<sobject> scope;
        BatchCreateRecordsNPS batch = new BatchCreateRecordsNPS(); 
        batch.start(bc);
        batch.execute(bc,reportList);
        batch.finish(bc);
    }
    /*public static testmethod void BatchTest2() {
        Nps_Report__c Nps = new Nps_Report__c(Name = 'Report Data1', No_of_records_Percentage__c = '10%' ,Report_Name__c= 'NPS Report');
        insert Nps;
       // Nps_Report__c nps= Nps_Report__c.getValues('Report Data');
        List <Report> reportList = [SELECT DeveloperName,FolderName,Format,Id,Name FROM Report where name =: nps.Report_Name__c];
      
        Database.BatchableContext BC;
        List<sobject> scope;
        BatchCreateRecordsNPS batch = new BatchCreateRecordsNPS(); 
        batch.start(bc);
        //batch.execute(bc,reportList);
        batch.finish(bc);
    }*/
}