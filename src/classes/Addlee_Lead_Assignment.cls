public with sharing class Addlee_Lead_Assignment {
	public Addlee_Lead_Assignment() {
		
	}
	@future
	public static void allocateOwnerWebToLead(Set<Id> leadTriggerSet){
		
		List<Lead> leadTriggerList = [SELECT Id, Owner_Alias__c, OwnerId FROM Lead WHERE Id in:leadTriggerSet];
		Set<String> ownerAliases = new Set<String>();
		Map<String,Id> ownerAliasMap = new Map<String,Id>();
		ownerAliases = populateOwnerAliases(leadTriggerList);
		ownerAliasMap = populateOwnerAliasMap(ownerAliases);
		for(Lead eachLead :  leadTriggerList){
			if(eachLead.Owner_Alias__c!=null){
				if(ownerAliasMap.get(eachLead.Owner_Alias__c) != null){
					eachLead.ownerId = ownerAliasMap.get(eachLead.Owner_Alias__c);
				}
				System.DEBUG(LoggingLevel.ERROR, 'Owner : ' + eachLead.ownerId);
				eachLead.Owner_Alias__c = null;
			}
		}
		update leadTriggerList;
	}


	private static Set<String> populateOwnerAliases(List<Lead> leadTriggerList){
		Set<String> aliases = new Set<String>();
		for(Lead eachLead : leadTriggerList){

			if(eachLead.Owner_Alias__c != null){
				aliases.add(eachLead.Owner_Alias__c);
			}

		}
		return aliases;
	}

	private static Map<String,Id> populateOwnerAliasMap(Set<String> ownerAliases){

		Map<String,Id> ownerMap = new Map<String,Id>();
		List<User> owners = [SELECT Id, Alias FROM USER WHERE alias in :ownerAliases ];
		for(User eachUser : owners){

			if (ownerMap.get(eachUser.Alias) == null){
				ownerMap.put(eachUser.Alias,eachUser.Id);
			}

		}
		return ownerMap;

	}
}