/*
Scheduler for Calculate monthly spend on Business Accounts
*/

global class MonthlySpendBusinessAccounts_Schedular implements Schedulable {
  global void execute (SchedulableContext sc) {
    List<String> recordtypeNames=new List<String>{'Business Account','Read Only Business Account'};
    CalculateMonthlyBookingAverageOnAccount cms = new CalculateMonthlyBookingAverageOnAccount();
     cms.Query = 'select id,Date_of_Last_Booking__c,Consumer_Account_Number__c,Name,Grading__c,Account_Status__c,Date_changed_to_Current__c,IsPersonAccount,Marketing_Status__c,Sales_Ledger__c,Account_Managed__c,Total_Amount_of_Bookings__c from Account where (RecordType.Name = ' + '\'' + 'Business Account' + '\'' + ' OR RecordType.Name =' + '\'' + 'Read Only Business Account' + '\'' + ') and Date_of_Last_Booking__c = LAST_N_MONTHS:1 and Total_Amount_of_Bookings__c <= 49999';
     Database.executeBatch(cms,20);
    
  }
  

}