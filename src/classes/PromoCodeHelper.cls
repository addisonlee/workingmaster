/* When promocodes are running out in system , an email will be sent to send new list of codes to upload */

public Class PromoCodeHelper
{
    public Static void sendEmail(List<Promo_Code__c> lstpromocode, Map<Id,Promo_Code__c> mapPromoCode)
    {
        if(lstpromocode.size() ==1)
    {
        for(Promo_Code__c pcode:lstpromocode)
        {
            if(pcode.Allocated__c != mapPromoCode.get(pcode.Id).Allocated__c)
            {
                PromoCodeValue__c promoCustomSetting = PromoCodeValue__c.getValues('Promo code Values'); // Custom setting to show Promo code value to trigger email 
                string promoCount; // To check the count of codes left 
                
                if(promoCustomSetting.No_of_Promo_Codes__c != null)
                {
                    promoCount = promoCustomSetting.No_of_Promo_Codes__c;
                
                    integer PCount = [select count() from Promo_Code__c where Allocated__c = false];  // Checking where codes are not allocated 
                
                    if(pCount <= integer.valueof(promoCount)) { 
                        /*Emailtemplate template = new emailtemplate();
                        for(EmailTemplate e: [select id,name,body,Htmlvalue,subject from emailtemplate where name =:'PromoCodesTemplate']) {
                            if(e != null) 
                                template = e;
                        }*/
                        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                        String[] toAddresses;
                        if(promoCustomSetting.Email__c != null ) {
                            toAddresses = promoCustomSetting.Email__c.split(';');
                        }
                         // Sending email when codes are running out in the system
                        if(toAddresses != null) {
                            mail.setToAddresses(toAddresses);                
                            mail.setSubject('Promo Codes are running out in SF');
                            mail.setHtmlBody('Hi Team, <br/><br/> Promo Codes are running out in SF. There are ' + PCount + ' codes left in the system.Please send new sheet to upload in Salesforce. <br/><br/> Thanks, <br/> Salesforce Team ');
                            messaging.sendemailresult[] r=Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                        }
                    }
                }
            }
        }
    }
    }
}