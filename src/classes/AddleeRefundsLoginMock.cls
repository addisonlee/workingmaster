@isTest
global class AddleeRefundsLoginMock implements webservicemock {
 global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.loginResponse respElement =  new Addlee_refunds.loginResponse();
           respElement.sessionId = '6867';
           respElement.errorCode = '0';
       response.put('response_x', respElement); 
   }
}