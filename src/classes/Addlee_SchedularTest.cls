@isTest
private class Addlee_SchedularTest {

    @testSetup 
    static void insertCustomSettings() {
        Addlee_Trigger_Test_Utils.insertCustomSettings();
    }
    
    public static testMethod void testAddlee_CampaignMember_Batch_Schedular() {
        Test.StartTest();
    	Addlee_CampaignMember_Batch_Schedular sh1 = new Addlee_CampaignMember_Batch_Schedular();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('Addlee CampaignMember Batch Schedular', sch, sh1); 
    	Test.stopTest(); 
    } 
    public static testMethod void testAddlee_ScheduleContactsPhone() {
        Test.StartTest();
    	Addlee_ScheduleContactsPhone sh1 = new Addlee_ScheduleContactsPhone();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('Addlee ScheduleContactsPhone', sch, sh1); 
    	Test.stopTest(); 
    } 
    public static testMethod void testAddlee_ScheduleLeadMobileNumberFormat() {
        Test.StartTest();
    	Addlee_ScheduleLeadMobileNumberFormat sh1 = new Addlee_ScheduleLeadMobileNumberFormat();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('Addlee ScheduleLeadMobileNumberFormat', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testAddlee_ScheduleleadsPhone() {
        Test.StartTest();
    	Addlee_ScheduleleadsPhone sh1 = new Addlee_ScheduleleadsPhone();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('Addlee ScheduleleadsPhone', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testMarketingSatusConsumer_Schedular() {
        Test.StartTest();
    	MarketingSatusConsumer_Schedular sh1 = new MarketingSatusConsumer_Schedular();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('MarketingSatusConsumer Schedular', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testMarketingStatus_Scheduler() {
        Test.StartTest();
    	MarketingStatus_Scheduler sh1 = new MarketingStatus_Scheduler();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('MarketingStatus Scheduler', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testMonthlySpendBusinessAccounts_Schedular() {
        Test.StartTest();
    	MonthlySpendBusinessAccounts_Schedular sh1 = new MonthlySpendBusinessAccounts_Schedular();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('MonthlySpendBusinessAccounts Schedular', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testMonthlySpendForConsumers_Schedular() {
        Test.StartTest();
    	MonthlySpendForConsumers_Schedular sh1 = new MonthlySpendForConsumers_Schedular();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('MonthlySpendForConsumers Schedular', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testMonthlySpendForPersonAccounts_Schedular() {
        Test.StartTest();
    	MonthlySpendForPersonAccounts_Schedular sh1 = new MonthlySpendForPersonAccounts_Schedular();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('MonthlySpendForPersonAccounts Schedular', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testMonthlySpend_Scheduler() {
        Test.StartTest();
    	MonthlySpend_Scheduler sh1 = new MonthlySpend_Scheduler();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('MonthlySpend Scheduler', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testNospendLapsed_Batch_Scheduler_Lapsed() {
        Test.StartTest();
    	NospendLapsed_Batch_Scheduler_Lapsed sh1 = new NospendLapsed_Batch_Scheduler_Lapsed();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('NospendLapsed Batch Scheduler Lapsed', sch, sh1); 
    	Test.stopTest(); 
    }
    /*
    public static testMethod void testNospendLapsed_Batch_Scheduler_NoSpend() {
        Test.StartTest();
    	NospendLapsed_Batch_Scheduler_NoSpend sh1 = new NospendLapsed_Batch_Scheduler_NoSpend();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('NospendLapsed Batch Scheduler NoLapsed', sch, sh1); 
    	Test.stopTest(); 
    }
    */
    public static testMethod void testScheduleBatchCreateRecordsNPS() {
        Test.StartTest();
    	ScheduleBatchCreateRecordsNPS sh1 = new ScheduleBatchCreateRecordsNPS();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('ScheduleBatchCreateRecordsNPS', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testScheduleBookingSummaryAllBatch() {
        Test.StartTest();
    	ScheduleBookingSummaryAllBatch sh1 = new ScheduleBookingSummaryAllBatch();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('ScheduleBookingSummaryAllBatch', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testScheduleBookingSummaryBatch() {
        Test.StartTest();
    	ScheduleBookingSummaryBatch sh1 = new ScheduleBookingSummaryBatch();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('ScheduleBookingSummaryBatch', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testServicetypeForConsumers_Schedular() {
        Test.StartTest();
    	ServicetypeForConsumers_Schedular sh1 = new ServicetypeForConsumers_Schedular();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('ServicetypeForConsumers Schedular', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testServicetypeForPersonAccounts_Schedular() {
        Test.StartTest();
    	ServicetypeForPersonAccounts_Schedular sh1 = new ServicetypeForPersonAccounts_Schedular();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('ServicetypeForPersonAccounts Schedular', sch, sh1); 
    	Test.stopTest(); 
    }
    public static testMethod void testServicetype_Schedular() {
        Test.StartTest();
    	Servicetype_Schedular sh1 = new Servicetype_Schedular();
    	String sch = '0 0 23 * * ?'; 
    	system.schedule('Servicetype Schedular', sch, sh1); 
    	Test.stopTest(); 
    }
}