/*
Populate booker name in the booking summary 
developed by GVK.
*/
public class Addlee_BookerHandler{
 public map<string,Id> BookerIdContactId = new map<string,Id>();
  public list<Booking_Summary__c> Bookingstobeupdated = new list<Booking_Summary__c>();
   public set<Id> conIds = new set<Id>();
    public list<contact> contactstobeupdated = new list<Contact>();
     public set<Id> AccIds = new set<Id>();
      public void bookerhandler(list<Booking_Summary__c> bookings){
          for(Booking_Summary__c b:bookings){
          AccIds.add(b.Account__c);
           }
          // get the Contact of booking summary from contact.
          for(Contact c:[select Id,Name,Booker_Id__c From Contact where AccountId IN:AccIds AND Booker_Id__c!= null]){
          BookerIdContactId.put(c.Booker_Id__c,c.Id);        
          }
          // Populate Booker Name in Booking Summary
          for(Booking_Summary__c book:bookings){
          if(BookerIdContactId.containskey(book.Individual_Id__c)){
          book.Booker__c= BookerIdContactId.get(book.Individual_Id__c);
          conIds.add(book.Booker__c);
          system.debug('BOOKER NAME++++++'+book.Individual_Id__c);
          Bookingstobeupdated.add(book);
          }
          }
          if(!Bookingstobeupdated.isempty())
          update Bookingstobeupdated;
   //     update Contact Booked is equal to true
          for(Contact contactRecord:[select Id, AccountId,firstname,lastname,Booker_Id__c,Booked__c 
                                    FROM Contact 
                                    where ispersonaccount = false and Id IN:conIds]){
            contactRecord.Booked__c=true;
            contactstobeupdated.add(contactRecord);
            }
          if(!contactstobeupdated.isempty())
        update contactstobeupdated;
      }
    }