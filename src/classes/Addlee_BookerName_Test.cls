@isTest
public with sharing class Addlee_BookerName_Test {
   public static testmethod void testm(){
       AddLee_Trigger_Test_Utils.insertCustomSettings();  
       Account acc = AddLee_Trigger_Test_Utils.createAccountBooker(1).get(0);
       insert acc;      
       Contact c = new contact();
        c.LastName='Tst';
        c.Booker_Id__c='12345678';
        c.AccountId=acc.Id;
        insert c;  
        Booking_Summary__c b = new Booking_Summary__c();
        b.Name='Test';
        b.Account__c=acc.Id;
        b.Booker__c=c.Id;
        b.Individual_Id__c='12345678';       
        insert b;                 
        Addlee_BookernameScheduleClass sh1 = new Addlee_BookernameScheduleClass();      
        String sch = '0  00 1 3 * ?';
        system.schedule('Test', sch, sh1);
        database.executeBatch(new BookingSummaryBatch(),100);
    }
}