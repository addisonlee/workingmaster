/**
 **test class  for OptoutHandler_Account 
 */ 
@isTest
private class OptoutHandler_Account_Test {

    static testMethod void optOutone() {
    	Addlee_Trigger_Test_Utils.insertCustomSettings();
		Account testAccount = Addlee_Trigger_Test_Utils.createAccounts(1)[0];
		insert testAccount;
		
		Contact testContact = new Contact(LastName = '2 Weeks Test', accountId = testAccount.Id, Contact_Type__c = 'Main Contact', Email = 'test@test.com');
		insert testContact;
		testAccount.OPT_OUT_ALL_MARKETING_FOR_ACCOUNT__c = true ;
		
		update testAccount;
		
		
		Account testAccount1 = Addlee_Trigger_Test_Utils.createAccounts(1)[0];
		insert testAccount1;
		Contact testContact1 = new Contact(LastName = 'Test2', accountId = testAccount1.Id, Contact_Type__c = 'Other Contact', Email = 'test@test.com');
		insert  testContact1;
		testAccount1.Opt_Out_Of_Email_Marketing__c = true;
		update testAccount1; 
		update testContact1;
		Account testAccount2 = Addlee_Trigger_Test_Utils.createAccounts(1)[0];
		insert testAccount2;
		Contact testContact2 = new Contact(LastName = 'Test2', accountId = testAccount1.Id, Contact_Type__c = 'Other Contact', Email = 'test@test.com');
		insert  testContact2;
				
		testAccount2.Opt_Out_Of_SMS_Marketing__c = true;
        update testAccount2;
        update testContact2;
        system.assert(testContact2.News_Letters_And_Updates_SMS__c == false);
        
    }
     static testMethod void optOutSecond() {
    	Addlee_Trigger_Test_Utils.insertCustomSettings();
    	Contact con = Addlee_Trigger_Test_Utils.createContacts(1)[0];
    	insert con;
    	Account optoutAccount = Addlee_Trigger_Test_Utils.createAccounts(1)[0];
		insert optoutAccount;
		optoutAccount.Opt_Out_Of_Email_Marketing__c = true;
		update optoutAccount;
		Contact optoutContact2 = new Contact(LastName = 'optTest', accountId = optoutAccount.Id, Contact_Type__c = 'Other Contact',
											 Email = 'test@test.com');
		insert  optoutContact2;
        system.assert(optoutContact2.Surveys_And_Research_Email__c == false);
    }
}