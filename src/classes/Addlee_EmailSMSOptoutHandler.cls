public class Addlee_EmailSMSOptoutHandler
{
    public static void SMSEmailSetting(list<contact> contacts,map<Id,contact> oldrecordsMap)
    {
         for(contact c:contacts)
         { 
           if(trigger.isinsert)
           {
            // update all preference fields to be false and update email optout and sms optout to be true.
                 if(c.Optout_All__c==true)
                 {
                      c.Add_Lib_Lifestyle_Content_Email__c=false;
                      c.Add_Lib_Lifestyle_Content_Post__c=true;
                      c.Add_Lib_Lifestyle_Content_SMS__c=false;
                      c.News_Letters_And_Updates_Email__c=false;
                      c.Offers_Discounts_Competitions_Email__c=false;
                      c.News_Letters_And_Updates_Post__c=false;
                      c.News_Letters_And_Updates_SMS__c = false;
                      c.Offers_Discounts_Competitions_Post__c = false;
                      c.Offers_Discounts_Competitions_SMS__c = false;
                      c.Surveys_And_Research_Email__c = false;
                      c.Surveys_And_Research_Post__c = false;
                      c.Surveys_And_Research_SMS__c = false;
                      c.SMS_Opt_Out__c = true;
                      c.HasOptedOutOfEmail=true;
                 }
            }
         if(trigger.isupdate)
         {
         //update all email fields to be false
             if(c.HasOptedOutOfEmail==true && oldrecordsMap.get(c.Id).HasOptedOutOfEmail==false)
             {
                  c.Add_Lib_Lifestyle_Content_Email__c=false;
                  c.News_Letters_And_Updates_Email__c=false;
                  c.Offers_Discounts_Competitions_Email__c=false;
                  c.Surveys_And_Research_Email__c=false;
             }
         if(c.SMS_Opt_Out__c==true && oldrecordsMap.get(c.Id).SMS_Opt_Out__c==false )
          {
           //update all sms fields to be false.
              c.Add_Lib_Lifestyle_Content_SMS__c=false;
              c.News_Letters_And_Updates_SMS__c=false;
              c.Offers_Discounts_Competitions_SMS__c=false;
              c.Surveys_And_Research_SMS__c=false;
           }
             // If any one of the email preference is true update optout all to be false n Email optout to be false.
         if((c.Add_Lib_Lifestyle_Content_Email__c==true && oldrecordsMap.get(c.Id).Add_Lib_Lifestyle_Content_Email__c==false)|| 
        (c.News_Letters_And_Updates_Email__c==true && oldrecordsMap.get(c.Id).News_Letters_And_Updates_Email__c==false)|| 
        (c.Offers_Discounts_Competitions_Email__c==true && oldrecordsMap.get(c.Id).Offers_Discounts_Competitions_Email__c==false)||
        (c.Surveys_And_Research_Email__c==true && oldrecordsMap.get(c.Id).Surveys_And_Research_Email__c==false))
         {
          c.Optout_All__c =false;
          c.HasOptedOutOfEmail=false;
         }
            // If any one of the SMS Preference fields is true update optout all to be false n SMSoptout to be false.
         if((c.Add_Lib_Lifestyle_Content_SMS__c==true && oldrecordsMap.get(c.Id).Add_Lib_Lifestyle_Content_SMS__c==false)|| 
         (c.News_Letters_And_Updates_SMS__c==true && oldrecordsMap.get(c.Id).News_Letters_And_Updates_SMS__c==false)||
         (c.Offers_Discounts_Competitions_SMS__c==true && oldrecordsMap.get(c.Id).Offers_Discounts_Competitions_SMS__c==false)||
         (c.Surveys_And_Research_Email__c==true && oldrecordsMap.get(c.Id).Surveys_And_Research_Email__c==false))
         {
          c.Optout_All__c=false;
          c.SMS_Opt_Out__c=false;
         }
             //update all preference fields to be false and update email optout and sms optout to be true.
        if(c.Optout_All__c==true && oldrecordsMap.get(c.Id).Optout_All__c==false)
        {
             c.SMS_Opt_Out__c=true;
             c.HasOptedOutOfEmail=true;
             c.Add_Lib_Lifestyle_Content_SMS__c=false;
             c.News_Letters_And_Updates_SMS__c=false;
             c.Offers_Discounts_Competitions_SMS__c=false;
             c.Surveys_And_Research_Email__c=false;
             c.Add_Lib_Lifestyle_Content_Email__c=false;
             c.News_Letters_And_Updates_Email__c=false;
             c.Offers_Discounts_Competitions_Email__c=false;
             c.Surveys_And_Research_Email__c=false;
          }


        }

    }    
            
    }    
}