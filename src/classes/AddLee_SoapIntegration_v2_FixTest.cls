@isTest
private class AddLee_SoapIntegration_v2_FixTest {

	private static testMethod void coverTypes() {
        new AddLee_SoapIntegration_Test_v2_Fix.errorInfo();
        new AddLee_SoapIntegration_Test_v2_Fix.location();
        new AddLee_SoapIntegration_Test_v2_Fix.productOffer();
        new AddLee_SoapIntegration_Test_v2_Fix.product();
        new AddLee_SoapIntegration_Test_v2_Fix.customerError();
        new AddLee_SoapIntegration_Test_v2_Fix.customer();
        new AddLee_SoapIntegration_Test_v2_Fix.webSettings();
        new AddLee_SoapIntegration_Test_v2_Fix.businessArea();
        new AddLee_SoapIntegration_Test_v2_Fix.customerPreference();
        new AddLee_SoapIntegration_Test_v2_Fix.specialInstructionType();
        new AddLee_SoapIntegration_Test_v2_Fix.specialInstructionTypesRequest();
        new AddLee_SoapIntegration_Test_v2_Fix.response();
        new AddLee_SoapIntegration_Test_v2_Fix.loginRequest();
        new AddLee_SoapIntegration_Test_v2_Fix.request();
        new AddLee_SoapIntegration_Test_v2_Fix.salesLedger();
        new AddLee_SoapIntegration_Test_v2_Fix.grade();
        new AddLee_SoapIntegration_Test_v2_Fix.createCustomerRequest();
        new AddLee_SoapIntegration_Test_v2_Fix.bank();
        new AddLee_SoapIntegration_Test_v2_Fix.creditCard();
        new AddLee_SoapIntegration_Test_v2_Fix.statusRequest();
        new AddLee_SoapIntegration_Test_v2_Fix.singleReferenceErrorInfo();
        new AddLee_SoapIntegration_Test_v2_Fix.logoutResponse();
        new AddLee_SoapIntegration_Test_v2_Fix.site();
        new AddLee_SoapIntegration_Test_v2_Fix.amendCustomerRequest();
        new AddLee_SoapIntegration_Test_v2_Fix.agentRequest();
        new AddLee_SoapIntegration_Test_v2_Fix.gradeResponse();
        new AddLee_SoapIntegration_Test_v2_Fix.loginResponse();
        new AddLee_SoapIntegration_Test_v2_Fix.status();
        new AddLee_SoapIntegration_Test_v2_Fix.jobsRequest();
        new AddLee_SoapIntegration_Test_v2_Fix.jobEx();
        new AddLee_SoapIntegration_Test_v2_Fix.jobDetailsResponse();
        new AddLee_SoapIntegration_Test_v2_Fix.job();
        new AddLee_SoapIntegration_Test_v2_Fix.jobDetailsRequest();
        new AddLee_SoapIntegration_Test_v2_Fix.jobsResponse();
        new AddLee_SoapIntegration_Test_v2_Fix.price();
        new AddLee_SoapIntegration_Test_v2_Fix.jobIndividual();
        new AddLee_SoapIntegration_Test_v2_Fix.vehicle();
        new AddLee_SoapIntegration_Test_v2_Fix.driverInfo();
        new AddLee_SoapIntegration_Test_v2_Fix.agent();
        new AddLee_SoapIntegration_Test_v2_Fix.logoutRequest();
        new AddLee_SoapIntegration_Test_v2_Fix.customerResponse();
        new AddLee_SoapIntegration_Test_v2_Fix.multiReferenceErrorInfo();
        new AddLee_SoapIntegration_Test_v2_Fix.invoicingPolicy();
    }
    
    
    private class CustomerManagementSOAP_V3MockImpl implements WebServiceMock
    {
        public void doInvoke(
            Object stub, Object request, Map<String, Object> response,
            String endpoint, String soapAction, String requestName,
            String responseNS, String responseName, String responseType)
        {
            if(request instanceof AddLee_SoapIntegration_Test_v2_Fix.loginRequest)
                {
                    AddLee_SoapIntegration_Test_v2_Fix.loginResponse customerResponse = new AddLee_SoapIntegration_Test_v2_Fix.loginResponse();
                    customerResponse.sessionId = 'Test Login Response';
                    customerResponse.errorCode = 0;
                    response.put('response_x', customerResponse);
                }
                
            else if(request instanceof AddLee_SoapIntegration_Test_v2_Fix.createCustomerRequest)
                response.put('response_x', new AddLee_SoapIntegration_Test_v2_Fix.customerResponse());
            else if(request instanceof AddLee_SoapIntegration_Test_v2_Fix.amendCustomerRequest)
                response.put('response_x', new AddLee_SoapIntegration_Test_v2_Fix.customerResponse());
            else if(request instanceof AddLee_SoapIntegration_Test_v2_Fix.jobsRequest)
                response.put('response_x', new AddLee_SoapIntegration_Test_v2_Fix.jobsResponse());
            else if(request instanceof AddLee_SoapIntegration_Test_v2_Fix.jobDetailsRequest)
                response.put('response_x', new AddLee_SoapIntegration_Test_v2_Fix.jobDetailsResponse());
            else if(request instanceof AddLee_SoapIntegration_Test_v2_Fix.logoutRequest)
                response.put('response_x', new AddLee_SoapIntegration_Test_v2_Fix.logoutResponse());
            return;
        }
    }
    
    @IsTest
    private static void coverGeneratedCodeFileBasedOperations()
    {    	
    	// Null Web Service mock implementation
        System.Test.setMock(WebServiceMock.class, new CustomerManagementSOAP_V3MockImpl());
        Log_Integration__c integrationSettings = new Log_Integration__c(Name='shamrock_setting', CurrentSessionId__c='12345', Username__c='username', Password__c='password', EndPoint__c='Test');
		Insert integrationSettings;
        // Only required to workaround a current code coverage bug in the platform
        //AddLee_SoapIntegration_Test_v2_Fix metaDataService = new AddLee_SoapIntegration_Test_v2_Fix();
        // Invoke operations  
        test.startTest();
        AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort metaDataPort = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
        metaDataPort.Login(null,null);
        metaDataPort.CreateCustomer(null, null, null);
        metaDataPort.AmendCustomer(null, null, null);
        metaDataPort.GetJobs(null, null, null, null, null, null, null);
        metaDataPort.GetJobDetails(null, null, null);
        metaDataPort.Logout();
        test.stopTest();
    }
}