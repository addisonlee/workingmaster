/**
* @author Francesco Iervolino
* @description Integration Orchestrator (Shamrock Integration)
*/
public without sharing class AddLee_IntegrationWrapper{


    public static String loginCalloutSynch(){

        AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort soapClient = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
        String sessionId = soapClient.Login(AddLee_IntegrationUtility.getUsername(),AddLee_IntegrationUtility.getPassword());
        
        return sessionId;
    }
    public static Map<Account, Log__c> createAccountCalloutSynchronous(String idAcc, String idLog, String sessionId){
        try{
            //System.DEBUG('Batch Create within callout Class');
            Log__c log      = initializeLog(idLog);
            Account acc     = initializeAccount(idAcc);
            Map<Account,Log__c> toBeReturned = new Map<Account,Log__c>();
            
            AddLee_ResponseParser parser = new AddLee_ResponseParser();
            AddLee_SoapIntegration_Test_v2_Fix.customerResponse wsResponse = parser.createCustomer(log, acc, sessionId);
            System.DEBUG('<Debug : response : > ' + wsResponse );
            parser.parseResponse(wsResponse);
            
            toBeReturned.put(acc,log);
            //Track the Action
            return toBeReturned;
    
        }catch(Exception e){
            return parseException(idAcc,idLog,e);
        }
    }
    
    public static Map<Account, Log__c> updateAccountCalloutSynchronous(String idAcc, String idLog, String sessionId){
        try{
            //System.DEBUG('Batch Update within callout Class');
            Log__c log      = initializeLog(idLog);
            Account acc     = initializeAccount(idAcc);
            Map<Account,Log__c> toBeReturned = new Map<Account,Log__c>();
            
            System.DEBUG('Problem Here -0 ??');
            AddLee_ResponseParser parser = new AddLee_ResponseParser();
            AddLee_SoapIntegration_Test_v2_Fix.customerResponse wsResponse = parser.amendCustomer(log, acc, sessionId);
            System.DEBUG('<Debug : response : > ' + wsResponse );
            parser.parseResponse(wsResponse);
            
            toBeReturned.put(acc,log);
            //Track the Action
            return toBeReturned;
    
        }catch(Exception e){
            return parseException(idAcc,idLog,e);
        }
    
    }
    
    @future (callout=true)
    public static void createAccountCalloutAsynch(String idAcc, String idLog){      
        
        try{
            
            Log__c log      = initializeLog(idLog);
            Account acc     = initializeAccount(idAcc);
            //If parentId exists Map will have key parentAccount with customer
            Map<String,AddLee_SoapIntegration_Test_v2_Fix.customer> customers = AddLee_IntegrationUtility.createSoapCustomer(acc.Id,acc.parentId);
            System.DEBUG(logginglevel.ERROR,'customers : ' + customers);
            List<AddLee_SoapIntegration_Test_v2_Fix.customer> childCustomers = new List<AddLee_SoapIntegration_Test_v2_Fix.customer>();
            AddLee_SoapIntegration_Test_v2_Fix.customer parentCustomer;
            AddLee_SoapIntegration_Test_v2_Fix.customer customer;
            if(customers.get('parentAccount') != null){
                parentCustomer = customers.get('parentAccount');
                customer = customers.get('childAccount');
                childCustomers.add(customer);
            }else{
                parentCustomer = null;
                customer = customers.get('childAccount');
            }
            
            AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort soapClient = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
            
            String sessionId = AddLee_IntegrationUtility.getSessionId();
            
            AddLee_SoapIntegration_Test_v2_Fix.customerResponse wsResponse;
            if(parentCustomer !=null ){
                System.DEBUG(LoggingLevel.ERROR,'Actual Request : ' + childCustomers + ' Parent Customer : ' + parentCustomer);
                wsResponse = soapClient.createCustomer(sessionId, parentCustomer, childCustomers);
                System.debug(logginglevel.ERROR,'parent update 1: ' + parentCustomer);
                System.DEBUG(logginglevel.ERROR,'sessionId 1: ' + sessionId);
                System.DEBUG(logginglevel.ERROR,'childCustomers 1: ' + childCustomers);
                parseResponse(true,wsResponse,log,acc,soapClient, null, sessionId, childCustomers, parentCustomer);
            }else{
                System.DEBUG(LoggingLevel.ERROR,'Actual Request : ' + customer);
                wsResponse = soapClient.createCustomer(sessionId, customer, null);
                System.DEBUG(logginglevel.ERROR,'First Callout response : ' + wsResponse);
                System.debug(logginglevel.ERROR,'single update');
                parseResponse(true,wsResponse,log,acc,soapClient, customer, sessionId, null, null);
            }
            //Update the SessionId
            AddLee_IntegrationUtility.storeSessionId(sessionId);
            
            //System.debug('<<<CHECKPOINT_CREATE_5>>>');
            
            //Track the Action*/
            update log;
    
        }catch(Exception e){
            //Log the result
            System.DEBUG('Exception : ' + e);
            Log__c log = [SELECT Id, Description__c,Long_Description__c, Status__c FROM Log__c WHERE Id = :idLog];
            if(e.getMessage().length() > 250){  
                log.Description__c = 'SFDC (Exception: '+e.getMessage().left(200)+')';
            }else{
                log.Description__c = 'SFDC (Exception: '+e.getMessage().left(200)+')';
            }
            log.Long_Description__c = 'SFDC (Exception: '+e.getMessage().left(200)+')'; 
            log.Status__c = 'Failed'; 
            update log;
        }
    }
    
    @future (callout=true)
    public static void updateAccountCalloutAsynch(String idAcc, String idLog){  

        try{
            
            Log__c log      = initializeLog(idLog);
            System.DEBUG('log : ' + log);
            Account acc     = initializeAccount(idAcc);
            System.DEBUG('account : ' + acc);
            //If parentId exists Map will have key parentAccount with customer
            Map<String,AddLee_SoapIntegration_Test_v2_Fix.customer> customers = AddLee_IntegrationUtility.createSoapCustomer(acc.Id,acc.parentId);
            System.DEBUG('customers : ' + customers);
            List<AddLee_SoapIntegration_Test_v2_Fix.customer> childCustomers = new List<AddLee_SoapIntegration_Test_v2_Fix.customer>();
            AddLee_SoapIntegration_Test_v2_Fix.customer parentCustomer;
            AddLee_SoapIntegration_Test_v2_Fix.customer customer;
            if(customers.get('parentAccount') != null){
                parentCustomer = customers.get('parentAccount');
                customer = customers.get('childAccount');
                childCustomers.add(customer);
            }else{
                parentCustomer = null;
                customer = customers.get('childAccount');
            }
            
            AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort soapClient = new AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort();
            
            String sessionId = AddLee_IntegrationUtility.getSessionId();
            
            AddLee_SoapIntegration_Test_v2_Fix.customerResponse wsResponse;
            
            wsResponse = soapClient.amendCustomer(sessionId, customer, parentCustomer);
            System.DEBUG('Response : ' + wsResponse);
            parseResponse(false,wsResponse,log,acc,soapClient, customer, sessionId, null, parentCustomer);
            System.DEBUG('Response : ' + wsResponse);
            //Update the SessionId
            AddLee_IntegrationUtility.storeSessionId(sessionId);
            
            //System.debug('<<<CHECKPOINT_CREATE_5>>>');
            
            //Track the Action*/
            update log;
    
        }catch(Exception e){
            //Log the result
            Log__c log = [SELECT Id, Description__c,Long_Description__c, Status__c FROM Log__c WHERE Id = :idLog];
            if(e.getMessage().length() > 250){  
                log.Description__c = 'SFDC (Exception: '+e.getMessage().left(200)+')';
            }else{
                log.Description__c = 'SFDC (Exception: '+e.getMessage().left(200)+')';
            } 
            log.Long_Description__c = 'SFDC (Exception: '+e.getMessage().left(200)+')'; 
            log.Status__c = 'Failed'; 
            update log;
        }   
            
    }
    
    private static Log__c initializeLog(String idLog){
        return [SELECT Id, Type__c, Status__c, Description__c FROM Log__c WHERE Id = :idLog];
    }
    
    private static Account initializeAccount(String idAcc){
        return [SELECT Id, exsistInShamrock__c, parentId FROM Account WHERE Id = :idAcc];
    }
    
    /*private static AddLee_SoapIntegration_Test_v1.CustomerManagementWebServicePort initiateIntegrationSoapClient(){
         return new AddLee_SoapIntegration_Test_v1.CustomerManagementWebServicePort();
             
    }
    
    private static AddLee_SoapIntegration_Test_v1.customer initiateIntegrationSoapCustomer(String idAcc){
        return new AddLee_SoapIntegration_Test_v1.customer(idAcc);
    }*/
    
    private static void createResponseDebug(AddLee_SoapIntegration_Test_v2_Fix.customerResponse response){
        System.debug('<<<CREATE_RESPONSE>>>: '+response);
        System.debug('<<<CREATE_RESPONSE_DESCRIPTION>>>: '+response.description);
        System.debug('<<<CREATE_RESPONSE_ERRORCODE>>>: '+response.errorCode);
        System.debug('<<<CREATE_RESPONSE_ERRORINFO>>>: '+response.errorInfo);
        System.debug('<<<CREATE_RESPONSE_STACKTRACE>>>: '+response.stackTrace);
        System.debug('<<<CREATE_RESPONSE_ERRORLIST>>>: '+response.errorList);
    }
    
    private static void amendResponseDebug(AddLee_SoapIntegration_Test_v2_Fix.customerResponse response){
        System.debug('<<<UPDATE_RESPONSE>>>: '+response);
        System.debug('<<<UPDATE_RESPONSE_DESCRIPTION>>>: '+response.description);
        System.debug('<<<UPDATE_RESPONSE_ERRORCODE>>>: '+response.errorCode);
        System.debug('<<<UPDATE_RESPONSE_ERRORINFO>>>: '+response.errorInfo);
        System.debug('<<<UPDATE_RESPONSE_STACKTRACE>>>: '+response.stackTrace);
        System.debug('<<<UPDATE_RESPONSE_ERRORLIST>>>: '+response.errorList);
    }
    
    private static Map<Account,Log__c> parseException(String idAcc, String idLog, Exception e){
        Map<Account,Log__c> toBeReturned = new Map<Account,Log__c>();
        Log__c log = [SELECT Id, Description__c, Status__c FROM Log__c WHERE Id = :idLog];
        Account acc = [SELECT Id, exsistInShamrock__c FROM Account WHERE Id = :idAcc];
        if(!(e.getMessage().length() > 250))
            log.Description__c = 'SFDC (Exception: '+e.getMessage()+')'; 
        log.Long_Description__c = 'SFDC (Exception: '+e.getMessage()+')'; 
        log.Status__c = 'Failed';
        toBeReturned.put(acc,log);
        return toBeReturned;
    }
    
    private static void parseResponse(Boolean createAccount,AddLee_SoapIntegration_Test_v2_Fix.customerResponse response, Log__c log, Account account, AddLee_SoapIntegration_Test_v2_Fix.CustomerManagementWebServicePort soapClient, AddLee_SoapIntegration_Test_v2_Fix.customer customer, String sessionId, List<AddLee_SoapIntegration_Test_v2_Fix.customer> childCustomers, AddLee_SoapIntegration_Test_v2_Fix.customer parentCustomer){
        System.debug(logginglevel.ERROR,'<<<Response Code : >>> : ' + response.errorCode);
        System.debug(logginglevel.ERROR,'<<<log : >>> : ' + log);
        System.debug(logginglevel.ERROR,'<<<accoount : >>> : ' + account);
        System.debug(logginglevel.ERROR,'<<<child customer : >>> : ' + childCustomers);
        System.debug(logginglevel.ERROR,'<<<parentcustomer : >>> : ' + parentCustomer);
        System.debug(logginglevel.ERROR,'<<<customer : >>> : ' + customer);
        if(response.errorCode == 0 && response.errorList == null){
            //Log the result - Completed
            log.Description__c = 'SFDC(Action Performed with success.)';
            log.Long_Description__c = 'SFDC(Action Performed with success.)';
            log.Status__c = 'Completed'; 
            
            //Flag exsistence of the account in Salesforce
            if(createAccount){  
                account.exsistInShamrock__c = true; 
                update account; 
            }
                
        }else if(response.errorCode == -4 && response.description == 'Session expired'){
            
            //Try to generate a new sessionId
            sessionId = AddLee_IntegrationWrapper.loginCalloutSynch();
            System.debug('<<<Response Code : SessionId Refresh>>> : ' + sessionId);
            //Retry the creation
            if(createAccount){
                System.debug(logginglevel.ERROR,'Creating a callout - Create Customer ... Parse Response ');
                if(parentCustomer !=null ){
                    System.debug(logginglevel.ERROR,'parent update 2: ' + parentCustomer);
                    System.DEBUG(logginglevel.ERROR,'sessionId 2: ' + sessionId);
                    System.DEBUG(logginglevel.ERROR,'childCustomers 2: ' + childCustomers);
                    response = soapClient.createCustomer(sessionId, parentCustomer, childCustomers);
                    System.DEBUG(logginglevel.ERROR,'Second Callout response : ' + response);
                    System.DEBUG(logginglevel.ERROR,'Second Callout response code : ' + response.errorCode);
                }else{
                    
                    System.debug(logginglevel.ERROR,'single update : ' + customer);
                    response = soapClient.createCustomer(sessionId, customer, null);
                    System.DEBUG(logginglevel.ERROR,'Second Callout response : ' + response);
                }
                //createResponseDebug(response);
            }else{
                System.debug('Creating a callout - Amend Customer ... Parse Response error code -4 ');
                response = soapClient.AmendCustomer(sessionId, customer, parentCustomer);
                //amendResponseDebug(response);
            }       
                    
            //Parse the response
            if(response.errorCode == 0 && response.errorList == null){
                //Log the result - Completed
                log.Description__c = 'SFDC(Action Performed with success.)';
                log.Long_Description__c = 'SFDC(Action Performed with success.)';
                log.Status__c = 'Completed'; 
                
                //Flag exsistence of the account in Salesforce  
                if(createAccount){
                    account.exsistInShamrock__c = true; 
                    update account; 
                }
                
            }else if(response.errorCode == 0 && response.errorList != null){
                //Log the result - Failed
                if(createAccount){
                    if(String.valueOf(response.errorList).length() > 250){
                        log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                    }else{
                        log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                    }
                    log.Long_Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(250)+')';
                }
                else{
                    if(response.errorList != null && String.valueOf(response.errorList).length() > 250){
                        log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                    }else{
                        log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                    }
                    log.Long_Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(250)+')';
                }
                log.Status__c = 'Failed';           
            }else if(response.errorCode == -13300){
                System.debug('Creating a callout - Amend Customer ...  error code -13300 inside session expire');
                response = soapClient.AmendCustomer(sessionId, customer, parentCustomer);
                //amendResponseDebug(response);
                System.DEBUG('Customer amend response : ' + response);
                //Parse the response
                if(response.errorCode == 0 && response.errorList == null){
                    //Log the result - Completed
                    log.Description__c = 'SFDC(Action Performed with success.)';
                    log.Long_Description__c = 'SFDC(Action Performed with success.)';
                    log.Status__c = 'Completed'; 
                    
                //Flag exsistence of the account in Salesforce  
                        account.exsistInShamrock__c = true; 
                        update account; 
                
                }else if(response.errorCode == 0 && response.errorList != null){
                    //Log the result - Failed
                    if(createAccount){
                        if(String.valueOf(response.errorList).length() > 250){
                            log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                        }else{
                            log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                        }
                        log.Long_Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                    }
                    else{
                        if(String.valueOf(response.errorList).length() > 250){
                            log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                        }else{
                            log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                        }
                        log.Long_Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                    }
                    log.Status__c = 'Failed'; 
                }else{
                    //Log the result - Failed
                    if(response.errorList != null && String.valueOf(response.errorList).length() > 250){    
                        log.Description__c = 'SHM (CODE:'+response.errorCode+' INFO:'+response.errorInfo+' DESCRIPTION:'+response.description+')';
                    }else{
                        log.Description__c = 'SHM (CODE:'+response.errorCode+' INFO:'+response.errorInfo+' DESCRIPTION:'+response.description+')';
                    }
                    log.Long_Description__c = 'SHM (CODE:'+response.errorCode+' INFO:'+response.errorInfo+' DESCRIPTION:'+response.description+')';
                    log.Status__c = 'Failed'; 
                }
            }else{
                //Log the result - Failed
                if(response.errorList != null && String.valueOf(response.errorList).length() > 250 ){
                    log.Description__c = 'SHM (CODE:'+response.errorCode+' INFO:'+response.errorInfo+' DESCRIPTION:'+response.description+')';
                }else{
                    log.Description__c = 'SHM (CODE:'+response.errorCode+' INFO:'+response.errorInfo+' DESCRIPTION:'+response.description+')';
                }
                log.Long_Description__c = 'SHM (CODE:'+response.errorCode+' INFO:'+response.errorInfo+' DESCRIPTION:'+response.description+')';
                log.Status__c = 'Failed'; 
            }

        }else if(response.errorCode == 0 && response.errorList != null){
            //Log the result - Failed
            if(createAccount){
                if(String.valueOf(response.errorList).length() > 250){
                    log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                }else{
                    log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                }
                log.Long_Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
            }
            else{ 
                if(String.valueOf(response.errorList).length() > 250){
                    log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                }else{
                    log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                }
                log.Long_Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
            }
            log.Status__c = 'Failed';           
        }else if(response.errorCode == -13300){
            System.debug('Creating a callout - Amend Customer ... error code -13300 outside');
            response = soapClient.AmendCustomer(sessionId, customer, parentCustomer);
            //amendResponseDebug(response);
            //Parse the response
            if(response.errorCode == 0 && response.errorList == null){
                //Log the result - Completed
                log.Description__c = 'SFDC(Action Performed with success.)';
                log.Long_Description__c = 'SFDC(Action Performed with success.)';
                log.Status__c = 'Completed'; 
                
                //Flag exsistence of the account in Salesforce  
                    account.exsistInShamrock__c = true; 
                    update account; 
                
            }else if(response.errorCode == 0 && response.errorList != null){
                //Log the result - Failed
                if(createAccount){
                    if(String.valueOf(response.errorList).length() > 250){
                        log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                    }else{
                        log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                    }
                    log.Long_Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                }
                else{
                    if(String.valueOf(response.errorList).length() > 250){
                        log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                    }else{
                        log.Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                    }
                    log.Long_Description__c = 'SHM (ERROR_LIST:'+String.valueOf(response.errorList).left(200)+')';
                }
                log.Status__c = 'Failed'; 
            }
        }else{
            //Log the result - Failed
            if(response.errorList != null && String.valueOf(response.errorList).length() > 250){    
                log.Description__c = 'SHM (CODE:'+response.errorCode+' INFO:'+response.errorInfo+' DESCRIPTION:'+response.description+')';
            }else{
                log.Description__c = 'SHM (CODE:'+response.errorCode+' INFO:'+response.errorInfo+' DESCRIPTION:'+response.description+')';
            }
            log.Long_Description__c = 'SHM (CODE:'+response.errorCode+' INFO:'+response.errorInfo+' DESCRIPTION:'+response.description+')';
            log.Status__c = 'Failed'; 
        }
    }
}