/* 
 **Description : schedular class to populate marketing status of business accounts using 'CalculateMarketingStatusOnAccount' class
 */
global class MarketingStatus_Scheduler implements Schedulable {
     global void execute(SchedulableContext SC){
      List<String> recordtypeNames=new List<String>{'Business Account','Read Only Business Account'};
        CalculateMarketingStatusOnAccount csa = new CalculateMarketingStatusOnAccount();
        csa.Query = 'select id,Sales_Status__c,Date_of_Last_Booking__c,Consumer_Account_Number__c,Name,Grading__c,Account_Status__c,Date_changed_to_Current__c,IsPersonAccount,Marketing_Status__c,Sales_Ledger__c,Account_Managed__c,Total_Amount_of_Bookings__c from Account where RecordType.Name = ' + '\'' + 'Business Account' + '\'' + ' OR RecordType.Name =' + '\'' + 'Read Only Business Account' + '\'';
        Database.executeBatch(csa,50);
     }
     
}