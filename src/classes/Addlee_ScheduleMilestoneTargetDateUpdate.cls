/* 
*    Author        :    Prateek Gandhi
*    Description   :     Scheduler class for 'Addlee_MilestoneTargetDateUpdate' class 
*/
global class Addlee_ScheduleMilestoneTargetDateUpdate implements Schedulable{
   global void execute(SchedulableContext SC){
        Addlee_MilestoneTargetDateUpdate obj= new Addlee_MilestoneTargetDateUpdate();
        Id batchprocessid = Database.executeBatch(obj, 200);
   }
}