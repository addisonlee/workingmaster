@isTest
global class AdleeRefundTestMockService implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
       Addlee_refunds.complaintResponse respElement =  new Addlee_refunds.complaintResponse();
             respElement.errorCode = '0';
             respElement.description = 'No error';
             respElement.id = 1213121;
             respElement.number_x = 559985 ;
        
          
       response.put('response_x', respElement); 
   }
}