/*
* Output Class for CreateComplainCallout
*/

global class AdleeServiceOutput {
    public CreateComplainOutput createComplainOutput;
   
    
    public Class CreateComplainOutput{
        public string errorCode;
        public long id;
        public long numberValue;
        public String errorDescription;
    }
    
   

}