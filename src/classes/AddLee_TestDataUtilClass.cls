Public Class AddLee_TestDataUtilClass { 
Account acccc = new Account();
Contact conn = new Contact();
Account acccc2 = new Account();
Account acccc3 = new Account();
Contact conn2 = new Contact();
Campaign camp = new Campaign();
Campaign camp2 = new Campaign();
Booking_Summary__c booksum = new Booking_Summary__c();
Booking_Summary__c booksum2 = new Booking_Summary__c();
Public Void Data_InsertLogIntegrationCustomSetting(){
Log_Integration__c setting = new Log_Integration__c ();
   setting.Name = 'shamrock_setting';
   setting.CurrentSessionId__c= 'TEST';
   setting.enableUpdates__c= true;
   setting.EndPoint__c = 'https://wsplatform.addisonlee.com/customer-management-ws/CustomerManagementWebService1.0';
   setting.Password__c= 'salesforce';
   setting.Username__c= 'salesforce';
   insert setting;
}
Public Void Data_InsertMobileNumberCustomSetting(){
Mobile_Number__c mn = new Mobile_Number__c();
mn.name='Number';
mn.Prefix__c ='0';
insert mn;
}
Public Void Data_InsertLapseNoSpendCustomSetting(){
NoSpendLapseCampaignCS__c setting = new NoSpendLapseCampaignCS__c();
   setting.Name = 'CampaignName';
   setting.Lapsed__c= 'Lapsed';
   setting.No_Spend__c= 'No spend';
   insert setting;
}
Public Void Data_InsertAccount(){

   acccc.Name = 'Test Account';
   acccc.Account_Status__c = 'Prospect';
   acccc.Marketing_Status__c = 'Lapsed';
   acccc.Sales_Ledger__c='Sales Ledger';
   acccc.Grading__c ='P2';
   acccc.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
   insert acccc;
}
Public Void Data_InsertContact(){
   conn.LastName = 'Test Account';
   conn.AccountId = acccc.id;
   conn.Active__c = true;
   conn.Phone = '989898989';
   conn.MobilePhone = '989898989';
   conn.Email = 'sonusharmacs@gmail.com';
   conn.Contact_Type__c = 'Other Contact';
   //conn.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
   insert conn;
}
Public Void Data_InsertAccount2(){
   acccc2.Name = 'Test Account';
   acccc2.Account_Status__c = 'Prospect';
   acccc2.Marketing_Status__c = 'No spend';
   acccc2.Sales_Ledger__c='Sales Ledger';
   acccc2.Grading__c ='P2';
   acccc2.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
   insert acccc2;
}
Public Void Data_InsertAccount3(){
   acccc3.LastName = 'Test Account 3';
   acccc3.Account_Status__c = 'Prospect';
   acccc3.Marketing_Status__c = 'Lapsed';
  // acccc3.Sales_Ledger__c='Sales Ledger';
   //acccc3.Grading__c ='P2';
   acccc3.Consumer_Account_Number__c = '1';
   acccc3.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Consumer').getRecordTypeId();
   insert acccc3;
}
Public Void Data_InsertContact2(){
   conn2.LastName = 'Test Account';
   conn2.AccountId = acccc2.id;
   conn2.Active__c = true;
   conn2.Phone = '989898989';
   conn2.MobilePhone = '989898989';
   conn2.Email = 'test@test.com';
   conn2.Contact_Type__c = 'Other Contact';
   //conn.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
   insert conn2;
}
Public Void Data_InsertCampaign(){
   camp.Name = 'No spend';
   //camp.AccountId = acccc2.id;
   //conn.Account_Nature__c = 'Lapsed';
   //conn.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
   insert camp;
}
Public Void Data_InsertCampaign2(){
   camp2.Name = 'Lapsed';
   //camp2.AccountId = acccc2.id;
   //conn.Account_Nature__c = 'Lapsed';
   //conn.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
   insert camp2;
}
Public Void Data_InsertBookingSummary(){
   booksum.Name = 'Book Sum1';
   booksum.Date_From__c = System.today()-5;
   booksum.Account__c= acccc.id;
   booksum.Service_Type__c = 'Lapsed';
   booksum.Date_Time__c = System.today()-5;
   booksum.Number_of_Bookings__c = 10;
   booksum.Pick_Up_time__c = system.today();
   //conn.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
   insert booksum;
}
Public Void Data_InsertBookingSummary2(){
   booksum2.Name = 'Book Sum1';
   booksum2.Date_From__c = System.today()-50;
   booksum2.Account__c= acccc.id;
   booksum2.Service_Type__c = 'Lapsed';
   booksum2.Date_Time__c = System.today()-50;
   booksum2.Number_of_Bookings__c = 10;
   booksum.Pick_Up_time__c = system.today()-1;
   //conn.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
   insert booksum2;
}

}