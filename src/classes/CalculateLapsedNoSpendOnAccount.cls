/*
 ** this batch class to insert lapsed and no spend contacts to campaigns 
 ** 15/01/2015
*/
global class CalculateLapsedNoSpendOnAccount implements Database.Batchable<sObject>{
   global String Query;
   global String AccountId;
   Public NoSpendLapseCampaignCS__c sculs= NoSpendLapseCampaignCS__c.getValues('CampaignName');
   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(Query);
   }

   global void execute(Database.BatchableContext BC, 
                       List<Account> bscope){
                      
                       Map<String,Campaign> mapCampaign = new Map<String,Campaign>();
                       Map<String,String> mapCampaignContact = new Map<String,String>();
                       Map<String,String> mapContactIds = new Map<String,String>();
                       for(Campaign  camp:[Select Id,Name from Campaign where Name=:sculs.Lapsed__c OR Name=:sculs.No_Spend__c]){
                       mapCampaign.put(camp.name,camp);
                       }
                       List<CampaignMember> lstCampaignMember = new List<CampaignMember>();
                        //check account status , account managed and marketing status
                       for(Account accc:bscope){
                       //if(((accc.Account_Status__c!='Ask Accounts')&&(accc.Account_Managed__c != true) && (accc.Account_Status__c!='Deleted') && (accc.Account_Status__c!='OnHold')&&(accc.Grading__c!='N1') && (accc.Grading__c!='P1')) && (((accc.Marketing_Status__c==sculs.Lapsed__c ) && (accc.Sales_Ledger__c=='Sales Ledger') &&((accc.Grading__c!='N1')|| (accc.Grading__c!='P1'))) || ((accc.Marketing_Status__c==sculs.No_Spend__c) && (accc.Sales_Ledger__c=='Sales Ledger')&&(accc.Account_Managed__c != true)  &&(accc.Grading__c!='N1') && (accc.Grading__c!='P1')))){
                       if(((accc.Account_Status__c =='Current')&&(accc.Date_changed_to_Current__c <=(System.today()-180))&&(accc.Date_of_Last_Booking__c <=(System.today()-180))&&(accc.Account_Managed__c != true)  &&(accc.Grading__c!='N1') &&(accc.Grading__c!='P2')&&(accc.Grading__c!='P5') &&(accc.Grading__c!='P1') && (accc.Marketing_Status__c==sculs.Lapsed__c ) && (accc.Sales_Ledger__c !='WestOne Sales Ledger')) || ((accc.Marketing_Status__c==sculs.No_Spend__c) &&(accc.Date_changed_to_Current__c <=(System.today()-180))&& (accc.Sales_Ledger__c!='WestOne Sales Ledger')&&(accc.Account_Managed__c != true)  &&(accc.Grading__c!='P1') &&(accc.Grading__c!='P2')&&(accc.Grading__c!='N1')&&(accc.Grading__c!='P5') && (accc.Account_Status__c =='Current'))){
                       AccountId = accc.id;
                       DateTime dt = (System.now()-20);
                       Datetime startDate = Datetime.newInstance(dt.year()-1, dt.month(), dt.day(), 0, 0, 0);
                       List<Contact> lstContact  =  [SELECT Id FROM Contact where AccountId=:AccountId and Contact_Type__c ='Main Contact']; //Lastmodifieddate>:startDate and 
                       for(Contact conn:lstContact){
                       mapContactIds.put(conn.id,conn.id);
                       }
                       for(CampaignMember cmm:[Select Id,ContactId from CampaignMember where ContactId=:mapContactIds.keyset()]){
                       mapCampaignContact.put(cmm.ContactId,cmm.ContactId);
                       }
                       //add campaign member to campaign if campaign member is not exict in campaign
                       for(Contact conn:lstContact){
                       if(mapCampaign.containskey(accc.Marketing_Status__c) && !mapCampaignContact.containskey(conn.id)){
                       CampaignMember cmm = new CampaignMember();
                       cmm.CampaignId = mapCampaign.get(accc.Marketing_Status__c).id;
                       cmm.Status = 'New';
                       cmm.ContactId = conn.id;
                       lstCampaignMember.add(cmm);
                       }
                       }
                       }
                        }
                        insert lstCampaignMember;
                        
                       }
                       
                       
                      
   global void finish(Database.BatchableContext BC){

   
   }

}