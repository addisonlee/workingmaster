@isTest
public with sharing class AddLee_Lead_Conversion_Test {
	@isTest
	static void Lead_ConvertLead_isLeadConverted(){
		//http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_dml_convertLead.htm
		AddLee_Trigger_Test_Utils.insertTestEmployeeSizeBand();
    	AddLee_Trigger_Test_Utils.insertTestTargetActiveUsersBand();
    	AddLee_Trigger_Test_Utils.insertTestExpectedActiveUsersBand();
    	AddLee_Trigger_Test_Utils.insertTestTargetSpendExpenditure();
    	AddLee_Trigger_Test_Utils.insertTestExpectedSpendExpenditure();
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
		Lead myLead =  AddLee_Trigger_Test_Utils.createLeads(1)[0];
		myLead.email='fry.son@test.com';
		myLead.No_of_London_Employees__c = '1-5'; 
		myLead.Industry = 'Professional, Scientific and Technical';
		myLead.Industry_Sub_Sector__c = 'Management Consulting';
		insert myLead;
		Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(myLead.id);
		
		LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
		lc.setConvertedStatus(convertStatus.MasterLabel);
		
		Database.LeadConvertResult lcr = Database.convertLead(lc);
		System.assert(lcr.isSuccess());
	}
	
	@isTest
	static void Lead_ConvertLead_isCompetitorCopiedFromLead(){
		AddLee_Trigger_Test_Utils.insertTestEmployeeSizeBand();
	  	AddLee_Trigger_Test_Utils.insertTestTargetActiveUsersBand();
	  	AddLee_Trigger_Test_Utils.insertTestExpectedActiveUsersBand();
	  	AddLee_Trigger_Test_Utils.insertTestTargetSpendExpenditure();
	  	AddLee_Trigger_Test_Utils.insertTestExpectedSpendExpenditure();
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
		Lead myLead =  AddLee_Trigger_Test_Utils.createLeads(1)[0];
		myLead.email='fry.son@test.com';
		myLead.No_of_London_Employees__c = '1-5'; 
		myLead.Industry = 'Professional, Scientific and Technical';
		myLead.Industry_Sub_Sector__c = 'Management Consulting';
		insert myLead;

		Competitor__c competitor = new Competitor__c(Name = 'Fry' );
		insert competitor;
		
		JNC_CompetitorLead__c competitorJunctionLead = new JNC_CompetitorLead__c(Competitor__c = competitor.Id, Lead__c = myLead.Id);
		insert competitorJunctionLead;
		
		Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(myLead.id);
		
		LeadStatus convertStatus = [SELECT Id, MasterLabel FROM LeadStatus WHERE IsConverted=true LIMIT 1];
		lc.setConvertedStatus(convertStatus.MasterLabel);
		
		Database.LeadConvertResult lcr = Database.convertLead(lc);
		System.assert(lcr.isSuccess());
		
		myLead = [SELECT Id, ConvertedAccountId FROM Lead WHERE ID =: myLead.id];
		System.assert(myLead.ConvertedAccountId <> null);
		
		Account convertedAccount = [SELECT Id, (SELECT id, Account__c, Competitor__c FROM Competitors__r ) FROM Account WHERE id =: myLead.ConvertedAccountId];
		//ensure competitors created
		System.assertEquals(1,convertedAccount.Competitors__r.size());
		
		for(JNC_CompetitorAccount__c eachCompetitorAccount : convertedAccount.Competitors__r){
			System.assertEquals(eachCompetitorAccount.Competitor__c,competitorJunctionLead.Competitor__c);
		}
		
	}
	
	
}