@isTest
public Class PreferencesController_Test{
 
    public static testMethod void updContact() {
    
        //List<Contact> contactsToInsert = AddLee_Trigger_Test_Utils.createContacts(1);
        
        //List<Contact> allContactsToInsert = new List<Contact>(contactsToInsert);
        
        Log_Integration__c LogIntegration = new Log_Integration__c();
        LogIntegration.name = 'shamrock_setting';
        LogIntegration.CurrentSessionId__c = 'TEST';
        LogIntegration.Enable_Bulk_Processing__c = false;
        LogIntegration.enableUpdates__c = false;
        LogIntegration.EndPoint__c = 'ttps://wsplatform.addisonlee.com/customer-management-ws/CustomerManagementWebService1.0';
        LogIntegration.Password__c = 'salesforce';
        LogIntegration.Username__c = 'salesforce';
        insert LogIntegration;
        
        Account acccc = new Account();
        acccc.Name = 'Test Account';
        acccc.Account_Status__c = 'Prospect';
        acccc.Marketing_Status__c = 'Lapsed';
        acccc.Sales_Ledger__c='Sales Ledger';
        acccc.Grading__c ='P2';
        acccc.RecordTypeId =Schema.SObjectType.Account.getRecordTypeInfosByName().get('Business Account').getRecordTypeId();
        
           
        insert acccc;
        System.assert(acccc != null);
        
        list<Contact>conn = new list<Contact>();
        Contact conn3 = new Contact(LastName = 'Test Account1',AccountId = acccc.id,Active__c = true,Phone = '989898989',Mobile_Number__c=false,MobilePhone = '7898918989', et4ae5__Mobile_Country_Code__c='ET',Email = 'test@gmail.com');
        insert conn3;
        
        
        Test.startTest();
          ApexPages.currentpage().getParameters().put('id',conn3.Id);
          PreferencesController contr=new PreferencesController();
          contr.postfield =true;
          contr.smsfield =true;
          contr.emailfield =true;
          
          contr.saveMethod();
        Test.stopTest();
        }
}