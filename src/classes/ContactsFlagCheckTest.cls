@isTest
public class ContactsFlagCheckTest{

    public static TestMethod void runBatch()
    {
        AddLee_Trigger_Test_Utils.insertCustomSettings();  
        Account acc = AddLee_Trigger_Test_Utils.createAccountBooker(1).get(0);
        insert acc;   
        
        Grouping__c grp=new Grouping__c();
        grp.Grouping__c =1;
        grp.External_Field__c ='TestTst';
        insert grp; 
        
        List<Contact> lstcontact=new List<Contact>();
        Contact c = new contact();
        c.FirstName ='Test';
        c.LastName='Tst';
        c.phone ='4152992960';
        c.mobilePhone ='4152992960';
        c.Email ='demo@demo.com';
        c.AccountId=acc.Id;
        c.Grouping_No__c =grp.Id;
        lstcontact.add(c);  
        
        Contact c1 = new contact();
        c1.FirstName ='Test';
        c1.LastName='Tst';
        c1.mobilePhone ='4152992960';
        c1.Email ='demo@demo.com';
        c1.phone ='4152992960';
        c1.AccountId=acc.Id;
        lstcontact.add(c1);
        
        if(!lstcontact.isEmpty())
        {
            insert lstcontact;
        }
        
        database.executeBatch(new ContactsFlagCheck('Contact'),1000);
    }
    
    public static TestMethod void runLeadBatch()
    {
        List<Lead> lstLead=new List<Lead>();
         AddLee_Trigger_Test_Utils.insertCustomSettings();     
        Lead l = new Lead();
        l.FirstName ='Test';
        l.LastName='Tst';
        l.phone ='4152992960';
        l.mobilePhone ='4152992960';
        l.Email ='demo@demo.com';
        l.Company='Test';
        lstLead.add(l);  
        
        Lead l1 = new Lead();
        l1.FirstName ='Test';
        l1.LastName='Tst';
        l1.phone ='4152992960';
        l1.mobilePhone ='4152992960';
        l1.Email ='demo@demo.com';
        l1.Company='Test';
        lstLead.add(l1);
        
        if(!lstLead.isEmpty())
        {
            insert lstLead;
        }
        
        database.executeBatch(new ContactsFlagCheck('Lead'),1000);
    }
}