public class AddLee_SoapIntegration_Test_v1 {
    /*public class errorInfo {
        public AddLee_SoapIntegration_Test_v1.multiReferenceErrorInfo multiReferenceErrorInfo;
        public AddLee_SoapIntegration_Test_v1.singleReferenceErrorInfo singleReferenceErrorInfo;
        private String[] multiReferenceErrorInfo_type_info = new String[]{'multiReferenceErrorInfo','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] singleReferenceErrorInfo_type_info = new String[]{'singleReferenceErrorInfo','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'multiReferenceErrorInfo','singleReferenceErrorInfo'};
    }
    public class location {
        public String address;
        public Double latitude;
        public Double longitude;
        public String postcode;
        public String town;
        private String[] address_type_info = new String[]{'address','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] latitude_type_info = new String[]{'latitude','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] longitude_type_info = new String[]{'longitude','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] postcode_type_info = new String[]{'postcode','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] town_type_info = new String[]{'town','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'address','latitude','longitude','postcode','town'};
    }
    public class customerError {
        public String description;
        public Integer errorCode;
        public String name;
        public Long number_x;
        public String stackTrace;
        private String[] description_type_info = new String[]{'description','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] errorCode_type_info = new String[]{'errorCode','http://www.haulmont.com/SHM',null,'1','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] number_x_type_info = new String[]{'number','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] stackTrace_type_info = new String[]{'stackTrace','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'description','errorCode','name','number_x','stackTrace'};
    }
    public class customer {
        public AddLee_SoapIntegration_Test_v1.agent accountManager;
        public AddLee_SoapIntegration_Test_v1.bank bank;
        public AddLee_SoapIntegration_Test_v1.site billingAddress;
        public DateTime birthDate;
        public String bookingEmail;
        public String bookingNote;
        public Integer creditDays;
        public Double creditLimit;
        public Double discountRate;
        public Boolean generateIndividualInvoice;
        public AddLee_SoapIntegration_Test_v1.grade grade;
        public Long id;
        public String invoiceClearanceType;
        public AddLee_SoapIntegration_Test_v1.invoicingPolicy invoicingPolicy;
        public AddLee_SoapIntegration_Test_v1.site mainAddress;
        public String message;
        public String name;
        public Long number_x;
        public AddLee_SoapIntegration_Test_v1.salesLedger salesLedger;
        public AddLee_SoapIntegration_Test_v1.agent salesman;
        public Double serviceCharge;
        public AddLee_SoapIntegration_Test_v1.status status;
        public AddLee_SoapIntegration_Test_v1.subStatus subStatus;
        public String type_x;
        public Boolean useWeb;
        public String vatRate;
        private String[] accountManager_type_info = new String[]{'accountManager','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] bank_type_info = new String[]{'bank','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] billingAddress_type_info = new String[]{'billingAddress','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] birthDate_type_info = new String[]{'birthDate','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] bookingEmail_type_info = new String[]{'bookingEmail','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] bookingNote_type_info = new String[]{'bookingNote','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] creditDays_type_info = new String[]{'creditDays','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] creditLimit_type_info = new String[]{'creditLimit','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] discountRate_type_info = new String[]{'discountRate','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] generateIndividualInvoice_type_info = new String[]{'generateIndividualInvoice','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] grade_type_info = new String[]{'grade','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] id_type_info = new String[]{'id','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] invoiceClearanceType_type_info = new String[]{'invoiceClearanceType','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] invoicingPolicy_type_info = new String[]{'invoicingPolicy','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] mainAddress_type_info = new String[]{'mainAddress','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] message_type_info = new String[]{'message','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] number_x_type_info = new String[]{'number','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] salesLedger_type_info = new String[]{'salesLedger','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] salesman_type_info = new String[]{'salesman','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] serviceCharge_type_info = new String[]{'serviceCharge','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] status_type_info = new String[]{'status','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] subStatus_type_info = new String[]{'subStatus','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] type_x_type_info = new String[]{'type','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] useWeb_type_info = new String[]{'useWeb','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] vatRate_type_info = new String[]{'vatRate','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'accountManager','bank','billingAddress','birthDate','bookingEmail','bookingNote','creditDays','creditLimit','discountRate','generateIndividualInvoice','grade','id','invoiceClearanceType','invoicingPolicy','mainAddress','message','name','number_x','salesLedger','salesman','serviceCharge','status','subStatus','type_x','useWeb','vatRate'};

        
        public customer(String idAcc){  
            Account acc     = [SELECT Id, Name, SHM_invoicingPolicy_code__c, 
                                                SHM_invoicingPolicy_id__c, 
                                                SHM_invoicingPolicy_name__c,
                                                SHM_message__c,
                                                SHM_name__c,
                                                SHM_number_x__c,
                                                SHM_serviceCharge__c,
                                                SHM_type_x__c,
                                                SHM_useWeb__c,
                                                SHM_discountRate__c,
                                                SHM_vatRate__c,
                                                SHM_bookingNote__c,
                                                SHM_creditDays__c,
                                                SHM_creditLimit__c,
                                                SHM_generateIndividualInvoice__c,
                                                SHM_invoiceClearanceType__c,
                                                SHM_id__c,
                                                SHM_birthday__c,
                                                SHM_status_code__c,
                                                SHM_status_id__c,
                                                SHM_status_name__c,
                                                SHM_substatus_code__c,
                                                SHM_substatus_id__c,
                                                SHM_substatus_name__c,
                                                SHM_bookingEmail__c,
                                               	SHM_salesLedger_code__c,
                                                SHM_salesLedger_id__c,
                                                SHM_salesLedger_name__c,
                                                SHM_grade_id__c,
                                                SHM_grade_name__c,
                                                SHM_main_address__c,
												SHM_main_contactName__c,
												SHM_main_email__c,
												SHM_main_fax__c,
												SHM_main_latitude__c,
												SHM_main_longitude__c,
												SHM_main_postcode__c,
												SHM_main_telephone__c,
												SHM_main_town__c,
												SHM_billing_address__c,
												SHM_billing_contactName__c,
												SHM_billing_email__c,
												SHM_billing_fax__c,
												SHM_billing_latitude__c,
												SHM_billing_longitude__c,
												SHM_billing_postcode__c,
												SHM_billing_telephone__c,
												SHM_billing_town__c
                                FROM Account WHERE Id = :idAcc];
                                
            System.debug('<<<CHECKPOINT_ACCOUNT>>>: '+acc);
            
            
            AddLee_SoapIntegration_Test_v1.agent cst_accountMng = new AddLee_SoapIntegration_Test_v1.agent();
            AddLee_SoapIntegration_Test_v1.bank cst_bank = new AddLee_SoapIntegration_Test_v1.bank();
            AddLee_SoapIntegration_Test_v1.site cst_billingAddress = new AddLee_SoapIntegration_Test_v1.site();
            AddLee_SoapIntegration_Test_v1.site cst_mainAddress = new AddLee_SoapIntegration_Test_v1.site();
            AddLee_SoapIntegration_Test_v1.grade cst_grade = new AddLee_SoapIntegration_Test_v1.grade();
            AddLee_SoapIntegration_Test_v1.invoicingPolicy cst_invoicingPolicy = new AddLee_SoapIntegration_Test_v1.invoicingPolicy();
            AddLee_SoapIntegration_Test_v1.salesLedger cst_salesLedger = new AddLee_SoapIntegration_Test_v1.salesLedger();
            AddLee_SoapIntegration_Test_v1.agent cst_salesman = new AddLee_SoapIntegration_Test_v1.agent();
            AddLee_SoapIntegration_Test_v1.status cst_status = new AddLee_SoapIntegration_Test_v1.status();
            AddLee_SoapIntegration_Test_v1.subStatus cst_substatus = new AddLee_SoapIntegration_Test_v1.subStatus();
            
            System.debug('<<<CHECKPOINT_SOAP_1>>>');
            
            //SFDC Record TYpe
            //Business Account  Standard
            //Consumer Account  Person
            //Indipendent Traveller Person
            //Personal Custom   Person
            //Supplier/Partner  Standard
            
            //Billing address
            AddLee_SoapIntegration_Test_v1.location billing_location = new AddLee_SoapIntegration_Test_v1.location();
            billing_location.address        = acc.SHM_billing_address__c;
            billing_location.latitude       = this.getIntegerValue(acc.SHM_billing_latitude__c);             
            billing_location.longitude      = this.getIntegerValue(acc.SHM_billing_longitude__c);            
            billing_location.postcode       = acc.SHM_billing_postcode__c;
            billing_location.town           = acc.SHM_billing_town__c;
            cst_billingAddress.contactName  = acc.SHM_billing_contactName__c; 
            cst_billingAddress.email        = acc.SHM_billing_email__c;            
            cst_billingAddress.fax          = acc.SHM_billing_fax__c;              
            cst_billingAddress.telephone    = acc.SHM_billing_telephone__c;
            cst_billingAddress.location     = billing_location;
            
            
            System.debug('<<<CHECKPOINT_SOAP_2>>>');
            
            //Main address details
            AddLee_SoapIntegration_Test_v1.location main_location = new AddLee_SoapIntegration_Test_v1.location();
            main_location.address       = acc.SHM_main_address__c;               
            main_location.latitude      = this.getIntegerValue(acc.SHM_main_latitude__c);                 	
            main_location.longitude     = this.getIntegerValue(acc.SHM_main_longitude__c);               
            main_location.postcode      = acc.SHM_main_postcode__c;
            main_location.town          = acc.SHM_main_town__c;
            cst_mainAddress.contactName = acc.SHM_main_contactName__c;
            cst_mainAddress.email       = acc.SHM_main_email__c;
            cst_mainAddress.fax         = acc.SHM_main_fax__c;
            cst_mainAddress.telephone   = acc.SHM_main_telephone__c;
            cst_mainAddress.location    = main_location;
            
            System.debug('<<<CHECKPOINT_SOAP_3>>>');

            //Grade 
            cst_grade.id    = this.getIntegerValue(acc.SHM_grade_id__c);
            cst_grade.name  = acc.SHM_grade_name__c;
            
            //InvoicingPolicy
            cst_invoicingPolicy.code    = acc.SHM_invoicingPolicy_code__c;
            cst_invoicingPolicy.id      = this.getIntegerValue(acc.SHM_invoicingPolicy_id__c);
            cst_invoicingPolicy.name    = acc.SHM_invoicingPolicy_name__c;  
            
            //SalesLedger
            cst_salesLedger.code    = acc.SHM_salesLedger_code__c;
            cst_salesLedger.id      = this.getIntegerValue(acc.SHM_salesLedger_id__c);
            cst_salesLedger.name    = acc.SHM_salesLedger_name__c;

            //Status
            cst_status.code     = acc.SHM_status_code__c;
            cst_status.id       = this.getIntegerValue(acc.SHM_status_id__c);
            cst_status.name     = acc.SHM_status_name__c;
            
            //Substatus
            cst_substatus.code  = acc.SHM_substatus_code__c;
            cst_substatus.id    = this.getIntegerValue(acc.SHM_substatus_id__c);
            cst_substatus.name  = acc.SHM_substatus_name__c;            
        
            //Object related
            this.billingAddress     = cst_billingAddress;
            this.mainAddress        = cst_mainAddress;
            this.grade              = cst_grade;
            this.invoicingPolicy    = cst_invoicingPolicy;
            this.salesLedger        = cst_salesLedger;
            this.status             = cst_status;
            this.subStatus          = cst_substatus;

            //Other Fields
            this.birthDate      			= acc.SHM_birthday__c;               
            this.bookingEmail   			= acc.SHM_bookingEmail__c;  			
            this.bookingNote                = acc.SHM_bookingNote__c;           
            this.creditDays                 = this.getIntegerValue(acc.SHM_creditDays__c);              
            this.creditLimit                = acc.SHM_creditLimit__c;                   
            this.discountRate               = acc.SHM_discountRate__c;                  
            this.generateIndividualInvoice  = acc.SHM_generateIndividualInvoice__c; 
            this.id                         = this.getIntegerValue(acc.SHM_id__c);          
            this.invoiceClearanceType       = acc.SHM_invoiceClearanceType__c;      
            this.message                    = acc.SHM_message__c;           
            this.name                       = acc.SHM_name__c;      
            this.number_x                   = (Long)Decimal.valueOf(acc.SHM_number_x__c);                
            this.serviceCharge              = acc.SHM_serviceCharge__c;             
            this.type_x                     = acc.SHM_type_x__c;        
            this.useWeb                     = acc.SHM_useWeb__c;                        
            this.vatRate                    = acc.SHM_vatRate__c;                   
            
        }
        
        public Integer getIntegerValue(Decimal val){
            if(val != null)
                return Integer.valueOf(val);
            
            return 0;
        }

        public Integer getIntegerValue(String val){
            if(val != null)
                return Integer.valueOf(val);
            
            return 0;
        }       

    }
    public virtual class response {
        public String description;
        public Integer errorCode;
        public AddLee_SoapIntegration_Test_v1.errorInfo errorInfo;
        public String stackTrace;
        private String[] description_type_info = new String[]{'description','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] errorCode_type_info = new String[]{'errorCode','http://www.haulmont.com/SHM',null,'1','1','false'};
        private String[] errorInfo_type_info = new String[]{'errorInfo','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] stackTrace_type_info = new String[]{'stackTrace','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'description','errorCode','errorInfo','stackTrace'};
    }
    public class loginRequest {
        public String password;
        public String username;
        private String[] password_type_info = new String[]{'password','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] username_type_info = new String[]{'username','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'password','username'};
    }
    public class request {
        public String sessionHandle;
        private String[] sessionHandle_type_info = new String[]{'sessionHandle','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'sessionHandle'};
    }
    public class salesLedger {
        public String code;
        public Long id;
        public String name;
        private String[] code_type_info = new String[]{'code','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] id_type_info = new String[]{'id','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'code','id','name'};
    }
    public class grade {
        public Long id;
        public String name;
        private String[] id_type_info = new String[]{'id','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'id','name'};
    }
    public class createCustomerRequest {
        
        /* Fix in the response 
        public String sessionHandle;
        public AddLee_SoapIntegration_Test_v1.customer customer;
        private String[] sessionHandle_type_info = new String[]{'sessionHandle','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] customer_type_info = new String[]{'customer','http://www.haulmont.com/SHM',null,'0','1','true'};       
        
        public AddLee_SoapIntegration_Test_v1.customer[] childCustomers;
        private String[] childCustomers_type_info = new String[]{'childCustomers','http://www.haulmont.com/SHM',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'sessionHandle', 'customer', 'childCustomers'};
    }
    public class bank {
        public String accountName;
        public String accountNumber;
        public String address;
        public String name;
        public String sortCode;
        private String[] accountName_type_info = new String[]{'accountName','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] accountNumber_type_info = new String[]{'accountNumber','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] address_type_info = new String[]{'address','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] sortCode_type_info = new String[]{'sortCode','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'accountName','accountNumber','address','name','sortCode'};
    }
    public class statusRequest {
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class singleReferenceErrorInfo {
        public String referenceExample;
        public String referencePattern;
        private String[] referenceExample_type_info = new String[]{'referenceExample','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] referencePattern_type_info = new String[]{'referencePattern','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'referenceExample','referencePattern'};
    }
    public class logoutResponse {
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class site {
        public String contactName;
        public String email;
        public String fax;
        public AddLee_SoapIntegration_Test_v1.location location;
        public String telephone;
        private String[] contactName_type_info = new String[]{'contactName','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] email_type_info = new String[]{'email','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] fax_type_info = new String[]{'fax','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] location_type_info = new String[]{'location','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] telephone_type_info = new String[]{'telephone','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'contactName','email','fax','location','telephone'};
    }
    public class amendCustomerRequest {
        /* Fix error 
        public String sessionHandle;
        private String[] sessionHandle_type_info = new String[]{'sessionHandle','http://www.haulmont.com/SHM',null,'0','1','false'};
        
        public AddLee_SoapIntegration_Test_v1.customer customer;
        private String[] customer_type_info = new String[]{'customer','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'sessionHandle','customer'};
    }
    public class agentRequest {
        public String initials;
        public String name;
        private String[] initials_type_info = new String[]{'initials','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'initials','name'};
    }
    public class gradeResponse {
        public AddLee_SoapIntegration_Test_v1.grade[] grades;
        private String[] grades_type_info = new String[]{'grades','http://www.haulmont.com/SHM',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'grades'};
    }
    public class loginResponse extends AddLee_SoapIntegration_Test_v1.response{
        public String sessionId;
        private String[] sessionId_type_info = new String[]{'sessionId','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'sessionId'};

        /* Fix error 
        public String description;
        public Integer errorCode;
        public AddLee_SoapIntegration_Test_v1.errorInfo errorInfo;
        public String stackTrace;
        private String[] description_type_info = new String[]{'description','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] errorCode_type_info = new String[]{'errorCode','http://www.haulmont.com/SHM',null,'1','1','false'};
        private String[] errorInfo_type_info = new String[]{'errorInfo','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] stackTrace_type_info = new String[]{'stackTrace','http://www.haulmont.com/SHM',null,'0','1','false'};
    }
    public class status {
        public String code;
        public Long id;
        public String name;
        private String[] code_type_info = new String[]{'code','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] id_type_info = new String[]{'id','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'code','id','name'};
    }
    public class CustomerManagementWebServicePort {
        public String endpoint_x = 'http://91.213.230.17:8080/customer-management-ws/CustomerManagementWebService1.0';
        //public String endpoint_x = 'http://requestb.in/116wsjn1';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://www.haulmont.com/SHM', 'AddLee_SoapIntegration_Test_v1'};
        public String Login(String password,String username) {
            AddLee_SoapIntegration_Test_v1.loginRequest request_x = new AddLee_SoapIntegration_Test_v1.loginRequest();
            AddLee_SoapIntegration_Test_v1.loginResponse response_x;
            request_x.password = password;
            request_x.username = username;
            Map<String, AddLee_SoapIntegration_Test_v1.loginResponse> response_map_x = new Map<String, AddLee_SoapIntegration_Test_v1.loginResponse>();
            response_map_x.put('response_x', response_x);
            
            System.debug('<<<REQUEST_X>>>: '+ request_x);
           
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://www.haulmont.com/SHM',
              'Login',
              'http://www.haulmont.com/SHM',
              'LoginResponse',
              'AddLee_SoapIntegration_Test_v1.loginResponse'}
            );
            
            
            System.debug('<<<RESPONSE_MAP>>>: '+ response_map_x);
            
            response_x = response_map_x.get('response_x');
            return response_x.sessionId;

        }
        public AddLee_SoapIntegration_Test_v1.customerResponse CreateCustomer(String sessionId, AddLee_SoapIntegration_Test_v1.customer customer, AddLee_SoapIntegration_Test_v1.customer[] childCustomers) {
            
            System.debug('<<<CHECK_INPUT_SESSION_ID>>>:'+sessionId);
            System.debug('<<<CHECK_INPUT_CUSTOMER>>>:'+customer);
            System.debug('<<<CHECK_INPUT_CHILD_CUSTOMER>>>:'+childCustomers);
            
            AddLee_SoapIntegration_Test_v1.createCustomerRequest request_x = new AddLee_SoapIntegration_Test_v1.createCustomerRequest();
            AddLee_SoapIntegration_Test_v1.customerResponse response_x;
            request_x.sessionHandle = sessionId;
            request_x.customer = customer;
            request_x.childCustomers = childCustomers;
            Map<String, AddLee_SoapIntegration_Test_v1.customerResponse> response_map_x = new Map<String, AddLee_SoapIntegration_Test_v1.customerResponse>();
            response_map_x.put('response_x', response_x);
            
            //System.debug('<<<CHECK_REQUEST_X>>>: '+ request_x);
            
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://www.haulmont.com/SHM',
              'CreateCustomer',
              'http://www.haulmont.com/SHM',
              'CreateCustomerResponse',
              'AddLee_SoapIntegration_Test_v1.customerResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
        public AddLee_SoapIntegration_Test_v1.customerResponse AmendCustomer(String sessionId, AddLee_SoapIntegration_Test_v1.customer customer) {
            
            System.debug('<<<UPDATE_CHECK_INPUT_SESSION_ID>>>:'+sessionId);
            System.debug('<<<UPDATE_CHECK_INPUT_CUSTOMER>>>:'+customer);
            
            AddLee_SoapIntegration_Test_v1.amendCustomerRequest request_x = new AddLee_SoapIntegration_Test_v1.amendCustomerRequest();
            AddLee_SoapIntegration_Test_v1.customerResponse response_x;
            request_x.sessionHandle = sessionId;
            request_x.customer = customer;
            Map<String, AddLee_SoapIntegration_Test_v1.customerResponse> response_map_x = new Map<String, AddLee_SoapIntegration_Test_v1.customerResponse>();
            response_map_x.put('response_x', response_x);
            
            System.debug('<<<UPDATE_CHECK_REQUEST_X>>>: '+ request_x);
            
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://www.haulmont.com/SHM',
              'AmendCustomer',
              'http://www.haulmont.com/SHM',
              'AmendCustomerResponse',
              'AddLee_SoapIntegration_Test_v1.customerResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
        public void Logout() {
            AddLee_SoapIntegration_Test_v1.logoutRequest request_x = new AddLee_SoapIntegration_Test_v1.logoutRequest();
            AddLee_SoapIntegration_Test_v1.logoutResponse response_x;
            Map<String, AddLee_SoapIntegration_Test_v1.logoutResponse> response_map_x = new Map<String, AddLee_SoapIntegration_Test_v1.logoutResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://www.haulmont.com/SHM',
              'Logout',
              'http://www.haulmont.com/SHM',
              'LogoutResponse',
              'AddLee_SoapIntegration_Test_v1.logoutResponse'}
            );
            response_x = response_map_x.get('response_x');
        }
    }
    public class agent {
        public String email;
        public Long id;
        public String initials;
        public String name;
        private String[] email_type_info = new String[]{'email','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] id_type_info = new String[]{'id','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] initials_type_info = new String[]{'initials','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'email','id','initials','name'};
    }
    public class logoutRequest {
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{};
    }
    public class customerResponse {
        /* Fix error 
        public String description;
        public Integer errorCode;
        public AddLee_SoapIntegration_Test_v1.errorInfo errorInfo;
        public String stackTrace;
        private String[] description_type_info = new String[]{'description','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] errorCode_type_info = new String[]{'errorCode','http://www.haulmont.com/SHM',null,'1','1','false'};
        private String[] errorInfo_type_info = new String[]{'errorInfo','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] stackTrace_type_info = new String[]{'stackTrace','http://www.haulmont.com/SHM',null,'0','1','false'};      
        
        
        public AddLee_SoapIntegration_Test_v1.customerError[] errorList;
        private String[] errorList_type_info = new String[]{'errorList','http://www.haulmont.com/SHM',null,'0','-1','true'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'errorList'};
    }
    public class multiReferenceErrorInfo {
        public Long entityId;
        public String entityName;
        public String referenceExample;
        public String referencePattern;
        private String[] entityId_type_info = new String[]{'entityId','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] entityName_type_info = new String[]{'entityName','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] referenceExample_type_info = new String[]{'referenceExample','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] referencePattern_type_info = new String[]{'referencePattern','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'entityId','entityName','referenceExample','referencePattern'};
    }
    public class subStatus {
        public String code;
        public Long id;
        public String name;
        private String[] code_type_info = new String[]{'code','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] id_type_info = new String[]{'id','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'code','id','name'};
    }
    public class invoicingPolicy {
        public String code;
        public Long id;
        public String name;
        private String[] code_type_info = new String[]{'code','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] id_type_info = new String[]{'id','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] name_type_info = new String[]{'name','http://www.haulmont.com/SHM',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.haulmont.com/SHM','false','false'};
        private String[] field_order_type_info = new String[]{'code','id','name'};
    }*/
}