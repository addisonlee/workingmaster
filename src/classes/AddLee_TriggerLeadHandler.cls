public with sharing class AddLee_TriggerLeadHandler {
    /* ATTRIBUTES */
    AddLee_Lead_Conversion leadConversion = new AddLee_Lead_Conversion();
    
    
    /*private static AddLee_TriggerLeadHandler instance;
    public static AddLee_TriggerLeadHandler getInstance() {
        if (instance == null) {
            instance = new AddLee_TriggerLeadHandler();
        }
        return instance;
    } */

    /* PUBLIC METHODS */

    /**
    * @author Nimil Christopher
    * @description fired on before insert event
    * @return void
    * @param newObjects: trigger.new - list of all records to insert
    */
    public void onBeforeInsert(final List<Lead> newObjects) {
        List<String> industrySegments = AddLee_SegmentationHelper.getLeadIndustrySegments();
        if(AddLee_ContextVariables.firstRunLead){
            AddLee_ContextVariables.employeeSizeBandMap = AddLee_SegmentationHelper.getEmployeeSizeBand(industrySegments);
            AddLee_ContextVariables.activeUsersMap = AddLee_SegmentationHelper.getActiveUsersSegment(industrySegments);
            AddLee_ContextVariables.expectedExpenditureSpendMap = AddLee_SegmentationHelper.getExpectedExpenditure(industrySegments);
            AddLee_ContextVariables.postCodeMap = AddLee_SegmentationHelper.getPostCodesMap();
            AddLee_ContextVariables.cryptoKey = Addlee_Crypto.getCryptoKey();
            AddLee_ContextVariables.bandingValues = AddLee_SegmentationHelper.getBandingValues();
            AddLee_ContextVariables.firstRunLead = false;
        }
        AddLee_SegmentationHelper.allocateBandingValues(newObjects, AddLee_ContextVariables.bandingValues);
        AddLee_SegmentationHelper.allocatePostCodeLead(newObjects, AddLee_ContextVariables.postCodeMap);
        AddLee_SegmentationHelper.allocateSegmentsLead(newObjects,AddLee_ContextVariables.employeeSizeBandMap);
        AddLee_SegmentationHelper.allocateActiveUsersEstimatesForLead(newObjects, AddLee_ContextVariables.activeUsersMap);
        AddLee_SegmentationHelper.allocateSpendExpenditureForLead(Trigger.new, AddLee_ContextVariables.expectedExpenditureSpendMap);
        AddLee_Crypto.encryptBankFields(Trigger.new,AddLee_ContextVariables.cryptoKey);
    }

    /**
    * @author Nimil Christopher
    * @description fired on after insert event
    * @return void
    * @param newObjects: trigger.new - list of all records that were inserted
    * @param newObjectsMap: trigger.new - map of all records that were inserted
    */
    public void onAfterInsert(final List<Lead> newObjects, final Map<Id, Lead> newObjectsMap) {
        if(!system.isFuture() && !system.isBatch()){
        	/*List<string> profileNames = new List<string>{'Addison Lee Sales Team', 'System Administrator'};
        	Map<id, Profile> profileMap = new Map<id, Profile>([Select id, Name from Profile Where Name IN :profileNames limit 2]);
        	if(profileMap.get(UserInfo.getProfileId()) != null){
        		AddLee_SegmentationHelper.allocateTNC(newObjects);
        	}*/
        	System.DEBUG('MTDebug you are here onAfterInsert 1 : ');
            Set <Id> leadIds = new Set <Id>();
            for(Lead eachLead : newObjects){
                if(eachLead.Owner_Alias__c != null){
                    leadIds.add(eachLead.Id);
                }
            }
            System.DEBUG('MTDebug you are here onAfterInsert 2 : ');
            if(leadIds.size() > 0){
                AddLee_Lead_Assignment.allocateOwnerWebToLead(leadIds);
                System.DEBUG('MTDebug you are here onAfterInsert 3 : ');
            }
            Id webuserId = [Select id from User where Name = 'Web User' limit 1].id;
			if(UserInfo.getUserId() == webuserId){
			    System.DEBUG('MTDebug you are here  onAfterInsert 4 : ');
				AddLee_Lead_Conversion.convertLeadToCurrentAccount( newObjectsMap.keySet()) ;
			} else {
                // The following part of processing was handeled by a batch process previously, 
                // we are converting it to a trigger based process now. 
                AddLee_Lead_Conversion.convertLeadonSchedule( newObjectsMap.keySet() );
            }
            
        }
        
    }

    /**
    * @author Nimil Christopher
    * @description fired on before update event
    * @return void
    * @param oldObjects: trigger.old - list of all records before the change to update
    * @param oldObjectsMap: trigger.oldMap - map of all records before the change to update
    * @param newObjects: trigger.new - list of all changed records to update
    * @param newObjectsMap: trigger.newMap - map of all changed records to update
    */
    public void onBeforeUpdate(final List<Lead> oldObjects, final Map<Id, Lead> oldObjectsMap,
                               final List<Lead> newObjects, final Map<Id, Lead> newObjectsMap) {
        List<String> industrySegments = AddLee_SegmentationHelper.getLeadIndustrySegments();
        if(AddLee_ContextVariables.firstRunLead){
            AddLee_ContextVariables.employeeSizeBandMap = AddLee_SegmentationHelper.getEmployeeSizeBand(industrySegments);
            AddLee_ContextVariables.activeUsersMap = AddLee_SegmentationHelper.getActiveUsersSegment(industrySegments);
            AddLee_ContextVariables.expectedExpenditureSpendMap = AddLee_SegmentationHelper.getExpectedExpenditure(industrySegments);
            AddLee_ContextVariables.postCodeMap = AddLee_SegmentationHelper.getPostCodesMap();
            AddLee_ContextVariables.cryptoKey = Addlee_Crypto.getCryptoKey();
            AddLee_ContextVariables.bandingValues = AddLee_SegmentationHelper.getBandingValues();
            AddLee_ContextVariables.firstRunLead = false;
        }
        AddLee_SegmentationHelper.allocateTNC(oldObjectsMap, newObjectsMap);
        //AddLee_SegmentationHelper.handleTNC(oldObjectsMap, newObjectsMap);
        AddLee_SegmentationHelper.allocateBandingValues(newObjects, AddLee_ContextVariables.bandingValues);
        AddLee_SegmentationHelper.allocatePostCodeLead(newObjects, AddLee_ContextVariables.postCodeMap);
        AddLee_SegmentationHelper.allocateSegmentsLead(newObjects,AddLee_ContextVariables.employeeSizeBandMap);
        AddLee_SegmentationHelper.allocateActiveUsersEstimatesForLead(newObjects, AddLee_ContextVariables.activeUsersMap);
        AddLee_SegmentationHelper.allocateSpendExpenditureForLead(Trigger.new, AddLee_ContextVariables.expectedExpenditureSpendMap);
        AddLee_Crypto.encryptBankFields(Trigger.new,AddLee_ContextVariables.cryptoKey);
    }

    /**
    * @author Nimil Christopher
    * @description fired on after update event
    * @return void
    * @param oldObjects: trigger.old - list of all records before the change to update
    * @param oldObjectsMap: trigger.oldMap - map of all records before the change to update
    * @param newObjects: trigger.new - list of all changed records to update
    * @param newObjectsMap: trigger.newMap - map of all changed records to update
    */
    public void onAfterUpdate(final List<Lead> oldObjects, final Map<Id, Lead> oldObjectsMap,
                              final List<Lead> newObjects, final Map<Id, Lead> newObjectsMap) { 
         //Added by Manish to avoid recurssions
         if(!system.isFuture() && !system.isBatch()){
             if(AddLee_ContextVariables.firstRunLeadAfterUpdate){
                 System.DEBUG('MTDebug you are here onAfterUpdate 1 : ');
                 leadConversion.copyCompetitorsToAccountOnConversion(newObjectsMap);   
                 Id webuserId = [Select id from User where Name = 'Web User' limit 1].id;
                 if(!system.isFuture() && !system.isBatch()){
        			if(UserInfo.getUserId() == webuserId){
        			    System.DEBUG('MTDebug you are here onAfterUpdate 2 : ');
        				AddLee_Lead_Conversion.convertLeadToCurrentAccount( newObjectsMap.keySet()) ;
        			} else {
                        // The following part of processing was handeled by a batch process previously, 
                        // we are converting it to a trigger based process now.
                        System.DEBUG('MTDebug you are here onAfterUpdate 3 : ');
                        AddLee_Lead_Conversion.convertLeadonSchedule( newObjectsMap.keySet() );
                    }
                 }
                 AddLee_ContextVariables.firstRunLeadAfterUpdate = false;
             }
         }    
    }

    /**
    * @author Nimil Christopher
    * @description fired on before delete event, deletes all the attached documents and sets the application active to false
    * @return void
    * @param oldObjects: trigger.old - list of all records before tdelete
    * @param oldObjectsMap: trigger.oldMap - map of all records before delete
    */
    public void onBeforeDelete(final List<Lead> oldObjects, final Map<Id, Lead> oldObjectsMap) {
        
    }

    /**
    * @author Nimil Christopher
    * @description fired on after delete event
    * @return void
    * @param oldObjects: trigger.old - list of all records after tdelete
    * @param oldObjectsMap: trigger.oldMap - map of all records after delete
    */
    public void onAfterDelete(final List<Lead> oldObjects, final Map<Id, Lead> oldObjectsMap) {
    }

    /**
    * @author Nimil Christopher
    * @description fired on after undelete event
    * @return void
    * @param newObjects: trigger.new - list of all records that are undeleted
    * @param newObjectsMap: trigger.new - map of all records that are undeleted
    */
    public void onAfterUndelete(final List<Lead> newObjects, final Map<Id, Lead> newObjectsMap) {
    }
    
}