public class AddLee_QuickSearch_Controller {

    public string businessName { get; set; }
    public string countryCode { get; set; }
    public string dunsNumber { get; set; }
    public string postCode { get; set; }
    public string town { get; set; }
    public boolean rendered { get; set; }
    public string results { get; set; }
    public string baseUrl { get; set; }
     
    public AddLee_QuickSearch_Controller(){
        baseUrl = ApexPages.currentPage().getHeaders().get('Host');
    }
    
}