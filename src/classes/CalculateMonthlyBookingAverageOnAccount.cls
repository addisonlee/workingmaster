/*
** Calculate Monthley Spend on account level.
*/ 
global class CalculateMonthlyBookingAverageOnAccount implements Database.Batchable<sObject>{
   global String Query;
   global String AccountId;

   global Database.QueryLocator start(Database.BatchableContext BC){
      return Database.getQueryLocator(Query);
   }

   global void execute(Database.BatchableContext BC, 
                       List<Account> bscope){
                       
                       List<Account> lstUpdateAccount = new List<Account>();
                       for(Account acc:bscope){
                       Map<Integer,Decimal> MapMonthBookingCount = new Map<Integer,Decimal>();
                       Map<Integer,Decimal> prevMapMonthBookingCount = new Map<Integer,Decimal>();
                       Map<Integer,Decimal> prevMapMonthBookingPriceCount = new Map<Integer,Decimal>();
                       Map<Integer,Decimal> MapMonthBookingPriceCount = new Map<Integer,Decimal>();
                       for(Integer iii=1;iii<=12;iii++){
                       MapMonthBookingCount.put(iii,0);
                       prevMapMonthBookingCount.put(iii,0);
                       prevMapMonthBookingPriceCount.put(iii,0);
                       MapMonthBookingPriceCount.put(iii,0);
                       }
                       AccountId = acc.Id;
                       Decimal totalcount=0;
                       DateTime dt = (System.now()-20);
                       Datetime startDate = Datetime.newInstance(dt.year()-1, dt.month(), dt.day(), 0, 0, 0);
                       
                     /* Date pres = system.today();
                       Date prev = pres.addyears(-1);                                              
                       Integer presyear = pres.year();
                       Integer prevyear = prev.year();*/
                       /* Future year test start */
                       DateTime mydate = DateTime.now();
                      //  date mydate = date.parse('01/01/2016');
                     //   Date pres = mydate.year();
                        datetime prev = mydate.addyears(-1);
                        integer presyear = mydate.year();
                        integer prevyear = prev.year();
                       /***************end*************/ 
                       //list of current and previous year Booking Summaries from total booking summaries. 
                       List<Booking_Summary__c> lstBooking  =  [SELECT Id,Account__c,Date_Time__c,Number_of_Bookings__c,Date_From__c,Total_Price__c,Total_Journey_Price_Currency__c,Service_Type__c FROM Booking_Summary__c where Date_From__c!=null and Total_Journey_Price_Currency__c!=null and Account__c=:AccountId limit 49999];//Lastmodifieddate>:startDate and

                       if(MapMonthBookingCount.size() >0 && lstBooking.size()>0 && prevMapMonthBookingCount.size()>0){
                       // Create Map for current year booking summaries.  
                       for(Booking_Summary__c book:lstBooking){
                       if(book.Date_From__c != null){
                       if(integer.valueof(book.Date_From__c.year()) == integer.valueof(presyear)){
                       system.debug('DATE OF THE YEAR : ----' +book.Date_From__c.year());
                       if(MapMonthBookingCount.containskey(book.Date_From__c.month())){
                       MapMonthBookingCount.put(book.Date_From__c.month(),MapMonthBookingCount.get(book.Date_From__c.month())+book.Number_of_Bookings__c);
                       if(book.Total_Journey_Price_Currency__c!=null){
                       MapMonthBookingPriceCount.put(book.Date_From__c.month(),MapMonthBookingPriceCount.get(book.Date_From__c.month())+book.Total_Price__c);
                       }
                       }
                       }
                       } else {
                       MapMonthBookingCount.put(book.Date_From__c.month(),book.Number_of_Bookings__c);
                       if(book.Total_Journey_Price_Currency__c!=null){
                       MapMonthBookingPriceCount.put(book.Date_From__c.month(),book.Total_Price__c);
                       }
                       }
                       totalcount=totalcount+book.Number_of_Bookings__c;
                       }
                       
                       //List<Booking_Summary__c> lstyerBooking  =  [SELECT Id,Account__c,Date_Time__c,Number_of_Bookings__c,Date_From__c,Total_Price__c,Total_Journey_Price_Currency__c,Service_Type__c FROM Booking_Summary__c where  Account__c=:AccountId and Date_From__c = :prev  limit 50000];//Lastmodifieddate>:startDate and
                       //if(prevMapMonthBookingCount.size()>0 && lstyerBooking.size()>0){
                       // create map last year booking summaries.
                       for(Booking_Summary__c book:lstBooking){
                       if(book.Date_From__c != null){
                       if(integer.valueof(book.Date_From__c.year()) == integer.valueof(prevyear)){
                       if(prevMapMonthBookingCount.containskey(book.Date_From__c.month())){
                       prevMapMonthBookingCount.put(book.Date_From__c.month(),prevMapMonthBookingCount.get(book.Date_From__c.month())+book.Number_of_Bookings__c);
                       if(book.Total_Journey_Price_Currency__c!=null){
                       prevMapMonthBookingPriceCount.put(book.Date_From__c.month(),prevMapMonthBookingPriceCount.get(book.Date_From__c.month())+book.Total_Price__c);
                       }
                       }
                       }
                       } else {
                       prevMapMonthBookingCount.put(book.Date_From__c.month(),book.Number_of_Bookings__c);
                       if(book.Total_Journey_Price_Currency__c!=null){
                       prevMapMonthBookingPriceCount.put(book.Date_From__c.month(),book.Total_Price__c);
                       }
                       }
                       totalcount=totalcount+book.Number_of_Bookings__c;
                       }
                       // total amount of bookings greater than zero per account means update those values according to month.                    
                       if(totalcount>0){
                       //acc.January_Percent__c = ((MapMonthBookingCount.get(1)*100)/totalcount);
                       //updating present january value 
                       if(MapMonthBookingCount.get(1)>0){
                      // acc.January_Average__c = (MapMonthBookingPriceCount.get(1)/MapMonthBookingCount.get(1));
                        acc.January_Average__c = (MapMonthBookingPriceCount.get(1));
                       }else{
                       acc.January_Average__c = null;
                       }
                       //updating previous january value 
                       if(prevMapMonthBookingCount.get(1)>0){
                       acc.January_Last_Year__c = (prevMapMonthBookingPriceCount.get(1));
                       }else{
                       acc.January_Last_Year__c = null;
                       }
                       //acc.February_Percent__c = ((MapMonthBookingCount.get(2)*100)/totalcount);
                       if(MapMonthBookingCount.get(2)>0){
                       acc.February_Average__c = (MapMonthBookingPriceCount.get(2));
                       }else{
                       acc.February_Average__c =null;
                       }
                       if(prevMapMonthBookingCount.get(2)>0){
                       acc.February_Last_Year__c = (prevMapMonthBookingPriceCount.get(2));
                       }else{
                       acc.February_Last_Year__c = null;
                       }
                       //acc.March_Percent__c = ((MapMonthBookingCount.get(3)*100)/totalcount);
                       if(MapMonthBookingCount.get(3)>0){
                       acc.March_Average__c = (MapMonthBookingPriceCount.get(3));
                       }else{
                       acc.March_Average__c=null;
                       }
                       if(prevMapMonthBookingCount.get(3)>0){
                       acc.March_Last_Year__c = (prevMapMonthBookingPriceCount.get(3));
                       }else{
                       acc.March_Last_Year__c=null;
                       }
                     //acc.April_Percent__c = ((MapMonthBookingCount.get(4)*100)/totalcount);
                       if(MapMonthBookingCount.get(4)>0){
                       acc.April_Average__c = (MapMonthBookingPriceCount.get(4));
                       }else{
                       acc.April_Average__c =null;
                       }
                       if(prevMapMonthBookingCount.get(4)>0){
                       acc.April_Last_Year__c = (prevMapMonthBookingPriceCount.get(4));
                       }else{
                       acc.April_Last_Year__c = null;
                       }
                       //acc.May_Percent__c = ((MapMonthBookingCount.get(5)*100)/totalcount);
                       if(MapMonthBookingCount.get(5)>0){
                       acc.May_Average__c = (MapMonthBookingPriceCount.get(5));
                       }else{
                       acc.May_Average__c = null;
                       }
                       if(prevMapMonthBookingCount.get(5)>0){
                       acc.May_Last_Year__c = (prevMapMonthBookingPriceCount.get(5));
                       }else{
                       acc.May_Last_Year__c = null; 
                       }                       
                       //acc.June_Percent__c = ((MapMonthBookingCount.get(6)*100)/totalcount);
                       if(MapMonthBookingCount.get(6)>0){
                       acc.June_Average__c = (MapMonthBookingPriceCount.get(6));
                       }else{
                       acc.June_Average__c = null;
                       }
                       if(prevMapMonthBookingCount.get(6)>0){
                       acc.June_Last_Year__c = (prevMapMonthBookingPriceCount.get(6));
                       }else{
                       acc.June_Last_Year__c = null;
                       }
                       //acc.July_Percent__c = ((MapMonthBookingCount.get(7)*100)/totalcount);
                       if(MapMonthBookingCount.get(7)>0){
                       acc.July_Average__c= (MapMonthBookingPriceCount.get(7));
                       }else{
                       acc.July_Average__c = null;
                       }
                       if(prevMapMonthBookingCount.get(7)>0){
                       acc.July_Last_Year__c = (prevMapMonthBookingPriceCount.get(7));
                       }else{
                        acc.July_Last_Year__c = null;
                       }
                       //acc.August_Percent__c = ((MapMonthBookingCount.get(8)*100)/totalcount);
                       if(MapMonthBookingCount.get(8)>0){
                       acc.August_Average__c = (MapMonthBookingPriceCount.get(8));
                       }else{
                       acc.August_Average__c = null;
                       }
                       if(prevMapMonthBookingCount.get(8)>0){
                       acc.August_Last_Year__c = (prevMapMonthBookingPriceCount.get(8));
                       }else{
                       acc.August_Last_Year__c = null;
                       }
                       //acc.September_Percent__c = ((MapMonthBookingCount.get(9)*100)/totalcount);
                       if(MapMonthBookingCount.get(9)>0){
                       acc.September_Average__c = (MapMonthBookingPriceCount.get(9));
                       }else{
                       acc.September_Average__c = null;
                       }
                       if(prevMapMonthBookingCount.get(9)>0){
                       acc.September_Last_Year__c = (prevMapMonthBookingPriceCount.get(9));
                       }else{
                       acc.September_Last_Year__c = null;
                       }
                       //acc.October_Percent__c = ((MapMonthBookingCount.get(10)*100)/totalcount);
                       if(MapMonthBookingCount.get(10)>0){
                       acc.October_Average__c = (MapMonthBookingPriceCount.get(10));
                       }else{
                       acc.October_Average__c = null;
                       }
                       if(prevMapMonthBookingCount.get(10)>0){
                       acc.October_Last_Year__c = (prevMapMonthBookingPriceCount.get(10));
                       }
                       else{
                        acc.October_Last_Year__c = null;
                       }
                       //acc.November_Percent__c = ((MapMonthBookingCount.get(11)*100)/totalcount);
                       if(MapMonthBookingCount.get(11)>0){
                       acc.November_Average__c = (MapMonthBookingPriceCount.get(11));
                       }else{
                       acc.November_Average__c = null;
                       }
                       if(prevMapMonthBookingCount.get(11)>0){
                       acc.November_Last_Year__c = (prevMapMonthBookingPriceCount.get(11));
                       }else{
                       acc.November_Last_Year__c = null;
                       }
                       //acc.December_Percent__c = ((MapMonthBookingCount.get(12)*100)/totalcount);
                       if(MapMonthBookingCount.get(12)>0){
                       acc.December_Average__c = (MapMonthBookingPriceCount.get(12));
                       }else{
                       acc.December_Average__c = null;
                       }
                       if(prevMapMonthBookingCount.get(12)>0){
                       acc.December_Last_Year__c = (prevMapMonthBookingPriceCount.get(12));
                       }else{
                       acc.December_Last_Year__c = null;
                       }
                       lstUpdateAccount.Add(acc);
                       }
                       }
                       Update lstUpdateAccount;
   }
   }
  
   global void finish(Database.BatchableContext BC){

   }
}