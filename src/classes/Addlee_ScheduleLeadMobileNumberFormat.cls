global class Addlee_ScheduleLeadMobileNumberFormat implements schedulable {
global void execute (SchedulableContext SC){
Addlee_LeadMobileNumberFormat_Batch lmn = new Addlee_LeadMobileNumberFormat_Batch();
lmn.query = 'select Id,firstname,lastname,MobilePhone,et4ae5__Mobile_Country_Code__c,Mobile_Number_To_Review__c from Lead where MobilePhone!=null and IsConverted = False';
Database.executebatch(lmn);
}
}