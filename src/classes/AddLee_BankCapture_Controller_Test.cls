@isTest
private class AddLee_BankCapture_Controller_Test {
	
	@isTest(seeallData = false)
	static void AddLee_BankCapture_Controller_BankDetailsValid() {

    	AddLee_Trigger_Test_Utils.insertCustomSettings();

		Lead testLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		testLead.Bank_Date_of_DD__c = System.today();
		insert testLead;

		/*Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
	    User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
	    	EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
	    	LocaleSidKey='en_US', ProfileId = p.Id, 
	    	TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@addisonlee.com');
	    User myUser = [Select ID FROM User WHERE Id =: UserInfo.getUserId()];*/
	   
			//Lead testLead = [ SELECT Id, FirstName, LastName, Street, Country, PostalCode, State, City, Bank_Sort_Code__c, Bank_Account_Name__c, Bank_Account_Number__c, Bank__c, Bank_Date_of_DD__c, Bank_Details_Captured__c FROM LEAD WHERE ID = '00Q110000036JHp' ];
			System.debug(testLead);
			Test.setMock(WebServiceMock.class, new AddLee_WebServiceMockImpl());
			
			 	Test.startTest();	
					ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
					AddLee_BankCapture_Controller theController = new AddLee_BankCapture_Controller(controller);
					theController.isApexTestRunning = true;
					theController.validate();
					
					
		    	Test.stopTest();
			 //AddLee_BankCapture_Controller_Test.call(testLead.Id);
					
	}
	/*@future(callout=true)
	public static void call(Id leadId){
		Lead testLead = [SELECT Id, FirstName, LastName, Street, Country, PostalCode, State, City, Bank_Sort_Code__c, Bank_Account_Name__c, Bank_Account_Number__c, Bank__c, Bank_Date_of_DD__c, Bank_Details_Captured__c FROM LEAD WHERE ID =: leadId];
		ApexPages.StandardController controller = new ApexPages.StandardController(testLead);
		AddLee_BankCapture_Controller theController = new AddLee_BankCapture_Controller(controller);
		theController.validate();
	}*/
	
}