/*
*    Author        :    Prateek Gandhi
*    Description   : 
*                    - create a scheduled batch job (running every minute) that will iterate over recent (LastModifiedDate-wise) CaseMilestone records that 
*                      are not completed yet ant the associated case is not closed
*                    - for each CaseMilestone copy TargetDate field value into Entitlement_Expiry_Time__c field of the related Case
*                    - if there is more than one active milestone for a given case, please use the one that has the least time left to deadline
*                    - use custom setting DateTime field to store the most recent timestamp
*    
*/    
global class Addlee_MilestoneTargetDateUpdate implements Database.Batchable<sObject>, Database.Stateful{

    Map<id,case> updateCaseMap=new Map<id,case>();
    List<CaseMilestone> milestoneList;
    Map<id,DateTime> caseIdTargetDateMap= new Map<id,DateTime>();
    Case caseRec;
    String query;
    DateTime previousTimeStamp;
    DateTime UpdateTimeStamp;
    Case_Milestone_Scheduler_Settings__c lastBatchTimeStamp;
    Datetime sysTime;
    String chron_exp;

    global Addlee_MilestoneTargetDateUpdate () {        
        lastBatchTimeStamp = Case_Milestone_Scheduler_Settings__c.getInstance('Setting');
        if (lastBatchTimeStamp == null) {
            lastBatchTimeStamp = new Case_Milestone_Scheduler_Settings__c(Name = 'Setting');
            lastBatchTimeStamp.Time_Stamp__c = System.now().addMinutes(-1);
        }
        previousTimeStamp = lastBatchTimeStamp.Time_Stamp__c;
    }
   
    global Database.QueryLocator start(Database.BatchableContext BC){
        query='SELECT id,TargetDate,IsCompleted,CreatedDate,LastModifiedDate,CaseId,'+
                ' case.isClosed,case.id,case.Entitlement_Expiry_Time__c FROM CaseMilestone'+ 
                ' where case.isClosed=false and IsCompleted=false and '+ 
                ' LastModifiedDate >:previousTimeStamp  ORDER BY TargetDate ASC';
        if (Test.IsRunningTest()) query += ' LIMIT 200';
        //fetching all the caseMilestones which were modified after the timeStamp stored in custon setting and are not completed not is the related case closed.
        UpdateTimeStamp=system.now();
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<CaseMileStone> milestoneList){
                
        for(CaseMileStone cmObj: milestoneList){
            if((cmObj.TargetDate!=cmObj.case.Entitlement_Expiry_Time__c) && // milestone target Date!= Entitlement_Expiry_Time__c on case
                ((!updateCaseMap.containsKey(cmObj.caseId)) || // 1st milestone milestone for a case
                (updateCaseMap.containsKey(cmObj.caseId) && updateCaseMap.get(cmObj.caseId).Entitlement_Expiry_Time__c>cmObj.TargetDate))){// if multiple milestones then update with the soonest tgt date from milestones
                caseRec=new case();
                caseRec.Id=cmObj.caseId;
                caseRec.Entitlement_Expiry_Time__c=cmObj.TargetDate;                    
                updateCaseMap.put(caseRec.id,caseRec);
            }
        }
        If(updateCaseMap.size()>0)
            Database.update(updateCaseMap.values(),false);
    }

   global void finish(Database.BatchableContext BC){

        lastBatchTimeStamp.Time_Stamp__c=UpdateTimeStamp ;
        upsert lastBatchTimeStamp; // updating the time stamp only when all the records are processed
        //Deleting the scheduled jobs where status=deleted
        list<cronTrigger> cronList=[SELECT NextFireTime,CronJobDetail.name,EndTime,Id,State FROM CronTrigger where CronJobDetail.name like 'MilestoneTargetDateUpdate%'];
        try {
            for (cronTrigger cTObj:cronList) {
                system.abortJob(cTObj.id);
            }        
        } catch (Exception e) {}
        // rescheduling the same batch in scheduler class 'Addlee_ScheduleMilestoneTargetDateUpdate' 
        if(cronList.size()>0){
            sysTime = System.now().addMinutes(5);
            chron_exp = '' + sysTime.second() + ' ' + sysTime.minute() + ' ' + sysTime.hour() + ' ' + sysTime.day() + ' ' + sysTime.month() + ' ? ' + sysTime.year();
            Addlee_ScheduleMilestoneTargetDateUpdate CaseMilestoneBatch = new Addlee_ScheduleMilestoneTargetDateUpdate();
            System.schedule('MilestoneTargetDateUpdate ' + sysTime.getTime() ,chron_exp,CaseMilestoneBatch); 
        }
    }
}