@isTest
private class Addlee_Lead_Assignment_Test {
	
	@isTest static void AddLee_Lead_Assignment_isOwnerAssigned() {
		// Implement test code
		AddLee_Trigger_Test_Utils.insertTestEmployeeSizeBand();
    	AddLee_Trigger_Test_Utils.insertTestTargetActiveUsersBand();
    	AddLee_Trigger_Test_Utils.insertTestExpectedActiveUsersBand();
    	AddLee_Trigger_Test_Utils.insertTestTargetSpendExpenditure();
    	AddLee_Trigger_Test_Utils.insertTestExpectedSpendExpenditure();
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
    	Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
    	User testUser = new User(Alias = 'NTest', Email='standarduser@testorg.com', 
      						EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
      						LocaleSidKey='en_US', ProfileId = p.Id, 
      						TimeZoneSidKey='America/Los_Angeles', UserName='ntest@testorg.com');
    	insert testUser;
		Lead myLead = AddLee_Trigger_Test_Utils.createLeads(1)[0];
		myLead.email='fry.son@test.com';
		myLead.No_of_London_Employees__c = '1-5';
		myLead.Industry = 'Professional, Scientific and Technical';
		myLead.Industry_Sub_Sector__c = 'Management Consulting';
		myLead.Owner_Alias__c = 'NTest';
		

		Test.startTest();
			insert myLead;
		Test.stopTest();

		myLead = [Select ownerId FROM Lead WHERE id =: myLead.Id];
		testUser = [Select Id FROM User WHERE id =: testUser.Id];
		System.assertEquals(myLead.ownerId, testUser.Id);
	}
}