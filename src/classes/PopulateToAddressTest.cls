/**
 ** test class for PopulateToAddress
 */
@isTest
private class PopulateToAddressTest {

    static testMethod void test1() {
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Case c = new case (Origin ='Website - WestOne',Type = 'Admin',Priority='Low' );
        insert c;
        EmailMessage newemail = new EmailMessage (FromAddress = 'test@test.com',incoming= true,
                                                ToAddress ='uataccountexec2@addisonlee.co.uk',
                                                Subject = 'Test',
                                                TextBody ='testtest',
                                                ParentId = c.Id );
        insert newemail; 
                                            
        
        Case cs = [select id, To_Address__c from case where Id =:newemail.ParentId limit 1];
        cs.To_Address__c = newemail.ToAddress;
        update cs;
        system.assertEquals(cs.To_Address__c ,'uataccountexec2@addisonlee.co.uk');
    
    }
}