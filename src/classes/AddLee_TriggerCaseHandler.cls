//with sharing
public class AddLee_TriggerCaseHandler {

    AddLee_CaseAccountAssignment accountNumberAsssignment = new  AddLee_CaseAccountAssignment();

    public void onBeforeInsert(final List<Case> newObjects) {
        accountNumberAsssignment.assignAccount(newObjects);
        AddLee_CaseHelper.processCases(newObjects);
    }

    public void onBeforeUpdate(final List<Case> oldObjects, final Map<Id, Case> oldObjectsMap,
                               final List<Case> newObjects, final Map<Id, Case> newObjectsMap) {
        accountNumberAsssignment.assignAccount(newObjects);
        List<Case> casesToProcess = new List<Case>();

        for(case caseRec: newObjects){
                if((oldObjectsMap.get(caseRec.Id).Origin != newObjectsMap.get(caseRec.Id).Origin ) ||
                (oldObjectsMap.get(caseRec.Id).AccountId != newObjectsMap.get(caseRec.Id).AccountId ) || 
                (oldObjectsMap.get(caseRec.Id).Type != newObjectsMap.get(caseRec.Id).Type ) ||
                (oldObjectsMap.get(caseRec.Id).Case_Type__c != newObjectsMap.get(caseRec.Id).Case_Type__c )){
                    casesToProcess.add(caseRec) ;                  
                }
        }
        if(casesToProcess.size()>0){
            AddLee_CaseHelper.processCases(casesToProcess);            
        }
    }

    public void onAfterUpdate(final List<Case> oldObjects, final Map<Id, Case> oldObjectsMap,
                              final List<Case> newObjects, final Map<Id, Case> newObjectsMap) {
        Map<Id,Case> casesClosed = new Map<Id,Case>();
        for(case caseRec: newObjects){
          //if(caseRec.X1st_Reponse__c || caseRec.isClosed && oldObjectsMap.get(caseRec.Id).isClosed != newObjectsMap.get(caseRec.Id).isClosed){
           if((caseRec.X1st_Reponse__c || caseRec.isClosed && oldObjectsMap.get(caseRec.Id).isClosed != newObjectsMap.get(caseRec.Id).isClosed) && caseRec.SlaExitDate == null){
                casesClosed.put(caseRec.Id,caseRec);
            }
        }
        if(casesClosed.size()>0){
            AddLee_CaseHelper.populateMilestoneCompletionDate(casesClosed);
        }
    }
    
    public void onAfterInsert(final List<Case> newObjects, final Map<Id, Case> newObjectsMap) {
    }
    /*
    public void onBeforeDelete(final List<Case> oldObjects, final Map<Id, Case> oldObjectsMap) {
        
    }

    public void onAfterDelete(final List<Case> oldObjects, final Map<Id, Case> oldObjectsMap) {
    }

    public void onAfterUndelete(final List<Case> newObjects, final Map<Id, Case> newObjectsMap) {
    }*/
}