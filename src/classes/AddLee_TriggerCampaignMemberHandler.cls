/**
 * Handler Class for AddLee_CampaignMember Trigger
 * Developed by Srini
**/
public class AddLee_TriggerCampaignMemberHandler {
	public void onAfterUpdate (final List<CampaignMember> newObjects) {
		List<id> contactIds = new List<id>();
		List<Id> openContactIds = new List<Id>();
		List<Contact> contactAccounts = new List<Contact>();
		List<Task> tasks = new List< Task>();
		//Map to get the contact id and Campaign Name
		Map<id,String> taskSubjectMap = new Map<id,String>();
		System.DEBUG('newObjects' + newObjects);
		//Contact id from campaign member
		for(CampaignMember cmRec :newObjects ){
			System.DEBUG('cmRec' + cmRec);
			if(cmrec.Status.equalsIgnoreCase('Email sent') 
				&& !cmrec.Campaign_name__c.equalsIgnoreCase('2B_4 Weeks Resend Galaxy Promotion')){
				contactIds.add(cmRec.ContactID);
				taskSubjectMap.put(cmRec.ContactId,cmRec.Campaign_name__c);				
			}else if(cmrec.Status.equalsIgnoreCase('Email sent') 
				&& cmrec.Campaign_name__c.equalsIgnoreCase('2B_4 Weeks Resend Galaxy Promotion')){
				openContactIds.add(cmRec.ContactID);
				taskSubjectMap.put(cmRec.ContactId,cmRec.Campaign_name__c);		
			}

		}
				
		//Creating Activity History on Account Record)
		for (Contact con :[select Id,Name ,AccountID,OwnerId from Contact where Id in :contactIds] ){
			Task tsk = new task ();
			tsk.whatId = con.AccountId;
			tsk.OwnerId = con.OwnerId;
			tsk.subject = taskSubjectMap.get (con.Id);
			tsk.Status = 'Completed';
			tasks.add(tsk);
		}

		//Creating Activity History on Account Record with Open Tasks)
		for (Contact con :[select Id,Name ,AccountID,OwnerId from Contact where Id in :openContactIds] ){
			Task tsk = new task ();
			tsk.whatId = con.AccountId;
			tsk.OwnerId = con.OwnerId;
			tsk.subject = taskSubjectMap.get (con.Id);
			tsk.Status = 'Open';
			tsk.Priority = 'High';
			tasks.add(tsk);
		}

		
		insert tasks;	
	}

}