/*Violation Trigger Handler
   **developed by veeran
  */ 
public class Addlee_EmailNotificationHandler {

    public static void populateEmail(List < Case > newList) {
        List < Id > ownerids = new List < ID > ();
        List < string > accmangername = new List < string > ();
        List < string > ownerName = new List < string > ();
        map<string,string>managerEmails = new map<string,string>();
        map<id,string>linemanagerEmails = new map<id,string>();
         //get the owenid account manager Id
        for (case c:newList) {
        if(c.Account_Manager__c !=null && c.Account_Manager__c !='')
            accmangername.add(c.Account_Manager__c);
            ownerids.add(c.OwnerId);
            system.debug('ownerids---' + ownerids);
            system.debug('size----' + ownerids.size());

        }
        //Soql to get user's email addresses
        if (accmangername.size() >0) {
        for(User usr:[SELECT id,Name,Email from User where Name IN:accmangername]){
            if(usr.Email != null && usr.Email !='')
            managerEmails.put(usr.Name,usr.Email);
            }
            system.debug('Aliass---' + managerEmails);
        }
        if (ownerids.size() >0) {
            for(User usr:[select id, Manager.Email,  Email from user where id IN: ownerids]){
            if(usr.Manager.Email != null && usr.Manager.Email !='')
            linemanagerEmails.put(usr.id,usr.Manager.Email);
            }
            system.debug('line+++++' + linemanagerEmails);

        }
        //Populate the email addresses in cases
        for (case c:newList) {
        if (managerEmails.containskey(c.Account_Manager__c )){
                    c.test_Account_Manager_Email__c = managerEmails.get(c.Account_Manager__c);
                }
                else{
                c.test_Account_Manager_Email__c = null;
            }
            if (linemanagerEmails.containskey(c.ownerid)){
            c.test_Line_Manager_Email__c = linemanagerEmails.get(c.ownerid);
                }
                else {
            c.test_Line_Manager_Email__c = null;
            }
        }
    }
}