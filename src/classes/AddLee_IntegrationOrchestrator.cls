public without sharing class AddLee_IntegrationOrchestrator {


    //Handle Generic Action
    public void handleAction(List<Log__c> logs){
        //Create the list of ids
        List<Account> accounts = AddLee_IntegrationUtility.getListAccount(logs);    
        Integer numberOfCallouts = 0;
        
        
        //Checkpoint
        System.debug('<<<<HANDLEACTION-LOGS>>>>:'+logs);
        System.debug('<<<<HANDLEACTION-ACCOUNTS>>>>:'+accounts);
        
        for(Account acc : accounts){
            for(Log__c log: acc.Logs__r){
                System.debug('<<<<HANDLEACTION-CanUseFutureContext>>>>:'+AddLee_IntegrationUtility.CanUseFutureContext());
                System.debug('<<<<HANDLEACTION-getMaxCalloutSingleTask>>>>:'+AddLee_IntegrationUtility.getMaxCalloutSingleTask());
                System.debug('<<<<HANDLEACTION-numberOfCallouts>>>>:'+numberOfCallouts);
                if(AddLee_IntegrationUtility.CanUseFutureContext() && numberOfCallouts < AddLee_IntegrationUtility.getMaxCalloutSingleTask()){
                    if(log.Type__c == 'Create'){
                        System.DEBUG('Acc : ' + acc.Id);
                        AddLee_IntegrationWrapper.createAccountCalloutAsynch(acc.Id, log.Id);
                        numberOfCallouts++;
                    }else if(log.Type__c == 'Update'){
                        System.DEBUG('Acc : ' + acc.Id);
                        System.DEBUG('log : ' + log.Id);
                        AddLee_IntegrationWrapper.updateAccountCalloutAsynch(acc.Id, log.Id);
                        numberOfCallouts++;
                    }
                }
            }   
        }   
    }
    
    
    //Handle insertions
    public void handleInsert(List<Log__c> newLogs){

        //Create the list of all the Logs in scope for the integration
        List<Log__c> logs = new List<Log__c>();
        for(Log__c newLog : newLogs){
            if(newLog.Status__c == 'Waiting')
                logs.add(newLog);
        }


        //Handle generic action
        this.handleAction(logs);
            
            
    }

    //Handle updates    
    public void handleUpdate(List<Log__c> oldLogs, List<Log__c> newLogs){   
        
        //Create maps to check the changes and define the logs in scope for the integration
        Map<String, Log__c> newMap = new Map<String, Log__c>();
        Map<String, Log__c> oldMap = new Map<String, Log__c>();
        List<Log__c> logs = new List<Log__c>();
        
        for(Log__c log : oldLogs)
            oldMap.put(log.Id, log);
        
        for(Log__c log : newLogs)
            newMap.put(log.Id, log);
        
        //Create the list of all the Logs in scope for the integration
        for(String id : newMap.keySet()){
            Log__c newLog = newMap.get(id);
            Log__c oldLog = oldMap.get(id);
            if(newLog.Status__c == 'Waiting')
                logs.add(newLog);
        }

        //Handle generic action
        this.handleAction(logs);

    }   
}