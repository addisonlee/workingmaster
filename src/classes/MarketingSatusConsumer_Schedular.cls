global class MarketingSatusConsumer_Schedular implements Schedulable  {
	global void execute(SchedulableContext SC){
	 	CalculateMarketingStatusOnAccount csa = new CalculateMarketingStatusOnAccount();
	 	csa.Query = 'select id,Date_of_Last_Booking__c,Consumer_Account_Number__c,Name,Grading__c,Account_Status__c,Date_changed_to_Current__c,IsPersonAccount,Marketing_Status__c,Sales_Ledger__c,Account_Managed__c,Total_Amount_of_Bookings__c from Account where Consumer_Account_Number__c = \'1\'';
	 	Database.executeBatch(csa);
	 }

}