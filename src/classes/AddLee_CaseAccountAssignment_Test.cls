@isTest
public class AddLee_CaseAccountAssignment_Test {
    @isTest
    public static void AddLee_CaseAccountNumberAssignment_isAccountAssignedWhenExternalIdExists() {
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account testAccount = AddLee_Trigger_Test_Utils.createAccountOne(1)[0];
        insert testAccount;
         Contact con = new Contact(FirstName='john', LastName='doe', Email='john@doe.com', AccountId=testAccount.Id);
        insert con;
        
        Asset ass = new Asset(AccountId=testAccount.Id,ContactId=con.Id, Name='testing');
        insert ass;
        
        Entitlement ent = new Entitlement(Name='Testing', AccountId=testAccount.Id, StartDate=Date.valueof(System.now().addDays(-2)), EndDate=Date.valueof(System.now().addYears(2)), AssetId=ass.Id);
        insert ent;

       
        Case testCase = new Case(Web_Account_Number__c = '1');
        
        Test.startTest();
            insert testCase;
        Test.stopTest();


        System.assertEquals(testAccount.Id, [SELECT AccountId FROM Case WHERE Id =: testCase.Id].AccountId);

    }

    @isTest
    public static void AddLee_CaseAccountNumberAssignment_isAccountNotAssignedWhenExternalIdNotExists() {
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account testAccount = AddLee_Trigger_Test_Utils.createAccountOne(1)[0];
        insert testAccount;
         Contact con = new Contact(FirstName='john', LastName='doe', Email='john@doe.com', AccountId=testAccount.Id);
        insert con;
        
        Asset ass = new Asset(AccountId=testAccount.Id,ContactId=con.Id, Name='testing');
        insert ass;
        
        Entitlement ent = new Entitlement(Name='Testing', AccountId=testAccount.Id, StartDate=Date.valueof(System.now().addDays(-2)), EndDate=Date.valueof(System.now().addYears(2)), AssetId=ass.Id);
        insert ent;

        
        Case testCase = new Case(Web_Account_Number__c = '2');
        
        Test.startTest();
            insert testCase;
        Test.stopTest();


        System.assertEquals(null, [SELECT AccountId FROM Case WHERE Id =: testCase.Id].AccountId);

    }
}