/* This is the batch class which is used to create NPS records segmentation engine from the selected reports*/

global class BatchCreateNPSRecords implements Database.Batchable<sObject>{
    
    public List<NPS__c> lstnps;
    public Map<Id,List<Id>> mapContactbookingSummIds;
    global BatchCreateNPSRecords(List<NPS__c> lstnps,Map<Id,List<Id>> mapContactbookingSummIds)
    {
        this.lstnps =lstnps;
        this.mapContactbookingSummIds=mapContactbookingSummIds;
    }
    
    global List<NPS__c> start(Database.BatchableContext BC) {
        return lstnps;
    }
    
    global void execute(Database.BatchableContext BC, List<NPS__c> scope){   
    
        insert scope;
        List<Interaction__c> lstinteraction=new List<Interaction__c>();
        for(NPS__c nps:scope){
            if(mapContactbookingSummIds.containsKey(nps.Contact__c)){
                for(Id bookingSummId:mapContactbookingSummIds.get(nps.Contact__c)){
                    System.debug(bookingSummId);
                    Interaction__c inte=new Interaction__c(NPS__c=nps.Id,Booking_Summary__c=bookingSummId);
                    lstinteraction.add(inte);
                }
            }
        }
        if(!lstinteraction.isEmpty()){
            insert lstinteraction;
        }
    }
    
    global void finish(Database.BatchableContext BC) //External id random creation on NPS record
    {
    } 
}