@isTest
public class CaseRefundWebServiceTest {

    static string caseId;
    static{
        //data setup
        //custom setting
        Refund_Integration__c setting = new Refund_Integration__c(name='Shamrock Refund',Username__c='salesforce',Password__c='salesforce',DefaultType__c='salesforce',DefaultTypeId__c='4564',End_Point__c='test endpoint');
        insert setting;        
        system.assert(setting.id <> null);
        AddLee_Trigger_Test_Utils.insertCustomSettings();
       List<Account> accounts = AddLee_Trigger_Test_Utils.createAccounts(1);
        Account testacc= accounts[0];
        insert testacc;
        system.assert(testacc.id <> null);
        //insert case
        Case newCase = new Case();               
      newCase.Case_Sub_Type__c = 'ClientServices';
          newCase.AccountId = testacc.id;
          newcase.Driver_Call_Sign__c = 'test';
          newcase.Net_Amount_To_Refund__c =10;
          newcase.Invoice_Number__c = '10';
          newcase.Refund_Notes__c = 'Test Note';
          newCase.Docket_Id__c = '123123';
          newCase.Docket_Value__c= 'true';
            newCase.Refund_Status__c = 'Pending Approval';
        insert newCase;
        system.assert(newCase.id <> null);
        caseId = newCase.id;

    }
    
    static testmethod void caseWebServiceRefundTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AdleeRefundTestMockService());
          //changing status to 'Approved' to invoke service call test
          Case tobeUpdatedCase = new Case(id=caseId,Refund_Status__c='Approved');
          update tobeUpdatedCase;
        
          

        
        Test.stopTest();
        
    }
    
     static testmethod void GetComplaintSubTypesMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddleeRefundGetComplaintSubTypesMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
          Addlee_refunds.complaintType testRequestComplain = new Addlee_refunds.complaintType();
          testRequestComplain.id =4354354;
          testRequestComplain.name ='test';
          stub.GetComplaintSubTypes(testRequestComplain);
          
          

        
        Test.stopTest();
        
    }
    
     static testmethod void GetAgentsMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AdleeRefundServiceGetAgentsMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
          stub.getAgents('Mr.','test');
          

        
        Test.stopTest();
        
    }
    
    static testmethod void GetJobsMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddleeRefundsGetJobsMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
          stub.GetJobs('22121',789,system.now(),system.now(),433543,654564,10);

        Test.stopTest();
        
    }
    
        
    static testmethod void GetSubStatusesMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AdleeRefundsGetSubStatusesMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
          Addlee_refunds.status testStatus = new Addlee_refunds.status();
              testStatus.code='4546';
              testStatus.id=554;
              testStatus.name='test' ; 
        stub.GetSubStatuses(testStatus);

        Test.stopTest();
        
    }
    
    static testmethod void GetStatusesMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddleeRefundsGetStatusesMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
          stub.GetStatuses();
 
        Test.stopTest();
        
    }
    
    static testmethod void CreateCustomerMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddleeRefundsCreateCustomerMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
            List<Addlee_refunds.customer> listofchildCustomers = new  List<Addlee_refunds.customer>();
           
       listofchildCustomers.add(generateCustomerdata());
        
        stub.CreateCustomer(listofchildCustomers,generateCustomerdata());
 
        Test.stopTest();
        
    }
    
      static testmethod void GetJobDetailsMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddleeRefundsGetJobDetailsMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
          stub.GetJobDetails(545564,true);
 
        Test.stopTest();
        
    }
    
     static testmethod void GetGradesMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddleeRefundsGetGradesMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
          stub.GetGrades();
 
        Test.stopTest();
        
    }
    
     static testmethod void GetSpecialInstructionTypesMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddLeeGetSpecialInstructionTypesMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
          stub.GetSpecialInstructionTypes();
 
        Test.stopTest();
        
    }
    
     static testmethod void GetInvoicingPoliciesMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddleeRefundsGetInvoicingPoliciesMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
          stub.GetInvoicingPolicies();
 
        Test.stopTest();
        
    }
    
    static testmethod void GetComplaintTypesMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddleeRefundsGetComplaintTypesMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
           Addlee_refunds.complaintTypesRequest testComplainReq = new  Addlee_refunds.complaintTypesRequest();
          testComplainReq.sessionHandle = '65655';
           stub.GetComplaintTypes(testComplainReq);
 
        Test.stopTest();
        
    }
    
     static testmethod void LoginMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddleeRefundsLoginMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
           
           stub.Login('testpwd','testUID');
 
        Test.stopTest();
        
    }
    
     static testmethod void AmendCustomerMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddleeRefundsAmendCustomerMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
           
           stub.AmendCustomer(generateCustomerdata(),generateCustomerdata());
 
        Test.stopTest();
        
    }
    
     static testmethod void logoutMockTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddleeRefundsLogoutMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
           
           stub.Logout();
 
        Test.stopTest();
        
    }
     static testmethod void GetSalesLedgersTest(){
                 
        Test.startTest();
          
          Test.setMock(WebServiceMock.Class,new AddleeRefundsGetSalesLedgersMock());
          Addlee_refunds.SalesForceManagementWebServicePort stub = new Addlee_refunds.SalesForceManagementWebServicePort();
           
           stub.GetSalesLedgers();
 
        Test.stopTest();
        
    }
    
    static Addlee_refunds.customer  generateCustomerdata(){
         Addlee_refunds.customer  customerdata = new Addlee_refunds.customer();
            Addlee_refunds.agent testAgent = new Addlee_refunds.agent();
            testAgent.email = 'dummy@test.com';
            testAgent.id = 2312312;
            testAgent.initials='Mr';
            testAgent.name = 'Test';
          customerdata.accountManager = testAgent;
        
          Addlee_refunds.bank testBank = new  Addlee_refunds.bank();
          testBank.accountName = 'test';
          testBank.accountNumber = '54654';
          testBank.address = '1 cal street';
          testBank.bankReference = 'test';
          testBank.mandateEndDate = system.now();
          testBank.mandateEndUid = '5465456';
          testBank.name = 'test';
          testBank.sortCode = '5465';
          testBank.mandateNumber = '657656';
        customerdata.bank = testBank;
        
       Addlee_refunds.site testbillingAddress = new   Addlee_refunds.site();
        testbillingAddress.contactName = 'Adlle';
        testbillingAddress.email= 'test@test.com';
        testbillingAddress.fax = 'test';
        testbillingAddress.telephone= '1231231231';
        
         Addlee_refunds.location siteloc = new Addlee_refunds.location();
            siteloc.address = 'test';
            siteloc.country = 'Uk';
            siteloc.latitude = 56;
            siteloc.longitude = 67;
            siteloc.postcode = '675';
       testbillingAddress.location =    siteloc;
            
        customerdata.billingAddress = testbillingAddress;
        
        customerdata.birthDate = system.now();
        customerdata.bookingEmail = 'test@test.com';
        customerdata.bookingNote = 'test';
        
        Addlee_refunds.businessArea testbusinessArea = new Addlee_refunds.businessArea ();
        testbusinessArea.name = 'test';
        customerdata.businessArea = testbusinessArea;
        
        Addlee_refunds.creditCard testcreditCard = new Addlee_refunds.creditCard();
        testcreditCard.cardNumber = '2132123412351234';
        testcreditCard.email = 'test@test.com';
        testcreditCard.expiryDate = system.now();
        testcreditCard.holderName = 'test';
        customerdata.creditCard = testcreditCard;
        
        customerdata.creditDays = 10;
        customerdata.creditLimit = 10;
        customerdata.customerPreferences = new List< Addlee_refunds.customerPreference>();
        Addlee_refunds.customerPreference testPreferences = new Addlee_refunds.customerPreference();
        testPreferences.id = 352343;
        Addlee_refunds.specialInstructionType specialInt = new Addlee_refunds.specialInstructionType();
          specialInt.code = '4453';
          specialInt.id=657665;
          specialInt.name= 'test';
        testPreferences.specialInstructionType = specialInt;
        customerdata.customerPreferences.add(testPreferences);
        
        customerdata.dateOpened = system.now();
        customerdata.discountRate=10;
        customerdata.generateIndividualInvoice=true;
        Addlee_refunds.grade testgrade = new  Addlee_refunds.grade();
          testgrade.id = 4546546;
          testgrade.name = 'grade';
        customerdata.grade = testgrade;
        
        customerdata.howHearComment = 'test';
        customerdata.id = 65776756;
        customerdata.invoiceClearanceType = 'test';
        
        Addlee_refunds.invoicingPolicy testinvoicingPolicy = new Addlee_refunds.invoicingPolicy ();
            testinvoicingPolicy.code = '4453';
          testinvoicingPolicy.id=657665;
          testinvoicingPolicy.name= 'test';
        customerdata.invoicingPolicy = testinvoicingPolicy;
        Addlee_refunds.site testmainAddress = new Addlee_refunds.site ();
        customerdata.mainAddress = customerdata.billingAddress;
        
        customerdata.message = 'test';
        customerdata.name = 'test';
        customerdata.number_x=43543;
        
        Addlee_refunds.salesLedger testsalesLedger = new Addlee_refunds.salesLedger();
          testsalesLedger.code = '4453';
          testsalesLedger.id=657665;
          testsalesLedger.name= 'test';
        customerdata.salesLedger = testsalesLedger;
        
        customerdata.salesman = new Addlee_refunds.agent();
          customerdata.salesman =   customerdata.accountManager;
       customerdata.serviceCharge = 564;
        
        customerdata.status = new Addlee_refunds.status();
        customerdata.subStatus = new Addlee_refunds.subStatus();
    customerdata.type_x = 'test';
        customerdata.useWeb = true;
        customerdata.vatRate ='4354';
        
        Addlee_refunds.webSettings webSettings = new Addlee_refunds.webSettings();
          webSettings.showPriceOnWeb= true;
          webSettings.webAccessAllowed = true;
        customerdata.webSettings = webSettings;
        
        return customerdata;
    }
    
   

}