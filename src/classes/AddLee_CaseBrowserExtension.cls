public with sharing class AddLee_CaseBrowserExtension {

    private final case thisCase;

    public String accountNumber { get; set; }
    public String invoiceNumber { get; set; }
    public String jobNumber { get; set; }
    public DateTime dateFrom { get; set; }
    public DateTime dateTo { get; set; }
    public boolean renderResults { get; set; }
    public boolean renderNullResults { get; set; }
    public boolean renderJobDetail { get; set; }
    public JobResponse selected { get; set; }
    public String sessionHandle { get; set; }
    public List<JobResponse> JobResponses { get; set; }
    public JobDetails jobDetailRecord { get; set; }
    public Integer maximumResultsToBeFetched { get; set; }
    public String Docketid {get;set;}
    public string Docket { get; set;}
    public AddLee_CaseBrowserExtension(ApexPages.StandardController stdController) {
        this.thisCase = (case)stdController.getRecord();
        accountNumber = thisCase.Account_Number__c;
        renderResults = false;
        sessionHandle = AddLee_IntegrationWrapper.loginCalloutSynch();
    }


    public PageReference search(){
        JobResponses = new List<JobResponse>();
        try{
            maximumResultsToBeFetched = 200; // Adjust here if you want to increase result set
            AddLee_SoapIntegration_Test_v2_Fix.jobsResponse jobResponse = AddLee_IntegrationUtility.getJobs(getLongValue(thisCase.Account_Number__c),  dateFrom,  dateTo, getLongValue(thisCase.Invoice_Number__c), getLongValue(thisCase.Job_Number__c), maximumResultsToBeFetched,  sessionHandle);
            System.debug('JobResponse : ' + jobResponse);
            if(jobResponse.errorCode == -4){
                sessionHandle = AddLee_IntegrationWrapper.loginCalloutSynch();
                search();
            }else if(jobResponse.errorcode == 0 && jobResponse.jobs != null && jobResponse.jobs.size() > 0){
                for(AddLee_SoapIntegration_Test_v2_Fix.Job eachJob : jobResponse.jobs){
                    JobResponse testWrapper = new JobResponse(eachJob);
                    JobResponses.add(testWrapper);
                }
            }else if(jobResponse.errorcode == 0 && jobResponse.jobs == null ){
                renderNullResults = true;
            }
        }catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,String.valueOf(e)));
        }
        
        if(JobResponses.size() > 0){
            renderResults  = true;
            renderNullResults = false;
        }
        if(JobResponses.size() >= 100){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Warning,'More than 100 results, If necessary adjust dates to limit results. Showing ' + maximumResultsToBeFetched + ' below'));
        }
        return null;
    }

    public PageReference getDetails(){
        for(JobResponse eachJobResponse : JobResponses){
            if(eachJobResponse.isSelected){
                selected = new JobResponse();
                selected.Id = eachJobResponse.Id;
                selected.isDocket = eachJobResponse.isDocket;
                system.debug('selected.Id>>>+'+ selected.Id );
                system.debug('selected.isDocket>>>>'+selected.isDocket);
            }
        }
        if(selected != null){
            AddLee_SoapIntegration_Test_v2_Fix.jobDetailsResponse jobDetailsResponse = AddLee_IntegrationUtility.getJobDetails(selected.id,  selected.isDocket,  sessionHandle);
            if(jobDetailsResponse.errorCode == -4){
                sessionHandle = AddLee_IntegrationWrapper.loginCalloutSynch();
                getDetails();
            }else if(jobDetailsResponse.errorcode == 0 && jobDetailsResponse.job != null ){
                jobDetailRecord = new JobDetails(jobDetailsResponse.job);
            }else if(jobDetailsResponse.errorcode == 0 && jobDetailsResponse.job == null ){
                renderNullResults = true;
            }
        }
        if(selected != null){
            renderResults = false;
            renderJobDetail = true;
        }else{
            renderNullResults = true;
        }
        return null;
    }

    public PageReference updateCase(){
        try{
        	If(jobDetailRecord.invoiceNumber != null){
            thisCase.Invoice_Number__c = String.ValueOf(jobDetailRecord.invoiceNumber);
        	}
        	if(jobDetailRecord.number_x != null){
            thisCase.Job_Number__c = String.ValueOf(jobDetailRecord.number_x);
        	}
           // if(jobDetailRecord.driverInfo.callsign != null){
           // thisCase.Driver_Call_Sign__c = String.ValueOf(jobDetailRecord.driverInfo.callsign);
           // }
             thisCase.Date_To__c = dateTo;
            thisCase.Date_From__c = dateFrom;
            if(selected.id != null){
            	system.debug('selected.id>>'+selected.id);
            thisCase.Docket_Id__c = String.ValueOf(selected.id);
            }
            else if (jobDetailRecord.Id != null){
            	system.debug('jobDetailRecord.uuid>>'+ jobDetailRecord.Id);
             thisCase.Docket_Id__c = String.ValueOf(jobDetailRecord.Id);	
            }
            if(selected.isDocket != null){
             system.debug('selected.isDocket>>'+selected.isDocket);
            thisCase.Docket_Value__c = String.valueOf(selected.isDocket); 
            }
            else if(jobDetailRecord.docket != null) {
            	system.debug('jobDetailRecord.docket>>'+ jobDetailRecord.docket);
            	thisCase.Docket_Value__c= String.ValueOf(jobDetailRecord.docket);
            	
            }
            system.debug(thisCase);
            update thisCase;
            return null;
           }catch (Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error,String.valueOf(e)));
                return null;
            }
    }

    public Class JobResponse{
        public Long accountNumber { get; set; }
        public Long invoiceNumber { get; set; }
        public String pickupAddress { get; set; }
        public String dropOffAddress { get; set; }
        public DateTime dateTimeOfBooking { get; set; }
        public String serviceType { get; set; }
        public Long Id { get; set; }
        public boolean isDocket { get; set; }

        //Extra Fields
        public String service { get; set; }
        public String pricing { get; set; }
        public String name { get; set; }
        public String jobNumber { get; set; }
        public String delay { get; set; }

        public Boolean isSelected { get; set; }

        public JobResponse(AddLee_SoapIntegration_Test_v2_Fix.Job testDataRecord){
            this.accountNumber     = testDataRecord.accountNumber;
            this.pickupAddress     = testDataRecord.pickupStop;
            this.dropOffAddress    = testDataRecord.dropStop;
            this.dateTimeOfBooking = testDataRecord.jobDate;
            this.serviceType       = testDataRecord.serviceCode;
            this.invoiceNumber     = testDataRecord.invoiceNumber;
            this.Id                = testDataRecord.Id;
            this.isSelected        = false;
            this.isDocket          = testDataRecord.docket;
        }

        public JobResponse(){}

    }

    public class JobDetails {
        public AddLee_SoapIntegration_Test_v2_Fix.price clientPrice { get; set; }
        public AddLee_SoapIntegration_Test_v2_Fix.jobIndividual contact { get; set; }
        public Integer delay { get; set; }
        public AddLee_SoapIntegration_Test_v2_Fix.driverInfo driverInfo { get; set; }
        public AddLee_SoapIntegration_Test_v2_Fix.price driverPrice { get; set; }
        public AddLee_SoapIntegration_Test_v2_Fix.jobIndividual[] jobActors  { get; set; }
        public Long accountNumber { get; set; }
        public Boolean docket { get; set; }
        public String dropStop { get; set; }
        public Long invoiceNumber { get; set; }
        public DateTime jobDate { get; set; }
        public Long number_x { get; set; }
        public String pickupStop { get; set; }
        public String serviceCode { get; set; }
        public String uuid { get; set; }
        public Long Id {get;set;}
        public JobDetails(AddLee_SoapIntegration_Test_v2_Fix.jobEx jobExRecord){
            this.clientPrice   = jobExRecord.clientPrice;
            this.contact       = jobExRecord.contact;
            this.delay         = jobExRecord.delay;
            this.driverInfo    = jobExRecord.driverInfo;
            this.driverPrice   = jobExRecord.driverPrice;
            this.jobActors     = jobExRecord.jobActors;
            
            this.accountNumber = jobExRecord.accountNumber;
            this.dropStop      = jobExRecord.dropStop;
            this.invoiceNumber = jobExRecord.invoiceNumber;
            this.jobDate       = jobExRecord.jobDate;
            this.number_x      = jobExRecord.number_x; 
            this.pickupStop    = jobExRecord.pickupStop;
            this.serviceCode   = jobExRecord.serviceCode;
            this.docket        = jobExRecord.docket;
            this.Id 		   = jobExRecord.id;	
        }
    }
    private Long getLongValue(String longText){
        if(longText !=null && longText.length() > 0)
            return Long.valueOf(longText);
        return null;
    }
}