@isTest
private class ACPUrlRewriterTest {
	
	
    static testMethod void mapRequestUrlTest() {
    	
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
    	insert TestDataFactory.createTestLead('Test Lead');
    	Lead testLead = [Select id from Lead limit 1];
        insert TestDataFactory.createTnC(testLead);
        AddLee_Terms_Conditions__c testTNC = [Select Id,Internal_Id__c,External_Id__c 
        										from AddLee_Terms_Conditions__c limit 1];
        ACPUrlRewriter acpURLRewriter = new ACPUrlRewriter();
        Test.startTest();
	        PageReference internalPage = acpURLRewriter.mapRequestUrl( new PageReference('/AddisonLee?sid='+testTNC.External_Id__c+'&accept=true' ));
	    	system.assertEquals(internalPage.getParameters().get('id'), testTNC.id );
	    	system.debug('Internal Page '+internalPage);
	    	PageReference failPage1 = acpURLRewriter.mapRequestUrl( new PageReference('/AddisonLee?sid=100&accept=true' ));
	    	system.assertEquals(failPage1.getUrl(), new PageReference('/FileNotFound').getUrl());
	    	PageReference failPage2 = acpURLRewriter.mapRequestUrl( new PageReference('/AddisonLee?sid=8yMsYrqp501qq1&accept=true' ));
	    	system.assertEquals(failPage2.getUrl(), new PageReference('/FileNotFound').getUrl());
	    	system.assertEquals(acpURLRewriter.mapRequestUrl( new PageReference('/Whatever' )), null);
    	Test.stopTest();
    	
    }
    
    static testMethod void generateUrlForTest() {
    	
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
        insert TestDataFactory.createTestLead('Test Lead');
    	Lead testLead = [Select id from Lead limit 1];
        insert TestDataFactory.createTnC(testLead);
        AddLee_Terms_Conditions__c testTNC = [Select Id,Internal_Id__c,External_Id__c 
        										from AddLee_Terms_Conditions__c limit 1];
        ACPUrlRewriter acpURLRewriter = new ACPUrlRewriter();
        Test.startTest();
        	acpURLRewriter.generateUrlFor(new List<PageReference>{ new PageReference('/ACP_Site_Terms?id='+testTNC.Id+'&accept=true' ),
        														   new PageReference('/someotherpage?id='+testTNC.Id+'&accept=true' )});
    	Test.stopTest();
    	
    }
    
}