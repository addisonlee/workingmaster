@isTest
private class AddLee_AccountSynchHelper_Test {
    
    /*
    @isTest
    static void AddLee_AccountSynchHelper_isLogCreatedWhenIntegrationReady() {
    	Integer expectedValue = 20;
        List<Account> testAccounts = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReady(true,expectedValue);
        
        Test.startTest();
        	insert testAccounts;
        Test.stopTest();
        
        Integer actualValue = [SELECT count() FROM Log__c WHERE Account__c in :testAccounts];
        
        System.AssertEquals(expectedValue,actualValue);
        
    }
    */
    
    /*
    @isTest
    static void AddLee_AccountSynchHelper_isLogCreatedWhenIntegrationReadyAndExistInShamrock() {
    	String expectedValue = 'Update';
        Account testAccount = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReady(true,1)[0];
        
        Test.startTest();
        	insert testAccount;
        Test.stopTest();
        
        String actualValue = [SELECT Type__c FROM Log__c WHERE Account__c = :testAccount.Id].Type__c;
        
        System.AssertEquals(expectedValue,actualValue);
        
    }
    */
    
    /*
    @isTest
    static void AddLee_AccountSynchHelper_isLogCreatedWhenIntegrationReadyAndNotExistInShamrock() {
    	String expectedValue = 'Create';
        Account testAccount = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReadyAndNotExistInShamrock();
        
        Test.startTest();
        	insert testAccount;
        Test.stopTest();
        
        String actualValue = [SELECT Type__c FROM Log__c WHERE Account__c = :testAccount.Id].Type__c;
        
        System.AssertEquals(expectedValue,actualValue);
        
    }
    */


// 19 Sep 

    @isTest
    static void AddLee_AccountSynchHelper_isAccountInsertedWhenIntegrationReady() {
        Integer expectedValue = 1;
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        List<Account> testAccounts = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReady(true,expectedValue);
        Test.startTest();
            insert testAccounts;
        Test.stopTest();
        
        Integer actualValue = [SELECT count() FROM Account WHERE id in :testAccounts];
        
        System.AssertEquals(expectedValue,actualValue);
        
    }
    
    @isTest
    static void AddLee_AccountSynchHelper_isAccountUpdatedWhenIntegrationReady() {
    	Integer expectedValue = 1;
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        List<Account> testAccounts = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReady(true,expectedValue);
        	insert testAccounts;
        	for(Account eachAccount: testAccounts)
        		eachAccount.Name = 'Changed';
        Test.startTest();
        	update testAccounts;
        Test.stopTest();
        
        Integer actualValue = [SELECT count() FROM Account WHERE id in :testAccounts];
        
        System.AssertEquals(expectedValue,actualValue);
        
    }

    
    @isTest
    static void AddLee_AccountSynchHelper_isLogUpdatedWhenIntegrationReady() {
    	Integer expectedValue = 1;
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        List<Account> testAccounts = AddLee_Trigger_Test_Utils.createAccountWithIntegrationReadyAndCurrent(true,expectedValue);
        	insert testAccounts;
        	for(Account eachAccount: testAccounts)
        		eachAccount.Name = 'Changed';
        Test.startTest();
        	update testAccounts;
        Test.stopTest();
        
        Integer actualValue = [SELECT count() FROM Log__c WHERE Account__c in :testAccounts];
        
        System.AssertEquals(expectedValue,actualValue);
        
    }
    
    
    @isTest
    static void AddLee_AccountSynchHelper_isInnerOuterPopulatedOnInsert(){
    	Post_Code_Lookup__c postCode1 = new Post_Code_Lookup__c(Inner_Outer__c = 'Inner', PostCode__c = 'DA1');
    	insert postCode1;
    	Post_Code_Lookup__c postCode2 = new Post_Code_Lookup__c(Inner_Outer__c = 'Outer', PostCode__c = 'DA2');
    	insert postCode2;
    	Post_Code_Lookup__c postCode3 = new Post_Code_Lookup__c(Inner_Outer__c = 'Outside', PostCode__c = 'DA4');
    	insert postCode3;
    	AddLee_Trigger_Test_Utils.insertCustomSettings();
    	Account testAccount1 = new Account(name = 'testAccount', SHM_name__c = 'testAccount', SHM_number_x__c = '12345', integrationReady__c = true, SHM_status_name__c = 'Prospect', SHM_main_postcode__c = 'DA1 1TL');
    	insert testAccount1;
    	
    	Account testAccount2 = new Account(name = 'testAccount', SHM_name__c = 'testAccount', SHM_number_x__c = '12345', integrationReady__c = true, SHM_status_name__c = 'Prospect', SHM_main_postcode__c = 'DA2 1TL');
    	insert testAccount2;
    	
    	Account testAccount3 = new Account(name = 'testAccount', SHM_name__c = 'testAccount', SHM_number_x__c = '12345', integrationReady__c = true, SHM_status_name__c = 'Prospect', SHM_main_postcode__c = 'DA21TL');
    	insert testAccount3;
    	
    	Account testAccount4 = new Account(name = 'testAccount', SHM_name__c = 'testAccount', SHM_number_x__c = '12345', integrationReady__c = true, SHM_status_name__c = 'Prospect', SHM_main_postcode__c = 'DA4 1TL');
    	insert testAccount4;
    	
    	testAccount1 = [SELECT Id, Inner_Outer__c FROM Account WHERE Id =: testAccount1.Id];
    	testAccount2 = [SELECT Id, Inner_Outer__c FROM Account WHERE Id =: testAccount2.Id];
    	testAccount3 = [SELECT Id, Inner_Outer__c FROM Account WHERE Id =: testAccount3.Id];
    	testAccount4 = [SELECT Id, Inner_Outer__c FROM Account WHERE Id =: testAccount4.Id];
    	
    	System.AssertEquals('Inner London', testAccount1.Inner_Outer__c);
    	System.AssertEquals('Outer London', testAccount2.Inner_Outer__c);
    	System.AssertEquals('NA', testAccount3.Inner_Outer__c);
    	System.AssertEquals('Outside London', testAccount4.Inner_Outer__c);
    }

    @isTest
    static void AddLee_AccountSynchHelper_isAccountNumberAssignedWhenSalesLedgerIsWestOne(){
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account testAccount1 = new Account(name = 'testAccount', Account_Status__c = 'Current', Sales_Ledger__c = 'WestOne Sales Ledger', Administration_Fee__c = 10.0, Industry = 'Business Services', Invoicing_Frequency__c = 'Daily', Payment_Terms__c = 100, Payment_Type__c = 'BACS', Credit_Checked__c = True, Credit_Limit__c = 100, SHM_name__c = 'testAccount', SHM_number_x__c = '002');
        insert testAccount1;

        testAccount1 = [SELECT Id, Account_Number__c FROM Account WHERE Id =: testAccount1.Id];
        TECH_Auto_Number__c autoNumber2  = [SELECT Id, Current_Number__c FROM TECH_Auto_Number__c WHERE Name =: 'WestOneRange'];
        System.assertEquals(5,Integer.valueOf(testAccount1.Account_Number__c));
        System.assertEquals(5,autoNumber2.Current_Number__c);

    }

    @isTest
    static void AddLee_AccountSynchHelper_isAccountNumberNOTAssignedWhenSalesLedgerIsWestOne(){
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account testAccount1 = new Account(name = 'testAccount', Account_Status__c = 'Current', Sales_Ledger__c = 'Sales Ledger', Administration_Fee__c = 10.0, Industry = 'Business Services', Invoicing_Frequency__c = 'Daily', Payment_Terms__c = 100, Payment_Type__c = 'BACS', Credit_Checked__c = True, Credit_Limit__c = 100, SHM_name__c = 'testAccount', SHM_number_x__c = '002');
        insert testAccount1;

        testAccount1 = [SELECT Id, Account_Number__c FROM Account WHERE Id =: testAccount1.Id];
        TECH_Auto_Number__c autoNumber2  = [SELECT Id, Current_Number__c FROM TECH_Auto_Number__c WHERE Name =: 'AddLeeRange'];
        System.assertNotEquals(5,Integer.valueOf(testAccount1.Account_Number__c));
        System.assertNOTEquals(5,autoNumber2.Current_Number__c);

    }

    @isTest
    static void AddLee_AccountSynchHelper_isAccountNumberAssignedWhenSalesLedgerIsNotWestOne(){
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account testAccount1 = new Account(name = 'testAccount', Account_Status__c = 'Current', Sales_Ledger__c = 'Sales Ledger', Administration_Fee__c = 10.0, Industry = 'Business Services', Invoicing_Frequency__c = 'Daily', Payment_Terms__c = 100, Payment_Type__c = 'BACS', Credit_Checked__c = True, Credit_Limit__c = 100, SHM_name__c = 'testAccount', SHM_number_x__c = '002');
        insert testAccount1;

        testAccount1 = [SELECT Id, Account_Number__c FROM Account WHERE Id =: testAccount1.Id];
        TECH_Auto_Number__c autoNumber2  = [SELECT Id, Current_Number__c FROM TECH_Auto_Number__c WHERE Name =: 'AddLeeRange'];
        System.assertEquals(2,Integer.valueOf(testAccount1.Account_Number__c));
        System.assertEquals(2,autoNumber2.Current_Number__c);

    }

    @isTest
    static void AddLee_AccountSynchHelper_isAccountNumberNOTAssignedWhenSalesLedgerIsNotWestOne(){
        AddLee_Trigger_Test_Utils.insertCustomSettings();
        Account testAccount1 = new Account(name = 'testAccount', Account_Status__c = 'Current', Sales_Ledger__c = 'WestOne Sales Ledger', Administration_Fee__c = 10.0, Industry = 'Business Services', Invoicing_Frequency__c = 'Daily', Payment_Terms__c = 100, Payment_Type__c = 'BACS', Credit_Checked__c = True, Credit_Limit__c = 100, SHM_name__c = 'testAccount', SHM_number_x__c = '002');
        insert testAccount1;

        testAccount1 = [SELECT Id, Account_Number__c FROM Account WHERE Id =: testAccount1.Id];
        TECH_Auto_Number__c autoNumber2  = [SELECT Id, Current_Number__c FROM TECH_Auto_Number__c WHERE Name =: 'WestOneRange'];
        System.assertNotEquals(2,Integer.valueOf(testAccount1.Account_Number__c));
        System.assertNOTEquals(2,autoNumber2.Current_Number__c);

    }
    
}