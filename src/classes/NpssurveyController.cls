/*************************************************************************************************
This is the controller used by NPS_Survey page.When survey is received updating the NPS records
**************************************************************************************************/

public class NpssurveyController{
    
    public String settingValue{get;set;}
    public String categoryValue{get;set;}
    public String feedbackMessage{get;set;}
    
    public NpssurveyController()
    {
        settingValue =ApexPages.currentpage().getParameters().get('selection');
    }
    
    public pageReference updImage() {
        pageReference pg;
        List<NPS__c> qNps=[Select Id,External_Id__c from NPS__c Where External_Id__c = : ApexPages.currentpage().getParameters().get('pId')];
        if(qNps.isEmpty())
        {
            pg=new PageReference('/apex/FileNotFound');
            pg.setRedirect(true);
            return pg;
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'External Id is Not Found'));
            //return null;
        }
        if(qNps[0].External_Id__c == null){
            pg=new PageReference('/apex/FileNotFound');
            pg.setRedirect(true);
            return pg;
        }else{
            NPS__c nps=new NPS__c(id= qNps[0].Id);
            if(settingValue =='1')
            {
                nps.Score__c = 'Green';
            }else if(settingValue =='2')
            {
                nps.Score__c = 'Light Green';
            }else if(settingValue =='3')
            {
                nps.Score__c = 'Amber';
            }else 
            {
                nps.Score__c = 'Red';
            }
            update nps;
        }
        
        pg=new PageReference('/apex/NPS_Survey?selection='+settingValue +'&Pid='+ApexPages.currentpage().getParameters().get('pId'));
        pg.setredirect(true);
        return pg;
    }
    
    public void updcategory() {
        //return null;
    }
    
    public void updateNpsRecord()
    {
        
    }
    
    public PageReference Submit() { //On submit updating on NPS Record
        PageReference ref;
        System.debug('settingValue'+settingValue);
        List<NPS__c> qNps=[Select Id,External_Id__c from NPS__c Where External_Id__c = : ApexPages.currentpage().getParameters().get('pId')];
        if(qNps.isEmpty())
        {
            ref=new PageReference('/apex/FileNotFound');
            ref.setRedirect(true);
            return ref;
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'External Id is Not Found'));
            //return null;
        }
        if(qNps[0].External_Id__c == null){
            ref=new PageReference('/apex/FileNotFound');
            ref.setRedirect(true);
            return ref;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'External Id is null'));
            return null;
        }else{
            NPS__c nps=new NPS__c(id= qNps[0].Id);
            nps.Comments__c = comment;
            if(settingValue =='1')
            {
                nps.Score__c = 'Green';
            }else if(settingValue =='2')
            {
                nps.Score__c = 'Light Green';
            }else if(settingValue =='3')
            {
                nps.Score__c = 'Amber';
            }else 
            {
                nps.Score__c = 'Red';
            }
            nps.Category__c =categoryValue; //Updating on Category
            nps.External_Id__c =null; // External id is removed on update
            nps.Date_Responded__c=System.now();
            update nps;
            // ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'Updated Successfully'));
            ref=new PageReference('/apex/Thankyou_NPS');
            ref.setRedirect(true);
            return ref;
        }
    }
    
    //public String score{ get; set; }
    public String comment { get; set; }
    public pagereference Redirect() { //Redirect to Thankyou page after submnit
        List<NPS__c> qNps=[Select Id,External_Id__c from NPS__c Where External_Id__c = : ApexPages.currentpage().getParameters().get('pId')];
        PageReference ref;
        if(qNps.size() <= 0 || qNps[0].External_Id__c == null) {
            ref=new PageReference('/apex/FeedbackSubmitted_NPS');
            ref.setRedirect(true);
        }
        else
        {
            NPS__c nps=new NPS__c(id= qNps[0].Id);
            if(settingValue =='1')
            {
                nps.Score__c = 'Green';
            }else if(settingValue =='2')
            {
                nps.Score__c = 'Light Green';
            }else if(settingValue =='3')
            {
                nps.Score__c = 'Amber';
            }else 
            {
                nps.Score__c = 'Red';
            }
            update nps;
            ref = null;
        }
        return ref;
    }
}