@isTest
private class AddLee_Crypto_Test {
	
	@isTest 
	static void AddLee_Crypto_isPrivateKeyGenerated() {
		AddLee_Trigger_Test_Utils.insertCustomSettings();
		Blob privateKey = Addlee_Crypto.getCryptoKey();
		System.AssertNotEquals(privateKey,null);
	}
	
	@isTest (seeallData = false)
	static void AddLee_Crypto_isStringEncrypted() {
		// Implement test code
		AddLee_Trigger_Test_Utils.insertCustomSettings();
		Blob privateKey = Addlee_Crypto.getCryptoKey();
		String expectedString = 'TEST';
		String testString = 'TEST';
		String encodedString;
		String decodedString;
		Test.startTest();
			encodedString = Addlee_Crypto.encrypt(testString,  privateKey);
			decodedString = Addlee_Crypto.decrypt(encodedString, privateKey);
		Test.stopTest();

		System.AssertEquals(expectedString,decodedString);
	}

	@isTest(seeAllData = false)
	static Void AddLee_Crypto_isLeadBankDetailsEncrypted() {
		AddLee_Trigger_Test_Utils.insertCustomSettings();
		Integer expectedValue = 1;
		String expectedBankAccount = '202020';
		String expectedSortCode = '345667';
		Lead lead = AddLee_Trigger_Test_Utils.createLeads(expectedValue)[0];
		lead.Payment_Type__c = 'Direct Debit';
		lead.Bank_Account_Number__c = '202020';
		lead.Bank_Sort_Code__c = '345667';

		Test.startTest();
			insert lead;
		Test.stopTest();

		lead = [SELECT id, Bank_Account_Number__c, Bank_Sort_Code__c FROM Lead WHERE Id =: lead.Id];

		System.AssertNotEquals(expectedBankAccount, lead.Bank_Account_Number__c);
		System.AssertNotEquals(expectedSortCode, lead.Bank_Sort_Code__c);	
	}
	
}