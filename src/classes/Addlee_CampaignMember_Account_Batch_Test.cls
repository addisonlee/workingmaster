/** 
 * Test Class for Addlee_CampaignMember_Account_Batch
 * Developed by Nimil

**/
@isTest
private class Addlee_CampaignMember_Account_Batch_Test {
	
	@isTest(seealldata = false)
	static void CampaignMember_IsCampaignMemberPopulated_For_AccountOpened2WeeksAgo_With_Spend() {
		
		Addlee_Trigger_Test_Utils.insertCustomSettings();
		Account testAccount = Addlee_Trigger_Test_Utils.createAccountWithIntegrationReadyAndCurrent(true,1)[0];
		insert testAccount;
		testAccount.Date_changed_to_Current__c = System.today().addDays(-2 * 7);
		update testAccount;
		List<Booking_Summary__c> testBookingSummary = AddLee_Trigger_Test_Utils.createBookingSummaryTest(testAccount.Id, 2);
    	insert testBookingSummary;
		Contact testContact = new Contact(LastName = '2 Weeks Test', accountId = testAccount.Id, Contact_Type__c = 'Main Contact', email = 'test@test.com');
		insert testContact;

		Campaign testCampaign = new Campaign(Name = '1A_2 Weeks Send App Promotional Email', isActive = true);
		insert testCampaign;

		Group testGroup = new Group(Name = 'Marketing_Batch_Confirmation');
		insert testGroup;

		GroupMember testGroupMember = new GroupMember(groupId = testGroup.Id, userOrGroupId = UserInfo.getUserId());
		insert testGroupMember;

		Test.StartTest();
			ID batchprocessid = Database.executeBatch(new Addlee_CampaignMember_Account_Batch());
		Test.StopTest();

		System.assertEquals(testCampaign.Id,[Select Id,campaignId FROM CampaignMember WHERE campaignId =: testCampaign.Id].campaignId);

	}
	
	@isTest(seealldata = false)
	static void CampaignMember_IsCampaignMemberPopulated_For_AccountOpened2WeeksAgo_Without_Spend() {
		
		Addlee_Trigger_Test_Utils.insertCustomSettings();
		Account testAccount = Addlee_Trigger_Test_Utils.createAccountWithIntegrationReadyAndCurrent(true,1)[0];
		insert testAccount;
		testAccount.Date_changed_to_Current__c = System.today().addDays(-2 * 7);
		update testAccount;
		Contact testContact = new Contact(LastName = '2 Weeks Test', accountId = testAccount.Id,Contact_Type__c = 'Main Contact', email = 'test@test.com');
		insert testContact;

		Campaign testCampaign = new Campaign(Name = '2A_2 Weeks Send Galaxy Promotion', isActive = true);
		insert testCampaign;

		Group testGroup = new Group(Name = 'Marketing_Batch_Confirmation');
		insert testGroup;

		GroupMember testGroupMember = new GroupMember(groupId = testGroup.Id, userOrGroupId = UserInfo.getUserId());
		insert testGroupMember;
		
		Test.StartTest();
			ID batchprocessid = Database.executeBatch(new Addlee_CampaignMember_Account_Batch());
		Test.StopTest();

		System.assertEquals(testCampaign.Id, [Select Id,campaignId FROM CampaignMember WHERE campaignId =: testCampaign.Id].campaignId);
		
	}
	
}